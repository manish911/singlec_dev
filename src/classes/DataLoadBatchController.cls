/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */

public with sharing class DataLoadBatchController
{
    public String batchNumber {get; set; }
    public String sourceSystem {get; set; }
    public String query {get; set; }
    public Boolean enableBtn {get; set; }
    public Integer txUpdated {get; set; }

    public DataLoadBatchController() {
        enableBtn = true;
        txUpdated = 0;
    }
    
    // method is used to set Batct Query 
    // return type : void
    public void setBatchQuery() {
        if(sourceSystem != null)
            sourceSystem = String.escapeSingleQuotes(sourceSystem);
        if(batchNumber != null)
            batchNumber = String.escapeSingleQuotes(batchNumber);

        /**start Namespace for migrating to unmanaged package  **/
        String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        //String namespace = '';
        this.query = 'select '+namespace+'Balance__c, '+namespace+'Source_System__c, '+namespace+'Batch_Number__c from '+namespace+'Transaction__c where Transaction_Key__c != null and '+namespace+'Balance__c !=0 ';
        this.query = this.query + ' and ' + (sourceSystem == null || sourceSystem == '' ? namespace+'Source_System__c = null' : namespace+'Source_System__c =:sourceSystem ');
        this.query = this.query + ' and ' + (batchNumber == null || batchNumber == '' ? namespace+'Batch_Number__c = null' : namespace+'Batch_Number__c !=:batchNumber');
        this.query = this.query + ' LIMIT 30';

        // set this when migrating the code to orgs wihtout having managed packages
        /**end Namespace for migrating to unmanaged package  **/
    }
    
    // method is used to start execution 
    // return type : Pagereference
    public pagereference startExecution() {
        enableBtn = false;
        txUpdated = 0;
        return null;
    }
    
    // method is used to execute single Transaction 
    // return type : Pagereference
    public PageReference executeSingleTransaction() {
        setBatchQuery();
        List<Transaction__c> txList = Database.query(this.query);

        if(txList!= null && txList.size()>0) {
            for(Transaction__c tx : txList) {
                tx.Balance__c = 0;
            }
            
            //==========================================================================================  
                 list<String> lstFields = new list<String>{'Balance__c', 'Source_System__c', 'Batch_Number__c'};
               Map<String,Schema.SObjectField> m = Schema.SObjectType.Transaction__c.fields.getMap();
               for (String fieldToCheck : lstFields) {
                  // Check if the user has create access on the each field
                  if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                     if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                'Insufficient access'));
                        return null;
                      }
                   }
                }
            //========================================================================================
            
            update txList;
            txUpdated += txList.size();
        }
        else {
            enableBtn = true;
        }
        return null;
    }
}