/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for generating PDF for transactions
 */
global with sharing class ExtendedTransactionPdfGenerator Extends TransactionPDFGenerator {

    global TransactionPDFGenerator basePDFGenerator {get; set; }
    global Account acc {get; set; }
    global List<Object> headerList {get; set; }
    global String totalBillingBalance {get; set; }
    global String[] txIds {get; set; }
    global String selectedListView;
    static Map<String, Schema.SObjectField> accountFieldsMap = Schema.SObjectType.Account.fields.getMap();
    global String dt  {get; set; }
    List<Temp_Object_Holder__c> txsConfig;
    String transactionIdsKey;
    
    public String TrxOrdertype  {get;set;}
    public String TrxOrderby {get;set;}
    public OrganizationConfig_Singlec__c orgSingleObj { get; set; } 

    /**
     * Gets the page parameters and sets them
     */
    global ExtendedTransactionPdfGenerator(){
    
        orgSingleObj = [ SELECT Organization_Address__c,Organization_City__c,Organization_Country__c,Organization_Email__c,Organization_Fax__c,Organization_Name__c,Organization_Phone__c,Organization_PostalCode__c,Organization_State__c,Organization_Street__c FROM OrganizationConfig_Singlec__c LIMIT 1 ];

        String accIdParam = ApexPages.currentPage().getParameters().get('accId');
        
        TrxOrdertype = ApexPages.currentPage().getParameters().get('ordertype');
        TrxOrderby = ApexPages.currentPage().getParameters().get('orderby');
        
        //get Account object
        if(accIdParam != null)
        {
            this.accountId =  (Id) String.escapeSingleQuotes(accIdParam);
            acc = getAccount(this.accountId);
        }

        //-----------------------  get values by parameters -----------------------------------//
        // Additional parameters with "Param" text appended have been used to avoid xss hacks (security Audit).
       String transactionIdsKeyParam;
       if(ApexPages.currentPage().getParameters().get('key') != null){
        transactionIdsKeyParam = ApexPages.currentPage().getParameters().get('key');
        
        
        if(transactionIdsKeyParam != null && transactionIdsKeyParam != '')
            transactionIdsKey  = String.escapeSingleQuotes(transactionIdsKeyParam);
            transactionIdsKey  = transactionIdsKey.trim();
          
       } 
            //Flag whether invoice detail to be included or not
            String showDetailsParam = ApexPages.currentPage().getParameters().get('showDetails');
            
            if( showDetailsParam !=null && showDetailsParam !='') {
                this.showDetails = (String.escapeSingleQuotes(showDetailsParam) == 'true' ? true : false);
            }
            
            else
            {
                this.showDetails = false;
            }
          
        //the selected list view from parent page according to which columns would be generated in the pdf,
        //if not provided, default view is picked
        String listviewParam = ApexPages.currentPage().getParameters().get('listview');
        if(listviewParam !=null && listviewParam != '')
            this.selectedListView =  String.escapeSingleQuotes(listviewParam);
        else
            this.selectedListView = 'Default';

        //page param passed in case of single tx
        String txIdParam = ApexPages.currentPage().getParameters().get('txId');
        if(txIdParam !=null && txIdParam !='')
            this.txIds = txIdParam.split(',');
            
        
        if(transactionIdsKey != null &&  transactionIdsKey != '')
           // txsConfig =new List<Temp_Object_Holder__c>([select value__c from Temp_Object_Holder__c where key__c =:transactionIdsKey limit 1]);
           txsConfig =[select value__c,key__c from Temp_Object_Holder__c where key__c =: transactionIdsKey limit 1];
             
        if(txsConfig != null)
            this.txIds = (txsConfig.get(0).value__c !=null ? txsConfig.get(0).value__c.split(',') : null);

        // add addition filters  here
        this.lineItemAdditionalFilter = ' Order By Name ASC  ';
        this.txsAdditionalFilter = ' Order By Name ASC  ';
        this.dt = Date.today().format();

    }
   
    //Get account object populated with all the field values
    // return type : Account
    // parameters : accId - String
    private Account getAccount(String accId)
    {
        Account acc = null;
        String expr = '';
        try {
            for (String fieldName : accountFieldsMap.keySet())
            {
                if(fieldName != 'billingaddress' && fieldName != 'shippingaddress') 
                    expr += ', a.' + fieldName;
            }

            expr = addAccountRelationShipFields(expr);
            expr = expr.replaceFirst(',', ''); // remove leading comma
            String qry = 'Select ' + expr + ' from Account a where a.id=:accId';
            List<Account> accList =  Database.query( qry );
            if(accList != null && accList.size() > 0)
            {
                acc = accList.get(0);
            }
        } catch(Exception e)
        {
            throw e;
        }

        return acc;
    }
    // method is used to add Account RelationShip Fields 
    // return type : String
    // parameters : expr - String
    private String addAccountRelationShipFields(String expr)
    {
        expr += ', a.Owner.Fax , a.Owner.Phone, a.Owner.Email , a.Owner.Title, a.Owner.Name, a.Owner.FirstName, a.Owner.LastName, a.LastModifiedBy.Name , a.Parent.Site , a.Parent.AccountNumber , a.Parent.Name , a.CreatedBy.Name';
        return expr;
    }
    // method is used to get ExtList Of Txs
    // return type : List<Transaction__c>
    public List<Transaction__c> getExtListOfTxs(){
        return this.getLstOfTxs(this.txIds);
    }

    //get List of Transactions with Line Items
    // return type : List<TxWithLineItem>
    global List<TxWithLineItem > getListOfTxWithLineItem(){

        //get map of line items for the transactions //Map[txId: Line Item List]
        Map<ID,List<Line_Item__c>> extMapOfTxsWithLineItems =this.getMapOfTxsWithLineItems(this.txIds);
        List<TxWithLineItem > extLstTxWithLineItem = new List<TxWithLineItem >();

        for(Transaction__c nextTransObj : getExtListOfTxs()) {

            List<Line_Item__c> lstLineIteams = new List<Line_Item__c>();
            TxWithLineItem nextTxLineItemObj  = new TxWithLineItem();

            if(extMapOfTxsWithLineItems.get(nextTransObj.id)!=null) {
                lstLineIteams  = extMapOfTxsWithLineItems.get(nextTransObj.id);
                nextTxLineItemObj.lstLineItems = lstLineIteams;
            }
            nextTxLineItemObj.tranObj = nextTransObj;
            nextTxLineItemObj.calculations();
            extLstTxWithLineItem.add(nextTxLineItemObj);

        }

        // set pageBreakFalse for last page
        if(extLstTxWithLineItem.size()>0) {
            Integer listSize = extLstTxWithLineItem.size();
            TxWithLineItem ic = extLstTxWithLineItem.get(listSize-1);
            ic.pageBreakAfter = false;
        }

        return extLstTxWithLineItem;
    }

    global class TxWithLineItem {
        global List<Line_Item__c> lstLineItems {get; set; }
        global Transaction__c tranObj {get; set; }
        global Boolean pageBreakAfter {get; set; }
        global String BillToAddress {get; set; }
        global String ShipToAddress {get; set; }

        global Double subTotal {get; set; }
        global Double total {get; set; }
        global Double shippingHandling {get; set; }
        global Double paid {get; set; }
        global Double totalDue {get; set; }
        global Double salesTax {get; set; }

        global String subTotalStr {get; set; }
        global String totalStr {get; set; }
        global String shippingHandlingStr {get; set; }
        global String paidStr {get; set; }
        global String totalDueStr {get; set; }
        global String salesTaxStr {get; set; }

        global TxWithLineItem() {
            pageBreakAfter = true;
        }

        global void calculations(){
            subTotal = 0; total=0; totalDue=0;
            
            if(lstLineItems !=null)
            {
                for(Line_Item__c nextItem : lstLineItems)
                {

                    if(nextItem.Quantity__c == null) nextItem.Quantity__c = 1;
                    if(nextItem.Unit_Price__c == null) nextItem.Unit_Price__c = 0;
                    //@todo check if we can change the type of global field in a patch, if yes better to make the field string.
                    //this can be localized only via apex page..input fied. not possible via code as formatted string in locale
                    //is invalid to be converted as double
                    nextItem.Line_Total__c = nextItem.Quantity__c * nextItem.Unit_Price__c;
                    subTotal = subTotal + nextItem.Line_Total__c;
                }
            }

            if(subTotal > 0) {
                total = subTotal;
                totalDue= total;
            }

            salesTax = subTotal * 0.0825;
            //@TODAO : Bhavik : Ref : On-Demand Multi-Currency
            String currencyIsoCode = ( String )(UtilityController.isMultiCurrency() ? tranObj.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode());

            subTotalStr = UtilityController.formatDoubleValue(currencyIsoCode, subTotal);
            totalStr = UtilityController.formatDoubleValue(currencyIsoCode, total + salesTax);
            
            totalDueStr = UtilityController.formatDoubleValue(currencyIsoCode, totalDue);
            salesTaxStr = UtilityController.formatDoubleValue(currencyIsoCode, salesTax);
        }
    }

    // get dynamic list of transation fields according to selected listview
    // return type : List<List<List<Object>>>
    global List<List<List<Object>>> getSummaryTxs(){

        List<List<List<Object>>> lstSummaryTxs = new List<List<List<Object>>>();

        List<List<Id>> lstIds = new List<List<Id>>();
        List<Id> Ids = new  List<Id>();
        if(txIds !=null && txIds.size() >0) {
            for(String nextId : txIds) {
                if(nextId.trim() !=''  ) {
                    Ids.add(nextId);
                }
            }
            lstIds.add(Ids);
        }

        CustomListViewUtility utilObj = new CustomListViewUtility(this.selectedListView,lstIds,this.accountId,false,TrxOrderby ,TrxOrderType);
        headerList = new List<Object>();
        headerList = utilObj.getHeaders();
        lstSummaryTxs = utilObj.getDynamicTransactionFields();

        //String currencyIsoCode = AkritivConstants.CURRENCY_USD_CODE;
        
        String currencyIsoCode = ( String )(UtilityController.isMultiCurrency() ? acc.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode());
        
        totalBillingBalance =UtilityController.formatDoubleValue(currencyIsoCode, utilObj.getTotalTransactionBalance());
        return lstSummaryTxs;

    }

}