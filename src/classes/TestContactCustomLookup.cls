/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for ContactCustomLookup.
 */
@isTest
private class TestContactCustomLookup {

    static testMethod void displayContactsTest()
    {
        //@todo create a utlity class to prepare data set
        //create an account
        Account acc = DataGenerator.accountInsert();

        Id accountId = acc.Id;

        generateTestContacts(acc);

        List<Contact> retrievedContacts = new List<Contact>();
        retrievedContacts = [Select id, Name, Account.Name, Phone, FAX, Email, default__c from Contact where Account.Id = :accountId];

        Test.startTest();
        System.assertEquals(31, retrievedContacts.size());

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'To');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Email');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        String urlAccountID = ApexPages.currentPage().getParameters().get('AccountId');
        String addressType =ApexPages.currentPage().getParameters().get('addressType');
        String contactMethod = ApexPages.currentPage().getParameters().get('contactMethod');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);

        System.assertNotEquals(null, controller.getContactList());

        //@todo see corrsponding todo in class to have recperpage coming from property file and recperpageoptions too
        String recPerPage = controller.recPerPage;

        System.assertEquals(recPerPage, controller.getContactList().size().format());

        System.assert(controller.updateContact() != null);

        System.assertEquals(controller.cancel(), null);

        controller.changeRecPerPage();

        controller.next();

        controller.first();

        controller.last();

        controller.previous();

        Test.stopTest();

    }

    static testMethod void searchContactsTest()
    {
        //@todo create a utlity class to prepare data set
        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        //System.assertEquals(31, retrievedContacts.size());
        String searchText = 'John';

        Test.startTest();
        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'To');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Email');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);
        controller.searchText = searchText;

        PageReference ref = controller.setSearchContactList();

        System.assertNotEquals(null, controller.getContactList());
        //@todo need to work out here...
        Integer searchResult = controller.con.getResultSize();

        

        System.assertNotEquals(null, controller.getContactList());

        //@todo see corrsponding todo in class to have recperpage coming from property file and recperpageoptions too
        //String recPerPage = controller.recPerPage;

        //       System.assertEquals(recPerPage , searchResult);
        Test.stopTest();

    }

    static testMethod void createNewContactTest()
    {

        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'To');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Email');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);
        controller.setRenderNewContactSection();
        System.assertEquals(controller.renderNewContactSection, true);
        System.assertNotEquals(null, controller.getContactList());

        controller.newContact = getNewContact();

        controller.Save();

        Contact retrievedContact = [Select id, Name, firstName, Account.Name, Phone, FAX, Email, default__c from Contact where firstName = 'New Contact'];

        System.assertEquals('New Contact', retrievedContact.firstName);

    }

    static testMethod void processContactLookupEmailToTest()
    {

        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'To');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Email');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);

        
        System.assertEquals(accountId,acc.Id);

        //process email + To
        
        controller.contactList.get(0).selected = true;
        controller.contactList.get(1).selected = true;

        controller.processContacts();

    }

    static testMethod void processContactLookupEmailCCTest()
    {

        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'CC');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Email');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);

        
        System.assertEquals(controller.contactMethod,'Email');

        //process email + To

        controller.contactList.get(0).selected = true;
        controller.contactList.get(1).selected = true;

        controller.processContacts();

    }

    static testMethod void processContactLookupPhoneCCTest()
    {

        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'CC');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Phone');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);

        

        //process phone + cc
        System.assertEquals(accountId,acc.Id);
        controller.contactList.get(0).selected = true;
        controller.contactList.get(1).selected = true;

        controller.processContacts();

    }

    static testMethod void processContactLookupPhoneTOTest()
    {

        //create an account
        Account acc = DataGenerator.accountInsert();
        Id accountId = acc.Id;

        generateTestContacts(acc);

        ApexPages.currentPage().getParameters().put('AccountId', accountId);
        ApexPages.currentPage().getParameters().put('UseFor', 'Contact');
        ApexPages.currentPage().getParameters().put('addressType', 'To');
        ApexPages.currentPage().getParameters().put('contactMethod', 'Phone');
        ApexPages.currentPage().getParameters().put('CtrlId', 'abc.xyz');

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        ContactCustomLookup controller = new ContactCustomLookup(sc);

        

        //process phone + to
        System.assertEquals(controller.contactMethod, 'Phone');

        controller.contactList.get(0).selected = true;
        controller.contactList.get(1).selected = true;

        controller.processContacts();

    }
    private static Contact getNewContact()
    {

        Contact ct = new Contact(Salutation='Mr.', lastname='XYZ', firstName='New Contact', phone='123-121311',
                                 FAX='122-1212-1212', Email='john@co.in', Default__c=false);

        return ct;
    }

    private static void generateTestContacts(Account acc)
    {
        List<Contact> contactsToCreate = new List<Contact>();
        Contact ct = null;
        for(Integer x=0; x<30; x++) {
            ct = new Contact(AccountId=acc.Id,Salutation='Mr.', lastname='Lincoln', firstName='Abrahim', phone='123-121311',
                             FAX='122-1212-1212', Email='ablinco@co.in');
            contactsToCreate.add(ct);
        }

        ct = new Contact(AccountId=acc.Id,Salutation='Mr.', lastname='Lincoln', firstName='John', phone='123-121311',
                         FAX='122-1212-1212', Email='john@co.in');

        contactsToCreate.add(ct);

        insert contactsToCreate;

    }
   
}