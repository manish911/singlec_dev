/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   class is used to provide "Take Payment" functionality.
 *             this class request UTA for the payment
 */
public with sharing class TakePaymentParametersController{
public List<Payment_Configuration__c> payconf;
Public List<Payment_Processor__c> processorList;
public Map<Id,integer> utamount;

    public TakePaymentParametersController(){
        payconf = new List<Payment_Configuration__c>(); 
        processorList = new List<Payment_Processor__c>();
    }
    
    public Map<String,String> getConfigParams(){
        Map<String, String> configParams = new Map<String, String>();
         payconf = [select Parameter_Name__c,Parameter_Value__c from Payment_Configuration__c where Processor__r.Name='UTA' ];
            if(payconf!= null && payconf.size()>0){
                for(Payment_Configuration__c p : payconf){
                    configParams.put(p.Parameter_Name__c,p.Parameter_Value__c);
                }
            }
            
            return configParams;
    }
    public string createUserInfo(){
        Map<String, String>configParams = getConfigParams();
        
            String strInfo='';
            strInfo+='<USERINFO>';
            for(String paramName : configParams.keySet()){
            strInfo+= '<' + paramName + '>';
            strInfo+= configParams.get(paramName);
            strInfo+= '</' + paramName + '>';
        }
        strInfo +='</USERINFO>';
        System.debug('StrInfo---' + strInfo);
        return strInfo;
    
    }
        
    public String processInputeFields(Map<String,String> reqParamMap,List<Transaction__c> trans,Map<Id,Double> utamount){
        Integer icount = 0;
        string strInfo;
        String XMLStringTags = '<?xml version="1.0"  encoding="ISO-8859-1" ?>';
        XMLStringTags = XMLStringTags + '<WCA>';
        XMLStringTags += createUserInfo();
        
        Double UTAAmount;
        
        for(transaction__c tx : trans){
        
        UTAAmount = utamount.get(tx.Id);
         system.debug('--UTAAmount--'+UTAAmount);
          XMLStringTags = XMLStringTags + '<WCAITEM SEQUENCE="'+(icount++)+'">';
          for(String paramName : reqParamMap.keySet()){
         
           XMLStringTags += '<' + paramName + '>';
            XMLStringTags += reqParamMap.get(paramName);
            XMLStringTags += '</' + paramName + '>';
            
           }
           
            XMLStringTags = XMLStringTags + '<AMOUNT>'+integer.valueOf(UTAAmount)+'</AMOUNT>';
            XMLStringTags = XMLStringTags + '<MEMO></MEMO>';
            XMLStringTags = XMLStringTags + '<INVOICENO>'+tx.name+'</INVOICENO>';
            XMLStringTags = XMLStringTags + '<TRANNUM>'+tx.name+'</TRANNUM>';
            XMLStringTags = XMLStringTags + '</WCAITEM>';
           
           
            }
               
            XMLStringTags = XMLStringTags + '</WCA>';
         
            return XMLStringTags;
        } 
        
    public String createRequestResponse(Map<String,String> reqParamMap,List<Transaction__c> trans,Map<Id,Double> utamount){
        HttpRequest req = new HttpRequest();
        
        processorList = [Select Name,Base_URL__c From Payment_Processor__c where Name = 'UTA' ]; 
        If (processorList != null &&  processorList.size() > 0){
            req.setEndpoint(processorList.get(0).Base_URL__c);
        }
        
        req.setMethod('POST');
        req.setTimeout(100000);
        String test = processInputeFields(reqParamMap,trans,utamount);
        system.debug('=====Req======>>'+ test);
        req.setBody(test);
        Http http = new Http();
        HttpResponse res = http.send(req);
        System.debug('======res======>>'+res.getBody());
        String str = res.getBody();
        return str;
    }
}