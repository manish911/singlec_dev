/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 *  Usage   :   Controller is used to apply the payment that doesnt has Applied_To__c field value.
            The class suggests some transactions to which the payment can be applied.
 */
public with sharing class ApplyPaymentController {
    public Payment__c payment {get; set; }
    public Decimal totalAppliedBalance {get; set; }
    public Decimal totalUnappliedBalance {get; set; }
    public boolean IfPageHasErrors {get; set; }

    public Boolean isExactMatch {get; set; }
    public Boolean isClosestMatch {get; set; }
    public Boolean isOldestMatch {get; set; }
   

    public List<TransactionWrapper> exactMatchTxs {get; set; }
    public List<TransactionWrapper> closestMatchTxs {get; set; }
    public List<TransactionWrapper> oldestMatchTxs {get; set; }
   
    
    // Constructor sets the Default value of payment
    // param - sc Apexpages.Standardcontroller
    public ApplyPaymentController(Apexpages.Standardcontroller sc) {
        Id paymentId = (ID) sc.getId();
        if(paymentId != null ){
            this.payment = [select Id, Name, Amount__c,account__c, account__r.id from Payment__c where id=: paymentId];
           
        }
        totalAppliedBalance = 0;
        totalUnappliedBalance = 0;

        isExactMatch = false;
        isClosestMatch = false;
        isOldestMatch = false;
        initInvoiceMatching();
        calculateSummary();
        IfPageHasErrors = false;
       
    }
    
    public void initInvoiceMatching() {
        exactMatchTxs = getExactMatchTransaction();
        closestMatchTxs = getClosestMatchTransaction();
        oldestMatchTxs = getOldestMatchTransaction();
       
    }

    
    // get all the transactions with exact undisputed balance match
    // Return Type - List<TransactionWrapper>
    // No parameter ar passed in this method 
    private List<TransactionWrapper> getExactMatchTransaction() {
        List<Transaction__c> lstTx = [select Id, Indicator_Icons__c, Name, Amount__c,
                                      Undisputed_Balance__c, Days_Past_Due__c
                                      from Transaction__c
                                      where Transaction_Key__c != null and Undisputed_Balance__c =: payment.Amount__c and Account__c =: payment.account__c];

        List<TransactionWrapper> txWrapList = new List<TransactionWrapper>();
        Integer counter=1;
        if(lstTx != null && lstTx.size()>0) {
            isExactMatch = true;
            for(Transaction__c tx : lstTx) {
                txWrapList.add(new TransactionWrapper(tx, counter==1 ? true : false, counter==1 ? payment.Amount__c : 0));
                counter++;
            }
        }

        return txWrapList;
    }

    // get all the transactions with close undisputed balance match
    // Return Type - List<Transaction Wrapper>
    // No parameter are passed in this method 
    // we are getting 3 higher  balance transactions and 3 lower
    private List<TransactionWrapper> getClosestMatchTransaction() {
        // get list of Transaction having undisouted_balance grater then payment amount
        List<Transaction__c> lstTxHigher = [select Id, Indicator_Icons__c, Name, Amount__c,
                                            Undisputed_Balance__c, Days_Past_Due__c
                                            from Transaction__c
                                            where Transaction_Key__c != null and Account__c =: payment.account__c and Undisputed_Balance__c > : payment.Amount__c and Undisputed_Balance__c > 0
                                            order by Undisputed_Balance__c Limit 3];
        // get list of Transaction having undisouted_balance Less then payment amount
        List<Transaction__c> lstTxLower = [select Id, Indicator_Icons__c, Name, Amount__c,
                                           Undisputed_Balance__c, Days_Past_Due__c
                                           from Transaction__c
                                           where Transaction_Key__c != null and Undisputed_Balance__c < : payment.Amount__c and Account__c =: payment.account__c and Undisputed_Balance__c > 0
                                           order by Undisputed_Balance__c desc Limit 3];
        
        List<TransactionWrapper> txWrapList = new List<TransactionWrapper>();
        // add value in to Transaction Wrapper with payment value and undisputed balance having higest Transaction value
        if(lstTxHigher != null && lstTxHigher.size()>0) {
            for(Integer i=lstTxHigher.size()-1; i>=0; i--) {
                Transaction__c tx  = lstTxHigher.get(i);
                txWrapList.add(new TransactionWrapper(tx, false, Math.min(payment.Amount__c, tx.Undisputed_Balance__c)));
            }
        }
        if(lstTxLower != null && lstTxLower.size()>0) {
            for(Transaction__c tx : lstTxLower)
                txWrapList.add(new TransactionWrapper(tx, false, Math.min(payment.Amount__c, tx.Undisputed_Balance__c)));
        }

        return txWrapList;
    }

    // get 5 transaction that have most DPD
    // Return Type - List<TransactionWrapper>
    // No parameters are passed in this method 
    private List<TransactionWrapper> getOldestMatchTransaction() {
        List<Transaction__c> lstTxOldest = [select Id, Indicator_Icons__c, Name, Amount__c,
                                            Undisputed_Balance__c, Days_Past_Due__c
                                            from Transaction__c
                                            where Transaction_Key__c != null and Account__c =: payment.account__c and Undisputed_Balance__c < : payment.Amount__c and Undisputed_Balance__c > 0
                                            order by Days_Past_Due__c desc, Undisputed_Balance__c desc Limit 5];

        List<TransactionWrapper> txWrapList = new List<TransactionWrapper>();

        Decimal paymentAmt = payment.Amount__c;
        if(lstTxOldest != null && lstTxOldest.size()>0) {
            for(Transaction__c tx : lstTxOldest) {
                txWrapList.add(new TransactionWrapper(tx, paymentAmt>0 ? true : false, paymentAmt>0 ? Math.min(paymentAmt,tx.Undisputed_Balance__c) : 0));
                paymentAmt -= tx.Undisputed_Balance__c;
            }
        }
        return txWrapList;
    }
    // A payment line is applied to selected transaction
    // Return Type - Pagereference
    // No parameters are passed in this method      
    public Pagereference applyPayment() {
        List<Payment_Line__c> paymentsLines = new List<Payment_Line__c>();
        // add payment line which is haveing exect match value
        for(TransactionWrapper tw : exactMatchTxs) {
            if(tw.selected == true)
                paymentsLines.add(createPaymentLine(tw.tx.id,tw.appliedAmount));
        }
        // add payment line which is haveing closesr match value of payment
        for(TransactionWrapper tw : closestMatchTxs) {
            if(tw.selected == true)
                paymentsLines.add(createPaymentLine(tw.tx.id,tw.appliedAmount));
        }
        // add payment line which is haveing oldest payment value
        for(TransactionWrapper tw : oldestMatchTxs) {
            if(tw.selected == true)
                paymentsLines.add(createPaymentLine(tw.tx.id,tw.appliedAmount));
        }


        calculateSummary();
        // insert payment line value 
        if(totalAppliedBalance <= payment.Amount__c && paymentsLines != null && paymentsLines.size()>0){
           
            //========================================================================================== 
               Map<String,Schema.SObjectField> m = Schema.SObjectType.Payment_Line__c.fields.getMap();
               for (String fieldToCheck : m.keyset()) {
                    System.debug('=======fieldToCheck======'+fieldToCheck);
                  // Check if the user has create access on the each field
                  if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
                      System.debug('=======fieldToCheck2======'+fieldToCheck);
                      if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                        System.debug('=======fieldToCheck in======'+fieldToCheck);
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                'Insufficient access'));
                       return null;
                      }
                  }
                }
            //========================================================================================
            insert paymentsLines;
        }else {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.FATAL,Label.Label_Applied_balance_can_not_exeed_payment_amount));
            IfPageHasErrors = true;
        }

        return null;
    }
     // Creates a Payment Line for a Transaction
     // Return Type - Payment_Line__c (Custom Object Payment Line)
     // Parameters - ID appliedTo(Traansaction Id), Decimal appliedAmt (Amount applied in payment line) 
    public Payment_Line__c createPaymentLine(Id appliedTo, Decimal appliedAmt) {
        Payment_Line__c p = new Payment_Line__c();
        p.Applied_Amount__c = appliedAmt;
        p.Applied_To__c = appliedTo;
        p.Payment__c = payment.Id;
        return p;
    }
    // Calculates Applied Amount for all Payment Line Created 
    // Return Type - Void 
    // No parameters are passed in this method  
    public void calculateSummary() {
        Decimal totalAppliedAmt = 0;

        for(TransactionWrapper tw : exactMatchTxs) {
            if(tw.selected == true)
                totalAppliedAmt += tw.appliedAmount;
        }
        for(TransactionWrapper tw : closestMatchTxs) {
            if(tw.selected == true)
                totalAppliedAmt += tw.appliedAmount;
        }
        for(TransactionWrapper tw : oldestMatchTxs) {
            if(tw.selected == true)
                totalAppliedAmt += tw.appliedAmount;
        }
        
        totalAppliedBalance = totalAppliedAmt;
        totalUnappliedBalance = payment.Amount__c - totalAppliedBalance;
    }
    
    public class TransactionWrapper {
        public Transaction__c tx {get; set; }
        public Boolean selected {get; set; }
        public Decimal appliedAmount {get; set; }
        // constructor sets value of transaction id, selected and applied amount
        // param - Transaction__c inTx , Boolean sel , Decimal appliedAmt
        public TransactionWrapper(Transaction__c inTx, Boolean sel, Decimal appliedAmt) {
            this.tx = inTx;
            this.selected = sel;
            this.appliedAmount = appliedAmt;
        }
    }
}