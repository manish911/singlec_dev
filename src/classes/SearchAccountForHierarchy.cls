public with sharing class SearchAccountForHierarchy{
    
    public String searchedString {get;set;}
    public List<WrapperAccount> accWrapper {get;set;} 
    Public Account acc {get;set;}
    public List<Id> accIdList{get;set;}
   // public transient AccountStructure ast{get;set;}
    public transient AkritivAccountStructureForExportExcel ast1{get;set;}
    public Integer cnt{get;set;}
    public SearchAccountForHierarchy(ApexPages.StandardController controller) {
            accWrapper = new List<WrapperAccount>();
            ast1 = new AkritivAccountStructureForExportExcel();
             acc =   [Select Id,Name,ParentId from Account where Id = : APexPages.currentPage().getParameters().get('Id')];
             buildAccountIdList(acc);
              
            
    }
    
  /*  public void buildAccountIdList(Account acc){
        ast = new AccountStructure();
        List<AccountStructure.ObjectStructureMap> osMap =  ast.formatObjectStructure(acc.id);
        accIdList = new List<Id>();
        
        for(AccountStructure.ObjectStructureMap os : osMap){
           accIdList.add(os.account.Id);
        }
         system.debug('====accIdList======'+accIdList);
    }
    
  */
  
  public void buildAccountIdList(Account acc){
        ast1 = new AkritivAccountStructureForExportExcel();
        List<List<AkritivAccountStructureForExportExcel.ObjectStructureMap>> osMap =  ast1.formatObjectStructure(acc.id);
        accIdList = new List<Id>();
        
        for(List<AkritivAccountStructureForExportExcel.ObjectStructureMap> os : osMap){
            for(AkritivAccountStructureForExportExcel.ObjectStructureMap os1 : os){
                accIdList.add(os1.account.Id);
           }
        }
         system.debug('====accIdList======'+accIdList);
    }
  
    public PageReference fetchedAccounts(){
    system.debug('====accIdList======'+accIdList);
        accWrapper.clear();
        if(searchedString == null || searchedString == ''){
            Apexpages.addMessage(new ApexPages.Message(Apexpages.SEVERITY.INFO,Label.Label_Please_Enter_Account_Name));
            
            return null;
        }
        
        searchedString = searchedString.trim();
        String query = '';
        
        
        
        
        searchedString = string.escapeSingleQuotes(searchedString);
        String searchedString1 = '%' + searchedString + '%';
       
        if(acc.ParentId == null){
            query = 'Select Parent.AccountNumber,AccountNumber,Name,Total_AR__c from Account where Name  LIKE:searchedString1 and Id NOT in : accIdList limit 250';
        }
        else{
            query = 'Select Parent.AccountNumber,AccountNumber,Name,Total_AR__c from Account where Name  LIKE:searchedString1 and Id NOT in : accIdList limit 250';
        }
        
           system.debug('=====acc.ParentId ====='+acc.ParentId);
        
 
        List<Account>  accNew = Database.Query(query); //[Select Parent.AccountNumber,AccountNumber,Name,Total_AR__c,Risk_Rating__c from Account where Name LIKE \'%+searchedString+%\'];
        
        WrapperAccount wa;
        cnt = 0;

            for(Account ac : accNew){
              /*  Integer myDMLLimit = Limits.getLimitScriptStatements();
                Integer myDMLLimit1 = Limits.getScriptStatements();
                
                if(myDMLLimit1 > myDMLLimit - 50000){
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Label_The_hierarchy_displays));
                    return null;
                }*/
                
                wa = new WrapperAccount(); 
                wa.searchedAccount = ac;
                accWrapper.add(wa);
                cnt++;
            }

        
        return null;
    }
    
    public PageReference assignParent(){
        if(searchedString == null || searchedString == ''){
            Apexpages.addMessage(new ApexPages.Message(Apexpages.SEVERITY.INFO,Label.Label_Please_Enter_Account_Name));
            
            return null;
        }
        List<WrapperAccount> aw = new List<WrapperAccount>();
        List<Account> acctoUpdate = new List<Account>();
        Integer myDMLLimit;
        Integer myDMLLimit1 ;
        for(WrapperAccount a : accWrapper){
        
     /*    myDMLLimit = Limits.getLimitScriptStatements();
              myDMLLimit1 =  Limits.getScriptStatements();
                
               if(myDMLLimit1 > myDMLLimit - 30000){
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Label_The_hierarchy_displays));
                    break;
                 }
            */
            if(a.selectedAcc){
                if(acc.ParentId == null){
                    a.searchedAccount.ParentId = acc.Id;
                }else{ 
                    a.searchedAccount.ParentId = acc.Id;
                }
                acctoUpdate.add(a.searchedAccount);
            }else{
               aw.add(a); 
            }
         
        }
        update acctoUpdate;
       // 
        accWrapper.clear();
       // 
        accWrapper.addAll(aw);
        buildAccountIdList(acc);
        

       // 
        return null;
    }
    
    public class WrapperAccount {
        
        public Boolean selectedAcc {get;set;}
        Public Account searchedAccount {get;set;}
        
        public WrapperAccount(){
            selectedAcc = false;
            searchedAccount = new Account();
        }
            
   }
      public PageReference exportToExcel(){
        PageReference pr = new PageReference('/apex/ExportAkritivAccountHierarchyPageNew');
        return pr;
   }
   
   public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
        
        insert accObj;
        return accObj;
    }
   
   public static testMethod void testCase1(){
       Account accObj = SearchAccountForHierarchy.accountInsert();
       APexPages.currentPage().getParameters().put('Id',accObj.Id);
       System.assertEquals(ApexPages.currentPage().getParameters().get('Id'),accObj.Id);
       Account accObjNew = SearchAccountForHierarchy.accountInsert();
       accObjNew.ParentId = accObj.Id;
       update accObjNew;
        ApexPages.StandardController controller = new ApexPages.StandardController(accObj);
       SearchAccountForHierarchy sa = new SearchAccountForHierarchy(controller);
       sa.searchedString = accObj.Name;
       sa.fetchedAccounts();
       sa.assignParent();
     
   }
    public static testMethod void testCase2(){
       Account accObj = SearchAccountForHierarchy.accountInsert();
       
       Account accObjNew = SearchAccountForHierarchy.accountInsert();
       Account acc = DataGenerator.accountInsert();
       accObjNew.ParentId = accObj.Id;
       APexPages.currentPage().getParameters().put('Id',accObjNew.Id);
        System.assertEquals(ApexPages.currentPage().getParameters().get('Id'),accObjNew.Id);
       ApexPages.StandardController controller = new ApexPages.StandardController(accObjNew);
       SearchAccountForHierarchy sa = new SearchAccountForHierarchy(controller);
       sa.searchedString = accObj.Name;
       sa.fetchedAccounts();
       sa.accWrapper.get(0).selectedAcc = true;
       sa.assignParent();
   }
   public static testMethod void testCase3(){
       Account accObj = SearchAccountForHierarchy.accountInsert();
       
       Account accObjNew = SearchAccountForHierarchy.accountInsert();
       Account acc = DataGenerator.accountInsert();
       APexPages.currentPage().getParameters().put('Id',accObjNew.Id);
        System.assertEquals(ApexPages.currentPage().getParameters().get('Id'),accObjNew.Id);
       ApexPages.StandardController controller = new ApexPages.StandardController(accObjNew);
       SearchAccountForHierarchy sa = new SearchAccountForHierarchy(controller);
       sa.fetchedAccounts();
       sa.assignParent();
   }
}