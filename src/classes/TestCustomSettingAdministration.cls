/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for CustomSettingAdministration.
 */
@isTest
private class TestCustomSettingAdministration
{
	static testMethod void testCaseWithCustomSetting()
	{

		Test.startTest();

		//Initializing  the CustomSettingAdministrationController
		CustomSettingAdministrationController controller = new CustomSettingAdministrationController();

		if( SysConfig_Singlec__c.getOrgDefaults() == null)
		{
			SysConfig_Singlec__c insertSysConfig = new  SysConfig_Singlec__c();

			insertSysConfig.Batch_Job_Notification_Email__c = 'test@test.com';
			insertSysConfig.Billing_PDF_Rows_In_First_Page__c = 12;
			insertSysConfig.Billing_PDF_Rows_in_Subsequent_Pages__c = 10;
			insertSysConfig.Billing_Statement__c = 'Test';
			insertSysConfig.Billing_Statement_Excel_page__c = 'Test';
			insertSysConfig.Billing_Statement_Excel_Template__c = 'Test';
			insertSysConfig.Billing_Statement_PDF_page__c = 'Test';
			insertSysConfig.Billing_Statement_Pdf_Template__c = 'Test';
			insertSysConfig.Org_Logo__c = 'Test';
			insertSysConfig.IconsetURL__c = 'Test';
			insertSysConfig.Invoice_Details__c = 'Test';
			insertSysConfig.Invoice_PDF_Url_Prefix__c = 'Test';
			insertSysConfig.Invoice_Summary__c = 'Test';
			insertSysConfig.NameSpace__c = 'TestName';
			insertSysConfig.contact_Customer_Pdf__c = 'Test';

			insert insertSysConfig;
			controller.sysConfObj = insertSysConfig;
		}

		else
		{
			//Submitting values for System configuration
			controller.sysConfObj.Batch_Job_Notification_Email__c = 'test@test.com';
			controller.sysConfObj.Billing_PDF_Rows_In_First_Page__c = 12;
			controller.sysConfObj.NameSpace__c = 'TestName';
		}

		//Will update the cusatom setting for system configuration
		System.assertEquals(controller.saveSysConfig(), null);

		//Getting system configuration after update
		SysConfig_Singlec__c tempSysConfig =  SysConfig_Singlec__c.getOrgDefaults();

		//Checking the updated values for System configuration
		System.assertEquals(tempSysConfig.Batch_Job_Notification_Email__c, 'test@test.com');
		System.assertEquals(tempSysConfig.Billing_PDF_Rows_In_First_Page__c, 12);
		System.assertEquals(tempSysConfig.NameSpace__c, 'TestName');

		if(Risk_Configuration_Singlec__c.getOrgDefaults() == null)
		{
			Risk_Configuration_Singlec__c insertRiskConfig = new Risk_Configuration_Singlec__c();

			insertRiskConfig.Avg_DPD_Range1__c = 10;
			insertRiskConfig.Avg_DPD_Range2__c = 15;
			insertRiskConfig.Contact_Risk_Range1__c = 10;
			insertRiskConfig.Contact_Risk_Range2__c = 15;
			insertRiskConfig.Credit_Range1__c = 15;
			insertRiskConfig.Credit_Range2__c = 15;
			insertRiskConfig.High_Past_Due_Range1__c = 15;
			insertRiskConfig.High_Past_Due_Range2__c = 15;
			insertRiskConfig.Oldest_Age_Range_1__c = 15;
			insertRiskConfig.Oldest_Age_Range_2__c = 15;
			insertRiskConfig.Percent_Disputed_Range1__c = 15;
			insertRiskConfig.Percent_Disputed_Range2__c = 15;

			insert insertRiskConfig;
			controller.riskConfObj = insertRiskConfig;
		}
		else
		{
			//Submitting values for Risk configuration
			controller.riskConfObj.Avg_DPD_Range1__c = 10;
			controller.riskConfObj.Avg_DPD_Range2__c = 15;
			controller.riskConfObj.Contact_Risk_Range1__c = 10;
			controller.riskConfObj.Contact_Risk_Range2__c = 15;
		}

		//Will update the cusatom setting for risk configuration
		System.assertEquals(controller.saveRiskConfig(), null);

		//Getting system configuration after update
		Risk_Configuration_Singlec__c tempRiskConfig = Risk_Configuration_Singlec__c.getOrgDefaults();

		//Checking the updated values for System configuration
		System.assertEquals(tempRiskConfig.Avg_DPD_Range1__c, 10);
		System.assertEquals(tempRiskConfig.Avg_DPD_Range2__c, 15);
		System.assertEquals(tempRiskConfig.Avg_DPD_Range1__c, 10);
		System.assertEquals(tempRiskConfig.Contact_Risk_Range2__c, 15);

		Test.stopTest();

	}

	static testMethod void testCaseWithoutCustomSetting()
	{

		Test.startTest();

		//Initializing  the CustomSettingAdministrationController
		CustomSettingAdministrationController controller = new CustomSettingAdministrationController();

		controller.sysConfObj = null;
		controller.riskConfObj = null;

		//Will show the error on page that 'You should first make system configuration!'
		System.assertEquals(controller.saveSysConfig(), null);

		//Will show the error on page that 'You should first make risk configuration!'
		System.assertEquals(controller.saveRiskConfig(), null);

		Test.stopTest();

	}
}