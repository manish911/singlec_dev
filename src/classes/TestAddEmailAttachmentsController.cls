/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for AddEmailAttachmentsController
 */
@isTest
private class TestAddEmailAttachmentsController
{
    static testMethod void testAddEmailAttachmentsController()
    {
        Test.startTest();
        AddEmailAttachmentsController controller = new AddEmailAttachmentsController();

        controller.addAttachment();

        System.assertEquals(controller.attachmentsList.size(),0);

        controller.attach.Name = 'test';
        controller.attach.Body = Blob.valueOf('test');
        controller.addAttachment();

        System.assertEquals(controller.attachmentsList.size(),1);

        controller.attach.Name = 'test2';
        controller.attach.Body = Blob.valueOf('test2');
        controller.addAttachment();

        System.assertEquals(controller.attachmentsList.size(),2);

        controller.attachmentIdToRemove = 'pg:frm:pb:j_id95:j_id96:j_id99:0:attachmentFile';
        controller.removeAttachment();

        System.assertEquals(controller.attachmentsList.size(),1);

        controller.saveAttachments();
        Test.stopTest();
    }
}