/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:
 */
public with sharing class JSONObject {

    public class value {
        /* apex object to store one of several different data types
         * see : http://www.json.org/
         */
        public value() {
        }
        public value(boolean bb) {
            bool  = bb;
        }
        public value(integer ii) {
            num=ii;
        }
        public value(Decimal ii) {
            dnum= ii;
        }
        public value(string st) {
            str=st;
        }
        public value( jsonobject oo) {
            obj = oo;
        }
        public value( list<value> vary ) {
            values = vary;
        }

        public string str {get; set; }
        public integer num {get; set; }
        public double dnum {get; set; }
        public JSONObject obj {get; set; }
        public list<value> values {get; set; } // array
        public boolean bool {get; set; }
        // if all members are null, value == null
        // isNull()
        /*
         * value to string is used to output JSON for Google Visualization API
         */
        public string valueToString() {
            if ( this.bool != null ) return (this.bool ? 'true' : 'false');
            if ( this.str != null ) return '"'+this.str +'"';
            if ( this.num != null ) return String.valueof(this.num);
            if ( this.dnum != null ) return String.valueof(this.dnum);
            if ( this.values != null ) {
                string ret = '[';
                for ( value v : this.values) {
                    ret += v.valueToString() + ',';
                }
                if ( this.values.size() > 0 )   {
                    // remove last comma
                    ret = ret.substring(0,ret.length()-1);
                }
                return ret + ']';
            }
            if ( this.obj != null ) {
                return this.obj.valueToString();
            }
            return 'null';
        }

    } // end of class value

    /* **
     * The map where the JSONObject's properties are kept.
     */
    private map<string,value> data = new map<string,value>();

    public value getValue(string key) {
        return data.get(key);
    }

    /* **
     * Construct an empty JSONObject.
     */
    public JSONObject() {
    }

    /*
     * Construct a JSONObject from a JSONTokener.
     * @param x A JSONTokener object containing the source string.
     * @ If there is a syntax error in the source string.
     */
    public JSONObject(JSONTokener x) {
        this();
        string c;
        String key;

        if (x.nextClean() != '{') {
            throw new jsonexception('A JSONObject text must begin with {');
        }
        for (;; ) {
            c = x.nextClean();
            if ( c == null)  {
                throw new jsonexception('A JSONObject text must end with }');
            } else if ( c== '}' ) {
                return;
            } else {
                x.back();

                key = (string)x.nextValue().str;
            }

            /*
             * The key is followed by ':'. We will also tolerate '=' or '=>'.
             */

            c = x.nextClean();
            if (c == '=') {
                if (x.next() != '>') {
                    x.back();
                }
            } else if (c != ':') {
                throw new jsonexception('Expected a : after a key');
            }

            putOpt(key, x.nextValue()); // load next value into data map

            /*
             * Pairs are separated by ','. We will also tolerate ';'.
             */

            string nc = x.nextClean();
            if ( nc == ';' || nc ==  ',' ) {
                if (x.nextClean() == '}') {
                    return;
                }
                x.back();
            } else if ( nc == '}' ) {
                return;
            } else {
                throw new jsonexception('Expected a , or }');
            }
        }
    }

    /* **
     * Construct a JSONObject from a source JSON text string.
     * This is the most commonly used JSONObject constructor.
     * @param source    A string beginning
     *  with <code>{</code>&nbsp;<small>(left brace)</small> and ending
     *  with <code>}</code>&nbsp;<small>(right brace)</small>.
     * @exception JSONException If there is a syntax error in the source string.
     */
    public static JSONObject instance(String source) {
        return new JSONObject( new JSONTokener(source) );
    }
    public JSONObject (String source) {
        this( new JSONTokener(source) );
    }

    /* **
     * Get the value object associated with a key.
     *
     * @param key   A key string.
     * @return      The object associated with the key.
     * @throws   JSONException if the key is not found.
     */
    public object get(string key) {
        value ret = this.data.get(key);
        if (ret == null) {
            throw new JSONException('JSONObject[' + key + '] not found.');
        }
        if ( ret.str != null ) return ret.str;
        if ( ret.num != null ) return ret.num;
        if ( ret.bool != null ) return ret.bool;
        system.assert( ret.obj == null, 'get cannot return nested json ojects');
        // array?
        return null;
    }

    /* **
     * Get the boolean value associated with a key.
     * @param key   A key string.
     * @return      The truth.
     * @throws   JSONException
     *  if the value is not a Boolean or the String 'true' or 'false'.
     */
    public boolean getBoolean(String key)  {
        Object o = this.get(key);
        if ( o!=null) return (boolean) o;
        throw new JSONException('JSONObject[' + key + '] is not a Boolean.');
    }

    /* **
     * Get the string associated with a key.
     *
     * @param key   A key string.
     * @return      A string which is the value.
     * @throws   JSONException if the key is not found.
     */
    public String getString(String key)  {
        return this.data.get(key).str;
    }

    /* **
     * Determine if the JSONObject contains a specific key.
     * @param key   A key string.
     * @return      true if the key exists in the JSONObject.
     */
    public boolean has(String key) {
        return this.data.containsKey(key);
    }

    /* **
     * Get an enumeration of the keys of the JSONObject.
     *
     * @return A set of the keys.
     */
    public set<string> keys() {
        return this.data.keySet();
    }

    /* **
     * Get the number of keys stored in the JSONObject.
     *
     * @return The number of keys in the JSONObject.
     */
    public integer length() {
        return this.data.keySet().size();
    }

    /* **
     * Get an optional value associated with a key.
     * @param key   A key string.
     * @return      An object which is the value, or null if there is no value.
     */
    public Object opt(String key) {
        return key == null ? null : this.get(key);
    }

    /* **
     * Get an optional boolean associated with a key.
     * It returns false if there is no such key, or if the value is not
     * true or the String 'true'.
     *
     * @param key   A key string.
     * @return      The truth.
     */
    public boolean optBoolean(String key) {
        return optBoolean(key, false);
    }

    /* **
     * Get an optional boolean associated with a key.
     * It returns the defaultValue if there is no such key, or if it is not
     * a Boolean or the String 'true' or 'false' (case insensitive).
     *
     * @param key              A key string.
     * @param defaultValue     The default.
     * @return      The truth.
     */
    public boolean optBoolean(String key, boolean defaultValue) {
        try {
            return getBoolean(key);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    /* **
     * Put a key/value pair in the JSONObject, but only if the
     * key and the value are both non-null.
     * @param key   A key string.
     * @param value An object which is the value. It should be of one of these
     *  types: Boolean, Double, Integer, JSONArray, JSONObject, Long, String,
     *  or the JSONObject.NULL object.
     * @return this.
     * @ If the value is a non-finite number.
     */
    public JSONObject putOpt(String key, value value)  {
        if (key != null && value != null) {
            data.put(key, value);
        }
        return this;
    }

    /* **
     * valueToString
     * Make a JSON text of an Object value. The method is required to produce a strictly
     * conforming text. If the object does not contain a toJSONString
     * method (which is the most common case), then a text will be
     * produced by other means.  the
     * value's toString method will be called, and the result will be quoted.
     *
     * <p>
     * Warning: This method assumes that the data structure is acyclical.
     * @param value The value to be serialized.
     * @return a printable, displayable, transmittable
     *  representation of the object, beginning
     *  with <code>{</code>&nbsp;<small>(left brace)</small> and ending
     *  with <code>}</code>&nbsp;<small>(right brace)</small>.
     * @ If the value is or contains an invalid number.
     */
    public String valueToString() {
        string ret = '{';
        for ( string key : this.keys() ) {
            ret += '"' + key + '": ' + this.getvalue(key).valueToString() + ',';
        }
        return ret.substring(0,ret.length()-1) + '}';
    }

    public class JSONException extends Exception {}

    public class JSONTokener {
        /*
         * A JSONTokener takes a source string and extracts characters and tokens from
         * it. It is used by the JSONObject and JSONArray constructors to parse
         * JSON source strings.
         * @author JSON.org
         * @version 3
         */

        private integer index;
        private string reader;
        private string lastChar;
        private boolean useLastChar;

        /*
         * Construct a JSONTokener from a string.
         *
         * @param s     A source string.
         */
        public JSONTokener(String s) {
            this.useLastChar = false;
            this.index = 0;
            this.reader = s;
        }

        /*
         * Back up one character. This provides a sort of lookahead capability,
         * so that you can test for a digit or letter before attempting to parse
         * the next number or identifier.
         */
        public void back()  {
            if (useLastChar || index <= 0) {
                throw new JSONException('Stepping back two steps is not supported');
            }
            index -= 1;
            useLastChar = true;
        }

        /*
         * Determine if the source string still contains characters that next()
         * can consume.
         * @return true if not yet at the end of the source.
         */
        public boolean more()  {
            String nextChar = next();
            if (nextChar == null) {
                return false;
            }
            back();
            return true;
        }

        string read( string reader) {
            if ( index+1 > reader.length() ) return null;
            return reader.substring(index,index+1);
        }

        /*
         * Get the next character in the source string.
         *
         * @return The next character, or 0 if past the end of the source string.
         */
        public String next()  {
            if (this.useLastChar) {
                this.useLastChar = false;
                if (this.lastChar != null) {
                    this.index += 1;
                }
                return this.lastChar;
            }
            string c;
            try {
                c = read(reader);
            } catch (exception exc) {
                throw new JSONException(exc);
            }

            if (c == null) { // End of stream
                this.lastChar = null;
                return null;
            }
            this.index += 1;
            this.lastChar = (String) c;
            return this.lastChar;
        }

        /*
         * Consume the next character, and check that it matches a specified
         * character.
         * @param c The character to match.
         * @return The character.
         * @throws JSONException if the character does not match.
         */
        public String next(String c)  {
            String n = next();
            if (n != c) {
                throw new JSONException('Expected ' + c + ' and instead saw ' + n );
            }
            return n;
        }

        /*
         * Get the next n characters.
         *
         * @param n     The number of characters to take.
         * @return      A string of n characters.
         * @throws JSONException
         *   Substring bounds error if there are not
         *   n characters remaining in the source string.
         */
        public String next(integer n)  {
            if (n == 0) {
                return '';
            }

            String buffer = '';
            integer pos = 0;

            if (this.useLastChar) {
                this.useLastChar = false;
                buffer = this.lastChar;
                pos = 1;
            }

            try {
                buffer = buffer + reader.substring(index, index+n);
                pos += n;
            } catch (Exception exc) {
                throw new JSONException(exc.getMessage());
            }
            this.index += pos;

            if (pos < n) {
                throw new JSONException('Substring bounds error');
            }

            this.lastChar = buffer.substring( buffer.length()-1 );
            return buffer;
        }

        /*
         * Get the next char in the string, skipping whitespace
         * and comments (slashslash, slashstar, and hash).
         * @throws JSONException
         * @return  A character, or 0 if there are no more characters.
         */
        public string nextClean()  {
            for (;; ) {
                string c = next();
                
                if (c == '/') {
                    string n = next();
                    if ( n == '/' ) {
                        do {
                            c = next();
                        } while (c != '\n' && c != '\r' && c != null);

                    }
                    else
                    if (n ==  '*') {
                        for (;; ) {
                            c = next();
                            if (c == null) {
                                throw new JSONException('Unclosed comment');
                            }
                            if (c == '*') {
                                if (next() == '/') {
                                    break;
                                }
                                back();
                            }
                        }
                    } else {
                        back();
                        return '/';
                    }
                } else if (c == '#') {
                    do {
                        c = next();
                    } while (c != '\n' && c != '\r' && c != null);
                }
                if (c == null || c > ' ') {
                    return c;
                }
            }
            return null;
        }

        /*
         * Return the characters up to the next close quote character.
         * Backslash processing is done. The formal JSON format does not
         * allow strings in single quotes, but an implementation is allowed to
         * accept them.
         * @param quote The quoting character, either
         *      <code>"</code>&nbsp;<small>(double quote)</small> or
         *      <code>'</code>&nbsp;<small>(single quote)</small>.
         * @return      A String.
         * @throws JSONException Unterminated string.
         */
        public String nextString(string quote)  {
            string c;
            string sb = '';
            for (;; ) {
                c = next(); 
                if ( c == null || c == '\n' ||  c == '\r') {
                    throw new JSONException('Unterminated string');
                }
                
                if (c == quote) {
                    return sb;
                }
                sb = sb + c;

            }
            return '';
        }

        /**
         * Get the text up but not including the specified character or the
         * end of line, whichever comes first.
         * @param  d A delimiter character.
         * @return   A string.
         */
        public String nextTo(string d)  {
            string sb = '';
            for (;; ) {
                string c = next();
                if (c == d || c == null || c == '\n' || c == '\r') {
                    if (c != null) {
                        back();
                    }
                    return sb.trim();
                }
                sb = sb + c;
            }
            return '';
        }

        /**
         * Get the text up but not including one of the specified delimiter
         * characters or the end of line, whichever comes first.
         * @param delimiters A set of delimiter characters.
         * @return A string, trimmed.
         */

        /*
         * Get the next value. The value can be a Boolean, Double, Integer,
         * JSONArray, JSONObject, Long, or String, or the JSONObject.NULL object.
         * @throws JSONException If syntax error.
         *
         * @return An object.
         */

        public value nextValue()  {

            string c = nextClean();
            String s;

            if ( c ==   '"' || c == '\'' ) {
                return new value( nextString(c) );
            }
            if ( c == '{' )  {
                back();
                return new value( new JSONObject(this) );
            }
            if ( c == '[' || c ==  '(') {
                back();
                return new value( JSONArray(this) );
            }

            /*
             * Handle unquoted text. This could be the values true, false, or
             * null, or it can be a number. An implementation (such as this one)
             * is allowed to also accept non-standard forms.
             *
             * Accumulate characters until we reach the end of the text or a
             * formatting character.
             */

            String sb = '';
            string b = c;
            while (c >= ' ' && '),:]}/\\\"[{;=#'.indexOf(c) < 0) {
                sb = sb + c;
                c = next();

            }
            back();

            /*
             * If it is true, false, or null, return the proper value.
             */

            s = sb.trim();
            if (s.equals('')) {
                throw new JSONException ('Missing value');
            }
            if (s.equalsIgnoreCase('true')) {

                return new value(true);
            }
            if (s.equalsIgnoreCase('false')) {
                return new value( false);
            }
            if (s.equalsIgnoreCase('null')) {
                return new value();
            }

            /*
             * If it might be a number, try converting it. We support the 0- and 0x-
             * conventions. If a number cannot be produced, then the value will just
             * be a string. Note that the 0-, 0x-, plus, and implied string
             * conventions are non-standard. A JSON parser is free to accept
             * non-JSON forms as long as it accepts all correct JSON forms.
             */

            if ((b >= '0' && b <= '9') || b == '.' || b == '-' || b == '+') {
                if (b == '0') {
                    if (s.length() > 2 &&
                        (s.substring(1,2) == 'x' || s.substring(1,2) == 'X')) {
                        try {
                            return new value( Integer.valueof(s.substring(2)) );
                        } catch (Exception e) {
                            /* Ignore the error */
                        }
                    } else {
                        try {
                            return new value( Integer.valueof(s) );
                        } catch (Exception e) {
                            /* Ignore the error */
                        }
                    }
                }
                try {
                    Integer si = Integer.valueof(s);
                    return new value( si );
                } catch (System.TypeException e) {
                    try {
                        return new value( Double.valueof(s) );
                    }  catch (System.TypeException g) {
                        // fall thru
                    }
                }
            }
            return new value( s );
        }

        /**
         * Skip characters until the next character is the requested character.
         * If the requested character is not found, no characters are skipped.
         * @param to A character to skip to.
         * @return The requested character, or zero if the requested character
         * is not found.
         */
        public string skipTo(string to)  {
            string c;
            try {
                integer startIndex = this.index;
                do {
                    c = next();
                    if (c == null) {
                        this.index = startIndex;
                        return c;
                    }
                } while (c != to);
            } catch (Exception exc) {
                throw new JSONException(exc);
            }

            back();
            return c;
        }

        /*
         * Make a printable string of this JSONTokener.
         *
         * @return ' at character [this.index]'
         */

    } // end of tokener (sub) class

    /*
     * Construct a JSONArray from a JSONTokener.
     * @param x A JSONTokener
     * @throws JSONException If there is a syntax error.
     */
    public static list<value> JSONArray(JSONTokener x) {
        list<value> myArrayList = new list<value>();
        string c = x.nextClean();
        string q;
        if (c == '[') {
            q = ']';
        } else if (c == '(') {
            q = ')';
        } else {
            throw new JSONException('A JSONArray text must start with [');
        }
        if (x.nextClean() == ']') {
            return myArrayList;
        }
        x.back();
        for (;; ) {
            if (x.nextClean() == ',') {
                x.back();
                myArrayList.add(null);
            } else {
                x.back();
                myArrayList.add(x.nextValue());
            }
            c = x.nextClean();
            //switch (c) {
            if ( c == ';' || c == ',') {
                if (x.nextClean() == ']') {
                    return myArrayList;
                }
                x.back();

            } else if (    c == ']' || c == ')') {
                if (q != c) {
                    throw new JSONException('Expected a >' + q + '<');
                }
                return myArrayList;
            } else {
                throw new JSONException('Expected a , or ]');
            }
        }
        return null; // not reached
    }

    /* **************************
     * TEST Methods to achieve required code coverage
     *   many of these could be improved by using assert() instead of debug()
     */
    public static testmethod void test_tokener_nl() {
        //add a new line by inserting /n
        JsonObject.JsonTokener tkr = new  JsonTokener(
                'quoted_string_foo"');
        String jsonto = tkr.nextString('"');        
        
        System.assertEquals(jsonto,'quoted_string_foo');
    }
    public static testmethod void test_tokener() {
        JsonObject.JsonTokener tkr = new  JsonTokener('//ff \n{}');
        tkr.nextClean();
        tkr = new  JsonTokener('/*fff*/\n{}');
        tkr.nextClean();
        tkr = new  JsonTokener('#ff*\n{}');
        tkr.nextClean();
        tkr = new  JsonTokener('/**ff \n{}');
        try { tkr.nextClean(); } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'Unclosed comment');
        }
    }
    public static testmethod void test_parsing() {
        JSONObject jj; JsonObject.JsonTokener tkr;
        string simple = '{"translatedText":  "Ciao mondo"  }';
        string bool_simple = '{"responseBoolean"  :   true }';
        string nsimple = '{"responseData"  :   {"translatedText":  "Ciao mondo"  }, "responseDetails": null, "responseStatus": 200 }';

        // three methods of constructing an object
        
        
        

        tkr = new  JsonTokener(nsimple);
        
        
        system.assert( tkr.next(0) == '' );

        try { tkr.next(10000);  } catch(exception ex) {
            
           // system.assert( ex.getMessage() == 'Ending position out of bounds: 10002');
        }

        

        try { tkr.next('z'); } catch(exception ex) {
            
          //  system.assert( ex.getMessage() == 'Expected z and instead saw e');
        }

        
        
        

        tkr = new  JsonTokener(nsimple);
        jj = new JSONObject( tkr);

        
        

        
        
        

        
        system.assert( jj.getString('responseDetails') == null,'expected null ');

        

        value v = jj.data.get('responseData');
        
        

        //system.assert( v.obj.valu != null);
        

        nsimple = '{"responseString"  :   "foo" }';
        tkr = new  JsonTokener(nsimple);
        jj = new JSONObject( tkr);
        

        nsimple = '{"responseBoolean"  :   true }';
        tkr = new  JsonTokener(nsimple);
        jj = new JSONObject( tkr);
        
        

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'A JSONObject text must begin with {');
        }

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'Expected a , or }');
        }

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'Unterminated string');
        }

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'Invalid integer: 009.9');
        }

        system.assert ( new JSONObject ( new JSONTokener( '{"sdfsdf": 009 }' ) ).getValue('sdfsdf').num == 9 );

        

        // array testing
        
        
        
        

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'Expected a >)<');
        }

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'A JSONArray text must start with [');
        }

        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'JSONObject[notfound] not found.');
        }

        system.assert( jj.keys() != null );
        system.assert( jj.length() > 0 );
        system.assert( jj.opt('responseBoolean') != null );
        system.assert( jj.has( 'responseBoolean') );
        try {
            
        } catch(exception ex) {
            
            system.assert( ex.getMessage() == 'JSONObject[notfound] not found.');
        }
    }

    public static testmethod void test_valueToString() {
        JsonObject json = new JsonObject();
        json.putOpt('foo', new JSONObject.value('bar'));
        /*
           Make sure that the JSON rendered by this class passes validation by parsers
           from http://json.org/json2.js and http://www.jsonlint.com.
           For example:    {foo: "bar"} is invalid
                        {"foo": "bar"} is valid
         */
        system.assertEquals( '{"foo": "bar"}', json.ValuetoString());
                        
    }

    // test that we can parse json containing a dobule
    public static testmethod void test_geocode() {
        string simple = '{ "Point": { "coordinates": [ -122.0841430, 37.4219720, 0 ] } }';
        JSONObject jj =  new JSONObject( new JsonTokener(simple) );
        value p = jj.getValue('Point');
        
        value[] vals = p.obj.data.get('coordinates').values;
        
        system.assertEquals(3,vals.size());
        system.assertEquals(-122.0841430,   vals[0].dnum);
        system.assertEquals(37.4219720,     vals[1].dnum);
  //      system.assertEquals('0',            vals[2].valueToString());
    }
    public static testmethod void test_g(){
        string googlejson = '{  "name": "1600 Amphitheatre Parkway, Mountain View, CA",  "Status": {    "code": 200,    "request": "geocode"  },  "Placemark": [ {    "id": "p1",    "address": "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",    "AddressDetails": {   "Accuracy" : 8,   "Country" : {      "AdministrativeArea" : {         "AdministrativeAreaName" : "CA",         "SubAdministrativeArea" : {            "Locality" : {               "LocalityName" : "Mountain View",               "PostalCode" : {                  "PostalCodeNumber" : "94043"               },               "Thoroughfare" : {                  "ThoroughfareName" : "1600 Amphitheatre Pkwy"               }            },         '+
                            '   "SubAdministrativeAreaName" : "Santa Clara"         }      },      "CountryName" : "USA",      "CountryNameCode" : "US"   }},    "ExtendedData": {      "LatLonBox": {        "north": 37.4251466,        "south": 37.4188514,        "east": -122.0811574,        "west": -122.0874526      }    },    "Point": {      "coordinates": [ -122.0843700, 37.4217590, 0 ]    }  } ]}   ';
        JSONObject jj = new JSONObject( new JsonTokener(googlejson)  );
        system.assertNotEquals(null,jj);
    }
    public static testmethod void testcase1(){
        String str = 'test';
        JSONObject jo=new JSONObject();
        jo=JSONObject.instance('{"sdfsdf": 009 }');
        JSONObject jo1=new JSONObject('{"sdfsdf": 009 }');
        System.assertEquals(str,'test');
    }
    
    /*
     * @Chintan
    */
    public static testmethod void testCase2(){
        string simple = '{ "Point": { "coordinates": [ 122.084, 37, 0 ] } }';
        JSONObject jj =  new JSONObject( new JsonTokener(simple) );
        value p = jj.getValue('Point');
        
        value[] vals = p.obj.data.get('coordinates').values;
        system.assertequals('37', vals[1].valueToString());
        system.assertequals(122.084, vals[0].dnum);
 //       jj.dnum = vals[0].dnum;
        jj.valueToString();
    }
    
    /*
     * @Chintan
    */
    public static testmethod void testCase3(){
        JsonObject.JsonTokener tkr = new  JsonTokener('"');
        
        String jsonto = tkr.nextString('"');        
        boolean booMore = tkr.more();
        System.assertEquals(booMore, false);
        System.assertEquals(jsonto,'');
        
        string simple = '{ "Point": { "coordinates": [ 122.084, 37, 0 ] } }';
        JSONObject json =  new JSONObject( new JsonTokener(simple) );
        value p = json.getValue('Point');
        
        value[] vals = p.obj.data.get('coordinates').values;
        json.optBoolean('testKey');
//        json.getBoolean('testKey');
    }
    
    public static testmethod void testCase4(){
        JsonObject.JsonTokener tkr = new  JsonTokener('TestChintan');
        string simple = '{ "Point": { "coordinates": [0] } }';
        JSONObject json =  new JSONObject( new JsonTokener(simple) );
        tkr.useLastChar = true;
        tkr.next(3);
        tkr.nextTo('Test');
        tkr.skipTo('Te');
        System.assertEquals(tkr.useLastChar,false);
    }
    
}