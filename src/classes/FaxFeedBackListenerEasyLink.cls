/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Easy Link Fax Feedback Listner
 */
 
global class FaxFeedBackListenerEasyLink implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          List<String> arrSubject;
          String strMailSubject = '';
          String strMailTaskId= '';
          try
          {     
                strMailSubject = email.subject;                
                arrSubject = strMailSubject.split(':');
                if(arrSubject.size() <= 0)
                    return result;
                    
                strMailTaskId= arrSubject[1].trim();                                                                        
                List<Task> tsk = [select Id,Status,CallDisposition from Task where CallDisposition =: strMailTaskId and Status = 'Sending FAX'];
                system.debug('-----tsk ------>'+tsk.size());
                if(tsk.size() > 0){
                    Task t = tsk.get(0);   
                    
                    If(arrSubject[0].equalsIgnoreCase('delivery notice')){
                        t.status = 'FAX Sent';
                    }
                    else{
                        String[] strbody = email.plainTextBody.split('\n');
                        If(strbody.size() > 12){
                           t.CallDisposition = t.CallDisposition + '.Fax Failed:' + strbody[8] + '.' + strbody[9] + '.' + strbody[10] + '.' + strbody[11];
                        }
                        t.status = 'Fax Failed';
                    }
                    
                    //==========================================================================================  
                    list<String> lstFields = new list<String>{'CallDisposition','status'};
                       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
                       for (String fieldToCheck : lstFields) {
            
                          // Check if the user has create access on the each field
                            if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                             if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
            
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                        'Insufficient access'));
                               return null;
                              }
                            }
                        }
                    //========================================================================================
                    
                    update t;
                 }                                  
          }
          catch (Exception e)
          {
               System.debug('========' + e);
          }
          return result;
      }
                   
      @isTest
      public static void testFaxFailure(){      
        Trigger_Configuration__c triggerConf = new Trigger_Configuration__c();
        triggerConf.Update_Account_ContactLog_Details__c= false;
        upsert triggerConf;
        
        Task objTask = new Task();
        objTask.CallDisposition = 'A0000020130612035434';
        objTask.Subject = 'Call';
        objTask.Priority = 'Normal';
        objTask.Status = 'Sending Fax';
        insert objTask;
         System.assertEquals('Call',objTask.Subject);
        FaxFeedBackListenerEasyLink ffl = new FaxFeedBackListenerEasyLink();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'Failure notice: A0000020130612035434';
        email.plainTextBody = '';
       
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        ffl.handleInboundEmail(email,envelope);
      }
      
      @isTest
      public static void testFaxSuccess(){         
        Trigger_Configuration__c triggerConf = new Trigger_Configuration__c();
        triggerConf.Update_Account_ContactLog_Details__c= false;
        upsert triggerConf;
        
        Task objTask = new Task();
        objTask.CallDisposition = 'A0000020130612035434';
        objTask.Subject = 'Call';
        objTask.Priority = 'Normal';
        objTask.Status = 'Sending Fax';
        insert objTask;
        System.assertEquals('Call',objTask.Subject);
        FaxFeedBackListenerEasyLink ffl = new FaxFeedBackListenerEasyLink();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'Delivery notice: A0000020130612035434';
        email.plainTextBody = '';          
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        ffl.handleInboundEmail(email,envelope);
      }      
      @isTest
      public static void testFaxException(){                         
        FaxFeedBackListenerEasyLink ffl = new FaxFeedBackListenerEasyLink();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'Delivery noticeA0000020130612035434';
        email.plainTextBody = '';     
        System.assertEquals('Delivery noticeA0000020130612035434', email.subject);     
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        ffl.handleInboundEmail(email,envelope);
      }
  }