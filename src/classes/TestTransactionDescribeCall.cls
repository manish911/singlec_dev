/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Unit tests for testing TransactionDescribeCall apex class
 */
public class TestTransactionDescribeCall {

    static testmethod void Test_DescribeCall(){

        Test.startTest();

        //Initiallizing controller
        TransactionDescribeCall.FieldDescibe fieldObj = TransactionDescribeCall.getFieldData('name');
        //Checking fieldName should be equal to the sent field in controller
        System.assertEquals(fieldObj.fieldName,'name');

        Set<String> allFieldsSet = TransactionDescribeCall.getAllFields();
        //Checking fields set size should be greater then 0
        System.assert(allFieldsSet.size() > 0);

        Set<String> lineItemSet = TransactionDescribeCall.getAllLineItemFields();
        //Checking lineItemSet set size should be greater then 0
        System.assert(lineItemSet.size() > 0);

        Test.stopTest();

    }
}