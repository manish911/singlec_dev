/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for BatchSetParentAccount
 */
@isTest
private class TestBatchSetParentAccount
{
    static testMethod void testBatchSetParentAccount()
    {
        //Insert Account
        Account tempAccount1 = DataGenerator.accountInsert();

        List<Transaction__c> txList1 = new List<Transaction__c>();

        //Insert List of Tx on Account
        txList1 = DataGenerator.transactionInsert(tempAccount1);

        List<Account> accList = new List<Account>();
        accList.add(tempAccount1);

        Account tempAccount2 = DataGenerator.accountInsert();

        List<Transaction__c> txList2 = new List<Transaction__c>();

        //Insert List of Tx on Account
        txList2 = DataGenerator.transactionInsert(tempAccount2);
        accList.add(tempAccount2);

        // start the test
        Test.startTest();

        BatchSetParentAccount controller = new BatchSetParentAccount();
        Id jobId = Database.executeBatch(controller);

        // check the job id it should be not null
        System.assertNotEquals(null, jobId);
        controller.execute(null, (List<SObject>)accList);
        controller.finish(null);

        System.assert(jobId != null);

        Test.stopTest();
    }
}