/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: For Portal User to view dispute for Related Transaction.
 */
public with sharing class PortalGenerateTransactionDisputes {

    public List<DisputeWithTransaction> disputesWithTxList {get; set; }
    private Double totalDisputeAmount;
    private Integer totalDisputes;
    public Boolean disputesCreated {get; set; }
    public String accountId {get; set; }
    public String[] transactionIds {get; set; }

    public String contactNoteTitle {get; set; }
    public String contactNote {get; set; }

    public String selectedList  {get; set; }
    public String attachmentIds {get; set; }
    public list<Attachment> customAttachments;
    private List<Transaction__c> txList = new List<Transaction__c>();
    private Boolean calledFromTxDetailPage;

    public PortalGenerateTransactionDisputes() {
        transactionIds = new String[] {ApexPages.currentPage().getParameters().get('txid')};
        if(transactionIds != null)
        {
            txList = [select Id, Name, Account__c,Balance__c, Amount__c from Transaction__c where id in:transactionIds];
            disputesWithTxList = new List<DisputeWithTransaction>();

            for(Transaction__c tx : txList) {
                DisputeWithTransaction dwt = new DisputeWithTransaction(tx);
                disputesWithTxList.add(dwt);
            }
        }
        calledFromTxDetailPage = true;
        selectedList = 'Default';
        contactNoteTitle = AkritivConstants.NOTE_DISPUTE;

    }

    //------------------------------------------------------------------------------------------//
    // GET ALL DISPUTE TYPES FROM THE METADATA BY DESCRIBE CALL
    //------------------------------------------------------------------------------------------//
    // property to get the dispute types select options
    public List<SelectOption> disputeTypes {
        get {
            if(disputeTypes == null) {
                disputeTypes = new List<SelectOption>();
                Schema.DescribeFieldResult fieldResult = Dispute__c.Type__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    disputeTypes.add(new SelectOption(f.getValue(),f.getLabel()));
                }
            }

            return disputeTypes;
        }
        set;
    }

    //------------------------------------------------------------------------------------------//
    // FUNCTION TO RETURN TO THE ANOTHER PAGE
    //------------------------------------------------------------------------------------------//
    // method is used to go back to account
    // return type : Pagereference
    public Pagereference goBackToAccount() {

        if(calledFromTxDetailPage && transactionIds!= null && transactionIds.get(0) != null)
            return new Pagereference('/'+transactionIds.get(0));

        return new Pagereference('/'+accountId);
    }

    //------------------------------------------------------------------------------------------//
    // EMAIL EDITOR's FUNCTIONALITY [EMAIL/PHONE/FAX etc..]
    //------------------------------------------------------------------------------------------//
    // method is used to execute
    // return type : Pagereference
    public Pagereference execute() {
        // create a savepoint before dml, so we will be on safeside in case of any exception and can rollback the operations
        Savepoint sp = Database.setSavepoint();
        try {
            saveDisputes();
            //add note for all selected tx
            if(contactNoteTitle != null && contactNoteTitle != '')
                addNote();
        }
        catch(Exception ex) {
            Database.rollback(sp);
        }

        return null;
    }

    //------------------------------------------------------------------------------------------//
    // MAIN FUNCTIONALITY TO SAVE THE GENERATED DISPUTES ON THE TRANSACTIONS
    //------------------------------------------------------------------------------------------//
    // method is used to save Disputes
    // return type : Pagereference
    public PageReference saveDisputes() {
        totalDisputeAmount = 0;
        totalDisputes = 0;

        List<Dispute__c> disputesToSave = new List<Dispute__c>();
        List<Transaction__c> selectedTransactions = new List<Transaction__c>();

        for(DisputeWithTransaction disputeTx : disputesWithTxList) {
            if(disputeTx.selected) {
                if(accountId == null) {
                    accountId = disputeTx.dispute.Account__c;
                }

                selectedTransactions.add(disputeTx.tx);
                disputesToSave.add(disputeTx.dispute);

                // add the dispute amount to the total for amount of all the disputes.
                if( disputeTx.dispute.Amount__c ==null || disputeTx.dispute.Amount__c == 0 )
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.Message_Valid_Dispute_Amount));
                    disputesCreated = false;
                    return null;
                }
                Double newAmt = disputeTx.dispute.Amount__c == null ? 0 : disputeTx.dispute.Amount__c;

                totalDisputeAmount = totalDisputeAmount + newAmt;
                // set the total number of disputes going to be created
                totalDisputes = totalDisputes + 1;

            }
        }
        // insert the new dispute if there is any in the list.
        if(disputesToSave.size()>0){
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Dispute__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert disputesToSave;
            }
        // set the message for created disputes with total amount and number of disputes.
        Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.INFO, totalDisputes+Label.Label_disputes_created_with_total_of+totalDisputeAmount+' dollars'));
        // set the disputesCreated flag to true
        disputesCreated = true;

        // return to the same page

        return null;
    }
    // method is used to get Selected Transactions
    // return type : List<Transaction__c>
    public List<Transaction__c> getSelectedTxs() {
        List<Transaction__c> selectedTransactions = new List<Transaction__c>();
        for(DisputeWithTransaction disputeTx : disputesWithTxList) {
            if(disputeTx.selected) {
                selectedTransactions.add(disputeTx.tx);
            }
        }
        return selectedTransactions;
    }

    //------------------------------------------------------------------------------------------//
    // ADD NOTE FUNCTIONALITY ON TRANSACTIONS
    //------------------------------------------------------------------------------------------//
    // method is used to add note
    // return type : void
    private void addNote()
    {
        List<Note> notesToInsert = new List<Note>();

        for(Transaction__c tx : getSelectedTxs())
        {
            // create a new note for all the tx individually with 'Contact Note' as note-title
            Note n = new Note();
            n.title = contactNoteTitle;
            //n.title = 'Follow-up Note';
            n.body = contactNote;

            if(tx.id != null)
                n.ParentId = tx.id;

            notesToInsert.add(n);
        }

        // insert the new notes list
        if(notesToInsert.size()>0){
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
              return;
              }
          }
        }
        //========================================================================================
            insert notesToInsert;
            }
    }

    //------------------------------------------------------------------------------------------//
    // WRAPPER CLASS TO STORE DISPUTE ALONG WITH ITS TRANSACTION DATA
    //------------------------------------------------------------------------------------------//
    /* INNER Class DisputeWithTransaction used to store the Dispute with its transaction data */
    public class DisputeWithTransaction {
        public Boolean selected {get; set; }
        public Transaction__c tx {get; set; }
        public Dispute__c dispute {get; set; }

        // Default constructor for the DisputeWithTransaction.
        public DisputeWithTransaction(Transaction__c trans) {
            tx = trans;
            selected = true;
            dispute = new Dispute__c();
            dispute.Amount__c = trans.Balance__c;
            dispute.Transaction__c = trans.Id;
            dispute.Account__c = trans.Account__c;
            dispute.Status__c = 'Unassigned';
        }

        public void setDisputeAmount(Double amt) {
            this.dispute.Amount__c = amt;
        }
    }

}