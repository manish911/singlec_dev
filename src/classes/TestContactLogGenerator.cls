/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for ContactCustomerController.
 */
@isTest
private class TestContactLogGenerator {

    static testMethod void testCreateContactLogEntry()
    {
        // create an test account
        Account acc = DataGenerator.accountInsert();
        
        Set<Id> accIdSet = new Set<Id>();
        accIdSet.add(acc.Id);

        // create a related contact to test account
        Contact contact = DataGenerator.contactInsert(acc);

        // create a list of transaction related to the test account
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(acc);

        Test.startTest();

        
        User_Preference__c currUserPref = new User_Preference__c();
        currUserPref.Name = Userinfo.getUserName();
        currUserPref.User__c = Userinfo.getUserId();

        ContactLogEntry c;
        ContactLogEntry c1;
        ContactLogEntry c2;
        ContactLogEntry c3;
        ContactLogEntry c4;
        ContactLogEntry c5;
        ContactLogEntry c6;
        ContactLogEntry c7;
        ContactLogEntry c8;
        
        List<ContactLogEntry> lstContactLogEntry = new List<ContactLogEntry>();
        
        Attachment tempAttach = new Attachment();
        tempAttach.Name = 'test';
        tempAttach.Body = Blob.valueOf('test');
        tempAttach.ParentId = acc.Id;
        insert tempAttach;
        List<Attachment> tempAttachLst = new List<Attachment>();
        tempAttachLst.add(tempAttach);
        try {
            c = new ContactLogEntry(contact.Id, acc.Id, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '');
             c1 = new ContactLogEntry(contact.Id, acc.Id, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','',Date.Today(),'');
             c2 = new ContactLogEntry(contact.Id, acc.Id, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','','');
             c3 = new ContactLogEntry(contact.Id, accIdSet, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','',Date.Today(),'');  
           
            c4 = new ContactLogEntry(contact.Id, accIdSet, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','','');     
          
            c5 = new ContactLogEntry(contact.Id, accIdSet, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','',Date.Today(),'');  
            c6 = new ContactLogEntry(contact.Id, acc.Id, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','',Date.Today(),'');
                                    
             c7 = new ContactLogEntry(contact.Id, accIdSet, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','');  
             c8 = new ContactLogEntry(contact.Id, accIdSet, txList,
                                    'Follow-up', 'Email',  'Email:follow-up', 'test notes', 'Contact', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, '','','');                 


        } catch(InvalidDataException e)
        {
            
        }

        c.store();
        c1.store();
        c2.store();
        c3.store();
        c4.store();
        c5.store();
        c6.store();
        c4.StoreMultiple();
        
        lstContactLogEntry.add(c);
        lstContactLogEntry.add(c1);
        lstContactLogEntry.add(c2);
        lstContactLogEntry.add(c3);
        lstContactLogEntry.add(c4);
        
        ContactLogEntry cl1;
        
        cl1 = new ContactLogEntry(contact.Id, acc.Id, txList,'Call-Us','FAX','FAX:follow-up','Fax Note','Fax', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, 'result','Email Test');
       

        
        ContactLogEntry cl2;
        
        cl2 = new ContactLogEntry(contact.Id, acc.Id, txList,'Call-Us','FAX','FAX:follow-up','Fax Note','Fax', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, 'result','Email Test','3');

        ContactLogEntry cl3;
        
        cl3 = new ContactLogEntry(contact.Id, acc.Id, txList,'Call-Us','FAX','FAX:follow-up','Fax Note','Fax', Userinfo.getUserId(),
                                    'Completed', ContactLogEntry.PRIORITY_DEFAULT, 'result','Email Test','3','');


        //check task got creatred or not
        Task contactTask  = [ Select id, WhoId, WhatId from Task where WhatId=:acc.Id limit 1];
        
        System.assertEquals(acc.Id, contactTask.WhatId);

        ApexPages.currentPage().getParameters().put('aId', contactTask.Id);
        
        ContactLogGenerator.getInstance().generateContactLogEntry(lstContactLogEntry);
        ContactLogGenerator.getInstance().generateContactLogEntry(new List<ContactLogEntry>());

        Test.stopTest();

        /*ActivityTransactionsController acontroller = new ActivityTransactionsController();
           List<Transaction__c> txList2  = acontroller.transactionList;
           System.assertNotEquals(null, txList2);
           
           System.assertEquals(txList.size(), txList2.size());

           acontroller.goBackToParent();
         */
         

    }
    

}