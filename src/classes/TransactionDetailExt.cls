/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Used to Remember which tab was user on in the last visit
 */

public with sharing class TransactionDetailExt {
    public String selectedTabTx {get; set; }
    public String tabNo {get; set; }
    public String objId {get; set; }
    List<User_Preference__c> tempUserPref {get; set; }
    public Transaction__c transactionController {get; set; }
    public Boolean isPortalUser {get; set; }
    public Boolean isAkritivUser { get; set; }
    public Boolean isNotAkritivUser { get; set; }
    public TransactionDetailExt(ApexPages.StandardController controller){

        List<Profile> profile =new List<Profile>([ SELECT Profile.Name FROM Profile where Id = :UserInfo.getProfileId()]);
        if(profile.get(0).Name =='Akritiv User'   ||  profile.get(0).Name == 'Akritiv Super User') {

            isAkritivUser = true;
            isNotAkritivUser = false;
        } else {

            isAkritivUser = false;
            isNotAkritivUser = true;
        }
        tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTabTx__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        isPortalUser = UtilityController.isPortalUser(Userinfo.getUserId());

        if(tempUserPref.size() > 0 ) {

            objId = tempUserPref[0].Id;
            if(tempUserPref[0].SelectedTabTx__c !=null ) {
                if(tempUserPref[0].SelectedTabTx__c == 1)
                    selectedTabTx = 'transactionsTab';
                else if (tempUserPref[0].SelectedTabTx__c == 2)
                    selectedTabTx = 'txNotesTab';

            }else{
                selectedTabTx='transactionsTab';

            }
        }
    }

}