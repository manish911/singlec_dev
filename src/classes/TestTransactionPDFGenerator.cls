/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Unit tests for testing TransactionPDFGenerator apex class
 */
public class TestTransactionPDFGenerator {
    static testMethod void Test_TransactionPDFGenerator(){

        //create an Account
        Account accObj = DataGenerator.accountInsert();

        String [] txIdsArr = new String [] {};
        txIdsArr = insertData(accObj);

        // initilize the TransactionPDFGenerator class
        Test.startTest();

        TransactionPDFGenerator transObj = new TransactionPDFGenerator();

       // System.assertEquals(transObj.getLstOfTxs(txIdsArr).size(), 5);

        transObj.getMapOfTxsWithLineItems(txIdsArr);

        System.assertEquals(transObj.hasErrorMesssages, null);
        System.assertEquals(transObj.showDetails, null);

        SysConfig_Singlec__c sysConfObj = new SysConfig_Singlec__c();

        if(ConfigUtilController.getSystemConfigObj() !=null) {
            sysConfObj = ConfigUtilController.getSystemConfigObj();

            if(sysConfObj.Org_Logo__c !=null && sysConfObj.Org_Logo__c !='') {
                System.assert(transObj.orgLogo != null);
            }
        }

        Test.stopTest();
    }
    
    static testMethod void Test_TransactionPDFGenerator2(){

        //create an Account
        Account accObj = DataGenerator.accountInsert();

        String [] txIdsArr = new String [] {};
        txIdsArr = insertData(accObj);

        // initilize the TransactionPDFGenerator class
        Test.startTest();

        TransactionPDFGenerator transObj = new TransactionPDFGenerator();
        transObj.txsAdditionalFilter = ' and Balance__c > 200 ';

       // System.assertEquals(transObj.getLstOfTxs(txIdsArr).size(), 5);

        transObj.getMapOfTxsWithLineItems(txIdsArr);

        System.assertEquals(transObj.hasErrorMesssages, null);
        System.assertEquals(transObj.showDetails, null);

        SysConfig_Singlec__c sysConfObj = new SysConfig_Singlec__c();

        if(ConfigUtilController.getSystemConfigObj() !=null) {
            sysConfObj = ConfigUtilController.getSystemConfigObj();

            if(sysConfObj.Org_Logo__c !=null && sysConfObj.Org_Logo__c !='') {
                System.assert(transObj.orgLogo != null);
            }
        }

        Test.stopTest();
    }

    public Static String [] insertData(Account acc){
        //create transaction  an their line items
        List<Transaction__c> txObjList = DataGenerator.transactionInsert(acc);

        //get saved  transaction ids
       // List<Transaction__c> tmpObjList = new List<Transaction__c>();
        
      
        
       // tmpObjList  = [select Id from Transaction__c where Account__c =:acc.Id];
        String txIds='';
        String [] txIdsArray = new String [] {};
        List<Line_Item__c > lineItemList = new List<Line_Item__c >();
        for(Transaction__c nextTx : txObjList) {
            txIds=txIds + nextTx.Id + ',';
            txIdsArray.add(nextTx.Id);
            //add line items
            Line_Item__c lineItemObj = new Line_Item__c();
            lineItemObj.Name ='testitem';
            lineItemObj.Line_Total__c =10.0;
            // lineItemObj.CurrencyIsoCode ='USD';
            lineItemObj.Transaction__c = nextTx.Id;
            lineItemList.add(lineItemObj);
        }
        insert lineItemList;

        //insert selected txs into temo_object and further use into pdf generation

        String txIdsStr = '';
        String key ='';
        key = Userinfo.getUserName()+Datetime.now();
        Temp_Object_Holder__c txIdsConfig = new Temp_Object_Holder__c();
        txIdsConfig.key__c = key;
        txIdsConfig.value__c = txIds;

        insert (txIdsConfig);

        return txIdsArray;
    }

}