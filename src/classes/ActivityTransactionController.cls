/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * This class is responsible for fetching the activity transactions records
 * for the given activity. It will then fetch transaction information for the
 * the retrieved transactions and display to the user.
 */
public with sharing class ActivityTransactionController {

    public String accountId {get; set; }
    public List<Transaction__c> transactionList {get; set; }
    public List<List<Object>> txRows {get; set; }
    public List<Object> txHeaders {get; set; }
    public Task taskObj  {get; set; }
    public List<List<List<Object>>> txDetailList {get; set; }
     public String comment {get; set; }
    public ActivityTransactionController(ApexPages.StandardController controller)
    {
        taskObj = (Task) controller.getRecord();
       
            List<Task> t = [select WhatId,Description from Task where Id=:taskObj.Id];
            comment = t == null ? null : t.get(0).Description;
            accountId = t == null ? null : t.get(0).WhatId;
    
            if(accountId != null && accountId.startsWith('001'))
                getTransactionList();
        
    }
    // Get the list of transactions to be displayed in the new Cutom List View Created 
    // All transaction which satisfies the criteria are displayed as list in the new view created 
    // Used in Account Detail Page 
    // Return Type - Void 
    // No parameters are passed in this method 
    public void  getTransactionList()
    {
        txRows = new List<List<Object>>();
        txDetailList = new List<List<List<Object>>>();
        txHeaders = new List<Object>();
        // get Transaction field header at runtime to make custom list
        CustomListViewUtility lvUtil = new CustomListViewUtility(getListView(),getTransactionIds(), accountId,false,true);
        if(lvUtil !=null && lvUtil.getDynamicTransactionFields() !=null) {
            txDetailList = lvUtil.getDynamicTransactionFields();
        }
        txHeaders = lvUtil.getHeaders();

    }

     // Get the view as the user preference
     // Returns the list views which are created for different users 
     // list views are differentiate on the basis of different users 
     // Otherwise if view is not created then default view created by salesforce is returned 
    // Return Type - String
    // No parameters are passed in this method
    public String getListView(){
        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedListView__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];

        if(tempUserPref.size() > 0 ) {
            return tempUserPref[0].SelectedListView__c;
        }else{
            return 'Default';
        }
    }
    // Get the Transaction Ids
    // Returns list of Activities which are created for transaction selected 
    // Return Type - List<List<Id>>
    // No parameters are passed in this method 
    private List<List<Id>> getTransactionIds()
    {
        String activityId = ApexPages.currentPage().getParameters().get('aId');

        List<Activity_Transaction__c> actTxObj = new List<Activity_Transaction__c>();
        List<List<Id>> txListOfList = new List<List<Id>>();
        List<Id> transactionIds = new List<Id>{};

        try {
            // select current task related Transactions 
            actTxObj =  [Select Task_Id__c, Transaction_ids__c from Activity_Transaction__c
                         where Task_Id__c =:taskObj.Id Limit 1 ];
            if(actTxObj.size() >0 && actTxObj[0].Transaction_ids__c !=null) {
                for(Id nextTxId : actTxObj[0].Transaction_ids__c.split(',')) {
                    if(nextTxId !=null )
                        transactionIds.add(nextTxId);
                }
            }
            txListOfList.add(transactionIds);

        } catch(Exception ex)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       Label.Label_Query_Error + ex.getMessage()));
        }

        return txListOfList;
    }

}