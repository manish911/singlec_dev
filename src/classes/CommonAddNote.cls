public with sharing class CommonAddNote{

    public String commonNote { get; set; }
    public String location { get; set; }
    public String PageId { get; set; }
    
    public List<Dispute__c> dips;
    public List<Transaction__c> trxs;
    public List<Account> acs;
    
    
    public CommonAddNote(){
        try {
            String accId = ApexPages.currentPage().getParameters().get('accId');
            String dId = ApexPages.currentPage().getParameters().get('dsId');
            String TrxId = ApexPages.currentPage().getParameters().get('trxId');
            PageId = ApexPages.currentPage().getParameters().get('page');
            
            location  = ApexPages.currentPage().getParameters().get('location');
            
            if(dId != null && dId != '' && PageId == 'dispute'){
                dips = new List<Dispute__c>([select id, Name , Notes__c,LastModifiedDate, Account__c from Dispute__c where id =: dId limit 1]);
                
                if(dips.size() > 0 ){
                
                    acs = new List< Account >([ select Id , Name , LastModifiedDate, account_Key__c, Owner.Name from Account where Id =: dips.get(0).Account__c limit 1]);
                    
                   // commonNote = dips.get(0).LastModifiedDate + ' ' + userInfo.getName() + ' :';
                }
            
            }else if(TrxId != null && TrxId != '' && PageId == 'Transaction'){
                trxs= new List<Transaction__c>([select id, Name , Notes__c,LastModifiedDate, Account__c from Transaction__c where id =: TrxId limit 1]);
                
                if(trxs.size() > 0 ){
                
                    acs = new List< Account >([ select Id , Name , LastModifiedDate, account_Key__c, Owner.Name from Account where Id =: trxs.get(0).Account__c limit 1]);
                    
                   // commonNote = trxs.get(0).LastModifiedDate + ' ' + userInfo.getName() + ' :';
                }
                
            }else if(accId != null && accId != '' && PageId == 'Account'){
                acs= new List<Account>([select id, Name , Notes__c,LastModifiedDate  from Account where id =: accId limit 1]);
                
                if(acs.size() > 0 ){
                
                    acs = new List< Account >([ select Id , Name ,Notes__c, LastModifiedDate, account_Key__c, Owner.Name from Account where Id =: acs.get(0).Id limit 1]);
                    
                   // commonNote = trxs.get(0).LastModifiedDate + ' ' + userInfo.getName() + ' :';
                }
            }
        
        }catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public PageReference cancel() {
        return null;
    }


     public PageReference savenote() {
     
     //**********Date Format(Ex. MM-dd-yyy)************//
    
   /*  List<String> temp1 = (Date.Today() + ' ').split(' ');
                   
     List<String> temp3 = temp1.get(0).split('-');
   
     String temp4 = temp3.get(1) + '-' + temp3.get(2) + '-' + temp3.get(0); */
     
      //**********Date Format(Ex. 06-Nov-2012)************//
     
       String temp1 = DateTime.now().format('MMM');
        String temp2 = String.valueOf(DateTime.now().day());
        String temp3 = String.valueOf(DateTime.now().year());
        
            if(temp2.length() == 1 ){
            temp2 = '0'+ temp2;
        }

        
        
              system.debug('temp1 ==='+temp1 );
       // List<String> temp3 = temp1.get(0).split(' ');
         String temp4 = temp2 + '-' + temp1 + '-' + temp3;
        
        if(PageId == 'dispute'){
            Dispute__c dp = new Dispute__c();
            if(dips.size() > 0){
                dp = dips.get(0);
           
                if(dp.Notes__c == null)
                    dp.Notes__c = '';
                    // String temp = dp.LastModifiedDate + ' ';
                  //  List<String> temp1 = (dp.LastModifiedDate + ' ').split(' ');
                   // String temp2 = temp1.get(0);
                 //   List<String> temp3 = temp1.get(0).split('-');
                //    String temp4 = temp3.get(1) + '-' + temp3.get(2) + '-' + temp3.get(0);
                    String cNote = temp4 + ' by ' + userInfo.getName() + '\n';
                   // String cNote = dp.LastModifiedDate.toDate() + 'by' + userInfo.getName() + ' - ';
                    
                    dp.Notes__c = cNote + commonNote + '\n' + '\n'+ dp.Notes__c;
                    dp.note_date__c = Date.Today();
                
                //==========================================================================================  
                     list<String> lstFields = new list<String>{'Notes__c','note_date__c'};
                   Map<String,Schema.SObjectField> m = Schema.SObjectType.Dispute__c.fields.getMap();
                   for (String fieldToCheck : lstFields) {
                      // Check if the user has create access on the each field
                      if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                         if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return null;
                          }
                       }
                    }
                //========================================================================================
                
                update dp;    
            }
        }
        
        if(PageId == 'Transaction'){
            Transaction__c tr = new Transaction__c();
            if(trxs.size() > 0){
                tr = trxs.get(0);
           
                if(tr.Notes__c == null)
                    tr.Notes__c = '';
                    // String temp = dp.LastModifiedDate + ' ';
                 //   List<String> temp1 = (tr.LastModifiedDate + ' ').split(' ');
                   // String temp2 = temp1.get(0);
                //    List<String> temp3 = temp1.get(0).split('-');
                //    String temp4 = temp3.get(1) + '-' + temp3.get(2) + '-' + temp3.get(0);
                  
                    String cNote = temp4 + ' by ' + userInfo.getName() + '\n';
                   // String cNote = tr.LastModifiedDate + ' ' + userInfo.getName() + '\n';
                    tr.Notes__c = cNote + commonNote + '\n'+ '\n'+ tr.Notes__c;
                    tr.note_date__c = Date.Today();
                
                //==========================================================================================  
                     list<String> lstFields = new list<String>{'Notes__c','note_date__c'};
                   Map<String,Schema.SObjectField> m = Schema.SObjectType.Transaction__c.fields.getMap();
                   for (String fieldToCheck : lstFields) {
                      // Check if the user has create access on the each field
                      if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                         if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return null;
                          }
                       }
                    }
                //========================================================================================
                
                update tr;    
            }
        }
        if(PageId == 'Account'){
            Account acc = new Account();
            if(acs.size() > 0){
                acc = acs.get(0);
           
                if(acc.Notes__c == null)
                    acc.Notes__c = '';
                   // String temp = dp.LastModifiedDate + ' ';
                //    List<String> temp1 = (acc.LastModifiedDate + ' ').split(' ');
                   // String temp2 = temp1.get(0);
                //    List<String> temp3 = temp1.get(0).split('-');
                //    String temp4 = temp3.get(1) + '-' + temp3.get(2) + '-' + temp3.get(0);
                   
                    String cNote = temp4 + ' by ' + userInfo.getName() + '\n';
                  //  String cNote = acc.LastModifiedDate + ' ' + userInfo.getName() + '\n';
                    acc.Notes__c = cNote + commonNote + '\n'+ '\n'+ acc.Notes__c;
                    acc.note_date__c = Date.Today();
                
                 //==========================================================================================  
                     list<String> lstFields = new list<String>{'Notes__c'};
                   Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
                   for (String fieldToCheck : lstFields) {
                      // Check if the user has create access on the each field
                      if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                         if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return null;
                          }
                       }
                    }
                //========================================================================================
                
                update acc;    
            }
        }
        
        return null;
    }
    
    static testmethod void test(){
    List<Dispute__c> disList = [select id,name from Dispute__c limit 1];
 
    ApexPages.currentPage().getParameters().put('dsId',disList.get(0).Id);
   
    ApexPages.currentPage().getParameters().put('page','dispute');
     System.assertEquals('dispute', ApexPages.currentPage().getParameters().get('page')); 
    CommonAddNote common = new CommonAddNote();
    common.cancel();
    common.saveNote();
    }
    
    static testmethod void test1(){
   
        List<Account> accList = [select id,name from Account limit 1];
        Transaction__c akd = new Transaction__c();
        akd.Account__c = accList.get(0).Id;
        akd.Amount__c = 100;
        akd.Balance__c =100;
        akd.notes__c = null;
        insert akd;
        
        ApexPages.currentPage().getParameters().put('trxId',akd.Id);
        ApexPages.currentPage().getParameters().put('page','transaction');
          System.assertEquals('transaction', ApexPages.currentPage().getParameters().get('page'));
        CommonAddNote common = new CommonAddNote();
        common.cancel();
        common.saveNote();
   
    }
     static testmethod void test2(){
   
        Account acc = Datagenerator.accountInsert();
       
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        
        ApexPages.currentPage().getParameters().put('page','account');
         System.assertEquals('account', ApexPages.currentPage().getParameters().get('page'));
        CommonAddNote common = new CommonAddNote();
        common.cancel();
        common.saveNote();
   
    }
       
}