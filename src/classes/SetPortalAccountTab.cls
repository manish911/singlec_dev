/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: Class is used when Portal User Logs in the system.
 *        So the tabs which are related to Portal User are set via this class
 */
public with sharing class SetPortalAccountTab {
    public PageReference acctPage {get; set; }
    public List<User> currUser;
    
    public SetPortalAccountTab() {
        Id currUserId = Userinfo.getUserId();
        if(UtilityController.isPortalUser(currUserId))
        {   if(currUserId != null){
                currUser =new List<User>([select Contact.AccountId from User where id =: currUserId]);
            }
            if(currUser.get(0) !=null && currUser.get(0).Contact.AccountId != null) {
                Id accountId = currUser.get(0).Contact.AccountId;
                acctPage = new ApexPages.StandardController(new Account(id=currUser.get(0).Contact.AccountId)).view();
            }
        }
    }
    // method is used to go to Account Detail Page
    // return type : Pagereference
    public Pagereference goToAccountDetailPage() {
        if(acctPage != null)
        {
            acctPage.setRedirect(true);
        }
        return acctPage;
    }
}