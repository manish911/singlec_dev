/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   test class for BatchAfterFullLoadController
 */
@isTest
private class TestDisputeBatchAfterFullLoadController {
    static testMethod void runTest() {

        List<Dispute__c> dpList =  new List<Dispute__c>();

        //Account Insert
        Account tempAccount = DataGenerator.accountInsert();

        //List of tx on Account
        dpList = DataGenerator.disputeInsert(tempAccount, null);

        //String for query
        String queryStr = 'select Batch_Number__c from Dispute__c where id=\''+dpList[0].id+'\'';

        Test.startTest();

        DisputeBatchAfterFullLoadController batch = new DisputeBatchAfterFullLoadController(dpList[0].Batch_Number__c);

        System.assertEquals(batch.totalBalanceZeroedOut, 0);
        System.assertEquals(batch.totalDisputesProcessed, 0);
        System.assertEquals(batch.batchNumber, null);

        batch.query = queryStr;
        Database.executebatch(batch);
        batch.start(null);
        batch.execute(null, dpList );
        batch.finish(null);

        System.assertEquals(batch.query, 'select Batch_Number__c from Dispute__c where id=\''+dpList[0].id+'\'');
        System.assertEquals(batch.totalDisputesProcessed, 1);
        System.assertEquals(batch.batchNumber, null);

        Test.stopTest();
    }
}