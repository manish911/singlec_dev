/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class used to run  Data Load Batchjob and aging batch job
 */
public with sharing class ExecuteBatchController {

    public String Lastdate{ get; set; }
    public Boolean isInProgress { get; set; }
    public ID batchprocessid {get; set; }
    public AsyncApexJob aaj {get; set; }
    public Integer progress {get; set; }
    public String globalDefault = 'Default';
    public String custom = 'Custom';
    public String selectedBatchJob {get; set; }

    public String batchJobToRun {get; set; }
    public String sourceSystem {get; set; }
    public String batchNumber {get; set; }
    public BatchJobConfiguration_Singlec__c batchConfigObj {get; set; }
    public Integer agingBatchSize {get; set; }
    public Integer dataLoadBatchSize {get; set; }
    public String aajStatus {get; set;}
    public List<Transaction__c> txList{get;set;}
    
   public Decimal daysPast {get; set; }

    public ExecuteBatchController() {
       
       // List<Transaction__c> txList =[select id,Batch_Number__c,balance__c From Transaction__c where balance__c  != 0 and Batch_Number__c != null order by Batch_Number__c desc limit 1] ;
         
         List<Transaction__c> txList = new List<Transaction__c>();
        List<ApexClass >  classList= [Select a.Name, a.Id From ApexClass a where a.Name = 'BatchAgingCalculations' limit 1];
        List<AsyncApexJob > apexJobList;
        if(classList.size()>0){
             apexJobList = [Select a.CreatedDate,a.CompletedDate,  a.ApexClassId From AsyncApexJob a where a.ApexClassId =: classList.get(0).Id order By a.CompletedDate desc];
        }
        if(apexJobList.size() > 0){
             Lastdate = String.valueOf(apexJobList.get(0).CompletedDate);
        }
        batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
        if(batchConfigObj != null)
        {
            agingBatchSize = (batchConfigObj.Aging_Batch_Size__c != null && batchConfigObj.Aging_Batch_Size__c > 0 && batchConfigObj.Aging_Batch_Size__c < 201 ? batchConfigObj.Aging_Batch_Size__c.intValue() : 200);
            dataLoadBatchSize = (batchConfigObj.DataLoad_Batch_Size__c != null && batchConfigObj.DataLoad_Batch_Size__c > 0 && batchConfigObj.DataLoad_Batch_Size__c < 201 ? batchConfigObj.DataLoad_Batch_Size__c.intValue() : 200);
        }
        else
        {
            agingBatchSize = 200;
            dataLoadBatchSize = 200;
        }

        batchJobToRun = ApexPages.currentPage().getParameters().get('job');
        
        if(batchJobToRun == 'dataload')
            selectedBatchJob = 'DataLoadBatchJob';
       
        if(batchJobToRun == 'autodunning')
            selectedBatchJob = 'AutoDunningBatchJob';
            
        if(batchJobToRun == 'deletetxs')
            selectedBatchJob = 'PurgeOldClosedTransactions';
            
        if(batchJobToRun == 'caladpd')
            selectedBatchJob = 'ADPDCalculationBatchJob';
            
        if(batchJobToRun == 'taskdetail')
            selectedBatchJob = 'PopulateTaskLevelField';
            
        if(batchJobToRun == null)
            selectedBatchJob = 'AgingCalculationBatchJob';
                
        progress = 0;
        isInProgress = false;
       // aaj = new List<AsyncApexJob>();
    }
    // method is used to get batch jobs 
    // return type : List<Selectoption>
    public List<Selectoption> getBatchJobs() {
        List<Selectoption> batchJobsLst = new List<Selectoption>();
        batchJobsLst.add(new SelectOption('AgingCalculationBatchJob', 'Aging calculation batch job'));
        batchJobsLst.add(new SelectOption('ADPDCalculationBatchJob', 'Average DPD calculation batch job'));
        batchJobsLst.add(new SelectOption('DataLoadBatchJob', 'Transaction Data load batch Job'));
        batchJobsLst.add(new SelectOption('PopulateTaskLevelField', 'Populate Task Level Field'));
        batchJobsLst.add(new SelectOption('DisputeDataLoadBatchJob', 'Dispute Data load batch Job'));
        batchJobsLst.add(new SelectOption('SetParentAccountBatchJob', 'Set parent accounts batch job'));
        batchJobsLst.add(new SelectOption('AutoDunningBatchJob', 'Auto Dunning batch job'));
        batchJobsLst.add(new SelectOption('PurgeOldClosedTransactions', 'Purge Old Closed Transactions'));
        return batchJobsLst;
    }
    
    
    // method is used to execute batch update 
    // return type : Pagereference
    public Pagereference executeBatchUpdate() {
      txList = [select id,Batch_Number__c,balance__c From Transaction__c where Batch_Number__c=:batchNumber and balance__c  != 0 and Batch_Number__c != null Order by Batch_Number__c DESC limit 1] ;
        try {
            progress = 0;
            isInProgress = true;
            
            if(selectedBatchJob == 'AgingCalculationBatchJob') {
 
                BatchAgingCalculations bca = new BatchAgingCalculations(sourceSystem);
                batchprocessid = Database.executeBatch(bca, agingBatchSize);
               
            }else if(selectedBatchJob == 'ADPDCalculationBatchJob'){
            
                BatchADPDCalculations bcadpd = new BatchADPDCalculations(sourceSystem);
                batchprocessid = Database.executeBatch(bcadpd, agingBatchSize);
                
            }else if(selectedBatchJob == 'SetParentAccountBatchJob') {
                  
                BatchSetParentAccount bspa = new BatchSetParentAccount();
                batchprocessid = Database.executeBatch(bspa);
            }
            else if(selectedBatchJob == 'DataLoadBatchJob') {
                
                if(txList.size() > 0 ){ 
                    BatchAfterFullLoadController bafl = new BatchAfterFullLoadController(batchNumber, sourceSystem);
                    batchprocessid = Database.executeBatch(bafl, dataLoadBatchSize);
                }else{
                      progress = 0;
                    isInProgress = false;
                     Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR,'Please Enter Valid Batch Number'));
                     return null;
                   
                }    
                
            }
            else if(selectedBatchJob == 'DisputeDataLoadBatchJob') {
                 
                DisputeBatchAfterFullLoadController disputeBatch = new DisputeBatchAfterFullLoadController(batchNumber);
                batchprocessid = Database.executeBatch(disputeBatch, dataLoadBatchSize);
            }
            else if(selectedBatchJob == 'AutoDunningBatchJob') {
                 
                BatchCloseAutomatedTasks autoDunningBatch = new BatchCloseAutomatedTasks();
                batchprocessid = Database.executeBatch(autoDunningBatch, dataLoadBatchSize);
            }
            else if(selectedBatchJob == 'PopulateTaskLevelField') {
                 
                BatchPopulateTaskWithARDetail autoDunningBatch = new BatchPopulateTaskWithARDetail();
                batchprocessid = Database.executeBatch(autoDunningBatch, dataLoadBatchSize);
            }
            
            else if(selectedBatchJob == 'PurgeOldClosedTransactions') {
                
                if(daysPast == null || daysPast <= 0){
                    isInProgress = false;
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR,'Please Enter Number Of Days'));
                    return null;
                }
                BatchPurgeDataJob batchPurgeDataObj = new BatchPurgeDataJob (daysPast);
                batchprocessid = Database.executeBatch(batchPurgeDataObj , dataLoadBatchSize);
            }
            
            
        }
        catch (Exception e) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Error:'+e.getMessage()));
        }
        return null;
    }
    // method is used to refresh Job status 
    // return type : Pagereference
    public Pagereference refreshJobStatus() {
        if(batchprocessid != null)
            aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid ];
            
           //
        if(aaj != null && aaj.TotalJobItems != null && aaj.JobItemsProcessed != null)
        {

            if(aaj.TotalJobItems==0)
                progress=0;
            else
                progress = Integer.valueOf(aaj.JobItemsProcessed*100/aaj.TotalJobItems);
               
        }
        
        if(aaj != null && aaj.Status != null ){
        if(aaj.Status == 'Completed' || aaj.Status == 'Aborted' || aaj.Status == 'Failed')
            isInProgress = false;
        }
        return null;
    }
}