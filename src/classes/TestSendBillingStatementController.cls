/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for SendBillingStatementController.
 */
@isTest
private class TestSendBillingStatementController {

    static testMethod void mytestcase1() {

                 Account acc = DataGenerator.accountInsert();
                 Contact con = DataGenerator.contactInsert(acc);


                 Task tsk = DataGenerator.taskInsert(acc);
                 
                 String listviewName =AkritivConstants.CONST_SYSTEM_PDF_EXCEL_LIST;
                 ApexPages.currentPage().getParameters().put('taskId',tsk.Id);

                 SendBillingStatementController con1= new SendBillingStatementController();
         
                 Test.startTest();
                 System.assertEquals(  ApexPages.currentPage().getParameters().get('taskId'),tsk.Id) ;

                 Custom_List_View__c clvc = new Custom_List_View__c();



                 clvc.ListView_For_Object__c='Transaction__c';
                 //clvc.Name=listviewName;
                 clvc.name=listviewName;
                 clvc.Object_Fields__c='test object field';
                 insert clvc;
                 
                 //Boolean isPor=SendBillingStatementController.validatePhone();
                  //System.assertEquals(isPor , false) ;

                 // ApexPages.currentPage().getParameters().put('taskId',tsk.Id);
                 // controller1.saveCall();
                 con1.getSelectedTransactions();

                  Test.stopTest();

       }

     static testMethod void AtestTransactionLevelBillingStatement() {
        // delete UserPreference so that a new one will be get created
        delete [select id from User_Preference__c where User__c = : Userinfo.getUserId() limit 1];

        // create an test account
        Account acc = DataGenerator.accountInsert();

        // create a related contact to test account
        Contact contact = DataGenerator.contactInsert(acc);

        // create a list of transaction related to the test account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        // get comma separated ids of transaction to create a config object value
        String txIds = '';
        for(Transaction__c tx : txList)
            txIds += ','+tx.Id;

        // create a config object for comma separated tx ids.
        Temp_Object_Holder__c Config = new Temp_Object_Holder__c();
        Config.key__c = Userinfo.getUserName()+Datetime.now();
        Config.value__c = txIds;
        insert Config;

        // set the current page parameters
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('txIdsKey', Config.key__c);
    
        // instanciate the SendBillingStatementController
        SendBillingStatementController controller = new SendBillingStatementController();
        //System.assertEquals(Page.BillingStatementPdf.getUrl(), controller.previewPdf().getUrl());
        //controller.previewTemplateAttachment();
        controller.contactMethod = 'Email'; 
        
        String returnUrl = (new Pagereference('/'+acc.Id)).getUrl();

        Transaction__c transc = new Transaction__c();
        transc.Account__c = acc.Id;
        transc.Amount__c=100;
        transc.Balance__c=5000;
        insert transc;

        Test.startTest();
        controller.execute();
        controller.saveCall();
        controller.getAttachmentTypes();
        controller.getSelectedTransactions();
     
        controller.cancel();
        controller.getStatementTemplateStr();
        controller.prepareCofigSharingForPDFGen();
        // set the target contact to be contacted
        Controller.emailTargetToContactId  = contact.id;
        
        // send an email using an email template selected from list
        controller.currUserPref.BCC_Emails_To_Me__c = true;
        controller.ccAddresses = 'test@test.com';
        controller.bccAddresses = 'test@test.com';
        String fileName = 'billingStatementpdf';
        controller.followUpDays =1;
        
        controller.contactMethod = 'Email';
        Pagereference str = controller.execute();
            
        //System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : null);
        System.assertEquals(returnUrl, controller.cancel().getUrl());
       
        Attachment att = new attachment();
        String t = 'test123';
        blob b = blob.ValueOf(t);
        att.body = b;
        att.parentid = acc.Id;
        att.name = 'test';
        insert att;
        
        
        
        controller.attachmentIds = att.id;
       
       
        // send email without using template
        controller.emailSubject = 'test sub';
        controller.emailTemplateBody = 'test email body';
        
        controller.contactMethod = 'Email'; // this will call sendEmail method
        controller.execute();
        //System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : null);

        controller.contactMethod = 'Email';
        controller.emailSubject = null; // will return null as email- subject is required field
        controller.execute();
        //System.assertEquals(null, controller.execute()!=null ? null : controller.execute().getUrl());

        // send email template and call contact without a targetObjectId [contact]
        Controller.emailTargetToContactId  = null;

        controller.contactMethod = 'Email'; // this will call sendEmail method
        controller.execute();
        //System.assertEquals(null,controller.execute()!=null ? null : controller.execute().getUrl());
        
        
        // controller.getSelectedTransactionsWithAllFields() ;
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        Test.stopTest();
    }
    
 /*   static testMethod void testTransactionLevelBillingStatementForFax() {
        // delete UserPreference so that a new one will be get created
        delete [select id from User_Preference__c where User__c = : Userinfo.getUserId() limit 1];

        // create an test account
        Account acc = DataGenerator.accountInsert();

        // create a related contact to test account
        Contact contact = DataGenerator.contactInsert(acc);
           contact.Fax = '22222';
           Update contact;
        // create a list of transaction related to the test account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        // get comma separated ids of transaction to create a config object value
        String txIds = '';
        for(Transaction__c tx : txList)
            txIds += ','+tx.Id;

        // create a config object for comma separated tx ids.
        Temp_Object_Holder__c Config = new Temp_Object_Holder__c();
        Config.key__c = Userinfo.getUserName()+Datetime.now();
        Config.value__c = txIds;
        insert Config;

        // set the current page parameters
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('txIdsKey', Config.key__c);

        // instanciate the SendBillingStatementController
        SendBillingStatementController controller = new SendBillingStatementController();
        
        String returnUrl = (new Pagereference('/'+acc.Id)).getUrl();

        Transaction__c transc = new Transaction__c();
        transc.Account__c = acc.Id;
        transc.Amount__c=100;
        transc.Balance__c=5000;
        insert transc;

        Test.startTest();
        
        Controller.emailTargetToContactId  = contact.id;
        Controller.emailTargetToFaxId =  contact.id;
        controller.currUserPref.BCC_Emails_To_Me__c = true;
        controller.ccAddresses = 'test@test.com';
        controller.bccAddresses = 'test@test.com';
        String fileName = 'billingStatementpdf';
        
        
     //   Controller.emailTargetToFaxId = contact.Fax;
        Controller.emailSubject = 'test Subject';
        controller.emailTemplateBody = 'test email body';
        controller.followUpDays =1;
       // controller.contactMethod = 'Phone'; // this will call SaveCall method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        controller.contactMethod = 'Fax'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
      //  controller.contactMethod = 'Print'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        
        controller.execute();
        controller.updateUserPreference();
       // controller.getSelectedTransactionsWithAllFields();
        Test.stopTest();
    }*/
    
     static testMethod void testTransactionLevelBillingStatementForFax2() {
        // delete UserPreference so that a new one will be get created
        delete [select id from User_Preference__c where User__c = : Userinfo.getUserId() limit 1];

        // create an test account
        Account acc = DataGenerator.accountInsert();

        // create a related contact to test account
        Contact contact = DataGenerator.contactInsert(acc);
          
        // create a list of transaction related to the test account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        // get comma separated ids of transaction to create a config object value
        String txIds = '';
        for(Transaction__c tx : txList)
            txIds += ','+tx.Id;

        // create a config object for comma separated tx ids.
        Temp_Object_Holder__c Config = new Temp_Object_Holder__c();
        Config.key__c = Userinfo.getUserName()+Datetime.now();
        Config.value__c = txIds;
        insert Config;

        // set the current page parameters
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('txIdsKey', Config.key__c);

        // instanciate the SendBillingStatementController
        SendBillingStatementController controller = new SendBillingStatementController();
        
        String returnUrl = (new Pagereference('/'+acc.Id)).getUrl();

        Transaction__c transc = new Transaction__c();
        transc.Account__c = acc.Id;
        transc.Amount__c=100;
        transc.Balance__c=5000;
        insert transc;

        Test.startTest();
        
        Controller.emailTargetToContactId  = contact.id;
        
        controller.emailTemplateBody = 'test email body';
        controller.followUpDays =1;
      //  controller.contactMethod = 'Phone'; // this will call SaveCall method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
     //   controller.contactMethod = 'Fax'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        controller.contactMethod = 'Print'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        
        controller.execute();
         controller.contactMethod = 'Phone';
         controller.execute();
        controller.updateUserPreference();
       // controller.getSelectedTransactionsWithAllFields();
        Test.stopTest();
    }
    static testMethod void testTransactionLevelBillingStatementForCall() {
        // delete UserPreference so that a new one will be get created
        delete [select id from User_Preference__c where User__c = : Userinfo.getUserId() limit 1];

        // create an test account
        Account acc = DataGenerator.accountInsert();

        // create a related contact to test account
        Contact contact = DataGenerator.contactInsert(acc);
          
        // create a list of transaction related to the test account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        // get comma separated ids of transaction to create a config object value
        String txIds = '';
        for(Transaction__c tx : txList)
            txIds += ','+tx.Id;

        // create a config object for comma separated tx ids.
        Temp_Object_Holder__c Config = new Temp_Object_Holder__c();
        Config.key__c = Userinfo.getUserName()+Datetime.now();
        Config.value__c = txIds;
        insert Config;

        // set the current page parameters
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('txIdsKey', Config.key__c);

        // instanciate the SendBillingStatementController
        SendBillingStatementController controller = new SendBillingStatementController();
        
        String returnUrl = (new Pagereference('/'+acc.Id)).getUrl();

        Transaction__c transc = new Transaction__c();
        transc.Account__c = acc.Id;
        transc.Amount__c=100;
        transc.Balance__c=5000;
        insert transc;

        Test.startTest();
        
        Controller.emailTargetToContactId  = contact.id;
        Controller.emailTargetToFaxId = contact.id;
        controller.emailTemplateBody = 'test email body';
        controller.followUpDays =1;
      //  controller.contactMethod = 'Phone'; // this will call SaveCall method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        controller.contactMethod = 'Fax'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
       // controller.contactMethod = 'Print'; // this will call SendFax method
        System.assertEquals(returnUrl, controller.execute()!=null ? controller.execute().getUrl() : '/'+acc.Id);
        
        
        controller.execute();
       //  controller.contactMethod = 'Print';
        // controller.execute();
        controller.updateUserPreference();
          
       // controller.getSelectedTransactionsWithAllFields();
        Test.stopTest();
    }
    static testMethod void testcase() {
    
        SysConfig_Singlec__c sysConfObj = SysConfig_Singlec__c.getOrgDefaults();
        sysConfObj.is_Akritiv_Org__c = true;
        sysConfObj.Billing_Statement__c = 'TestBilling';
        update sysConfObj;
        
        SendBillingStatementController sd=new SendBillingStatementController();
        sd.isFollowUp = true;
        sd.followUpDays = null;
        sd.contactNoteTitle = null;
        
        sd.execute();
        
        String emailTargetToFaxId='';
        boolean g=sd.sendFaxEmail();
        System.assertEquals(sysConfObj.Billing_Statement__c,'TestBilling');
    }
    
    static testMethod void testCase2(){
        Account acc = DataGenerator.accountInsert();
        Contact con = DataGenerator.contactInsert(acc);   
        Task tsk = DataGenerator.taskInsert(acc);    
        
        ApexPages.currentPage().getParameters().put('taskId',tsk.Id);       
        ApexPages.currentPage().getParameters().put('pageId', 'conpage');
        ApexPages.currentPage().getParameters().put('conId', con.id);
 
        Test.startTest();
        SendBillingStatementController sd=new SendBillingStatementController();       
//        sd.sourceTaskIdForPage = tsk.id;
        sd.contactmethod = '';
        
        sd.addtoAddresses = 'invalid';
        sd.emailTargetToContactId = con.id;
        sd.emailSubject = 'Test Mail - Subject';
        sd.sendEmail();       
        sd.execute();
        System.assertEquals(sd.addtoAddresses,'invalid');
        Test.stopTest();
    }
   
    
}