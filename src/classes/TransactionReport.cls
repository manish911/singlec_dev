/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Top 10 transactions with Highest "days past due"
 */

public with sharing class TransactionReport {
    public List<Transaction__c> transactionList {get; set; }

    public TransactionReport()
    {
        transactionList = new List<Transaction__c>();
        transactionList = [select Id, Account__c,Balance__c,Days_Past_Due__c from Transaction__c order By Days_Past_Due__c DESC limit 10 ];
    }

    static testmethod void runTests() {
        // create an test account
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.Account_Key__c = '9812739172';
        insert acc;

        // create a list of transaction related to the test account
        List<Transaction__c> txList = new List<Transaction__c>();
        for(Integer i=0; i<20; i++) {
            Transaction__c tx = new Transaction__c();
            tx.Name = '990000'+i;
            tx.Account__c = acc.Id;
            tx.Amount__c = 50000;
            tx.Balance__c = 40000;
            tx.Promised_Amount__c = 5000;
            tx.Disputed_Amount__c = 23000;
            tx.Create_Date__c = Date.today()-7;
            txList.add(tx);
        }
        insert txList;

        TransactionReport txReport = new TransactionReport();
        System.assertNotEquals(null, txReport.transactionList);
        System.assertEquals(10, txReport.transactionList.size());

    }
}