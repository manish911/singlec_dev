/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for Account insert,payment insert,
            paymentline insert , task insert , contact insert etc.
 */
public class DataGenerator
{
    // method is used for account insert 
    // return type : Account
    public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
         //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert accObj;
        return accObj;
    }
    
    // method is used for Contact insert 
    // return type : Contact
    // parameters : acc - Account
    public static Contact contactInsert(Account acc)
    {
        // create a related contact to test account

        Contact contact = new Contact();
        contact.LastName = 'TestContact';
        contact.Email = 'test@test.com';
        contact.AccountId = acc.Id;
         //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Contact.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert contact;
        return contact;

    }
    
    // method is used for transaction Insert 
    // return type : List<Transaction__c>
    // parameters : acc - Account
    // Parameter: count - number of trnsactions inserted.
    public static List<Transaction__c> transactionInsert(Account acc, Integer count)
    {
        //create transaction on Account

        List<Transaction__c> txObjList = new List<Transaction__c>();
        List<Id> idLst = new  List<Id>();

        Account tempAcc = acc;

        for(Integer i=0; i<count; i++)
        {
            Transaction__c txObj = new Transaction__c();
            txObj.Account__c = tempAcc.Id;
            txObj.Amount__c = 12312 + i;
            txObj.Balance__c = 6336 + i;
            txObj.Name = 'testno' + i;
            txObj.Due_Date__c = Date.today().addMonths(-7);
            txObj.Close_Date__c = Date.today() +10;
            txObj.Promised_Amount__c = 5000;
            txObj.Disputed_Amount__c = 23000;
            txObj.Create_Date__c = Date.today().addMonths(-9);
            txObj.Bill_To_City__c = 'xyz';
            txObj.Ship_To_City__c='xyz';
            txObj.Bill_To_State__c = 'Ohio';
            txObj.Ship_To_State__c = 'Ohio';
            txObj.Bill_To_Country__c ='US';
            txObj.Ship_To_Country__c ='US';
            //txObj.akritiv__Transaction_Key__c = 'TRX' + System.now().format('yyyyMMddHHmmss');
            txObjList.add(txObj);
        }
         //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Transaction__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert txObjList;

        return txObjList;
    }
    
    
    // method is used for transaction Insert 
    // return type : List<Transaction__c>
    // parameters : acc - Account
    public static List<Transaction__c> transactionInsert(Account acc)
    {
        return transactionInsert(acc, 5);
    }
    
    // method is used for dispute Insert 
    // return type : List<Dispute__c>
    // parameters : acc - Account , tx - List < Transaction__c >
    public static List<Dispute__c> disputeInsert(Account acc, List<Transaction__c> tx)
    {
        //create dispute on tx
        if(tx != null)
        {
            List<Dispute__c> tempDisp = new List<Dispute__c>();
            for(Transaction__c tempTx : tx)
            {
                Dispute__c dispute = new Dispute__c();
                dispute.Amount__c = tempTx.Balance__c;
                dispute.Transaction__c = tempTx.Id;
                dispute.Account__c = acc.Id;
                tempDisp.add(dispute);
            }
             //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Dispute__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert tempDisp;
            return tempDisp;
        }
        else
        {
            List<Dispute__c> tempDisp = new List<Dispute__c>();
            Dispute__c dispute = new Dispute__c();
            dispute.Amount__c = 1234;
            dispute.Account__c = acc.Id;
            tempDisp.add(dispute);
              //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Dispute__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert tempDisp;
            return tempDisp;
        }

    }
    
    // method is used for lineItem Insert 
    // return type : List<Line_Item__c>
    // parameters : acc - Account , tx - List<Transaction__c>
    public static List<Line_Item__c> lineItemInsert(Account acc, List<Transaction__c> tx){
        String txIds='';
        List<Line_Item__c > lineItemList = new List<Line_Item__c >();
        if(tx != null)
        {
            for(Transaction__c nextTx : tx ) {
                txIds=txIds + nextTx.Id + ',';
                //add line items
                Line_Item__c lineItemObj = new Line_Item__c();
                lineItemObj.Name ='testitem';
                lineItemObj.Line_Total__c =10.0;
                lineItemObj.Transaction__c = nextTx.Id;
                lineItemList.add(lineItemObj);
            }
             //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Line_Item__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert lineItemList;
        }
        return lineItemList;
    }
    
    // method is used for tempObject Insert 
    // return type : String
    // parameters : acc - Account , tx List < Transaction__c >
    public static String tempObjectInsert(Account acc, List<Transaction__c> tx){
        String txIds='';
        if(tx != null)
        {
            for(Transaction__c nextTx : tx ) {
                txIds=txIds + nextTx.Id + ',';
            }
        }

        String key = Userinfo.getUserName()+Datetime.now();
        Temp_Object_Holder__c txIdsConfig = new Temp_Object_Holder__c();
        txIdsConfig.key__c = key;
        txIdsConfig.value__c = txIds;
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Temp_Object_Holder__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert (txIdsConfig);

        return key;

    }
    
    // method is used for task Insert 
    // return type : Task
    // parameters : acc - Account
    public static Task taskInsert( Account acc) {
        //create a Task

        Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate  = Date.today();
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert taskObj;
        return taskObj;
    }
    
    // method is used for activity transaction Insert 
    // return type : List<Activity_Transaction__c>
    // parameters : acc - Account , t - Task
    public static List<Activity_Transaction__c> activityTransactionInsert ( Account acc, Task t) {

        List<Activity_Transaction__c> txObjList = new List<Activity_Transaction__c>();

        for(Integer i=0; i<5; i++) {
            Activity_Transaction__c txObj = new Activity_Transaction__c();

            txObj.Task_Id__c = t.Id;
            txObj.Transaction_ids__c = acc.Id;

            txObjList.add(txObj);
        }
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Activity_Transaction__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert txObjList;

        return txObjList;

    }
    
    // method is used for payment Insert 
    // return type : Payment__c
    // parameters : acc - Account , amount - Integer
    public static Payment__c paymentInsert(Account acc, Integer amount )
    {
        //create payment on Account

        List<Payment__c> payObjList = new List<Payment__c>();

        Account tempAcc = acc;

        Payment__c payObj = new Payment__c();

        payObj.Account__c = tempAcc.Id;
        payObj.Amount__c = amount;
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Payment__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert payObj;
        return payObj;
    }

    // method is used for payment line Insert 
    // return type : Payment_Line__c
    // parameters : amount - Integer , payment - Payment__c , acc - Account , tx - Transaction__c
    public static Payment_Line__c  paymentlineInsert(Integer amount, Payment__c payment, Account acc, Transaction__c tx){
        String txIds='';
        Account tempAcc = acc;

        //add line items
        Payment_Line__c paylineObj = new Payment_Line__c();
        paylineObj.Applied_Amount__c =amount;
        paylineObj.Applied_To__c =tx.Id;
        paylineObj.Payment__c =payment.Id;
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Payment_Line__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert paylineObj;

        return paylineObj;

    }

    // method is used for opportunity Insert 
    // return type : Opportunity
    // parameters : a - Account
    public static Opportunity insetOpportunity( Account a ) {

        /* Opportunity o = new Opportunity();
        o.CloseDate = Date.today();
        o.StageName = 'Qualification';
        o.Name = 'Test Opportunity';
        o.AccountId = a.Id;

        insert o;

        PricebookEntry entry = [ Select p.UseStandardPrice, p.UnitPrice, p.SystemModstamp, p.ProductCode, p.Product2Id, p.Pricebook2Id, p.Name, p.LastModifiedDate, p.LastModifiedById, p.IsDeleted, p.IsActive, p.Id, p.CreatedDate, p.CreatedById From PricebookEntry p limit 1];

        List < OpportunityLineItem > lst = new List < OpportunityLineItem >();
        for ( Integer i = 0; i< 5; i++ ) {

            OpportunityLineItem item = new OpportunityLineItem();
            item.OpportunityId = o.Id;
            item.Quantity = 100;
            item.UnitPrice = entry.UnitPrice;

            item.Description = 'Test';
            item.PricebookEntryId = entry.Id;
            lst.add(item);

        }

        insert lst;
        return o; */
        return null;

    }

}