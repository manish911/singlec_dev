/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Unit tests for testing ExtendTransactionPdfGenerator apex class
 */
public class TestExtendTransactionPdfGen {
    static testMethod void Test_ExtendTransactionPDFGenerator(){

        //create an Account
        Account accObj = DataGenerator.accountInsert();

        List<Transaction__c> txObjs = DataGenerator.transactionInsert(accObj);

        ApexPages.currentPage().getParameters().put('accId',accObj.Id);
        //insert lineitems
        DataGenerator.lineItemInsert(accObj,txObjs);
        //insert txIds in temp object
        String key = DataGenerator.tempObjectInsert(accObj,txObjs);
        ApexPages.currentPage().getParameters().put('key',key);
        ApexPages.currentPage().getParameters().put('showDetails','true');
        ApexPages.currentPage().getParameters().put('listview','Default');
        ApexPages.currentPage().getParameters().put('txId',txObjs.get(0).Id);
        // initilize the TransactionPDFGenerator class
        Test.startTest();
        
        List<List<List<Object>>> listObj = new List<List<List<Object>>>();
        ExtendedTransactionPdfGenerator transObj = new ExtendedTransactionPdfGenerator();
        
        List<Transaction__c> txObjList = transObj.getExtListOfTxs();
        System.assert(txObjList.size() != null);
        List<ExtendedTransactionPdfGenerator.TxWithLineItem > txLineObj = transObj.getListOfTxWithLineItem();
        System.assertEquals(txLineObj.size(),txObjList.size());
        listObj = transObj.getSummaryTxs();      
        System.assert(listObj.size() != null);
        
        ExtendedTransactionPdfGenerator.TxWithLineItem txwith  = new ExtendedTransactionPdfGenerator.TxWithLineItem();
        txwith.calculations();
        Test.stopTest();
    }

}