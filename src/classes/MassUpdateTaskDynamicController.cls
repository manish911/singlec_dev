/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
   *Usage   :   The class is used to update fields of Task. This fields have to be retrieved dynamically using fieldset.
 */
public Class MassUpdateTaskDynamicController{
    
    public Task tskList{get;set;}
    public Boolean isDisabledUpdateBtn{get;set;}
    public String PARAMNAME {get;set;}
    Map<String,String> statusMap = new Map<String,String>();
   
    public MassUpdateTaskDynamicController(){
        isDisabledUpdateBtn = false;
        PARAMNAME =   ApexPages.currentPage().getParameters().get('location');
        tskList= new Task();
    }
    
    public void massUpdate() {
    
         String tempObjKey = ApexPages.currentPage().getParameters().get('key');
         
         if(tempObjKey != null && !tempObjKey.trim().equals('')) {
             Temp_Object_Holder__c idsObj = [ Select value__c from Temp_Object_Holder__c where Key__c = :tempObjKey.trim() LIMIT 1];
             
             
             if(idsObj != null && idsObj.value__c != null && !idsObj.value__c.equals('')) {
             
                 try {
                 
                     List<Schema.FieldSetMember> fieldSet = SObjectType.Task.FieldSets.Task_Update_FieldSet.getFields();
                     List<String> ids = idsObj.value__c.trim().split(',');
                     String selectQry = 'Select Id,';
                     for(Schema.FieldSetMember field : fieldSet) {
                         selectQry += String.valueOf(field.getFieldPath()) + ',';
                     }
                     selectQry = selectQry.subString(0,selectQry.length() - 1);
                     selectQry += ' From Task Where Id in (' ;
                     for(String id : ids) {
                         selectQry += '\'' + id + '\',';
                     }
                     selectQry = selectQry.subString(0,selectQry.length() - 1);
                     selectQry += ')' ;
                     List<Task> tsk = Database.query(selectQry);
                     system.debug('--------qry---------'+selectQry);
                     
                     for(Schema.FieldSetMember field : fieldSet) {
                     
                         //to check value is enter or not
                         System.debug('===field===='+field);
                         System.debug('===field.getFieldPath===='+field.getFieldPath());
                         system.debug('---------tskList.get(field.getFieldPath())-----'+tskList.get(field.getFieldPath()));
                         
                             
                             system.debug('--------------------tsk----------------'+tsk.size());
                             for(Task t : tsk) {
                                       if(tskList.get(field.getFieldPath()) != null && !String.valueOf(tskList.get(field.getFieldPath())).trim().equals('')) {
                                           if(field.getFieldPath().equalsIgnoreCase('Notes__c')) {
                                        //field.getFieldPath())
                                        //String noteHeader = Datetime.now().format('dd-MMM-yyyy') + ' by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                                        
                                            String temp1 = DateTime.now().format('MMM');
                                            String temp2 = String.valueOf(DateTime.now().day());
                                            String temp3 = String.valueOf(DateTime.now().year());
                                            
                                            if(temp2.length() == 1 ){
                                            temp2 = '0'+ temp2;
                                            }
                                            
                                            String temp4 = temp2 + '-' + temp1 + '-' + temp3;
                                            system.debug('test---'+ t.get(field.getFieldPath()));
                                            if(t.get(field.getFieldPath()) == null) {
                                                
                                                system.debug('----in---if');
                                                //t.Note_Date__c = Date.Today();
                                                string nt =  temp4 +' by '+UserInfo.getName()+' - '+ tskList.get(field.getFieldPath()) + '\n';
                                                t.put(field.getFieldPath(),nt); 
                                                system.debug('Called--');
                                                
                                            } else {
                                                //t.Note_Date__c = Date.Today();
                                                t.put(field.getFieldPath(), temp4 + ' by '+UserInfo.getName()+' - '+ tskList.get(field.getFieldPath()) + '\n'+'\n' + string.valueof(t.get(field.getFieldPath())));
                                                system.debug('Called1---');
                                            }   
                                       
                                       } 
                                   
                                    
                                   //=== Changes taken by Nirav Tejani on 5-9-14 for Append the comments ========
                                           else if (field.getFieldPath().equalsignoreCase('Description') && t.get(field.getFieldPath()) != null){
                                               string comments = t.get(field.getFieldPath())+'\n'+tskList.get(field.getFieldPath());
                                               t.put(field.getFieldPath(), comments);        
                                           }else{
                                               t.put(field.getFieldPath(), tskList.get(field.getFieldPath()));
                                           }
                                           System.debug('===t==='+t);
                                           System.debug('===tskList==='+tskList);
                                   //==========================================================================
                                        
                                 } // Outer If for TransList 
                                 
                             }// end of for loop
                          }
                    
                     if(tsk.size() > 0 ) {
                         update tsk;
                         isDisabledUpdateBtn = true;
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Selected Task updated successfully'));
                     } else {
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Update of selected Task failed.Please try again'));
                     }
                    
                 } catch (Exception e) {
                     System.debug('Exception : ' + e);
                 }
             }
                          
         }
         
         
    }
    
}