/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for PortalContactCSRController
 */
@isTest
private with sharing class TestPortalContactCSRTasksController {

    static testMethod void testPortalContactCSRTasksController()
    {

        Account accWithuser;
        Dispute__c dispObj;
        Contact conWithUser;

        User adminUser = DevUtilityController.getSystemAdministrator();
        
        if(adminUser != null)
        {
            System.runAs(adminUser)
            {
                //Create a new instance of Account Object with test values
                accWithuser = DataGenerator.accountInsert();
                

                //Create a new instance of the Contact with test values
                conWithUser = DataGenerator.contactInsert(accWithuser);

                List<Dispute__c> tempDispList = new List<Dispute__c>();
                tempDispList = DataGenerator.disputeInsert(accWithuser, null);

                dispObj = tempDispList[0];

            }
        }

        //List for getting the profile
        List<Profile> profile =
        [select id from profile where name like 'Akritiv Customer Portal User' LIMIT 1];

        //Create a new instance of the User with test values
        User portalUser;

        if(profile.size() > 0)
        {

            portalUser = new User(alias = 'standt', email = 'test@9876test.com',
                                  emailencodingkey = 'UTF-8', lastname = 'TestLasName', languagelocalekey = 'en_US',
                                  localesidkey = 'en_US', profileid = profile.get(0).id, contactId = conWithUser.id,
                                  timezonesidkey = 'America/Los_Angeles', username = Math.random() + '@test.com');
            insert portalUser;

            System.assertNotEquals(portalUser, null);

            System.RunAs(portalUser)
            {
                PortalContactCSRController controller = new PortalContactCSRController();
                controller.redirectToEmailPage();
                //controller.cancel() ;
            }

            ApexPages.StandardController apexObj = new ApexPages.StandardController (accWithuser);
            PortalContactCSRTasksController taskController = new PortalContactCSRTasksController(apexObj);
            taskController.getTaskList();

        }

    }

}