/*
 * Copyright (c) 2013-2014 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage       :   #This class is used to send email using Mandrill Email Service.
 * Date Created:   #30 July 2013
*/

public with sharing class MandrillEmailService{
    //Contains From Email Address of email
    public String strFromEmail;
    //Contains From Name
    public String strFromName;
    //Contains List of To Email Addressess as a replacement for setToaddress of salesforce
    public String[] strToEmails;
    //Contains List of Actual To Email Addressess
    public String[] strActualTo;
    //Contains List of To Email Addressess as a replacement for setTargetObjectID of salesforce
    public String strTargetToID; 
    //Contains List of CC Email Addressess    
    public String[] strCCEmails;    
    //Contains List of BCC Email Addressess        
    public String[] strBCCEmails;
    //Reply to EmailID
    public String strReplyTo;
    //Contains Email Subject
    public String strSubject;
    //Contains Email HTML Body    
    public String strHtmlBody;    
    //Contains Email Text Body    
    public String strTextBody;
    //Contains List of Attachments        
    public List<clsMandrillAttachments> mailAttachment;
   //--** public list<attachment> listMailAttachment=null;
   //--** public String[] listAttachIds = null;
    
    public string strRequest;
    
    //for status code & status in response
    public string stcode;
    public string ststring;
    public boolean isSuccess;
    //Class to Store Attachments as List
    public class clsMandrillAttachments{
        public string strName {get;set;}
        public string strType {get;set;}
        public blob blbContent {get;set;}
        public integer attLength; 
    }
    
    //Method to Send Email
    public boolean SendEmail(){
        //decide To address from "setTargetObjectID" or "setToaddress" of salesforce
        strActualTo=new list<string>();
        if(strTargetToID!=null && strTargetToID!='')
        {  list<Contact> conlist=[select email from contact where id=:strTargetToID];
           strActualTo.add(conlist.get(0).email); 
        }
        if(strToEmails!=null && strToEmails.size()>0)
        {  for(string s:strToEmails)
             strActualTo.add(s);
         }
                    
       // String strRequest;
        
        //----decrypting api key from custom setting---start------------------------------------------------------------------------------------
            //--for reference,the below commented code is used to produce encrypted data of api key--start--
              //retrieve crypto key--start---
            /* 
              String StrEncodedCryptoKey ='rU7WA2ENzuZ9Lhcwj57HQw==';//secure cryptokey-better to remove from custom setting as algorithm possibilities are just 3
              blob BlobCryptoKey = EncodingUtil.base64Decode(StrEncodedCryptoKey); 
              //retrieve crypto key--end---
              String strInput='pbgepgr-JFDIcj2ATaserw';
              Blob BlobInput = Blob.valueOf(strInput);
              Blob BlobEncryptedData = Crypto.encryptWithManagedIV('AES128', BlobCryptoKey, BlobInput);
              String strEncodedData = EncodingUtil.base64Encode(BlobEncryptedData); 
            */               
            //--for reference,--to produce encoded data of api key--end---------------------------------------------------------------------------
        //retrieve crypto key--start---
        String StrEncodedCryptoKey ='rU7WA2ENzuZ9Lhcwj57HQw==';//secure cryptokey-better to remove from custom setting as algorithm possibilities are just 3
        blob BlobCryptoKey = EncodingUtil.base64Decode(StrEncodedCryptoKey); 
        
        //retrieve crypto key--end---
        
        SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
        String strInputtobedecoded=seo.Mandrill_API_key__c;
        Blob BlobInputtobedecoded=null;
        if(strInputtobedecoded!=null && strInputtobedecoded!='')
          BlobInputtobedecoded=EncodingUtil.base64Decode(strInputtobedecoded);
        else
          { system.debug('---Mandrill_API_key__c empty----');
            return null;
          } 
        
        Blob BlobDecryptedData = Crypto.decryptwithManagedIV('AES128',BlobCryptoKey,BlobInputtobedecoded);
            
        String strDecryptedData='';
        if(BlobDecryptedData!=null)
          strDecryptedData=BlobDecryptedData.toString();
          
        system.debug('---strDecryptedData:'+strDecryptedData);   
        //----decrypting api key from custom setting---end--
        
        String strAPIKey=strDecryptedData; 
        StrEncodedCryptoKey='';
        strInputtobedecoded='';
        strDecryptedData='';
        BlobCryptoKey=null;
        BlobInputtobedecoded=null;
        BlobDecryptedData=null;
        //ConfigUtilController.getEmailAPI(); 
        
      
      //-----------------*****************************
         if(String.IsBlank(strFromEmail) && strFromEmail == null)
            {strFromEmail = UserInfo.getUserEmail(); 
            //removing + sign from "from email address"
             // strFromEmail=strFromEmail.replace('+','%2B');        
              strFromEmail=strFromEmail.replaceAll('\\+','');
              system.debug('>>>>strfromemail::'+strFromEmail);
            }
       
                 
            if(String.IsBlank(strFromName) && strFromName == null)
            {strFromName=UserInfo.getName();
             system.debug('>>>>strfromemail::'+strFromName); 
            }   
         
       //  strFromEmail='joe.blake@vinayakinfosys.in';
       //  strFromName='Joe Blake';
       //------------------*********************************  
            
            
        strRequest = 'key='+ strAPIKey + '&message[from_email]=' + strFromEmail + '&message[subject]=' + strSubject; //&message[preserve_recipients]=true
        
        if(String.IsNotBlank(strFromName))
            strRequest += '&message[from_name]=' + strFromName;        
        
        if(String.IsNotBlank(strReplyTo))
            strRequest += '&message[headers][Reply-To]=' + strReplyTo;
        
        
        if(String.IsNotBlank(strHtmlBody))
         { strHtmlBody=strHtmlBody.replace('&','%26');
           strRequest += '&message[html]=' + strHtmlBody;
          }   
        if(String.IsNotBlank(strTextBody)){     
            strRequest += '&message[text]=' + strTextBody;
        }   
        Integer i=0;
        if(strActualTo != null && strActualTo.size() > 0){        
            for(String s : strActualTo){
                strRequest = strRequest +  '&message[to]['+ i +'][email]=' + s;
                i=i+1;
            }
        }
                
        i=0;
        
        if(strCCEmails != null && strCCEmails.size() > 0){
            for(String strCCEmail : strCCEmails){
                strRequest = strRequest +  '&message[headers][cc]['+ i +']=' + strCCEmail;
                i=i+1;
            }
        }
        if(strBCCEmails!= null && strBCCEmails.size() > 0){
          for(String strBCCEmail : strBCCEmails){
            strRequest = strRequest +  '&message[bcc_address]=' + strBCCEmail;
          }
        }
        strAPIKey='';
        strFromName='';
        strReplyTo='';
        strHtmlBody='';
        strTextBody='';
        strToEmails=null;
        strActualTo=null;
        strCCEmails=null;
        strBCCEmails=null;
                                                   
        if(mailAttachment != null && mailAttachment.size()>0){
            for(clsMandrillAttachments objAttachment: mailAttachment){
                system.debug('I('+i+')::'+'attachmentLength::'+objAttachment.attLength);
                if(objAttachment.attLength==null)
                { objAttachment.attLength=0;
                }
                if(objAttachment.attLength>2100000)
                { 
                  return false;
                }
                System.debug(' @@@ heap limit 1('+i+') : ' + Limits.getHeapSize() );
                strRequest += '&message[attachments]['+ i +'][name]=' + objAttachment.strName;
                if(objAttachment.strType==null || objAttachment.strType=='')
                {  objAttachment.strType='application/octet-stream';
                }
                strRequest+= '&message[attachments]['+ i +'][type]=' + objAttachment.strType;
                System.debug(' @@@ heap limit 2('+i+') : ' + Limits.getHeapSize() );
                Double imem=Limits.getHeapSize()+objAttachment.attLength;
                system.debug(' @@@ attach body + currentheap :' +imem );
                if(imem>Limits.getLimitHeapSize() )
                { return false;
                }
                String body64=EncodingUtil.base64Encode(objAttachment.blbContent);
                //objAttachment.blbContent=null;
                system.debug('@@@ curr-b4 nulling- heap size 2.3('+i+') : '+Limits.getHeapSize()); 
                objAttachment=null;
                system.debug('@@@ curr-aftre nulling- heap size 2.3('+i+') : '+Limits.getHeapSize()); 
                
                String body64url=EncodingUtil.urlEncode(body64, 'UTF-8');
                body64=null; 
                
                system.debug('ForI['+i+']length of body64url::'+body64url.length());
                imem=Limits.getHeapSize()+body64url.length();
                system.debug('@@@ heap limit 2.4('+i+') : ' +Limits.getHeapSize()+'AND available heap::'+Limits.getLimitHeapSize());
                system.debug('@@@ total heap limit 2.6('+i+') : ' +imem+' & current total::'+(Limits.getHeapSize()+body64url.length()) );
                if(imem>Limits.getLimitHeapSize() )
                { return false;
                }
                strRequest+='&message[attachments]['+ i +'][content]=' + body64url;
                body64url=null;
                               
                //-- +'&message[attachments]['+ i +'][base64 ]=true';  
                //objAttachment=null;           
                imem=Limits.getHeapSize();            
                System.debug(' @@@ heap limit 3('+i+') : ' + imem );                                  
                i=i+1;                        
            }
            mailAttachment=null;
        }
       
        
       //------------------------
       
        System.debug('---1length of strrequest in kB---'+strRequest.length());
        System.debug('MandrillEmailService.SendEmail.strRequest>>>' + strRequest);
        
      try{  
            HttpRequest objHttpReq = new HttpRequest();
            HttpResponse objHttpResp = new HttpResponse();
            Http objHttp = new Http();
            
            objHttpReq.setHeader('Content-Type','application/x-www-form-urlencoded');
            String endpoint = 'https://mandrillapp.com/api/1.0/messages/send.json'; 
            objHttpReq.setEndpoint(endpoint);
            objHttpReq.setMethod('POST');
            //objHttpReq.setMethod('GET');
            if(strRequest.length()<3000000)
            {objHttpReq.setBody(strRequest);
             objHttpResp = objHttp.send(objHttpReq);
             stcode=String.valueOf(objHttpResp.getStatusCode());
             ststring=objHttpResp.getStatus();
             System.debug('MandrillEmailService.SendEmail.objHttpResp>>>' + objHttpResp);
             isSuccess=true;
            }else
            {isSuccess=false;
            } 
         }
       catch(Exception e){
            System.debug('Exception while sending mail from mandrill ');
            System.debug(':::::::: Exception ::::::::::::::'+e);
            System.debug('Line number: ' + e.getLineNumber());
            System.debug('Stack trace: ' + e.getStackTraceString());
       }      
       System.debug('---2length of strrequest in kB---'+(strRequest.length()/1000));
       return isSuccess;
    }           
 }