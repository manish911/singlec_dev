/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Schedulabel job to delete all the junk data upto last day.
 */

global class DeleteTempDataScheduledJob implements Schedulable {
	// method is used to execute Schedulable Context 
	// return type : void
	// parameters : sc - SchedulableContext
	global void execute(SchedulableContext sc) {
		Date curr = date.today().addDays(-1);
		Datetime dT = Datetime.newInstance(curr.year(), curr.month(), curr.day());
		delete [select id from Temp_Object_Holder__c where CreatedDate <: dT];
	}
}