/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class will give you the ability to mass update of Transactions.
 */

public Class MassUpdateTransactionController{
    Map<String,String> customFieldDataTypeMap;
    Map<String,String> customFieldLabelMap;
    //public list<TransactionWrapper> trxWrapperList{get;set;}
    public list<String> SelectedFields {get;set;}
    public Map<String,List<SelectOption>> picklistMap{get;set;}
    public Transaction__c transList{get;set;}
    String fields;
    public Boolean isDisabledUpdateBtn{get;set;}
    public String PARAMNAME {get;set;}
    public Account controllerAccount {get; set; }
    public String currencyIsoCode {get; set; }
    
    public MassUpdateTransactionController(){
    
        //trxWrapperList = new list<TransactionWrapper>();
        isDisabledUpdateBtn = false;
        SelectedFields = new list<String>();
        PARAMNAME =   ApexPages.currentPage().getParameters().get('location');
        customFieldDataTypeMap = new Map<String,String>();
        customFieldLabelMap = new Map<String,String>();
        picklistMap = new Map<String,List<SelectOption>>();
        transList= new Transaction__c();
        system.debug('----transList----' + transList);
        //decimal totalBalance;
        }
    
    //*********** #AFL-38 : Mass Update enhancement to include balance total of items selected**********
    //*********** By Twinkle Shah   
    /*public decimal getTotalBalance(){  
        String tempObjKey = ApexPages.currentPage().getParameters().get('key');
        decimal totalBalance = 0.0;
        

        
        if(tempObjKey != null && !tempObjKey.trim().equals('')) {
             Temp_Object_Holder__c idsObj = [ Select value__c from Temp_Object_Holder__c where Key__c = :tempObjKey.trim() LIMIT 1];
          
             if(idsObj != null && idsObj.value__c != null && !idsObj.value__c.equals('')) {
                    List<String> selectedIds = idsObj.value__c.trim().split(',');
                    List<Transaction__c> selectedTranlist = new List<Transaction__c>();
                    selectedTranlist = [select Balance__c from Transaction__c where Id in : selectedIds];
                    system.debug('----selectedTranlist---' + selectedTranlist);
                    
                        if(selectedTranlist.size()>0 && selectedTranlist != null ){
                        for(Transaction__c Tx : selectedTranlist) {
                            
                                
                                if(Tx.Balance__c !=null && Tx.Balance__c !=0){
                                    totalBalance = totalBalance + Tx.Balance__c;
                                    system.debug('---totalBalance----' + totalBalance );
                                }
                           
                        }
                    }
                    
             }
         }
         return totalBalance;
    }*/
    
    
    
    public void massUpdate() {
        String tempObjKey = ApexPages.currentPage().getParameters().get('key');
          
         if(tempObjKey != null && !tempObjKey.trim().equals('')) {
             Temp_Object_Holder__c idsObj = [ Select value__c from Temp_Object_Holder__c where Key__c = :tempObjKey.trim() LIMIT 1];
             
             
             if(idsObj != null && idsObj.value__c != null && !idsObj.value__c.equals('')) {
             
                 try {
                 
                     List<Schema.FieldSetMember> fieldSet = SObjectType.Transaction__c.FieldSets.Mass_Update_Transaction_fields.getFields();
                     List<String> ids = idsObj.value__c.trim().split(',');
                     String selectQry = 'Select Id' + ',';
                     for(Schema.FieldSetMember field : fieldSet) {
                         selectQry += String.valueOf(field.getFieldPath()) + ',';
                     }
                     selectQry = selectQry.subString(0,selectQry.length() - 1);
                     selectQry += ' From Transaction__c Where Id in (' ;
                     for(String id : ids) {
                         selectQry += '\'' + id + '\',';
                     }
                     selectQry = selectQry.subString(0,selectQry.length() - 1);
                     selectQry += ')' ;
                     List<Transaction__c> trxn = Database.query(selectQry);
                     
                    
                     //Datetime myDT = Datetime.now();
                     //String noteHeader = Datetime.now().format('dd-MMM-yyyy') + ' by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();

                     for(Schema.FieldSetMember field : fieldSet) {
                     
                         //to check value is enter or not
                         system.debug('---------transList.get(field.getFieldPath())-----'+transList.get(field.getFieldPath()));
                         
                             
                             system.debug('--------------------trxn----------------'+trxn.size());
                             for(Transaction__c t : trxn) {
                                 if(transList.get(field.getFieldPath()) != null && !String.valueOf(transList.get(field.getFieldPath())).trim().equals('')) {
                                     if(field.getFieldPath().equalsIgnoreCase('Notes__c')) {
                                        //field.getFieldPath())
                                        //String noteHeader = Datetime.now().format('dd-MMM-yyyy') + ' by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                                        
                                        String temp1 = DateTime.now().format('MMM');
                                        String temp2 = String.valueOf(DateTime.now().day());
                                        String temp3 = String.valueOf(DateTime.now().year());
                                        
                                        if(temp2.length() == 1 ){
                                        temp2 = '0'+ temp2;
                                        }
                                        
                                        String temp4 = temp2 + '-' + temp1 + '-' + temp3;
                                        if(t.get(field.getFieldPath()) == null) {
                                            
                                            system.debug('----in---if');
                                            t.Note_Date__c = Date.Today();
                                            t.put(field.getFieldPath(), temp4 +' by '+UserInfo.getName()+' - '+ transList.get(field.getFieldPath()) + '\n'); 
                                            
                                            
                                        } else {
                                            t.Note_Date__c = Date.Today();
                                            t.put(field.getFieldPath(), temp4 + ' by '+UserInfo.getName()+' - '+ transList.get(field.getFieldPath()) + '\n'+'\n' + string.valueof(t.get(field.getFieldPath())));
                                        }
                                        
                                    } else {
                                        t.put(field.getFieldPath(), transList.get(field.getFieldPath()));
                                        
                                    }
                                
                             } // Outer If for TransList
                             If(transList.get('Reason_Code__c') != null && (transList.get('Sub_Reason_Code__c') == null)){
                                 t.put('Sub_Reason_Code__c' , ''); 
                                 system.debug('Called---');
                             }
                             If(transList.get('Reason_Code__c') == null && (transList.get('Sub_Reason_Code__c') == null)){
                                 t.put('Reason_Code__c' , t.get('Reason_Code__c'));
                                 t.put('Sub_Reason_Code__c' , t.get('Sub_Reason_Code__c'));
                                 system.debug('Called---');
                             }
                             
                         } // For Loop on Transaction
                       
                     } // For Loop On Field Set
                     if(trxn.size() > 0 ) {
                         update trxn;
                         isDisabledUpdateBtn = true;
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,label.Label_Selected_Items_Updated_Successfully));
                     } else {
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,label.Label_Update_Of_Selected_Items_Failed_Please_Try_Again));
                     }
                    
                } catch (Exception e) {
                     System.debug('Exception : ' + e);
                 }
             }
                          
         }
         
         
    }
    
}