@isTest
public class TestGenerateLineItemsDisputes{
    
    public static Map<id,Id> tempaccount = new Map<id,Id>();
    public static Map<Id,Double> AmtMap = new Map<Id,Double>();
    public static Set<Id> accountIdList = new set<ID>();
    public static String tmp='00XP0000000Dq28';
    public static String lineIDs = '';
    public static Map<ID, Double> parenttempaccount = new Map<ID, Double>();
    public static List<String> lineItemId = new List<String>();
    public static List<Temp_Object_Holder__c> lineItemConfig = new  List<Temp_Object_Holder__c>();
    public static Temp_Object_Holder__c tmpLine;
    
    
    Static testMethod void runtest(){
        Account acc = new Account();
        Account pacc = new Account();
        Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();
        triggerconf.Enable_Daily_Notes__c = true;
        UPSERT triggerconf;

        //User prefrences detail
        User_Preference__c up= new User_Preference__c();
        up.User__c =Userinfo.getUserId();
        INSERT up;

        User usr = [Select id from User where Id = :UserInfo.getUserId()];
 
        System.RunAs(usr){
            // create a test account and parent account
            acc =  DataGenerator.accountInsert();
            pacc = DataGenerator.accountInsert();
            acc.ParentID = pacc.id;
            update acc;
        
        //create dispute
        Dispute__c dpd= new  Dispute__c();
        dpd.Owner__c=Userinfo.getUserId();
        dpd.Account__c=acc.id;
        insert dpd;
        
        tempaccount.put(acc.id,pacc.id);
        parenttempaccount.put(acc.id,5);
        SysConfig_Singlec__c con = insertSinglec('test', 'test2', 'test3','test@gmail.com');
        
        Contact cont = insertContact('TEST_CONTACT', acc.Id, 'test@akritiv.com');
        
        Transaction__c tx = new  Transaction__c();
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(acc);
        tx = txList.get(0);
        accountIdList.add(tx.Account__c);
        
        System.debug('### Trx List SIZE : '+txList.size());
        
        Task tsk = new Task();
        tsk.Subject = 'Akritiv test';
        tsk.Priority = 'Normal';
        tsk.Status = 'Normal';
        tsk.OwnerId = UserInfo.getUserId();
        Insert tsk;
        
       //Line_Item__c item = new Line_Item__c();
        List<Line_Item__c> listLineItems = DataGenerator.lineItemInsert(acc,txList);
        
        FOR(Line_Item__c i : listLineItems){
            lineIDs += ',' + i.id;
        }
        
        lineIDs = lineIDs.substring(1, lineIDs.length());
        
        Dispute__c dd= new Dispute__c();
        
        Temp_Object_Holder__c config = new  Temp_Object_Holder__c();
        config.key__c = UserInfo.getUserId()+ DateTime.now();
        config.value__c = lineIDs;
        insert config;
        
        // instanciate the contactCustomerController
        Attachment a1 = new Attachment();        
        a1.name='my att01';  
        a1.ParentId = acc.id;
        a1.body=blob.valueOf('test');
        Insert a1;
        
  
        // set the current page parameters
       // ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('taskId',tsk.Id);
        ApexPages.currentPage().getParameters().put('txid',tx.Id);
        ApexPages.currentPage().getParameters().put('txIds',tx.Id);
        ApexPages.currentPage().getParameters().put('dispamt',''+tx.Balance__c);
        ApexPages.currentPage().getParameters().put('ordertype','ASC');
        ApexPages.currentPage().getParameters().put('orderby','Name');
        ApexPages.currentPage().getParameters().put('lineItemIds',lineIDs);
        ApexPages.currentPage().getParameters().put('tempKey', config.key__c);
        
        Test.startTest();
        GenerateLineItemsDisputes controller = new GenerateLineItemsDisputes();   
        //GenLitem.saveDisputes();
        controller.isFollowUp = true;
        System.assertEquals(controller.isFollowUp, true);
        controller.saveCall();
        controller.attachmentIds = a1.id;
        controller.emailSubject = 'TEST Subject';
        controller.emailTemplateBody = 'TEST BODY';
        controller.emailTargetToContactId = cont.Id;
        controller.addtoAddresses = 'test@test.com';        
        controller.ccAddresses = 'abc@gmail.com';
        controller.bccAddresses ='abc@gmail.com';
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();
        controller.emailTargetToContactId = null;
        controller.execute(); 
        GenerateLineItemsDisputes.DynamicDisputeField obj = new GenerateLineItemsDisputes.DynamicDisputeField('','');       
        Test.stopTest();
        }
}
/*
    Static testMethod void testMethod1(){
            // create a test account
        Account acc = accountInsert();
        
        SysConfig_Singlec__c sysConfObj = SysConfig_Singlec__c.getOrgDefaults();
        sysConfObj.Capture_Notes_On_Transaction__c= true;
        sysConfObj.Billing_Statement_PDF_page__c = 'SYSTEM_BillingStatementPDF';
        upsert sysConfObj;
        
        SysConfig_Singlec__c con = insertSinglec('test', 'test2', 'test3','test@gmail.com');
        
        Contact cont = insertContact('TEST_CONTACT', acc.Id, 'test@akritiv.com');
        
        Transaction__c tx = new  Transaction__c();
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = transactionInsert(acc,2);
        tx = txList.get(0);
        

        Task tsk = new Task();
        tsk.Subject = 'Akritiv test';
        tsk.Priority = 'Normal';
        tsk.Status = 'Normal';
        tsk.OwnerId = UserInfo.getUserId();
        Insert tsk;
        
 //       Line_Item__c item = new Line_Item__c();
        List<Line_Item__c> listLineItems = DataGenerator.lineItemInsert(acc,txList);
        
        FOR(Line_Item__c i : listLineItems){
            lineIDs += ',' + i.id;
        }
        
        lineIDs = lineIDs.substring(1, lineIDs.length());
        
        //Dispute__c dd= new Dispute__c();
        
        Temp_Object_Holder__c config = new  Temp_Object_Holder__c();
        config.key__c = UserInfo.getUserId()+ DateTime.now();
        config.value__c = lineIDs;
        insert config;
        
        // instanciate the contactCustomerController
        Attachment a1 = new Attachment();        
        a1.name='my att01';  
        a1.ParentId = acc.id;
        a1.body=blob.valueOf('test');
        Insert a1;

        // set the current page parameters
        // ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('taskId',tsk.Id);
        ApexPages.currentPage().getParameters().put('txid',tx.Id);
        ApexPages.currentPage().getParameters().put('txIds',tx.Id);
        ApexPages.currentPage().getParameters().put('dispamt',''+tx.Balance__c);
        ApexPages.currentPage().getParameters().put('ordertype','ASC');
        ApexPages.currentPage().getParameters().put('orderby','Name');
        ApexPages.currentPage().getParameters().put('lineItemIds',lineIDs);
        ApexPages.currentPage().getParameters().put('tempKey', config.key__c);
        
        Test.startTest();
        
        
        
        GenerateLineItemsDisputes controller = new GenerateLineItemsDisputes();   
        //GenLitem.saveDisputes();
        controller.isFollowUp = true;
        System.assertEquals(controller.isFollowUp, true);
        controller.saveCall();
        controller.attachmentIds = a1.id;
        controller.emailSubject = 'TEST Subject';
        controller.emailTemplateBody = 'TEST BODY';
        controller.emailTargetToContactId = cont.Id;
        controller.addtoAddresses = 'test@test.com';        
        controller.ccAddresses = null;
        controller.bccAddresses =null;
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();
        controller.contactMethod = 'Phone';
        controller.emailTargetToContactId = cont.id;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.execute();
        controller.sendEmail();
        
        
        controller.followUpDays = null;
        controller.isFollowUp = true;
        controller.execute();
        
        controller.contactNoteTitle = null;
        controller.execute();
        
        controller.contactMethod = 'Print';
        controller.emailTargetToContactId = null;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.emailTemplateBody='Test Body';
        controller.execute();
        
        controller.contactMethod = 'Fax';
        controller.followUpDays = 7;
        controller.NoteFollowupTitle = 'Test';
        controller.isFollowUp = true;
        controller.execute();
        
        controller.contactMethod = 'DNC';
        controller.emailTargetToContactId = null;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.sourceTaskIdForPage=tsk.id;
        controller.execute();
        
        Test.stopTest();
    }

      Static testMethod void testMethod2(){
        Account acc = DataGenerator.accountInsert();
        
        SysConfig_Singlec__c sysConfObj = SysConfig_Singlec__c.getOrgDefaults();
        sysConfObj.Use_Native_Note__c = true;
        sysConfObj.Billing_Statement_PDF_page__c = 'SYSTEM_BillingStatementPDF';
        upsert sysConfObj;
        
        SysConfig_Singlec__c con = insertSinglec('test', 'test2', 'test3','test@gmail.com');
        Contact cont = insertContact('TEST_CONTACT', acc.Id, 'test@akritiv.com');
        
        Transaction__c tx = new  Transaction__c();
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(acc);
        tx = txList.get(0);
        

        Task tsk = new Task();
        tsk.Subject = 'Akritiv test';
        tsk.Priority = 'Normal';
        tsk.Status = 'Normal';
        tsk.OwnerId = UserInfo.getUserId();
        Insert tsk;
        
        List<Line_Item__c> listLineItems = DataGenerator.lineItemInsert(acc,txList);
        
        FOR(Line_Item__c i : listLineItems){
            lineIDs += ',' + i.id;
        }
        
        lineIDs = lineIDs.substring(1, lineIDs.length());

        Temp_Object_Holder__c config = new  Temp_Object_Holder__c();
        config.key__c = UserInfo.getUserId()+ DateTime.now();
        config.value__c = lineIDs;
        insert config;
        
        // instanciate the contactCustomerController
        Attachment a1 = new Attachment();        
        a1.name='my att01';  
        a1.ParentId = acc.id;
        a1.body=blob.valueOf('test');
        Insert a1;

        // set the current page parameters
        // ApexPages.currentPage().getParameters().put('accId',acc.Id);
        ApexPages.currentPage().getParameters().put('taskId',tsk.Id);
        ApexPages.currentPage().getParameters().put('txid',tx.Id);
        ApexPages.currentPage().getParameters().put('txIds',tx.Id);
        ApexPages.currentPage().getParameters().put('dispamt',''+tx.Balance__c);
        ApexPages.currentPage().getParameters().put('ordertype','ASC');
        ApexPages.currentPage().getParameters().put('orderby','Name');
        ApexPages.currentPage().getParameters().put('lineItemIds',lineIDs);
        ApexPages.currentPage().getParameters().put('tempKey', config.key__c);
        
        Test.startTest();
        
        GenerateLineItemsDisputes controller = new GenerateLineItemsDisputes();
        controller.contactMethod = 'Phone';
        controller.emailTargetToContactId = cont.id;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.execute();
        controller.sendEmail();
        
        controller = new GenerateLineItemsDisputes();
        controller.followUpDays = null;
        controller.isFollowUp = true;
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();       
        controller.contactNoteTitle = null;
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();
        controller.contactMethod = 'Print';
        controller.emailTargetToContactId = null;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.emailTemplateBody='{}';
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();
        controller.contactMethod = 'Fax';
        controller.followUpDays = 7;
        controller.NoteFollowupTitle = 'Test';
        controller.isFollowUp = true;
        controller.execute();
        
        controller = new GenerateLineItemsDisputes();
        controller.contactMethod = 'DNC';
        controller.emailTargetToContactId = null;
        controller.followUpDays = 7;
        controller.isFollowUp = true;
        controller.NoteFollowupTitle = 'Test';
        controller.sourceTaskIdForPage =tsk.id;
        controller.execute();
        
        Test.stopTest();
     }
     */
    static SysConfig_Singlec__c insertSinglec(String disLines, String billingPDF, String billing, String s1){
        SysConfig_Singlec__c con = new SysConfig_Singlec__c();
        con.Create_Dispute_Lines_View__c = disLines;
        con.Billing_Statement_PDF_page__c = billingPDF;
        con.Billing_Statement__c = billing;
        con.CC_Email_Service_Id__c=s1;
        insert con;
        return con;
    }
    
    static Contact insertContact(String lastName, Id AccId, String email){
        Contact con = new Contact();
        con.LastName = lastName;
        con.AccountId = AccId;
        con.Email = email;
        insert con;
        return con;
    }
    
    //Methods use to insert Account and transaction for testing purpose
      public static Account accountInsert(){
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Base_Total_AR__c =0;
        insert accObj;
        return accObj;
    }
    
    public static List<Transaction__c> transactionInsert(Account acc, Integer count){
        //create transaction on Account

        Trigger_Configuration__c obj = new Trigger_Configuration__c();
        obj.Manage_Account_Key_Trigger__c = false;
        UPSERT obj;       
        List<Transaction__c> txObjList = new List<Transaction__c>();
        List<Id> idLst = new  List<Id>();
        Account tempAcc = acc;
        for(Integer i=0; i<count; i++){
            Transaction__c txObj = new Transaction__c();
            txObj.Account__c = tempAcc.Id;
            txObj.Amount__c = 12312 + i;
            txObj.Balance__c = 6336 + i;
            txObj.Base_Balance__c = 6336 + i;
            txObj.Name = 'testno' + i;
            txObj.Due_Date__c = Date.today().addMonths(-7);
            txObj.source_system__c = 'Test';         
            txObjList.add(txObj);
        }

        insert txObjList;

        return txObjList;
    }
}