/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class  generates metadata of Transaction object.
 */

global with sharing class TransactionDescribeCall {

    static Map<String,FieldDescibe> objectDetails = new Map<String,FieldDescibe>();
    static Set<String> existingFields;
    static Set<String> existingFieldsLineItem;
    static Set<String> lineItemFields;
    static Set<String> txFields;

    static {

        Map<String, Schema.SObjectField> FieldsMap = Schema.SObjectType.Transaction__c.fields.getMap();
        existingFields = FieldsMap.keySet();

        Map<String, Schema.SObjectField> FieldsMapLineItem = Schema.SObjectType.Line_Item__c.fields.getMap();
        existingFieldsLineItem = FieldsMapLineItem.keySet();

        lineItemFields = new Set<String>();
        txFields = new Set<String>();
        //exclude system fields for line item
        for(String fieldName : existingFieldsLineItem)
        {
            if(fieldName !='CreatedById'
               && fieldName !='CreatedDate' && fieldName !='LastModifiedById'
               && fieldName !='LastModifiedDate' && fieldName != 'SystemModstamp'
               && fieldName !='CurrencyIsoCode' &&  fieldName !='id'
               &&  fieldName !='lastactivitydate' &&  fieldName !='isdeleted'
               ) {

                lineItemFields.add(fieldName);
            }
        }

        //exclude system fields for transaction and name field
        for(String fieldName : existingFields)
        {
            if( fieldName !='Indicator_Icons__c' &&  fieldName !='CreatedById'
                && fieldName !='CreatedDate' && fieldName !='LastModifiedById'
                && fieldName !='LastModifiedDate' && fieldName != 'SystemModstamp'
                &&  fieldName !='id'
                &&  fieldName !='lastactivitydate' &&  fieldName !='isdeleted'
                && (!(fieldName.contains('without_sign')))
                ) {

                txFields.add(fieldName);
            }
        }

        for(String nextField : existingFields ) {

            Schema.SObjectField field = FieldsMap.get(nextField);
            Schema.DescribeFieldResult fieldDesc = field.getDescribe();

            FieldDescibe fieldDescribeObj = new FieldDescibe();
            fieldDescribeObj.fieldName = nextField;
            fieldDescribeObj.fieldLabel = fieldDesc.getLabel();
            fieldDescribeObj.fieldType =  fieldDesc.getType();
            fieldDescribeObj.fieldNamespaceName =  fieldDesc.getName();

            objectDetails.put(nextField,fieldDescribeObj);

        }
    }
    // method is used to get Field data 
    // return type : FieldDescibe
    // parameters : fieldName - String
    global static FieldDescibe getFieldData(String fieldName){                        
        if(objectDetails.get(fieldName.toLowerCase()) !=null) {
            return objectDetails.get(fieldName.toLowerCase());
        }
        else
            return null;
    }
    // method is used to get all fields
    // return type : Set<String>
    global Static Set<String> getAllFields(){
        return txFields;
    }
    // method is used to get all Line Item Fields
    // return type : Set<String>
    global static Set<String> getAllLineItemFields(){

        return lineItemFields;

    }

    global class FieldDescibe {

        global String fieldName {get; set; }
        global String fieldLabel {get; set; }
        global Schema.Displaytype fieldType {get; set; }
        global String fieldNamespaceName {get; set; }

    }

}