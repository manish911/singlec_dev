/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This is test class AccountHierarchyPageController.
 */


@istest(seeAllData = true)
public with sharing class AccountHierarchyPageControllerTest {
// Runs the test method for this class and generates average code coverage 

    static testMethod void testController() {

    //   List<Account> a = [  select Id , ParentId from account where Account_Key__c != null and ParentId!=null limit 1];
          Account acc = [select Id,ParentId from account where parentId != null limit 1];
          List<Account> a = [select Id from account where Id=: acc.parentId limit 1];
          // List<Account> a = [  select  Id from account where Account_Key__c != null  limit 1];
           
        ApexPages.StandardController controller = new ApexPages.StandardController(a.get(0));
        String accountId = a.get(0).Id;

        Test.startTest();
        AccountHierarchyPageController con = new AccountHierarchyPageController(controller);
        System.assertEquals(accountId,a.get(0).Id);
        Test.stopTest();
    }
}