public with sharing class DetailController {
    public Account accId {get; set; }
    public Transaction__c oldesttrxId {get; set; }
    List<Transaction__c> trxs {get; set; }
    List<Note> notes {get; set; }
    List<Note> txnotes {get; set; }
    public Task  taskId {get; set; }   
    String packageHostAddress {get; set; }
    public String key {get;set;}     
    public String location { get; set; }
    public String callresult{ get; set; }
    public String txIdsForURL {get;set;}
    public boolean buttonDisabled {get;set;}
   // public boolean buttonDisabled1 {get;set;}
    
    public String contactCustomer {get;set;}
    Public String customerPromiseToPay {get;set;}
    Public String generateDispute {get;set;}
    Public String accountdetail {get;set;}
    Public String taskkeyvalue {get;set;}
  
    public  List < String > taskIds = new List < String >();
    
    public Integer counter=0; 
    public Integer proccedtask { get; set; }
    public Integer totaltask { get; set; }
    
    public List<String> selectedFields {get ; set; }
    public List<TxLabelListWithAttributes> lstOfLabels {get; set; }
    public List<wrapperTransactionList> wrapList {get; set; }
    List<TxListWithAttributes> tempList = new List<TxListWithAttributes>{};
    public Integer columnToSort {get; set; }
    public Boolean isAsc  {get; set; }
    public Integer secondaryColumnToSort {get; set; }
    public Integer previousSecondColumnToSort {get; set; }
    public String secondaryOrderType {get; set; }
    public Boolean isSecondarySort {get; set; }
    
    public String comments {get;set;}
    public String notetitle {get;set;}
    Public String noteBody {get;set;}
    public String notetitletrx {get;set;}
    Public String noteBodytrx {get;set;}
    
    Public List<Contact> con{get;set;}
    Public List<Task> openTask{get;set;}
    Public List<Task> activityTask{get;set;}
               
        
    public DetailController(){
        
        totaltask = 0;
        packageHostAddress = ApexPages.currentPage().getHeaders().get('host');
        
        location = ApexPages.currentPage().getParameters().get('location');
        buttonDisabled = false;
       // buttonDisabled1 = true;
        
        if(ApexPages.currentPage().getparameters().get('key') != null && ApexPages.currentPage().getparameters().get('key') != ''){
            key =  ApexPages.currentPage().getparameters().get('key');
            key = key.trim();
        }
        
        
        if(key != null){
            Temp_Object_Holder__c temp = [ select Id, Value__c from Temp_Object_Holder__c where key__c = : key];
            String [] taskstr = temp.Value__c.split(',');
            //
        
            for ( String s : taskstr ) {
            
                taskIds.add(s);
            }
        }    
    }
    
     public void prepareAccounts() {
        
        totaltask = taskIds.size();
      
       /* if(totaltask == 1){
         buttonDisabled = true;
        }*/
        proccedtask = counter;
        
        if ( taskIds.size() == counter){
          
             proccedtask = counter-1;
             buttonDisabled = true;
             //buttonDisabled1 = false;
             //
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Label.Label_All_Tasks_Are_Completed'));  
             return;
        }
            
           
            taskId = [ select Id,Description ,CallDisposition,Subject,Status,Priority ,What.type,WhatID,type, Contact_Method__c,Contact_Type__c from Task where Id =:taskIds.get(counter)];  
            
          
                    
           if(taskId.WhatID != null && taskId.What.type =='Account'){
                 
                   String soql = '';
                        Schema.Sobjecttype objType = Account.getSObjectType();
                        Schema.Describesobjectresult sobjRes = objType.getDescribe();
                        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
                        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
                        sortSelectOption.sort();
                        
                        String fieldName;
                        
                       
        
                        for(String a : sortSelectOption) {
                            
                                                     
                            Schema.Sobjectfield field = fieldMap.get(a);
                            fieldName = field.getDescribe().getName();
                            
                            
                            if(fieldName != 'ShippingAddress' && fieldName != 'billingaddress' && fieldName != 'address' &&  fieldName != 'MailingAddress' && fieldName != 'OtherAddress')  
                                soql = fieldName +','+soql;  
                            
                           
                        }
                        Integer queryLength = soql.length();
                        soql = soql.substring(0,queryLength-1);
                        soql = 'select' + ' ' + soql;
                        soql = soql+ ' ' + 'from Account';
                       
                        soql = soql + ' ' + 'Where Id =\''+taskId.WhatID+'\' limit 1';                                                
                   
                accId = Database.query(soql);
                getAccountContacts();
                getOpenActivity();
                getActivityHistory();
              
                
                
                if(accId.Lead_Tx__c != null && accId.High_Past_Due_Tx__c != null ){
                    
                                
                        String query= 'select';
                        Schema.Sobjecttype objType1 = Transaction__c.getSObjectType();
                        Schema.Describesobjectresult sobjRes1 = objType1.getDescribe();
                        Map<String, Schema.Sobjectfield> fieldMap1 = sobjRes1.fields.getMap();
                        List<String> sortSelectOption1 = new List<String>(fieldMap1.keyset());
                        sortSelectOption1.sort();
                        
                        String fieldName1;
                        for(String a : sortSelectOption1) {
                        Schema.Sobjectfield field = fieldMap1.get(a);
                        //
                        fieldName1 = field.getDescribe().getName();
                        //
                        query = query+' '+ fieldName1+',';  
                        }
                        Integer queryLength1 = query.length();
                        query = query.substring(0,queryLength1-1);
                        query = query + ' ' + 'from Transaction__c';
                        query = query + ' ' + 'Where Id = \''+accId.High_Past_Due_Tx__c+'\' limit 1';
                        oldesttrxId = Database.query(query);
                        
                }
           
               taskkeyvalue = gettskkeyval();
               //
               contactCustomer  = 'https://'+packageHostAddress+'/apex/ConsoleContactCustomer?accId='+accId.Id+'&orderby=Name&ordertype=desc';
               customerPromiseToPay = 'https://'+packageHostAddress+'/apex/ConsolePromiseToPayPage?accId='+accId.Id+'&orderby=Name&ordertype=desc';
               generateDispute = 'https://'+packageHostAddress+'/apex/ConsoleGenerateTxDisputes?accId='+accId.Id+'&orderby=Name&ordertype=desc';
               accountdetail  = 'https://'+packageHostAddress+'/apex/AccountDetailPage?Id='+accId.Id;
           } else {
               
               counter++;
               prepareAccounts();
           }
          
           comments = '';
         
           getListContents();
    }
    public Set<Id> getSelectedTransactions() {
        Set<Id> selectedTxs = new Set<Id>();
        if( wrapList !=null &&  wrapList.size() > 0) {
            for(WrapperTransactionList transWrapper : wrapList) {
                if(transWrapper.selected)
                    selectedTxs.add(transWrapper.transId);
            }
        }
        return selectedTxs;
    }
    
    
    public void prepareselected() {
        
        txIdsForURL = gettskkeyval();
        
        //Temp_Object_Holder__c txIdsConfig = new Temp_Object_Holder__c();
        //txIdsConfig.key__c = Userinfo.getUserName()+Datetime.now();
        //txIdsConfig.value__c = gettskkeyval() ;
        //insert txIdsConfig;
                
        //taskkeyvalue = txIdsConfig.key__c  ;
    
    }
    
    public String gettskkeyval() {
               
        Set<Id> txsIds = getSelectedTransactions();
        
        
        String txIdsStr = '';
       
        for(Id txId : txsIds) {
        
        
            if ( txIdsStr == ''){
            
                txIdsStr = txId;
            } else {
            
                txIdsStr = txIdsStr + ',' +txId;
            }
        }
            
      
        
        return txIdsStr;
    }
    
    
    public List<Transaction__c> getTransactions() {
        
        if(accId != null) {
            String acid = accId.Id;
            String notestr =   '(Select Id, Title,Body,CreatedDate,CreatedBy.Name from Notes order by createdDate DESC limit 1)'; 
            String str = 'select name,Indicator_Icons__c ,So_Number__c,Po_Number__c,Amount__c,Balance__c,Due_Date__c, Days_Past_Due__c,Create_Date__c, '+notestr+' from Transaction__c where Account__c =:acId order by Days_Past_Due__c desc limit 999 ';
           
                       
         //   trxs = [select name,Indicator_Icons__c ,So_Number__c,Po_Number__c,Amount__c,Balance__c,Due_Date__c, Days_Past_Due__c,Create_Date__c from Transaction__c where Account__c =:accId.Id order by Days_Past_Due__c desc ];        
              trxs = Database.query(str);
        }
        return trxs;
    }
    
  
     public List<Note> getAccountNotes() {
            
                if(accId != null) 
                    notes  = [Select n.Title, 
                                n.SystemModstamp, 
                                n.ParentId, 
                                n.OwnerId, 
                                n.LastModifiedDate, 
                                n.LastModifiedById, 
                                n.IsPrivate, 
                                n.IsDeleted,
                                n.Id, 
                                n.CreatedDate, 
                                n.CreatedById, 
                                n.Body From Note n where n.ParentId =:accId.Id order by CreatedDate desc limit 5 ];

                return notes ;

    }
    
    public List<Contact> getAccountContacts() {
         
                if(accId != null) 
                    con = [Select name,email,fax,phone,default__c,locale__c,Title from contact where accountId =: accId.Id limit 999];

                return con ;

    }
      public List<Task> getOpenActivity() {
         
                if(accId != null) 
                    openTask = [Select Subject,WhoId,Priority, Status,Contact_Method__c, Rating__c, WhatId,  Type , ActivityDate from task where whatId=: accId.Id and Status != 'Label.Label_Completed'  order by ActivityDate Desc limit 999];

                return openTask;

    }
      public List<Task> getActivityHistory() {
         
                if(accId != null) 
                    activityTask = [Select Subject,WhoId,Priority, Status, WhatId,Contact_Method__c,Rating__c,  Type , ActivityDate from task where whatId=: accId.Id and Status =: 'Label.Label_Completed'  order by ActivityDate Desc limit 999];

                return activityTask;

    }
    
    public List<Note> getTransactionsNotes() {
            
                if(oldesttrxId != null) 
                    txnotes = [Select n.Title, 
                                n.SystemModstamp, 
                                n.ParentId, 
                                n.OwnerId, 
                                n.LastModifiedDate, 
                                n.LastModifiedById, 
                                n.IsPrivate, 
                                n.IsDeleted,
                                n.Id, 
                                n.CreatedDate, 
                                n.CreatedById, 
                                n.Body From Note n where n.ParentId =:oldesttrxId.Id order by CreatedDate desc limit 5 ];
                
                return txnotes ;

    }
    
    // method is used to get List Contents 
    // return type : void
    public void getListContents(){
         
         wrapList =null;
         selectedFields = new  List<String>();
         selectedFields.add('name');
         for(String nextField : getDefaultListView())
         {
             selectedFields.add(nextField);
         }
         //call method to create header
         getSelectedFieldsLabel(selectedFields);
         getWrapList();
    }
    
    
    //method is used to get default List View
    // return type : List < String >
    public static List<String> getDefaultListView(){
        List<String> defaultList = new List<String>();

        //Note------------do not add Id and Name fields on this list------------//

        defaultList.add('So_Number__c');
        defaultList.add('Po_Number__c');
        defaultList.add('Create_Date__c');
        defaultList.add('Due_Date__c');
        defaultList.add('Days_Past_Due__c');
        defaultList.add('Amount__c');
        defaultList.add('balance__c');

        return defaultList;

    }
    
    //inner class for customize columns of transaction objects
    public class wrapperTransactionList {
        public Boolean selected {get; set; }
        transient public List<TxListWithAttributes> txValues {get; set; }
        public String transId {get; set; }
        transient public String fieldType {get; set; }
        transient public String txBalance {get; set; }
        transient public Transaction__c txObject {get; set; }
        public wrapperTransactionList(Transaction__c txObj, List<TxListWithAttributes> txValues,string txBalanceVal ){
            this.txValues = new List<TxListWithAttributes>();
            this.txValues = txValues;
            this.txObject = new Transaction__c();
            this.txObject = txObj;
            transId = txObj.Id;
            this.selected = false;
            this.fieldType = fieldType;
            this.txBalance = txBalanceVal;
        }
    }
    
     public class TxListWithAttributes {
        transient public String txId {get; set; }
        transient public String fieldType {get; set; }
        transient public String fieldValue {get; set; }
        public TxListWithAttributes (String fieldType,String fieldValue){
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
        }
    }
    
    
    // WrapperClass which display Field Header    
    public class TxLabelListWithAttributes {
        transient public String txId {get; set; }
        public String fieldType {get; set; }
        public String fieldValue {get; set; }
        public TxLabelListWithAttributes (String fieldType,String fieldValue){
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
        }
    }
    
    //Create header for exported file
    //method is used to get selected fields label
    //return type : void 
    public void getSelectedFieldsLabel(List<String> fieldsList)
    {
        
        lstOfLabels = new List<TxLabelListWithAttributes>();

        //fetch field label through field name
        for(String fieldName : fieldsList)
        {
            TransactionDescribeCall.FieldDescibe fieldObj = TransactionDescribeCall.getFieldData(fieldName);
            
            
            lstOfLabels.add(new TxLabelListWithAttributes(String.valueOf(fieldObj.fieldType),fieldObj.fieldLabel ));

        }

    }
    
    
    public void next(){
    
  
        if ( taskId != null ){
            taskId.status = Label.Label_In_Progress;
            taskId.CallDisposition = callresult;
            
            if(taskId.Description != null)
               taskId.Description = taskId.Description + ' ' + comments; 
             else           
                 taskId.Description = comments;
                  
            //===============================================================  
             list<String> lstFields = new list<String>{'Description','CallDisposition','Status','Priority'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
           for (String fieldToCheck : lstFields) {

              // Check if the user has create access on the each field
                if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return;
                  }
                }
            }
        //========================================================================================     
            update taskId;
        }
        counter++;
        prepareAccounts();
    }
    
    public void completenext(){
    
        taskId.status = Label.Label_Completed;
        taskId.CallDisposition = callresult;
        if(taskId.Description != null)
            taskId.Description = taskId.Description + ' ' + comments;   
        else           
            taskId.Description = comments;          
        
        //===============================================================  
             list<String> lstFields = new list<String>{'Description','CallDisposition','Status','Priority'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
           for (String fieldToCheck : lstFields) {

              // Check if the user has create access on the each field
                if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return;
                  }
                }
            }
        //========================================================================================
        update taskId;
        counter++;
        prepareAccounts();
       
        
    }
    
     // method is used to get wrapperTransaction List 
    // return type : List<wrapperTransactionList> 
    public List<wrapperTransactionList> getWrapList() {

        wrapList = new List<wrapperTransactionList>();

        String noteId; String tempnotes; String balance; String txno; String name; String noteBody;
        String createdBy; DateTime createdDate; String tooltipStr;
        
        Integer loopvar = 1;
        if(getTransactions() != null){
            for (Transaction__c transObj : getTransactions())
            {
                
                tempnotes=''; noteId=''; noteBody =''; balance=''; txno='';
                createdBy = ''; createdDate = null;
    
                tempList = new List<TxListWithAttributes>();
                try{
                  
                    Integer i = 0;
                    
                    
                    for ( Note n : transObj.Notes ){
                    
                        noteId =n.Id;
                        tempnotes = n.Title;
                        noteBody =n.Body;
                        createdDate  = n.CreatedDate;
    
                        createdBy = n.CreatedBy.Name;
                        if(noteBody ==null || noteBody =='') {
                            noteBody='';
                        }
                        
                        tooltipStr = noteBody + '\n ' +  createdBy;
                        tooltipstr = String.escapeSingleQuotes(tooltipStr);
                        tooltipstr = tooltipstr.replace('"', '');
                       
                        break;
                    }
                
                
                }catch( Exception e) {
                
                    
                }
                
                
                Integer j=0;
                
                for(String field : selectedFields )
                {  j = j+1;
                  
                  TransactionDescribeCall.FieldDescibe fieldObj = TransactionDescribeCall.getFieldData(field);
    
                   if(field =='name') {
                     
                       txno =String.ValueOf(transObj.get(field));
                       String txLink  = '<a href="' + '/'+ transObj.Id + '"'  + ' style="text-decoration: underline;cursor: pointer; cursor: hand;" title="Open Transaction" target="_blank">' + txno + '</a>';
    
                       tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),txLink));
    
                   }
    
                   else if(fieldObj.fieldType ==Schema.DisplayType.DATE) {
                       if(transObj.get(field) !=null ) {
                           Date dtVar =(Date) transObj.get(field);
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),dtVar.format()));
    
                       }else{
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' '));
    
                       }
                   }
                   else if(fieldObj.fieldType ==Schema.DisplayType.CURRENCY) {
                     String formattedAmt = '';
                      Double amnt = (Double)transObj.get(field);
                       if(amnt !=null) {
    
                           //@TODAO : Bhavik : Ref : On-Demand Multi-Currency
                           //String currencyIsoCode = ( String )(UtilityController.isMultiCurrency() ? transObj.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode());
                           String currencyIsoCode = 'USD';
                           formattedAmt =  UtilityController.formatDoubleValue(currencyIsoCode, amnt);
                       }
                       tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),formattedAmt));
    
                   }
                   else if(fieldObj.fieldType == Schema.DisplayType.REFERENCE) {
                       if(transObj.get(field) !=null ) {
                         String refFieldName = field.subString(0,field.length()-1) + 'r';
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),String.valueOf(transObj.getSobject(refFieldName).get('name'))));
                       }else{
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' '));
                       }
                   }
                   else
                   {
                       if(transObj.get(field)!=null)
                       {
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),String.ValueOf(transObj.get(field))));
                       }
                       else
                       {
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' ' ));
    
                       }
                   }}
    
              //  
                String notesVal ='';
                if(tempnotes !='' &&  tempnotes !=null) {
                    notesVal = '<a href="' + '/apex/AccountDetailPage?id='+ accId.Id + '&tabsel=4' + '&txtSearch2='+ txNo + '"'  + ' style="text-decoration: underline;cursor: pointer; cursor: hand;"  title=\"' + tooltipStr +  '\">' + tempnotes + '</a>';
                    tempList.add(new TxListWithAttributes('notetype1', notesVal));
    
                    //add created note column value
                    tempList.add(new TxListWithAttributes('notedate',createdDate.format().split(' ')[0]));
                }else{
                    String addNote ='Add Note';
                    notesVal  = '';
    
                    TxListWithAttributes txListObj =  new TxListWithAttributes('notetype2', notesVal);
                    txListObj.txId = transObj.Id;
                    tempList.add(txListObj);
                    //add blank created note column value
                    tempList.add(new TxListWithAttributes('notedate',' '));
                }
    
                //get balance of tx
                string txBalance='0';
                if(transObj.get('balance__c')!=null) {
                    txBalance = String.valueOf(transObj.get('balance__c'));
    
                }
                
                wrapperTransactionList wrapObj = new wrapperTransactionList(transObj, tempList,txBalance );
                wrapList.add(wrapObj);
    
            }
        }
       
        return wrapList;
    }
    
    public PageReference updateNote(){
        Note note1 = new Note();
        
        if(accId != null && accId != null){
            note1.parentId = accId.Id;
            note1.Title = notetitle;
            note1.Body = noteBody;
            
            //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            
            upsert note1;
        }
         return null;
    }
    
    public PageReference updateNotetrx(){
        Note note2 = new Note();
        
        if(oldesttrxId != null && oldesttrxId != null){
            note2.parentId = oldesttrxId.Id;
            note2.Title = notetitletrx;
            note2.Body = noteBodytrx;
            
//========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
               return null;
              }
          }
        }
//========================================================================================
            
            upsert note2;
        }
         return null;
    }

}