/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */

/*
 * Usage: This class is used for calculating Sum of total disputed,
 *        Sum of total Promised,Sum of total AR and sum of ages etc.
 */
public with sharing class AccountHierarchyPageController {

    public Id stdConAccountId {get; set; }
    public list<Account> allChildAccounts {get; set; }
    public AccountResultHolder summaryAccount {get; set; }
    
    // Default constructor for the class 
    // Calculates value of Balance, Disputed Amount, Promised Amount and all Ages
    // of Transactions of an single Account 
    // Stores the calculated value in the respective field of Account standard object 
    public AccountHierarchyPageController(Apexpages.Standardcontroller sc) {
        summaryAccount = new AccountResultHolder();
        this.stdConAccountId = sc.getId();
        
        // Fetches data of all child account of a Parent Account 
        allChildAccounts = [select Id, Name, Account_Key__c, Total_Promised__c, Total_Disputed__c,Total_AR__c,
                            Age0__c, Age1__c, Age2__c, Age3__c, Age4__c, Age5__c
                            from Account where ParentId =: stdConAccountId ];
         
        // Calculates sum of Balance, Ages , Total Disputed and Total Promised of an Account
        List<AggregateResult> aggRes = [select SUM(Total_Promised__c) SUM_Promised,
                                        SUM(Total_Disputed__c) SUM_Disputed,
                                        SUM(Total_AR__c) SUM_AR,
                                        SUM(Age0__c) SUM_AGE0, SUM(Age1__c) SUM_AGE1,
                                        SUM(Age2__c) SUM_AGE2, SUM(Age3__c) SUM_AGE3,
                                        SUM(Age4__c) SUM_AGE4, SUM(Age5__c) SUM_AGE5
                                        from Account
                                        where ParentId =: stdConAccountId
                                                         GROUP BY ParentId];
        
        // Assigns value to total variable created in next class
        If(aggRes.size()>0) {
            AggregateResult result = aggRes.get(0);

            summaryAccount.totalPromised = (Decimal) result.get('SUM_Promised');
            summaryAccount.totalDisputed = (Decimal) result.get('SUM_Disputed');
            summaryAccount.totalAr = (Decimal) result.get('SUM_AR');
            summaryAccount.totalAge0 = (Decimal) result.get('SUM_AGE0');
            summaryAccount.totalAge1 = (Decimal) result.get('SUM_AGE1');
            summaryAccount.totalAge2 = (Decimal) result.get('SUM_AGE2');
            summaryAccount.totalAge3 = (Decimal) result.get('SUM_AGE3');
            summaryAccount.totalAge4 = (Decimal)result.get('SUM_AGE4');
            summaryAccount.totalAge5 = (Decimal) result.get('SUM_AGE5');
        }

    }

    public Class AccountResultHolder {
        public Decimal totalDisputed {get; set; }
        public Decimal totalPromised {get; set; }
        public Decimal totalAr {get; set; }
        public Decimal totalAge0 {get; set; }
        public Decimal totalAge1 {get; set; }
        public Decimal totalAge2 {get; set; }
        public Decimal totalAge3 {get; set; }
        public Decimal totalAge4 {get; set; }
        public Decimal totalAge5 {get; set; }
    }
    
}