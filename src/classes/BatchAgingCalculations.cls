/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for calculating Batch aging values.
 */
global class BatchAgingCalculations implements Database.Batchable<SObject>, Database.Stateful {

    public AccountWrapper aw;
    String email;
    public List<Data_Load_Batch__c> batchInfo ;
    global Id batchInfoId ;
    global String batchNumber;
    global String sourceSystem;
    global Account a = null;
    public RunningStandardDevCalc stdDevCalAll = new RunningStandardDevCalc ();
    global String SourceSys;
    
    public BatchAgingCalculations (){
    
    }
    public BatchAgingCalculations ( String SourceSystem){
       Sourcesys = SourceSystem;
    }

    

    //@TODO : Bhavik : Ref - On-Demand Multi-Currency
    global database.querylocator start(Database.BatchableContext bc){
        /**start Namespace for migrating to unmanaged package  **/
        String namespace= '';
        namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        //String namespace = ' ';
        String sQuery = '';
        // Check for multi currency ( if true fatch currency code also )
        if ( UtilityController.isMultiCurrency() ) {

            sQuery = 'select  CurrencyIsoCode, '+namespace+'Account__c, '+namespace+'Account__r.CurrencyIsoCode, '+namespace+'Balance__c , '+namespace+'Base_Balance__c , '+namespace+'Days_Past_Due__c,';

        } else {
            sQuery = 'select   '+namespace+'Account__c,  '+namespace+'Balance__c  , '+namespace+'Base_Balance__c , '+namespace+'Days_Past_Due__c,';
        }
        
        
        sQuery += ' '+namespace+'Due_Date__c, '+namespace+'Promised_Amount__c, '+namespace+'Promise_Date__c, '+namespace+'Disputed_Amount__c, '+namespace+'Undisputed_Balance__c';
        sQuery += ' from '+namespace+'Transaction__c';
        sQuery += ' where '+namespace+'Account__c !=null and '+namespace+'Balance__c != null and '+namespace+'Due_Date__c != null ';
        
        //By Kruti Tandel
        if(Sourcesys != null && Sourcesys != ''){
            sQuery += ' and ' + namespace+'Source_System__c =: Sourcesys';
        }

        SysConfig_Singlec__c sysconfig  = ConfigUtilController.getSystemConfigObj();

        if ( sysconfig.Aging_Filter__c != null &&  sysconfig.Aging_Filter__c.trim() != '') {

            sQuery += ' and ' + sysconfig.Aging_Filter__c;
        }
        sQuery += '  order by '+namespace+'Account__c , '+namespace+'Days_Past_Due__c DESC';
        
        if ( batchInfoId  != null ) {
           //* batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c, d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c  From Data_Load_Batch__c d where Id=:batchInfoId];
            
            batchInfo.get(0).Aging_Job_Start_Time__c = System.now();
            batchNumber = batchInfo.get(0).Batch_Number__c;
            sourceSystem = batchInfo.get(0).Source_System__c;
            
        }
        return Database.getQueryLocator(sQuery);
    }
    // Returns the curency conversion code set in the organization 
    // Return Type - Double 
    // Parameter - inputCurrencyCode String 
    public double getConversionRate(String inputCurrencyCode) {

        //@TODO : Bhavik : Ref - On-Demand Multi-Currency

        return UtilityController.getConversionRate(inputCurrencyCode);
    }
    // Start calculation for Aging  
    // Return Type - Void 
    // Parameter - BC Database.Batchablecontext, sObjs List<sObject>
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
        Boolean isupdate = false;

        List<Account> accountsToUpdate = new List<Account>();
        List<Account> accountsToUpdate2 = new List<Account>();
         
        List<Id> accountIds= new List<Id>(); 
        
        for(Sobject txSobject : sObjs) {
        
            Transaction__c txtemp = (Transaction__c) txSobject;
            accountIds.add( txtemp.Account__c );
        }
        
        Map < Id , Account > accountsMap = new Map <Id, Account>([ select Id ,Base_Total_AR__c, Total_AR__c,Age0__c from Account where Id in: accountIds ]);
        
        
         
        for(Sobject txSobject : sObjs) {
            Transaction__c tx = (Transaction__c) txSobject;
                 if(tx.Account__c != null && tx.Due_Date__c!= null && tx.Balance__c != null) {
                    if(aw == null) {
                        aw = new AccountWrapper(tx.Account__c);
                        a = new Account( id = tx.Account__c);
                        // Check for isbucketonBase ( if true calcualte base total AR and Base Total Past due else Total past due )
                        if( ConfigUtilController.isBucketOnBase() ){
                        
                             
                             a.Base_Total_AR__c = 0;
                             a.Base_Total_Past_Due__c =0;
                        }else{
                        
                            Account ac = accountsMap.get( a.Id );
                        
                            if (ac.Total_AR__c != null){                                  
                                a.Total_Past_Due__c = (ac.Total_AR__c).SetSCale(2);
                            }   
                       }
                        
                        addDetails(tx);
                    }
                    else if(aw.accountId == tx.Account__c) {
                        addDetails(tx);
                     }
                    else {
                        if(aw != null && aw.accountId != null) {
                            Account acc = new Account(Id = aw.accountId);
                            // Calculate bucket fields values 
                            acc.Age0__c = aw.age0;
                            acc.Age1__c = aw.age1;
                            acc.Age2__c = aw.age2;
                            acc.Age3__c = aw.age3;
                            acc.Age4__c = aw.age4;
                            acc.Age5__c = aw.age5;
                            
                            acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
                            acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;
    
                            if(aw.stdDevCal.getMean() != null)
                                acc.DPD_Mean__c = aw.stdDevCal.getMean();
                            if(aw.stdDevCal.getStandardDeviation() != null)
                                acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();
    
                            accountsToUpdate.add(acc); 
                            
                            
                            a.DPD__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Days_Past_Due__c : 0;
                            
                            if( ConfigUtilController.isBucketOnBase() && aw.age0 != null && a.Base_Total_Ar__c != null ){
                            
                                a.Base_Total_Past_Due__c = a.Base_Total_AR__c - aw.age0;
                            }else {
                            
                                a.Base_Total_Past_Due__c = 0;
                            }
                            accountsToUpdate2.add( a );
                                
                            
                            aw = new AccountWrapper(tx.Account__c);
                            a = new Account( id = tx.Account__c);
                            
                            
                            if( ConfigUtilController.isBucketOnBase() ){
                        
                             
                                     a.Base_Total_AR__c = 0;
                                     a.Base_Total_Past_Due__c =0;
                            }else{
                            
                                                    
                                Account ac = accountsMap.get( a.Id );
                                a.Total_Past_Due__c = (ac.Total_AR__c).setScale(2);
                                
                                
                                
                            }
                            addDetails(tx);
                            
                        }
                    }
                }
           }

        try {
            if(accountsToUpdate.size()>0){
            
                update accountsToUpdate;
            }
            
            if(accountsToUpdate2.size()>0){
                
                update accountsToUpdate2; 
            }
            
        } catch(Exception e){
            throw e;
        }
    }

    global void addDetails(Transaction__c tx){

        // to calculate avg pastdue date
        if(tx.Due_Date__c !=null) {
            aw.stdDevCal.push((double)tx.Due_Date__c.daysBetween(Date.Today()));
        }

        // aging calculation job
        Integer dpd = tx.Due_Date__c.daysBetween(Date.Today());
        //get the account crrency and transaction currency , get the converstion rate for both and calculate
        //the converted balance for calculation.

        String accCurrencyCode = (String)(UtilityController.isMultiCurrency() ? tx.Account__r.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode()); // tx.akritivM1__Account__r.CurrencyIsoCode;
        String txCurrencyCode =(String)(UtilityController.isMultiCurrency() ? tx.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode()); // tx.CurrencyIsoCode;

        double convertedTxBalance = (tx.Balance__c * getConversionRate(accCurrencyCode))/getConversionRate(txCurrencyCode);
        
        // Check for isbucketOnBase ( if true then No conversion take place( Calculation is done based on Base Balance) )
        if( ConfigUtilController.isBucketOnBase() && tx.Base_Balance__c != null){
        
             convertedTxBalance = tx.Base_Balance__c;
        }

        // Calculate Base total AR
        if(tx.Base_Balance__c != null && ConfigUtilController.isBucketOnBase()){
                    
            a.Base_Total_AR__c = a.Base_Total_AR__c + tx.Base_Balance__c;
                        
        }
        
        // Calcualte the value of bucket fields based on bucket configuration 
        if(ConfigUtilController.getAgingBucketConfigObj() != null ) {
            
            if( dpd <= ConfigUtilController.getBucketByAge(0).maxRange){
            
                aw.age0+= convertedTxBalance;
                
                if ( !ConfigUtilController.isBucketOnBase()  ) {
                
                    if ( a.Total_Past_Due__c == null )  a.Total_Past_Due__c = 0;                        
                        a.Total_Past_Due__c = (a.Total_Past_Due__c - convertedTxBalance).SetSCale(2);
                
                }
                       
            }else if( dpd<= ConfigUtilController.getBucketByAge(1).maxRange)
                aw.age1+= convertedTxBalance;
            else if( dpd <= ConfigUtilController.getBucketByAge(2).maxRange)
                aw.age2+= convertedTxBalance;
            else if( dpd <= ConfigUtilController.getBucketByAge(3).maxRange)
                aw.age3+= convertedTxBalance;
            else if( dpd <= ConfigUtilController.getBucketByAge(4).maxRange)
                aw.age4+= convertedTxBalance;
            else
                aw.age5+= convertedTxBalance;
        }
        
        
   
         if( ConfigUtilController.isBucketOnBase()){
            // Find Oldest invoice on account
            if(tx.Base_Balance__c >0 && tx.Due_Date__c !=null && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) && (tx.Disputed_Amount__c == null || tx.Disputed_Amount__c == 0) ) {
                if(aw.leadPastdueTx == null)
                    aw.leadPastdueTx = tx;
                else {
                    if(tx.Due_Date__c < aw.leadPastdueTx.Due_Date__c)
                        aw.leadPastdueTx = tx;
                    else if(tx.Due_Date__c == aw.leadPastdueTx.Due_Date__c && tx.Base_Balance__c > aw.leadPastdueTx.Base_Balance__c)
                        aw.leadPastdueTx = tx;
                }
            }
    
            // find the higest past due invoice
            // Broken promise will also be a part of calculations (promise date < today)
            if(tx.Base_Balance__c >0 && tx.Undisputed_Balance__c != null && tx.Due_Date__c < Date.today() && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) ) {
                if(aw.highPastdueTx == null)
                    aw.highPastdueTx = tx;
                else {
                    if(tx.Undisputed_Balance__c > aw.highPastdueTx.Undisputed_Balance__c )
                        aw.highPastdueTx = tx;
                }
            }
       }
       else{
       
        // Find oldest invoice on account
        if(tx.Balance__c >0 && tx.Due_Date__c !=null && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) && (tx.Disputed_Amount__c == null || tx.Disputed_Amount__c == 0) ) {
            if(aw.leadPastdueTx == null)
                aw.leadPastdueTx = tx;
            else {
                if(tx.Due_Date__c < aw.leadPastdueTx.Due_Date__c)
                    aw.leadPastdueTx = tx;
                else if(tx.Due_Date__c == aw.leadPastdueTx.Due_Date__c && tx.Balance__c > aw.leadPastdueTx.Balance__c)
                    aw.leadPastdueTx = tx;
            }
        }

        // find the higest past due invoice
        // Broken promise will also be a part of calculations (promise date < today)
        if(tx.Balance__c >0 && tx.Undisputed_Balance__c != null && tx.Due_Date__c < Date.today() && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) ) {
            if(aw.highPastdueTx == null)
                aw.highPastdueTx = tx;
            else {
                if(tx.Undisputed_Balance__c > aw.highPastdueTx.Undisputed_Balance__c )
                    aw.highPastdueTx = tx;
            }
        }
           
       }
    }
    // Methods assins value to fields in tarnsaction the values which are calculated 
    // Return Type - Void 
    // Parameter - bc Database.Batchablecontext
    global void finish(Database.BatchableContext bc){
        if(aw != null && aw.accountId != null) {
            Account acc = new Account(Id = aw.accountId);
            acc.Age0__c = aw.age0;
            acc.Age1__c = aw.age1;
            acc.Age2__c = aw.age2;
            acc.Age3__c = aw.age3;
            acc.Age4__c = aw.age4;
            acc.Age5__c = aw.age5;

            acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
            acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;

            if(aw.stdDevCal.getMean() != null)
                acc.DPD_Mean__c = aw.stdDevCal.getMean();

            if(aw.stdDevCal.getStandardDeviation() != null)
                acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();

            update acc;
        }
        
        if(a != null){
            // Check for isBucketonBase ( if true then set base total past due ) 
            if( ConfigUtilController.isBucketOnBase() && aw.age0 != null && a.Base_Total_Ar__c != null ){
                        
                   a.Base_Total_Past_Due__c = a.Base_Total_AR__c - aw.age0;
            }else {
            
                a.Base_Total_Past_Due__c = 0;
            }  
            a.DPD__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Days_Past_Due__c : 0;
            update a;
        }
        
         if (batchInfo != null ) {
            // Update batch info record
            batchInfo.get(0).Aging_Job_End_Time__c = System.now();
            batchInfo.get(0).Aging_Job_Status__c = 'Done';
            update batchInfo;
            
            //notify external sysem of this finish
            //String toAddress = ConfigUtilController.getBatchAutoCloseExternalEmailNotificationService();
             String toAddress = ConfigUtilController.getBatchAutoCloseEmailService();
                
            //notify system of this finish
            if (toAddress != null ) {
            
                //--email mechanism with mandrill--start-----
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                MandrillEmailService objMandrill = new MandrillEmailService();
                
                //--email mechanism with mandrill--end-----
                mail.setToAddresses(new String[] {toAddress});
                objMandrill.strToEmails = new String[] {toAddress};
             //--email mechanism with mandrill--start-----   
                mail.setReplyTo(toAddress);
                objMandrill.strReplyTo = toAddress;

                mail.setSenderDisplayName('Batch Processing :');
                objMandrill.strFromName = 'Batch Processing :';
              //--email mechanism with mandrill--end-----
                String mailBodyText = '';
                
              //  mail.setSubject('Akritiv Close Process');
             //--email mechanism with mandrill--start-----
              mail.setSubject( 'Akritiv ADPD Process');
               objMandrill.strSubject = 'Akritiv ADPD Process';
                //--email mechanism with mandrill--end-----
                
                 if ( batchInfo != null  ){
                      
                    mailBodyText ='<END>'+batchNumber+''+Userinfo.getOrganizationId()+''+sourceSystem+''+Userinfo.getOrganizationId()+''+batchInfo.get(0).Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                  

                 } else {
        
                    mailBodyText ='<END>'+batchNumber+''+Userinfo.getOrganizationId()+''+sourceSystem+''+Userinfo.getOrganizationId()+''+batchInfo.get(0).Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                    

                 }             
        
                mail.setPlainTextBody(mailBodyText);
           //--email mechanism with mandrill--start-----
           SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                 boolean us = seo.Use_Mandrill_Email_Service__c;
                 if(us){
                     objMandrill.SendEmail();
                       }
                 else{
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     }
                
                //--email mechanism with mandrill--end-----
             }
        }
    }

    global class AccountWrapper {
        public ID accountId {get; set; }
        public Double age0 {get; set; }
        public Double age1 {get; set; }
        public Double age2 {get; set; }
        public Double age3 {get; set; }
        public Double age4 {get; set; }
        public Double age5 {get; set; }
        public Transaction__c highPastdueTx {get; set; }
        public Transaction__c leadPastdueTx {get; set; }
        public RunningStandardDevCalc stdDevCal {get; set; }

        global AccountWrapper(Id accId) {
            this.accountId = accId;
            stdDevCal = new RunningStandardDevCalc();
            age0 = 0;
            age1 = 0;
            age2 = 0;
            age3 = 0;
            age4 = 0;
            age5 = 0;
        }
    }

    /*
     * Class to calculate standard deviation accurately in one pass.
     */
    public class RunningStandardDevCalc {
        private Integer n;
        private Double oldM, newM, oldS, newS;

        public RunningStandardDevCalc() {
            n=0;
        }
        //   Method to clear to all data before Standard Deviation Calculation
        //   Return Type - Void 
       //    No parameters are passed in this method
        public void Clear() {
            n = 0;
        }
        // Method use for calculating value of average Due Date for Billed and Unbilled transaction
        // Return Type - Void 
        // Parameter - x Double 
        public void push(double x){
            n++;
            if (n == 1) {
                oldM = newM = x;
                oldS = 0.0;
            }
            else {
                newM = oldM + (x - oldM)/n;
                newS = oldS + (x - oldM)*(x - newM);
                // set up for next iteration
                oldM = newM;
                oldS = newS;
            }
        }
        // Returns the value of variable n which is currently set after push method is called
        // Return Type - Integer 
        // No parameter are passed in this method
        public Integer getNumDataValues() {
            return n;
        }
        // Returns value of newly calculated mean in push method
        // Return Type - Double 
        // No parameters are passed in htis method
        public Double getMean() {
            return (n > 0) ? newM : 0.0;
        }
        // Returns the value of Variance calculated on the Due Date  
        // Return Type - Double
        // No parameters passed in this method 
        public Double getVariance() {
            return ( (n > 1) ? newS/(n - 1) : 0.0 );
        }
        // Returns the Average Standard Dveiation for current Due Date of that Account
        // Return Type - Double 
        // No Parameters are parameters are passed in this method 
        public Double getStandardDeviation() {
            return Math.sqrt( getVariance() );
        }
    }



}