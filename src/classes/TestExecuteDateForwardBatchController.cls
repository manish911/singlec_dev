/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for ExecuteDateForwardBatchController
 */
@isTest
private class TestExecuteDateForwardBatchController {

    static testMethod void runTest() {
        Account acc = DataGenerator.accountInsert();

        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        BatchJobLog__c BJL = new BatchJobLog__c();
        BJL.name = 'DateForwardBatchJob';
        BJL.last_run_date__c = Date.Today();
        insert BJL;

        Test.startTest();

        ExecuteDateForwardBatchController controller = new ExecuteDateForwardBatchController();
        System.assertEquals(null, controller.executeBatchUpdate());
        System.assertEquals(0, controller.progress);
        System.assertEquals(null, controller.refreshJobStatus());

        Test.stopTest();
    }
}