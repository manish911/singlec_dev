/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   The class provides the functionality to mark reserve
 *             the transaction.
 */
public with sharing class MarkReserveController {

    public List<TransactionWrapper> txWrapperList {get; set; }

    public Account acc {get; set; }
    public String accountId {get; set; }
    public String[] transactionIds {get; set; }

    private List<Temp_Object_Holder__c> txsConfig;
    private Boolean calledFromTxDetailPage;

    public Integer activityRating {get; set; }
    private List<Transaction__c> txList = new List<Transaction__c>();

    //------------------------------------------------------------------------------------------//
    // DEFAULT CONSTRUCTOR
    //------------------------------------------------------------------------------------------//
    public MarkReserveController() {
        accountId = ApexPages.currentPage().getParameters().get('accId');
        String transactionIdsKey = ApexPages.currentPage().getParameters().get('txIds');
        calledFromTxDetailPage = false;

        // if the page is called from Account Details page
        if(accountId != null && accountId != '' && transactionIdsKey!= null && transactionIdsKey != '') {
            txsConfig = new List<Temp_Object_Holder__c>([select value__c from Temp_Object_Holder__c where key__c =:transactionIdsKey limit 1]);
            transactionIds = txsConfig.get(0).value__c.split(',');
        }
        // else the page is called from transaction details level
        else {
            transactionIds = new String[] {ApexPages.currentPage().getParameters().get('txid')};
            calledFromTxDetailPage = true;
        }

        txWrapperList = new List<TransactionWrapper>();
        txList = [select Id, Name, Account__c,Balance__c, Amount__c, Reserved_Balance__c, Reserved_Reason_Code__c, Reserved_By__c, Reserved_Date__c from Transaction__c where id in:transactionIds];

        for(Transaction__c tx : txList) {
            TransactionWrapper wrapper = new TransactionWrapper(tx);
            txWrapperList.add(wrapper);
        }

        activityRating = UtilityController.getRatingStarNumber(AkritivConstants.DEFAULT_ACTIVITY_RATING);
    }

    //------------------------------------------------------------------------------------------//
    // GET ALL RESERVED REASON CODES FROM THE METADATA BY DESCRIBE CALL
    //------------------------------------------------------------------------------------------//
    // property to get the dispute types select options
    public List<SelectOption> reservedReasonCodes {
        get {
            if(reservedReasonCodes == null) {
                reservedReasonCodes = new List<SelectOption>();
                Schema.DescribeFieldResult fieldResult = Transaction__c.Reserved_Reason_Code__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    reservedReasonCodes.add(new SelectOption(f.getValue(),f.getLabel()));
                }
            }

            return reservedReasonCodes;
        }
        set;
    }
    // Returns List of selected transaction in TransactionWrapper class
    // Return Type - List<Transaction__c>
    // No parameters are passed in this method
    public List<Transaction__c> getSelectedTxs() {
        List<Transaction__c> selectedTransactions = new List<Transaction__c>();
        for(TransactionWrapper wrapper : txWrapperList) {
            if(wrapper.selected) {
                selectedTransactions.add(wrapper.tx);
            }
        }
        return selectedTransactions;
    }
    // Marks a transaction reserve from the list of Transaction in TransactionWrapper class
    // Return Type - Pagereference
    // No parameter are passed in this Method
    public PageReference markReserve()
    {
        List<Transaction__c> txList = new List<Transaction__c>();
        Map<Transaction__c,Double> txMap = new Map<Transaction__c,Double>();
        for(TransactionWrapper wrapper : txWrapperList)
        {
            if(wrapper.selected)
            {
                if(wrapper.tx.Reserved_Balance__c ==null || wrapper.tx.Reserved_Balance__c <= 0 )
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Label_Please_enter_valid_Reserved_Balance));
                    return null;
                }
                if(wrapper.tx.Reserved_Balance__c >  Math.abs(wrapper.tx.Balance__c))
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.Label_Reserved_Balance_cannot_be_greater_than_Transaction_Balance));
                    return null;
                }

                if(wrapper.tx.Balance__c < 0){
                   // wrapper.tx.Balance__c = wrapper.tx.Balance__c + wrapper.tx.Reserved_Balance__c;
                   // txList.add(wrapper.tx);
                   txMap.put(wrapper.tx, wrapper.tx.Balance__c + wrapper.tx.Reserved_Balance__c);
                }else if (wrapper.tx.Balance__c > 0){
                    txMap.put(wrapper.tx, wrapper.tx.Balance__c - wrapper.tx.Reserved_Balance__c);
                    //wrapper.tx.Balance__c = wrapper.tx.Balance__c - wrapper.tx.Reserved_Balance__c;
                   // txList.add(wrapper.tx);
                }
            }
        }
      //  if(txList != null)
       //     upsert txList;
       for(Transaction__c tx : txMap.keySet()){
      
           tx.Balance__c = txMap.get(tx);
            system.debug('======tx.Balance__c=========' +tx.Balance__c);
           txList.add(tx);
       }
       //==========================================================================================  
        list<String> lstFields = new list<String>{'Reserved_Balance__c','Reserved_Reason_Code__c','Reserved_By__c','Reserved_Date__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Transaction__c.fields.getMap();
           for (String fieldToCheck : lstFields) {

              // Check if the user has create access on the each field
                if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return null;
                  }
                }
            }
        //========================================================================================
       update txList;
       txList.clear();
       return goBackToSourcePage();
    }

    //------------------------------------------------------------------------------------------//
    // FUNCTION TO RETURN TO THE ANOTHER PAGE
    //------------------------------------------------------------------------------------------//
    // method is used to go back to source page
    // return type : Pagereference
    public Pagereference goBackToSourcePage() {
        if(txsConfig != null) {
        
           
            //===================================================================================
                if (!Temp_Object_Holder__c.sObjectType.getDescribe().isDeletable()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return null;
                  }
            //===================================================================================
        
            delete txsConfig;
            txsConfig = null;
        }
        if(calledFromTxDetailPage && transactionIds!= null && transactionIds.get(0) != null)
            return new Pagereference('/'+transactionIds.get(0));

        return new Pagereference('/'+accountId);
    }

    /* Wrapper class to store Tx with selected flag */
    public class TransactionWrapper {
        public Transaction__c tx {get; set; }
        /* Selected flag set to true when a Tx is selected/checked from the page */
        public Boolean selected {get; set; }
        // Default construnctor for the Wrapper Class "TransactionWrapper"
        public TransactionWrapper(Transaction__c tx) {
            this.selected = true;
            this.tx = tx;
            this.tx.Reserved_Balance__c = 0.0;
            this.tx.Reserved_By__c = UserInfo.getUserId();
            this.tx.Reserved_Date__c = Date.today();
        }
    }

}