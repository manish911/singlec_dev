/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 *  Usage   :   This class is responsible for generating Tasks for the various
            activities like contact customer,promise, dispute etc.
 */
public with sharing class ContactLogGenerator
{
    private static ContactLogGenerator generator = null;
    Map<Id,String> AcountIDOwnerMap = new Map<Id,String>();
    Public String EditedEmailTemplateBody;
     
    private ContactLogGenerator()
    {
    }
    
    // Method to get insrance of contact log generator
    public static ContactLogGenerator getInstance()
    {
        if(generator == null)
        {
            generator = new ContactLogGenerator();
        }
        return generator;

    }

    //@todo will remove once integrated
    public String inputValue {get; set; }
    // param - entry - contact log entry
    // return - task contact task
    public Task generateContactLogEntry(ContactLogEntry entry)
    {
        Task contactTask = constructTask(entry);
        //contactTask.
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert contactTask;
      
        
        //if(contactTask != null && contactTask.status != 'Completed' && entry.contactType == AkritivConstants.CONTACT_TYPE_CONTACT)
        if( contactTask != null)
            generateActivityTransaction(contactTask.Id, entry.transactionIds);
       
        return contactTask;

    }
    // param - entries - contact log entry list
    // insert tasks
    public void generateContactLogEntry(List < ContactLogEntry > entries )
    {
        List < Task > contactTasks = constructTask(entries);

        //contactTask.
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
        insert contactTasks;

        Integer i = 0;
        List < Activity_Transaction__c > actxs = new List < Activity_Transaction__c >();
        for ( ContactLogEntry entry : entries) {

            Task task = contactTasks.get( i++ );
            if( task != null ) {
                Activity_Transaction__c atx =   prepareActivityTransaction(task.Id, entry.transactionIds);
                if ( atx != null ) {
                     actxs.add( atx );
                }
            }
        }

        if ( actxs.size() > 0 ) {
            //==========================================================================================  
       Map<String,Schema.SObjectField> m2 = Schema.SObjectType.Activity_Transaction__c.fields.getMap();
       for (String fieldToCheck : m2.keyset()) {
          // Check if the user has create access on the each field
          if (m2.get(fieldToCheck).getDescribe().isCustom() && !m2.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m2.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
            insert actxs;
        }

    }
    // param - entries - contact log entry list
    // return - list of Tasks for what contact made
    private List<Task > constructTask(List < ContactLogEntry >  entries)
    {   Map<Id,Boolean> checkIsPortalUser = new Map<Id,Boolean>();
        for(User currUser : [select Id, ContactId,Email from User where Email != null]){
            checkIsPortalUser.put(currUser.Id,(currUser!= null && currUser.ContactId != null ? true : false));
            //return (currUser!= null && currUser.ContactId != null ? true : false);)
        }
        List < Task > tasks = new List < Task >();
        for ( ContactLogEntry entry : entries ) {
            Task contactTask = new Task();

            contactTask.WhatId = entry.accountId;
            contactTask.WhoId = entry.contactId;

            if(checkIsPortalUser.get(entry.userId))
            {

                if(entry != null && entry.accountOwnerId != null) {

                    contactTask.ownerId = entry.accountOwnerId;
                }

            }
            else
            {
                contactTask.ownerId = entry.userId;
            }

            if(entry.activityRating != null)
                contactTask.Rating__c = entry.activityRating;

            //contactTask.contact_method__c = entry.contactMethod;
            contactTask.Type = entry.contactMethod;
            contactTask.Contact_Type__c = entry.contactType;

            if(entry.notes != null && entry.notes.length() > 0) {
                String notesStr = (entry.notes.length()> 255 ? entry.notes.substring(0,255) : entry.notes.substring(0,entry.notes.length()));
                contactTask.Notes__c = notesStr;
            }

            //if(entry.contactType == AkritivConstants.CONTACT_TYPE_BILLING)
            if(entry.transactionIds == null || entry.transactionIds.size() == 0)
            {
                contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_ACCOUNT;
            }
            else
            {
            }

            //specific case to handle contact customer when no tx are selected. contact type isfollowup
            //and tx are empty but level is set to account as we need cc screen tocome from followup task perfrom
            //button
            if((entry.transactionIds == null || entry.transactionIds.size() == 0) &&
               entry.contactType== AkritivConstants.CONTACT_TYPE_CONTACT)
            {
                contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_TRANSACTION;
            }

            Integer maxComments= 32000;
            contactTask.Description ='';

            if(checkIsPortalUser.get(entry.userId))
            {
                if(entry.comments != null && entry.comments.length() > 0) {
                    String PORTAL_COMMENTS = 'Dispute created for Customer Portal:';
                    maxComments = 31960;
                    contactTask.Description = PORTAL_COMMENTS;
                }
            }

            if(entry.comments != null && entry.comments.length() > 0)
            {
                String taskDesc = (entry.comments.length()> maxComments ? entry.comments.substring(0,maxComments) : entry.comments.substring(0,entry.comments.length()));
                contactTask.Description += taskDesc;
            }

            if(entry.activityDate != null)
                contactTask.ActivityDate = entry.activityDate;
            else
                contactTask.activityDate = Date.today();

            contactTask.Subject = entry.subject;
            contactTask.priority= entry.priority;
            contactTask.status = entry.status;

            contactTask.activity_category__c = entry.category;
            contactTask.calldisposition = entry.callResult;

            tasks.add(contactTask );
        }

        return tasks;

    }
    // param - entries - contact log entry list
    // return - list of Tasks for what contact made
    private Task constructTask(ContactLogEntry entry)
    {
        Task contactTask = new Task();

        contactTask.WhatId = entry.accountId;
        if(entry.contactId != null)
            contactTask.WhoId = entry.contactId;

        if(UtilityController.isPortalUser(entry.userId))
        {
            List<Account> acc = [Select a.OwnerId from Account a where a.id = :entry.accountId];
            if(acc != null && acc.size() > 0)
                contactTask.ownerId = acc.get(0).OwnerId;
        }
        else
        {
            contactTask.ownerId = entry.userId;
        }

        if(entry.activityRating != null)
            contactTask.Rating__c = entry.activityRating;

        //contactTask.contact_method__c = entry.contactMethod;
        contactTask.Type = entry.contactMethod;
        contactTask.Contact_Type__c = entry.contactType;
        
        
       // contactTask.Transaction_List__c = s;
        
        if(entry.notes != null && entry.notes.length() > 0) {
            String notesStr = (entry.notes.length()> 255 ? entry.notes.substring(0,255) : entry.notes.substring(0,entry.notes.length()));
            contactTask.Notes__c = notesStr;
        }

        //if(entry.contactType == AkritivConstants.CONTACT_TYPE_BILLING)
        if(entry.transactionIds == null || entry.transactionIds.size() == 0)
        {
               contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_ACCOUNT;
        }
        else
        {
        }

        //specific case to handle contact customer when no tx are selected. contact type isfollowup
        //and tx are empty but level is set to account as we need cc screen tocome from followup task perfrom
        //button
        if((entry.transactionIds == null || entry.transactionIds.size() == 0) &&
           entry.contactType== AkritivConstants.CONTACT_TYPE_CONTACT)
        {
            contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_TRANSACTION;
        }

        Integer maxComments= 32000;
        contactTask.Description ='';

        if(UtilityController.isPortalUser(entry.userId))
        {
            if(entry.comments != null && entry.comments.length() > 0) {
                String PORTAL_COMMENTS = 'Dispute created for Customer Portal:';
                maxComments = 31960;
                contactTask.Description = PORTAL_COMMENTS;
            }
        }

        if(entry.comments != null && entry.comments.length() > 0)
        {
            String taskDesc = (entry.comments.length()> maxComments ? entry.comments.substring(0,maxComments) : entry.comments.substring(0,entry.comments.length()));
            //#DEL-163
            //By Twinkle Shah
            contactTask.Description = taskDesc.unescapeHtml4();
        }

        if(entry.activityDate != null)
            contactTask.ActivityDate = entry.activityDate;
        else
            contactTask.activityDate = Date.today();

        contactTask.Subject = entry.subject;
        contactTask.priority= entry.priority;
        contactTask.status = entry.status;

        contactTask.activity_category__c = entry.category;
        contactTask.calldisposition = entry.callResult;

        return contactTask;

    }
    // param - transactions - Transaction list
    // insert activity Transaction to generate activity
    private void generateActivityTransaction(Id taskId, List<Transaction__c> transactions) {
        if(taskId != null) {
            Activity_Transaction__c activityTx = new Activity_Transaction__c();
            activityTx.Task_Id__c = taskId;
            activityTx.Transaction_ids__c = TransactionsCollection.getCommaSeparatedTxIds(transactions);
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Activity_Transaction__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
            insert activityTx;
        }
    }
    // param - transactions - Transaction list
    // insert activity Transaction To Prepare Transaction
    private Activity_Transaction__c prepareActivityTransaction(Id taskId, List<Transaction__c> transactions) {
        if(taskId != null) {
            Activity_Transaction__c activityTx = new Activity_Transaction__c();
            activityTx.Task_Id__c = taskId;
            activityTx.Transaction_ids__c = TransactionsCollection.getCommaSeparatedTxIds(transactions);
            System.debug('--------------EditedEmailTemplateBody From prepareActivityTransaction------------'+EditedEmailTemplateBody);
            if( EditedEmailTemplateBody != Null && EditedEmailTemplateBody != '' ){
                        
                activityTx.EmailTemplateBody__c = EditedEmailTemplateBody;
            }
            
            return activityTx;
        }

        return null;
    }
    
     // param - entry - contact log entry
    // return - task contact task
    // insert multiple task for single entery.
    //JIRA Issue #AKTPROD-133
    //By Kruti Tandel
    public List<Task> generateTaskListLogEntries(ContactLogEntry entry)
    {
        List<Task> contactTask = constructTaskList(entry);
        
        
        EditedEmailTemplateBody = entry.comments;
        System.debug('--------------EditedEmailTemplateBody------------'+EditedEmailTemplateBody);
        
        //contactTask.
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert contactTask;
      
        
         Integer i = 0;
        List < Activity_Transaction__c > actxs = new List < Activity_Transaction__c >();
        for ( Task task : contactTask) {

           // Task task = contactTasks.get( i++ );
            if( task != null ) {
                Activity_Transaction__c atx =   prepareActivityTransaction(task.Id, entry.transactionIds);
                
                if ( atx != null ) {
                    
                    actxs.add( atx );
                }
            }
        }

        if ( actxs.size() > 0 ) {
   /* //==========================================================================================  
       Map<String,Schema.SObjectField> m1 = Schema.SObjectType.Activity_Transaction__c.fields.getMap();
       for (String fieldToCheck : m1.keyset()) {
          // Check if the user has create access on the each field
          if (m1.get(fieldToCheck).getDescribe().isCustom() && !m1.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m1.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'checkContactlogGenline382Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
     */
           
            insert actxs;
            System.debug('---------------------actxs-------------------------'+actxs);
        }
        return contactTask;

    }
    
    // param - entries - contact log entry list
    // return - list of Tasks for what contact made
    private List<Task> constructTaskList(ContactLogEntry entry)
    {
         Map<Id,Boolean> checkIsPortalUser = new Map<Id,Boolean>();
        for(User currUser : [select Id, ContactId,Email from User where Email != null]){
            checkIsPortalUser.put(currUser.Id,(currUser!= null && currUser.ContactId != null ? true : false));
            //return (currUser!= null && currUser.ContactId != null ? true : false);)
        }
        
         List<Account> accList = [Select Id,OwnerId from Account where Id in:entry.accountIds];
            if(accList != null && accList.size() > 0){
                for(Account acc:accList){
                   AcountIDOwnerMap.put(acc.Id,acc.OwnerId);
                }
            
            }
        List < Task > tasks = new List < Task >();
        
          
            
        for(Id accountsIds : entry.accountIds){

             Task contactTask = new Task();
             system.debug('---accountsIds --'+accountsIds );
            contactTask.WhatId = accountsIds;
            contactTask.WhoId = entry.contactId;

            if(checkIsPortalUser.get(entry.userId))
            {

              /*  if(entry != null && entry.accountOwnerId != null) {

                    contactTask.ownerId = entry.accountOwnerId;
                }*/
                if(entry != null && accountsIds != null){
                     contactTask.ownerId = AcountIDOwnerMap.get(accountsIds);
                }

            }
            else
            {
                contactTask.ownerId = entry.userId;
            }

            if(entry.activityRating != null)
                contactTask.Rating__c = entry.activityRating;

            //contactTask.contact_method__c = entry.contactMethod;
            contactTask.Type = entry.contactMethod;
            contactTask.Contact_Type__c = entry.contactType;

            if(entry.notes != null && entry.notes.length() > 0) {
                String notesStr = (entry.notes.length()> 255 ? entry.notes.substring(0,255) : entry.notes.substring(0,entry.notes.length()));
                contactTask.Notes__c = notesStr;
            }

            //if(entry.contactType == AkritivConstants.CONTACT_TYPE_BILLING)
            if(entry.transactionIds == null || entry.transactionIds.size() == 0)
            {
                contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_ACCOUNT;
            }
            else
            {
            }

            //specific case to handle contact customer when no tx are selected. contact type isfollowup
            //and tx are empty but level is set to account as we need cc screen tocome from followup task perfrom
            //button
            if((entry.transactionIds == null || entry.transactionIds.size() == 0) &&
               entry.contactType== AkritivConstants.CONTACT_TYPE_CONTACT)
            {
                contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_TRANSACTION;
            }

            Integer maxComments= 32000;
            contactTask.Description ='';

            if(checkIsPortalUser.get(entry.userId))
            {
                if(entry.comments != null && entry.comments.length() > 0) {
                    String PORTAL_COMMENTS = 'Dispute created for Customer Portal:';
                    maxComments = 31960;
                    contactTask.Description = PORTAL_COMMENTS;
                }
            }

            if(entry.comments != null && entry.comments.length() > 0)
            {
                String taskDesc = (entry.comments.length()> maxComments ? entry.comments.substring(0,maxComments) : entry.comments.substring(0,entry.comments.length()));
                //contactTask.Description += taskDesc;
                contactTask.Description = taskDesc.unescapeHtml4();
            }

            if(entry.activityDate != null)
                contactTask.ActivityDate = entry.activityDate;
            else
                contactTask.activityDate = Date.today();

            contactTask.Subject = entry.subject;
            contactTask.priority= entry.priority;
            contactTask.status = entry.status;

            contactTask.activity_category__c = entry.category;
            contactTask.calldisposition = entry.callResult;
            system.debug('---contactTask ----'+contactTask );
            tasks.add(contactTask );
            system.debug('---------------'+tasks);
            }
        

        return tasks;

    }

}