@isTest
private class testMassTransferAccountController {

   static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        MassTransferAccountController mac = new MassTransferAccountController();
        mac.u1Id = null;
      
       // mac.u1Name = Userinfo.getUserName();
        mac.u2Id = Userinfo.getUserId();
        mac.u2Name = Userinfo.getUserName();
        mac.singleMoreFilter1.selectedCondition1 = '!=';
        mac.singleMoreFilter1.selectedaccountCustomFields = 'Name';
        system.assertEquals(mac.singleMoreFilter1.selectedCondition1 ,'!='); 
        mac.singleMoreFilter1.strequals = '';
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);
        mac.searchSelectedaccount();
    }
     static testMethod void myUnitTest() {
        // TO DO: implement unit test
        MassTransferAccountController mac = new MassTransferAccountController();
        mac.u1Id = Userinfo.getUserId();
        mac.u1Name = Userinfo.getUserName();
        mac.u2Id = Userinfo.getUserId();
        mac.u2Name = Userinfo.getUserName();
        mac.singleMoreFilter1.selectedCondition1 = '!=';
        mac.singleMoreFilter1.selectedaccountCustomFields = 'Name';
        mac.singleMoreFilter1.strequals = 'test';
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);
       // mac.searchSelectedaccount();
        mac.singleMoreFilter1.selectedCondition1 = '!=';   
        mac.singleMoreFilter1.selectedaccountCustomFields = 'akritiv__Last_Credit_Review_Date__c';    
        mac.singleMoreFilter1.strequals = '7/7/2010';      
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);      
        mac.searchSelectedaccount();      
        mac.singleMoreFilter1.selectedCondition1 = '<';
        mac.singleMoreFilter1.selectedaccountCustomFields = 'AnnualRevenue';
        mac.singleMoreFilter1.strequals = '200';
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);             
        mac.searchSelectedaccount();
        mac.singleMoreFilter1.selectedCondition1 = '!=';    
        mac.singleMoreFilter1.selectedaccountCustomFields = 'CreatedDate';    
        mac.singleMoreFilter1.strequals = '11/22/2011 11:11 PM';      
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1); 
        system.assertEquals(mac.singleMoreFilter1.selectedCondition1 ,'!='); 
        mac.searchSelectedaccount();   
        mac.singleMoreFilter1.strequals = '11/11/2010'; 
        mac.searchSelectedaccount();
        mac.refreshJobStatus();
      
    }
       static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        MassTransferAccountController mac = new MassTransferAccountController();
        mac.u2Id = null;
       // mac.u1Name = Userinfo.getUserName();
        mac.u1Id = Userinfo.getUserId();
        mac.u1Name = Userinfo.getUserName();
        mac.singleMoreFilter1.selectedCondition1 = '!=';
        system.assertEquals(mac.singleMoreFilter1.selectedCondition1 ,'!='); 
        mac.singleMoreFilter1.selectedaccountCustomFields = 'Name';
        mac.singleMoreFilter1.strequals = '';
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);
        mac.searchSelectedaccount();
    }
    static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        MassTransferAccountController mac = new MassTransferAccountController();
        mac.u2Id = null;
       // mac.u1Name = Userinfo.getUserName();
        mac.u1Id = null;
       // mac.u1Name = Userinfo.getUserName();
        mac.singleMoreFilter1.selectedCondition1 = '!=';
        system.assertEquals(mac.singleMoreFilter1.selectedCondition1 ,'!='); 
        mac.singleMoreFilter1.selectedaccountCustomFields = 'Name';
        mac.singleMoreFilter1.strequals = '';
        mac.moreFilterWrapperList.add(mac.singleMoreFilter1);
        mac.searchSelectedaccount();
    }
}