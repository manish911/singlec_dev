/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Class used to hold the collection of transaction objects.
 */

global class TransactionsCollection {
    /* List of list of transaction*/
    global List<List<Transaction__c>> listOfListOfTransactions {get; set; }

    // constructor1
    global TransactionsCollection(List<List<Transaction__c>> lstLstTx) {
        this.listOfListOfTransactions = lstLstTx;
    }

    // constructor2
    global TransactionsCollection(List<Transaction__c> lstTx) {
        if(lstTx != null) {
            this.listOfListOfTransactions = new List<List<Transaction__c>>();
            this.listOfListOfTransactions.add(lstTx);
        }
    }

    /* return listOfListOfTransactions property */
    global List<List<Transaction__c>> getTransactionsListOfList() {
        return this.listOfListOfTransactions;
    }
    // method is used to get TransactionIds List Of List
    // return type : List<List<Id>>
    // parameters : txCollection - TransactionsCollection
    global static List<List<Id>> getTransactionIdsListOfList(TransactionsCollection txCollection) {
        if(txCollection == null)
            return null;

        List<List<Id>> txIdListOfList = new List<List<Id>>();
        if(txCollection != null) {
            for(List<Transaction__c> txList : txCollection.getTransactionsListOfList()) {
                List<Id> txIdList = new List<Id>();
                for(Transaction__c tx : txList) {
                    txIdList.add(tx.Id);
                }
                txIdListOfList.add(txIdList);
            }
        }
        return txIdListOfList;
    }
    // method is used to get TransactionIds List Of List
    // return type : List<List<Id>>
    // parameters : txIdsList - List < Id >
    global static List<List<Id>> getTransactionIdsListOfList(List<Id> txIdsList) {
        if(txIdsList == null)
            return null;

        List<List<Id>> txIdsListOfList;
        if( txIdsList.size() > 0 && txIdsList != null) {
            txIdsListOfList = new List<List<Id>>();
            txIdsListOfList.add(txIdsList);
        }

        return txIdsListOfList;
    }
    // method is used to get Comma Separated TxIds
    // return type : String
    // parameters : txIdsList - List < Id >
    global static String getCommaSeparatedTxIds(List<Transaction__c> txList) {
        String txIds = '';
        if(txList != null) {
            for(Transaction__c tx : txList)
                txIds += tx.id+',';
        }
        return txIds;
    }
    // method is used to getCommaSeparatedTxIds
    // return type : String
    // parameters : txIdsList - List < Id > 
    global static String getCommaSeparatedTxIds(List<Id> txIdsList) {
        String txIds = '';
        if(txIdsList != null) {
            for(String txId : txIdsList)
                txIds += txId+',';
        }
        return txIds;
    }
}