/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
   *Usage   :   The class is used to update a dispute
 */
public with sharing class UpdateDispute {

    public string keyParam;
    public Dispute__c disputeObj {get; set; }
    public List<Temp_Object_Holder__c> disTempObj;
    public String selectedStatus {get; set; }
    public String selectedType {get; set; }
    public String selectedResolutionCode {get; set; }
    public String noteTitle {get; set; }
    public String noteBody {get; set; }
    public Boolean isUpdate {get; set; }
    Public Boolean isShowResolution {get; set; }
    String[] disIds;
    Map<String,String> statusMap = new Map<String,String>();
   
    public Integer activityRating {get; set; }
    public String parentLoc {get; set; }
    Map<Id,String> AcountIDOwnerMap = new Map<Id,String>();
    public UpdateDispute(){

        // Populate the parameter into separate variables and then escape single quotes to them
        // For security purpose (To avoid XSS hacks).
        String keyUrlParam = Apexpages.currentPage().getParameters().get('key');
        String parentLocParam = Apexpages.currentPage().getParameters().get('location');

        keyParam = (keyUrlParam!=null && keyUrlParam !='' ? String.escapeSingleQuotes(keyUrlParam) : null);
        parentLoc  = (parentLocParam!=null && parentLocParam !='' ? String.escapeSingleQuotes(parentLocParam) : null);

        isUpdate= false;
        activityRating = UtilityController.getRatingStarNumber(AkritivConstants.DEFAULT_ACTIVITY_RATING);
        disTempObj = new List<Temp_Object_Holder__c>();
        disTempObj = [select value__c from Temp_Object_Holder__c where key__c =:keyParam limit 1];
        disIds = new String[] {};
        if(disTempObj.size() > 0)
            disIds = (disTempObj[0].value__c !=null ? disTempObj[0].value__c.replace('\'','').split(',') : null);

        selectedStatus ='None';
        selectedType = 'None';
        isShowResolution = false;
        disputeObj = new Dispute__c();
       
    }

    // property to get the dispute resolutionCodes
    // return type : List<SelectOption>
    public List<SelectOption> resolutionCodes {
        get {
            if(resolutionCodes == null) {
                resolutionCodes = new List<SelectOption>();
                Schema.DescribeFieldResult fieldResult = Dispute__c.Resolution_Code__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    resolutionCodes.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }

            return resolutionCodes;
        }
        set;
    }

    // property to get the dispute types select options
    // return type : List<SelectOption>
    public List<SelectOption> status {
        get {
            if( status== null) {
                status = new List<SelectOption>();
                status.add(new SelectOption('None','No Change'));
                statusMap.put('None','No Change');
                Schema.DescribeFieldResult fieldResult = Dispute__c.Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    status.add(new SelectOption(f.getValue(),f.getLabel()));
                    statusMap.put(f.getLabel(),f.getValue());
                }
            }

            return status;
        }
        set;
    }

    // property to get the dispute types select options
    // return type : List<SelectOption>
    public List<SelectOption> disType {
        get {
            if( disType== null) {
                disType = new List<SelectOption>();
                disType.add(new SelectOption('None','No Change'));
                
                Schema.DescribeFieldResult fieldResult = Dispute__c.Type__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    disType.add(new SelectOption(f.getValue(),f.getLabel()));
                    
                }
            }

            return disType;
        }
        set;
    }
    
    public void disputcatogary(){

        
    }
    // method is used to update Dispute
    // return type : PageReference
    public PageReference updateDispute(){
        List<Task> taskEntries = new List<Task>();
        
        if( selectedStatus !='None' ||  disputeObj.Type__c !='None' || disputeObj.owner__c !=null) {
            List<Dispute__c> lstDisputes =new List<Dispute__c>();
            system.debug('------dispute ids:' + disIds);
            lstDisputes =[select Id,name,Type__c,Sub_Type__c,Status__c,Owner__c,Resolution_Code__c, Notes__c, Note_Date__c, Account__r.Id, Account__r.Name,Transaction__c,Transaction__r.Name from Dispute__c where Id in :disIds];
            
           
            if(lstDisputes.size()>0 && lstDisputes!= null) {
                List<Note> listNotes = new List<Note>();
               
                for(Dispute__C nextDispute : lstDisputes) {
                    
                    if(selectedStatus !='None') {
                        nextDispute.Status__c = selectedStatus;
                        system.debug('----status----' + nextDispute.Status__c);
                        if(selectedStatus =='Closed'){
                            nextDispute.Resolution_Code__c = selectedResolutionCode;
                        }
                        else{
                            nextDispute.Resolution_Code__c ='';
                        }
                    }
             
                    
                    if(disputeObj.Type__c != null){
                        nextDispute.Type__c = disputeObj.Type__c;
                        system.debug('---type1----'+ nextDispute.Type__c );
                        }
                  
                    
                    if(disputeObj.sub_Type__c != null){
                        nextDispute.Sub_Type__c  = disputeObj.sub_Type__c;
                        system.debug('---subtype1----'+ nextDispute.Sub_Type__c );
                        }
                    
                    if(disputeObj.Type__c != null && disputeObj.sub_Type__c == null)
                    {
                        nextDispute.Type__c = disputeObj.Type__c;
                        nextDispute.Sub_Type__c = null;
                    }
                        
                    
                    
                    if( disputeObj.sub_Type__c == 'None' ){   
                             nextDispute.Sub_Type__c = '';
                        }
                    if(disputeObj.owner__c !=null)
                        nextDispute.Owner__c =  disputeObj.Owner__c;
                    
                     //---------Test---------------------By Twinkle Shah---------------For Reportable Notes
                    
                    noteBody = noteBody.trim();
                    system.debug('notebodyfromvf--' + noteBody);
                    String temp1 = DateTime.now().format('MMM');
                    String temp2 = String.valueOf(DateTime.now().day());
                    String temp3 = String.valueOf(DateTime.now().year());
                    
                    if(temp2.length() == 1 ){
                        temp2 = '0'+ temp2;
                    }
                    
                    String temp4 = temp2 + '-' + temp1 + '-' + temp3;
                    if(noteBody == '' || noteBody == null){
                        if(nextDispute.Notes__c == null){
                            nextDispute.Notes__c =  '' +'\n';
                            system.debug('note1---' + nextDispute.Notes__c );}
                        else{
                            nextDispute.Notes__c = nextDispute.Notes__c;
                        }
                    }else{ 
                        if(nextDispute.Notes__c == null){
                            nextDispute.Note_Date__c = Date.Today();
                            nextDispute.Notes__c =  temp4 +' by '+UserInfo.getName()+' - '+ noteBody;
                            system.debug('note2---' + nextDispute.Notes__c );}
                        else{
                            nextDispute.Note_Date__c = Date.Today();
                            nextDispute.Notes__c = temp4 +' by '+UserInfo.getName()+' - '+ noteBody +'\n' + nextDispute.Notes__c;
                        }
                    }
                }
                try {
                    //==========================================================================================  
             list<String> lstFields = new list<String>{'Type__c','Status__c','Owner__c'};
           Map<String,Schema.SObjectField> m1 = Schema.SObjectType.Dispute__c.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
                if(!m1.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m1.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return null;
                  }
                }
            }
        //========================================================================================


                    update lstDisputes;
                    
                    
                    isUpdate = true;
                   system.debug('listDisputes----'+ lstDisputes);
                   
                    //entry into  log
                    
                    for(Dispute__C disputeObj : lstDisputes) {

                        String userName = UserInfo.getUserName();

                        String comments = 'Dispute <' + disputeObj.Name + '>, type of <' + disputeObj.Type__c + '>'
                                          + ' status of <' + disputeObj.status__c;

                        if(selectedStatus =='Closed') {
                            comments = comments + '> Resolution Code is  <'+ disputeObj.Resolution_Code__c;
                        }

                        comments = comments +  '> updated by <'+ userName + '> ';
                        
                        
                        /*********** #AKTPROD-295 Dispute Update Task Comments(Transaction Name Should be Display in comments) ***********/
                        if(disputeObj.transaction__c != null)
                        {
                            comments += ' for Transaction <' +  disputeObj.transaction__r.Name + '>';
                        }
                        else
                        {
                            comments += ' for Account <' +  disputeObj.Account__r.Name + '>';
                        }
                        /*ContactLogEntry entry = new ContactLogEntry(null, disputeObj.account__r.Id, null, AkritivConstants.CONTACT_TYPE_DISPUTE,AkritivConstants.CONTACT_TYPE_OTHER, 'Dispute Update',
                                                                    noteBody, AkritivConstants.CONTACT_LEVEL_ACCOUNT, Userinfo.getUserId(), AkritivConstants.TASK_COMPLETED,ContactLogEntry.PRIORITY_DEFAULT, '',comments, UtilityController.getRatingLabel(activityRating));
                        entry.store();*/
                        
                        /******** #AKTPROD-286 To avoid Too many SOQL queries: 101 ******/
                        Task contactTask = new Task();
                        contactTask.WhatId = disputeObj.account__r.Id;
                        
                        contactTask.ownerId = Userinfo.getUserId();
                        contactTask.Status=AkritivConstants.TASK_COMPLETED;
                        contactTask.Rating__c = UtilityController.getRatingLabel(activityRating);
                        contactTask.Contact_Type__c = AkritivConstants.CONTACT_TYPE_DISPUTE;
                        contactTask.Notes__c = noteBody;
                        contactTask.Contact_Level__c = AkritivConstants.CONTACT_LEVEL_ACCOUNT ;
                        contactTask.Description = comments; 
                        contactTask.Subject= 'Dispute Update';
                        contactTask.Priority=ContactLogEntry.PRIORITY_DEFAULT;
                        contactTask.Note_Title__c ='Dispute Update';
                        contactTask.Type= AkritivConstants.CONTACT_TYPE_OTHER;
                        system.debug('---Task Type---' + contactTask.Type);
                        contactTask.ActivityDate = date.today();
                        contactTask.Rating__c = UtilityController.getRatingLabel(activityRating);
                        TaskEntries.add(contactTask);
                        system.debug('----TaskEntries----' + taskEntries );
                         
                        }
                        if(taskEntries != null){
                            insert taskEntries;
                            system.debug('----TaskEntries----' + taskEntries ); 
                        }
                        Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, Label.Message_Refresh_Dispute_List));
                        return null;

                } catch(Exception ex){
                    ApexPages.addMessages(ex);
                    return null;
                }
         
            
        }
        }else{
            isUpdate = false;
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, Label.Message_No_Changes_Change_Option));
            return null;
        }
      
        return null;
    }
    // method used to show resolution
    // return type : void
    public void showResolution(){
        if(selectedStatus == 'Closed') {
            isShowResolution = true;
        }else{
            isShowResolution=false;
        }
    }

}