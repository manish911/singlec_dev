/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Class will be used to schdule a a bacth job to purge the data.
 */
global class DataPurgeJob implements Schedulable {
    // method is used to execute schedule context 
    // return type : void
    // parameters : sc - SchedulableContext
     global Decimal trxage = ConfigUtilController.getTransactionAge();
    global void execute(SchedulableContext sc) {
        // call the data purge batch job
        BatchPurgeDataJob purgeJob = new BatchPurgeDataJob(trxage);
        Database.executeBatch(purgeJob);
    }
}