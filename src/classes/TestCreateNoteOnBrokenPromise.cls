/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for the Trigger CreateNoteOnBrokenPromise.
 */
@isTest
private class TestCreateNoteOnBrokenPromise {
    static testMethod void testTrigger() {
        // create an test account
        Account acc = DataGenerator.accountInsert();

        // insert a transaction related with the account created above
        List<Transaction__c> tx = new List<Transaction__c>();
        tx= DataGenerator.transactionInsert(acc);

        tx[0].Create_Broken_Promise_Note__c = true;
        update tx[0];

        Test.startTest();
        List<Note> noteCreatedList = [Select n.Title, n.ParentId, n.Body From Note n where parentId=:tx[0].Id ];
        System.assertNotEquals(null, noteCreatedList);

        if(noteCreatedList.size()>0) {
            System.assertEquals('Broken Promise',noteCreatedList.get(0).Title);
//          System.assertEquals('Tx Number: '+tx.Name+'\nTx Amount: '+tx.Amount__c+'\nTx Due Date: '+tx.Due_Date__c, noteCreatedList.get(0).Body);
            Test.stopTest();

        }
    }
}