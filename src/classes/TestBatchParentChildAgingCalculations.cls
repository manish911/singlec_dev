/* 
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for BatchParentChildAgingCalculations.
 */
@isTest
public class TestBatchParentChildAgingCalculations{
    static testMethod void TestBatchParentChildAgingCalculationsClass(){
       
       List<Account> accList = new List<Account>();
       
       Account tempParentAcc = new Account();
       tempParentAcc = insertParentAccount();
       accList.add(tempParentAcc);
       
       Account tempChildAcc = new Account();
       tempChildAcc = insertChildAccount();
       tempChildAcc.ParentId = tempParentAcc.Id;
       update tempChildAcc;
       accList.add(tempChildAcc);
       Test.startTest();
       User u1 = [Select id from user where profile.name =: 'System Administrator' and IsActive = true limit 1];
       System.RunAs(u1){
       Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();
       triggerconf.Enable_Daily_Notes__c=false;
       upsert triggerconf;
       }
       
       BatchParentChildAgingCalculations pcAgingCalc = new BatchParentChildAgingCalculations();
       pcAgingCalc.start(null);
       pcAgingCalc.execute(null,accList);
       pcAgingCalc.finish(null);
       Test.stopTest();
    }
static testMethod void TestBatchParentChildAgingCalculationsClass1(){
      List<Account> accList = new List<Account>();
       
       Account tempParentAcc = new Account();
       tempParentAcc = insertParentAccount();
       accList.add(tempParentAcc);

       Test.startTest();
       User u1 = [Select id from user where profile.name =: 'System Administrator' and IsActive = true limit 1];
       System.RunAs(u1){
       Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();
       triggerconf.Enable_Daily_Notes__c=false;
       upsert triggerconf;
       }
       
       BatchParentChildAgingCalculations pcAgingCalc = new BatchParentChildAgingCalculations();
       pcAgingCalc.start(null);
       pcAgingCalc.execute(null,accList);
       pcAgingCalc.finish(null);
       Test.stopTest();
       
}
    
public static Account insertParentAccount(){
    //create an Parent Account
    Account accObj1 = new Account(Name='testParentAccount');
    accObj1.Account_Key__c =''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
    accObj1.ownerId = UserInfo.getUserId();
    accObj1.Age1__c = 34.85;
    accObj1.Age2__c = 112672.36;
    accObj1.Age3__c = 2210063.62;
    accObj1.Age4__c = 22592409.14;
    accObj1.Age5__c = 110723882.11;
    accObj1.Base_Total_AR__c=11111111;
    accObj1.Base_Total_Past_Due__c = 5665;
    
    insert accObj1;
    return accObj1;
    }   

public static Account insertChildAccount(){
    //create an Child Account
    Account accObj2 = new Account(Name='testChildAccount');
    accObj2.Account_Key__c =''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
    accObj2.ownerId = UserInfo.getUserId();
    accObj2.Age1__c = 15.85;
    accObj2.Age2__c = 2672.36;
    accObj2.Age3__c = 10063.62;
    accObj2.Age4__c = 225409.14;
    accObj2.Age5__c = 1123882.11;
    accObj2.Base_Total_AR__c=5231546;
    accObj2.Base_Total_Past_Due__c = 20165;
    insert accObj2;
    return accObj2;
    }   
}