public with sharing class TaskActivityTransactionController 
{
    Public Task taskObj {get; set;}
    Public String taskComment {get; set;}
    
    public TaskActivityTransactionController(ApexPages.StandardController controller)
    {
        //get Task Record
        taskObj = (Task) Controller.getRecord();
        
        //Select task from the Records
        List<Task> taskList = [Select WhatId, Description, Subject From Task Where Id =: taskObj.Id];
        
        //get task Comment from Task List
        taskComment = taskList == null ? null : taskList.get(0).Description;
    }
}