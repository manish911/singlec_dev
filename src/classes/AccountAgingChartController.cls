/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is used for generating chart from Account aging data.
 */

public with sharing class AccountAgingChartController {

    public AccountWrapper aw;
    public Id stdConAccountId {get; set; }
    public Boolean unbilledsupport {get; set; }
    public Double current {get; set; }
    public Double age1To30 {get; set; }
    public Double age31To60 {get; set; }
    public Double age61To90 {get; set; }
    public Double age91To180 {get; set; }
    public Double ageOver180 {get; set; }
    public RunningStandardDevCalc runstd = new RunningStandardDevCalc();
    public AccountAgingChartController (  Apexpages.Standardcontroller sc ) {

        this.stdConAccountId = sc.getId();
        
        Map<String, Schema.SObjectField> txMap = Schema.SObjectType.Transaction__c.fields.getMap();
        Schema.SObjectField field = txMap.get('BilledUnbilled__c');

        if (field != null  ) {

            unbilledsupport = true;
            getUnBilledBucket( );

        } else {

            unbilledsupport = false;
        }
    }
    // This method displays all Bucket values in chart format for Billed Transaction  
    // The chart which are used to display value are used from Google Visualization API
    // Values are viewed on the basis of BucketConfig_Singlec__c in Custom Setting  
    // Return Type - String 
    // No parameters are passed in this method 
    public String getChart( ) {

        SysConfig_Singlec__c sysconfig = null;
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            sysconfig = SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            sysconfig =  SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        } else {

            sysconfig =  SysConfig_Singlec__c.getOrgDefaults();
        }
        
        //String namespace = sysconfig.NameSpace__c+'__';
        String namespace = '';

        Id AccountId = stdConAccountId;
        String sQuery = '';
        if ( sysconfig.Is_Multi_Currency__c ) {

            sQuery = 'select  CurrencyIsoCode, '+namespace+'Account__c, '+namespace+'Account__r.CurrencyIsoCode, '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }else {

            sQuery = 'select   '+namespace+'Account__c,  '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }

        String billed = 'BILLED';
        sQuery += ' '+namespace+'Due_Date__c, '+namespace+'Promised_Amount__c, '+namespace+'Promise_Date__c, '+namespace+'Disputed_Amount__c, '+namespace+'Undisputed_Balance__c';
        sQuery += ' from '+namespace+'Transaction__c';
        sQuery += ' where '+namespace+'Account__c !=null and '+namespace+'Balance__c != null and '+namespace+'Due_Date__c != null ';

        if ( unbilledsupport ) {

            sQuery += ' and BilledUnbilled__c =:billed';
        }

        sQuery += ' and '+namespace+'Account__c =:AccountId';
        List < Sobject > sObjs = Database.query(sQuery);
        Boolean isupdate = false;
        Integer c = 1;
        Integer d = 1;
        List<Account> accountsToUpdate = new List<Account>();
        for(Sobject txSobject : sObjs) {

            Transaction__c tx = (Transaction__c) txSobject;

            if(tx.Account__c != null && tx.Due_Date__c!= null && tx.Balance__c != null) {


                if(aw == null) {
                    aw = new AccountWrapper(tx.Account__c);
                    addDetails(tx);

                }
                else if(aw.accountId == tx.Account__c) {
                    addDetails(tx);
                }
                else {
                    if(aw != null && aw.accountId != null) {
                        Account acc = new Account(Id = aw.accountId);

                        acc.Age0__c = aw.age0;
                        acc.Age1__c = aw.age1;
                        acc.Age2__c = aw.age2;
                        acc.Age3__c = aw.age3;
                        acc.Age4__c = aw.age4;
                        acc.Age5__c = aw.age5;

                        accountsToUpdate.add(acc);

                        aw = new AccountWrapper(tx.Account__c);
                        addDetails(tx);
                    }
                }
            }
        }

        
        Account acc = finish();

        if(acc != null ) {
            String url;
            if ( acc.Age0__c == 0
                 && acc.Age1__c == 0
                 && acc.Age2__c == 0
                 && acc.Age3__c == 0
                 && acc.Age4__c == 0
                 && acc.Age5__c == 0 ) {

                url =   'http://chart.apis.google.com/chart?chst=d_simple_text_icon_left&chld=Data Not Available|14|000|info|24|000|FFF';
                return url;
            }

            String values =  '&chd=t:';
            String labels =  '&chs=400x100&chl=';
            Boolean prependComma = false;
            Double max = 0;
            if ( acc.Age0__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+acc.Age0__c;
                prependComma =true;
                labels = labels + 'Current('+acc.Age0__c+')|';

                max = acc.Age0__c;
            }
            if ( acc.Age1__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age1__c;
                prependComma =true;
                labels = labels + '1-30('+acc.Age1__c+')|';

                if ( prependComma ) {

                    max = ( max > acc.Age1__c ) ? max : acc.Age1__c;
                }
            }
            if ( acc.Age2__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age2__c;
                prependComma =true;
                labels = labels + '31-60('+acc.Age2__c+')|';

                if ( prependComma ) {

                    max = ( max > acc.Age2__c ) ? max : acc.Age2__c;
                }
            }

            if ( acc.Age3__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age3__c;
                prependComma =true;
                labels = labels + '61-90('+acc.Age3__c+')|';

                if ( prependComma ) {

                    max = ( max > acc.Age3__c ) ? max : acc.Age3__c;
                }
            }

            if ( acc.Age4__c > 0 ) {

                values = values +( prependComma ? ',' : '' )+  acc.Age4__c;
                prependComma =true;
                labels = labels + '91-180('+acc.Age4__c+')|';

                if ( prependComma ) {

                    max = ( max > acc.Age4__c ) ? max : acc.Age4__c;
                }
            }

            if ( acc.Age5__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age5__c;

                labels = labels + 'Over 180('+acc.Age5__c+')|';

                if ( prependComma ) {

                    max = ( max > acc.Age5__c ) ? max : acc.Age5__c;
                }
            }
            String chds = '&chds=0,'+max;
            url = 'http://chart.apis.google.com/chart?cht=p3&chco=0000FF,00FF00&'+chds+values+labels;
            return url;
        }
        return 'http://chart.apis.google.com/chart?chst=d_simple_text_icon_left&chld=Data Not Available|14|000|info|24|000|FFF';

    }
    // This method displays all Bucket values in chart format for Unbilled Transaction
    // The chart which are used to display value are used from Google Visualization API
    // Values are viewed on the basis of BucketConfig_Singlec__c in Custom Setting  
    // Return Type - String 
    // No parameters are passed in this method 
    public String getUnBilledChart( ) {

        SysConfig_Singlec__c sysconfig = null;
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            sysconfig = SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            sysconfig =  SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        } else {

            sysconfig =  SysConfig_Singlec__c.getOrgDefaults();
        }
        String namespace = sysconfig.NameSpace__c+'__';

        Id AccountId = stdConAccountId;
        String sQuery = '';
        if ( sysconfig.Is_Multi_Currency__c ) {

            sQuery = 'select  CurrencyIsoCode, '+namespace+'Account__c, '+namespace+'Account__r.CurrencyIsoCode, '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }else {

            sQuery = 'select   '+namespace+'Account__c,  '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }

        String unbilled = 'UNBILLED';
        if ( unbilledsupport ) {

            sQuery += ' Total_Billed_Unbilled__c, ';
        }

        sQuery += '  '+namespace+'Due_Date__c, '+namespace+'Promised_Amount__c, '+namespace+'Promise_Date__c, '+namespace+'Disputed_Amount__c, '+namespace+'Undisputed_Balance__c';
        sQuery += ' from '+namespace+'Transaction__c';
        sQuery += ' where '+namespace+'Account__c !=null and '+namespace+'Balance__c != null and '+namespace+'Due_Date__c != null ';

        if ( unbilledsupport ) {

            sQuery += ' and BilledUnbilled__c =:unbilled';
        }

        sQuery += ' and '+namespace+'Account__c =:AccountId';

        List < Sobject > sObjs = Database.query(sQuery);

        Boolean isupdate = false;

        List<Account> accountsToUpdate = new List<Account>();
        for(Sobject txSobject : sObjs) {
            Transaction__c tx = (Transaction__c) txSobject;
            if(tx.Account__c != null && tx.Due_Date__c!= null && tx.Balance__c != null) {

                if(aw == null) {
                    aw = new AccountWrapper(tx.Account__c);
                    addUnbilledDetails(tx);
                }
                else if(aw.accountId == tx.Account__c) {
                    addUnbilledDetails(tx);
                }
                else {
                    if(aw != null && aw.accountId != null) {
                        Account acc = new Account(Id = aw.accountId);

                        acc.Age0__c = aw.age0;
                        acc.Age1__c = aw.age1;
                        acc.Age2__c = aw.age2;
                        acc.Age3__c = aw.age3;
                        acc.Age4__c = aw.age4;
                        acc.Age5__c = aw.age5;

                        acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
                        acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;

                        if(aw.stdDevCal.getMean() != null)
                            acc.DPD_Mean__c = aw.stdDevCal.getMean();
                        if(aw.stdDevCal.getStandardDeviation() != null)
                            acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();

                        accountsToUpdate.add(acc);

                        aw = new AccountWrapper(tx.Account__c);
                        addUnbilledDetails(tx);
                    }
                }
            }
        }
        
        Account acc = finish();
        
        // Dispalys the calculated value of aging in chart format using google visualization api 
        if(acc != null ) {
            String url;
            if ( acc.Age0__c == 0
                 && acc.Age1__c == 0
                 && acc.Age2__c == 0
                 && acc.Age3__c == 0
                 && acc.Age4__c == 0
                 && acc.Age5__c == 0 ) {

                url =   'http://chart.apis.google.com/chart?chst=d_simple_text_icon_left&chld=Data Not Available|14|000|info|24|000|FFF';
                return url;
            }
            
            String values =  '&chd=t:';
            String labels =  '&chs=400x100&chl=';
            Boolean prependComma = false;
            Double max = 0;
            if ( acc.Age0__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+acc.Age0__c;
                prependComma =true;
                labels = labels + 'Current('+acc.Age0__c+')|';
                max = acc.Age0__c;
            }
            if ( acc.Age1__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age1__c;
                prependComma =true;
                labels = labels + '1-30('+acc.Age1__c+')|';
                if ( prependComma ) {

                    max = ( max > acc.Age1__c ) ? max : acc.Age1__c;
                }
            }
            if ( acc.Age2__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age2__c;
                prependComma =true;
                labels = labels + '31-60('+acc.Age2__c+')|';
                if ( prependComma ) {

                    max = ( max > acc.Age2__c ) ? max : acc.Age2__c;
                }
            }

            if ( acc.Age3__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age3__c;
                prependComma =true;
                labels = labels + '61-90('+acc.Age3__c+')|';
                if ( prependComma ) {

                    max = ( max > acc.Age3__c ) ? max : acc.Age3__c;
                }
            }

            if ( acc.Age4__c > 0 ) {

                values = values +( prependComma ? ',' : '' )+  acc.Age4__c;
                prependComma =true;
                labels = labels + '91-180('+acc.Age4__c+')|';
                if ( prependComma ) {

                    max = ( max > acc.Age4__c ) ? max : acc.Age4__c;
                }
            }

            if ( acc.Age5__c > 0 ) {

                values = values + ( prependComma ? ',' : '' )+ acc.Age5__c;

                labels = labels + 'Over 180('+acc.Age5__c+')|';
                if ( prependComma ) {

                    max = ( max > acc.Age5__c ) ? max : acc.Age5__c;
                }
            }
            String chds = '&chds=0,'+max;
            url = 'http://chart.apis.google.com/chart?cht=p3&chco=FFFF10,FF0000'+chds+values+labels;
            return url;
        }
        return 'http://chart.apis.google.com/chart?chst=d_simple_text_icon_left&chld=Data Not Available|14|000|info|24|000|FFF';

    }
        
        // This method displays all Bucket values for Unbilled Transaction
        public void getUnBilledBucket( ) {

        SysConfig_Singlec__c sysconfig = null;
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            sysconfig = SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            sysconfig =  SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        } else {

            sysconfig =  SysConfig_Singlec__c.getOrgDefaults();
        }
        String namespace = sysconfig.NameSpace__c+'__';

        Id AccountId = stdConAccountId;
        String sQuery = '';
        if ( sysconfig.Is_Multi_Currency__c ) {

            sQuery = 'select  CurrencyIsoCode, '+namespace+'Account__c, '+namespace+'Account__r.CurrencyIsoCode, '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }else {

            sQuery = 'select   '+namespace+'Account__c,  '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }

        String unbilled = 'UNBILLED';
        if ( unbilledsupport ) {

            sQuery += ' Total_Billed_Unbilled__c, ';
        }

        sQuery += '  '+namespace+'Due_Date__c, '+namespace+'Promised_Amount__c, '+namespace+'Promise_Date__c, '+namespace+'Disputed_Amount__c, '+namespace+'Undisputed_Balance__c';
        sQuery += ' from '+namespace+'Transaction__c';
        sQuery += ' where '+namespace+'Account__c !=null and '+namespace+'Balance__c != null and '+namespace+'Due_Date__c != null ';

        if ( unbilledsupport ) {

            sQuery += ' and BilledUnbilled_Type__c =:unbilled';
        }

        sQuery += ' and '+namespace+'Account__c =:AccountId';

        List < Sobject > sObjs = Database.query(sQuery);
        

        Boolean isupdate = false;

        List<Account> accountsToUpdate = new List<Account>();
        for(Sobject txSobject : sObjs) {
            Transaction__c tx = (Transaction__c) txSobject;
            if(tx.Account__c != null && tx.Due_Date__c!= null && tx.Balance__c != null) {

                if(aw == null) {
                    aw = new AccountWrapper(tx.Account__c);
                    addUnbilledDetails(tx);

                }
                else if(aw.accountId == tx.Account__c) {
                    addUnbilledDetails(tx);
                }
                else {
                    if(aw != null && aw.accountId != null) {
                        Account acc = new Account(Id = aw.accountId);

                        acc.Age0__c = aw.age0;
                        acc.Age1__c = aw.age1;
                        acc.Age2__c = aw.age2;
                        acc.Age3__c = aw.age3;
                        acc.Age4__c = aw.age4;
                        acc.Age5__c = aw.age5;

                        acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
                        acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;

                        if(aw.stdDevCal.getMean() != null)
                            acc.DPD_Mean__c = aw.stdDevCal.getMean();
                        if(aw.stdDevCal.getStandardDeviation() != null)
                            acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();

                        accountsToUpdate.add(acc);

                        aw = new AccountWrapper(tx.Account__c);
                        addUnbilledDetails(tx);
                    }
                }
            }
        }

        
        Account acc = finish();

        current = 0;
        age1To30 = 0;
        age31To60 = 0;
        age61To90 = 0;
        age91To180 = 0;
        ageOver180 = 0;

        if(acc != null ) {

            current = acc.Age0__c;
            age1To30 = acc.Age1__c;
            age31To60 = acc.Age2__c;
            age61To90 = acc.Age3__c;
            age91To180 = acc.Age4__c;
            ageOver180 = acc.Age5__c;

        }
    }
    // This method add details of Billed Transactions in Transaction object
    // Details of all Aging values after running of Aging Calculation are added in the respective field 
    // Aging value is calculated on the basis of Due Date of Transaction and current Date means Days Past Due  
    // Retun Type - Void 
    // Parameters - tx Transacxtion__c  
    public void addDetails(Transaction__c tx){
        
        SysConfig_Singlec__c sysconfig = null;
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            sysconfig = SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            sysconfig =  SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        } else {

            sysconfig =  SysConfig_Singlec__c.getOrgDefaults();
        }

        // to calculate avg pastdue date
        if(tx.Due_Date__c !=null) {
            aw.stdDevCal.push((double)tx.Due_Date__c.daysBetween(Date.Today()));
        }

        // aging calculation job
        Integer dpd = tx.Due_Date__c.daysBetween(Date.Today());
        //get the account crrency and transaction currency , get the converstion rate for both and calculate
        //the converted balance for calculation.

        String accCurrencyCode = (String)(sysconfig.Is_Multi_Currency__c ? tx.Account__r.get('CurrencyIsoCode') : sysconfig.Default_Currency__c ); // tx.akritivM1__Account__r.CurrencyIsoCode;
        String txCurrencyCode =(String)(sysconfig.Is_Multi_Currency__c  ? tx.get('CurrencyIsoCode') : sysconfig.Default_Currency__c ); // tx.CurrencyIsoCode;

        double convertedTxBalance = (tx.Balance__c);

        BucketConfig_Singlec__c agbconf  = null;
        if(BucketConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            agbconf =  BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(BucketConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            agbconf =  BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        // Sets Maximum and Minimum value of Bucket on the basis of value returned in 
        // DPD variable. Also adds value of Balance to the respective Age. 
        if(agbconf != null ) {
            if( dpd <= getBucketByAge(0).maxRange)
                aw.age0+= convertedTxBalance;
            else if( dpd<= getBucketByAge(1).maxRange)
                aw.age1+= convertedTxBalance;
            else if( dpd <= getBucketByAge(2).maxRange)
                aw.age2+= convertedTxBalance;
            else if( dpd <= getBucketByAge(3).maxRange)
                aw.age3+= convertedTxBalance;
            else if( dpd <= getBucketByAge(4).maxRange)
                aw.age4+= convertedTxBalance;
            else
                aw.age5+= convertedTxBalance;
        }

    }
    // This method add details of Unbilled Transactions in Transaction object
    // Details of all Aging values after running of Aging Calculation are added in the respective field 
    // Aging value is calculated on the basis of Due Date of Transaction and current Date means Days Past Due
    // Return Type - Void 
    // Parameters - tx Trnsaction__c 
    public void addUnbilledDetails(Transaction__c tx){
        SysConfig_Singlec__c sysconfig = null;
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            sysconfig = SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            sysconfig =  SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        } else {

            sysconfig =  SysConfig_Singlec__c.getOrgDefaults();
        }

        // to calculate avg pastdue date
        if(tx.Due_Date__c !=null) {
            aw.stdDevCal.push((double)tx.Due_Date__c.daysBetween(Date.Today()));
        }

        // aging calculation job
        Integer dpd = tx.Due_Date__c.daysBetween(Date.Today());
        //get the account crrency and transaction currency , get the converstion rate for both and calculate
        //the converted balance for calculation.

        String accCurrencyCode = (String)(sysconfig.Is_Multi_Currency__c ? tx.Account__r.get('CurrencyIsoCode') : sysconfig.Default_Currency__c ); // tx.akritivM1__Account__r.CurrencyIsoCode;
        String txCurrencyCode =(String)(sysconfig.Is_Multi_Currency__c  ? tx.get('CurrencyIsoCode') : sysconfig.Default_Currency__c ); // tx.CurrencyIsoCode;

        double convertedTxBalance = (tx.Balance__c);
        if ( unbilledsupport ) {

            convertedTxBalance = (Double)tx.get('Total_Billed_Unbilled__c');
        }

        BucketConfig_Singlec__c agbconf  = null;
        if(BucketConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            agbconf =  BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(BucketConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            agbconf =  BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        // Sets Maximum and Minimum value of Bucket on the basis of value returned in 
        // DPD variable. Also adds value of Balance to the respective Age. 
        if(agbconf != null ) {
            if( dpd <= getBucketByAge(0).maxRange)
                aw.age0+= convertedTxBalance;
            else if( dpd<= getBucketByAge(1).maxRange)
                aw.age1+= convertedTxBalance;
            else if( dpd <= getBucketByAge(2).maxRange)
                aw.age2+= convertedTxBalance;
            else if( dpd <= getBucketByAge(3).maxRange)
                aw.age3+= convertedTxBalance;
            else if( dpd <= getBucketByAge(4).maxRange)
                aw.age4+= convertedTxBalance;
            else
                aw.age5+= convertedTxBalance;
        }

    }

    public class AgingBucket {
        public String label {get; set; }
        public Integer minRange {get; set; }
        public Integer maxRange {get; set; }

        public AgingBucket(String l, Integer min, Integer max) {
            this.label = l;
            this.minRange = min;
            this.maxRange = max;
        }
    }
    // This method returns value of Aging Bucket on the basis of Days Past Due for Account
    // The value of Bucket is classified on the Aging Calculation which is done in Aging calculation Job
    // Return Type - AgingBucket class
    // Parameter - age Integer 
    public static AgingBucket getBucketByAge(Integer age) {
        BucketConfig_Singlec__c abc = getAgingBucketConfigObj();
    
    // Returns Minimum and Maximum range of Bucket on the basis of 
    //User ID or Profile Id returned in method getAgingBucketConfigObj 
        if(abc != null) {
            if(age == 0 ) //2147483647
                return new AgingBucket('Current',-2147483646,0);
            else if(age == 5)
                return new AgingBucket('Over ' + abc.Age4__c.intValue(), (abc.Age4__c).intValue()+1,2147483647);
            else if(age == 1)
                return new AgingBucket('1-'+abc.Age1__c.intValue(), 1, abc.Age1__c.intValue());
            else if(age == 2)
                return new AgingBucket(abc.Age1__c.intValue()+1+'-'+abc.Age2__c.intValue(), abc.Age1__c.intValue()+1, abc.Age2__c.intValue());
            else if(age == 3)
                return new AgingBucket(abc.Age2__c.intValue()+1+'-'+abc.Age3__c.intValue(), abc.Age2__c.intValue()+1, abc.Age3__c.intValue());
            else if(age == 4)
                return new AgingBucket(abc.Age3__c.intValue()+1+'-'+abc.Age4__c.intValue(), abc.Age3__c.intValue()+1, abc.Age4__c.intValue());
        }

        return null;
    }
    // Assigns calculated value of ages to fields created in Transaciton
    // Differentiate transactions in different categories of an Account 
    // The calculated value is finally assigned to ages of that transaction
    // Return Type - Account 
    // No parameters are passed in this method 
    public Account finish(){
        if(aw != null && aw.accountId != null) {
            Account acc = new Account(Id = aw.accountId);
            acc.Age0__c = aw.age0;
            acc.Age1__c = aw.age1;
            acc.Age2__c = aw.age2;
            acc.Age3__c = aw.age3;
            acc.Age4__c = aw.age4;
            acc.Age5__c = aw.age5;

            aw = null;

            return acc;
        }

        return null;
    }
    // This method returns the value of Bucket Config object on the basis of User or Profile
    // The values of BucketConfig_Singlec__c are returned on the basis of user or which profile is logged in
    // Return Type - akritiv__BucketConfig_Singlec__c (Custom Setting)
    // No parameters are passed in this method  
    public static BucketConfig_Singlec__c getAgingBucketConfigObj() {
        if(BucketConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(BucketConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        return BucketConfig_Singlec__c.getOrgDefaults();
    }

    public class AccountWrapper {
        public ID accountId {get; set; }
        public Double age0 {get; set; }
        public Double age1 {get; set; }
        public Double age2 {get; set; }
        public Double age3 {get; set; }
        public Double age4 {get; set; }
        public Double age5 {get; set; }
        public Transaction__c highPastdueTx {get; set; }
        public Transaction__c leadPastdueTx {get; set; }
        public RunningStandardDevCalc stdDevCal {get; set; }

        public AccountWrapper(Id accId) {
            this.accountId = accId;
            stdDevCal = new RunningStandardDevCalc();
            age0 = 0;
            age1 = 0;
            age2 = 0;
            age3 = 0;
            age4 = 0;
            age5 = 0;
        }
    }

    //
    // Class to calculate standard deviation accurately in one pass.
    //
    public class RunningStandardDevCalc {
        private Integer n;
        private Double oldM, newM, oldS, newS;

        public RunningStandardDevCalc() {
            n=0;
        }
    //  Method to clear to all data before Standard Deviation Calculation
    // Return Type - Void 
    // No parameters are passed in this method
        public void Clear() {
            n = 0;
        }
    // Method use for calculating value of average Due Date for Billed and Unbilled transaction
    // Return Type - Void 
    // Parameter - x Double 
        public void push(double x){
            n++;
            if (n == 1) {
                oldM = newM = x;
                oldS = 0.0;
            }
            else {
                newM = oldM + (x - oldM)/n;
                newS = oldS + (x - oldM)*(x - newM);
                // set up for next iteration
                oldM = newM;
                oldS = newS;
            }
        }
    // Returns the value of variable n which is currently set after push method is called
    // Return Type - Integer 
    // No parameter are passed in this method 
        public Integer getNumDataValues() {
            return n;
        }
    // Returns value of newly calculated mean in push method
    // Return Type - Double 
    // No parameters are passed in htis method
        public Double getMean() {
            return (n > 0) ? newM : 0.0;
        }
    // Returns the value of Variance calculated on the Due Date  
    // Return Type - Double
    // No parameters passed in this method 
        public Double getVariance() {
            return ( (n > 1) ? newS/(n - 1) : 0.0 );
        }
    // Returns the Average Standard Dveiation for current Due Date of that Account
    // Return Type - Double 
    // No Parameters are parameters are passed in this method 
        public Double getStandardDeviation() {
            return Math.sqrt( getVariance() );
        }
    }
    

          
}