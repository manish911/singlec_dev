/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   test class for AfterInsertFullDataLoadBatch
 */
@isTest
private class TestAfterInsertFullDataLoadBatch {
    static testMethod void testTrigger() {
        // create an test account
        Account acc = DataGenerator.accountInsert();

        //Tx List On Account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        Data_Load_Batch__c dlb = new Data_Load_Batch__c();
        dlb.Run_Batch_Aging__c = true;
        dlb.Load_Type__c = 'FULL';
        System.assertEquals(dlb.Load_Type__c,'FULL');
        dlb.Batch_Number__c = '9999999';
        dlb.Source_System__c = '888888';
        insert dlb;

        dlb.Run_Batch_Aging__c = false;
        update dlb;
    }
     static testMethod void testTrigger1() {
        // create an test account
        Account acc = DataGenerator.accountInsert();

        //Tx List On Account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        Data_Load_Batch__c dlb = new Data_Load_Batch__c();
        dlb.Run_Batch_Aging__c = true;
        dlb.Result__c = 'ERROR';
        dlb.Load_Type__c = 'ERROR';
        System.assertEquals(dlb.Run_Batch_Aging__c,true);
        dlb.Batch_Number__c = '9999999';
        dlb.Source_System__c = '888888';
        insert dlb;

        dlb.Run_Batch_Aging__c = false;
        update dlb;
    }
}