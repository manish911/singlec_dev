/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for CommonAjaxController.
 */
@isTest
private class TestCommonAjaxController {
    static testMethod void testCase1(){
        Account accObj = new Account();

        Id accId = accObj.Id;

        ApexPages.StandardController apexObj = new ApexPages.StandardController (accObj);
        CommonAjaxController controller = new CommonAjaxController();

        List<Contact> con =controller.getRecords('testcontact');

        //Task tsk = new Task();
        //tsk.AccountId=accId;
        //insert tsk;

        Temp_Object_Holder__c tmpObj = new Temp_Object_Holder__c();
        tmpObj.Key__c='testkey';
        insert tmpObj;

        String tskey=tmpObj.Key__c;
        Test.startTest();

        // System.assertEquals(null,controller.doGet());
        System.assertEquals(null,controller.jsonString);

        controller.doGet();

        ApexPages.currentPage().getParameters().put('recordtype','testrecord');
        ApexPages.currentPage().getParameters().put('accId',accId);

        ApexPages.currentPage().getParameters().put('taskkey','testTask');

        // controller.getAccountDetail(accId);

        controller.getResult();

        // controller.getMyTasks(tskey);

        Test.stopTest();

    }
}