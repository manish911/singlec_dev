/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This Test class for AccountAgingChartController.
 */

@istest(seeAllData = true)
public with sharing class AccountAgingChartControllerTest{
static testMethod void testAging() {
        
        Account a = [ select Id from account where Account_Key__c != null limit 1 ];
        
        ApexPages.StandardController controller = new ApexPages.StandardController(a);
     
        Test.startTest();
            AccountAgingChartController con = new AccountAgingChartController(controller);
            String url = con.getChart();
            System.assert( con.aw == null );
            String url2 = con.getUnBilledChart();
            
            
          
            AccountAgingChartController.getBucketByAge(0);
            con.getUnBilledBucket();
                       
            con.runstd.Clear();
            con.runstd.getNumDataValues();
            con.runstd.getMean();
            con.runstd.getVariance();
            con.runstd.getVariance();
            con.runstd.getStandardDeviation();
            
                       
            
        Test.stopTest();
    }
    
    public static testMethod void testCase1(){
        
        Account tempAcc = DataGenerator.accountInsert();
        Account tempAcc1 = DataGenerator.accountInsert();       
        ApexPages.StandardController controller = new ApexPages.StandardController(tempAcc);        
        List<Transaction__c> txList = new List<Transaction__c>();        
        
        Test.startTest();
        AccountAgingChartController con = new AccountAgingChartController(controller);

        //con.aw = new AccountAgingChartController.AccountWrapper(tempAcc.Id);
        //con.aw.age0 = 1;        
        txList = DataGenerator.transactionInsert(tempAcc,1);
        system.assertEquals(txlist.get(0).account__c,tempAcc.Id);
        String url = con.getChart();
        String url2 = con.getUnBilledChart();
        con.getUnBilledBucket();
        
        AccountAgingChartController con2 = new AccountAgingChartController(controller);
        con2.aw = new AccountAgingChartController.AccountWrapper(tempAcc1.Id);        
        
        txList = DataGenerator.transactionInsert(tempAcc,1);
        String url3 = con2.getChart();
        String url4 = con2.getUnBilledChart();
        con2.getUnBilledBucket();
        
        AccountAgingChartController con3 = new AccountAgingChartController(controller);

        con3.aw = new AccountAgingChartController.AccountWrapper(tempAcc.Id);
        con3.aw.age0 = 1; 
        con3.aw.age1 = 1; 
        con3.aw.age2 = 1; 
        con3.aw.age3 = 1; 
        con3.aw.age4 = 1;        
        con3.aw.age5 = 1;        
        txList = DataGenerator.transactionInsert(tempAcc,1);
        system.assertEquals(txlist.get(0).account__c,tempAcc.Id);
        String url5 = con3.getChart();
        String url6 = con3.getUnBilledChart();
        con3.getUnBilledBucket();
        
        AccountAgingChartController con4 = new AccountAgingChartController(controller);

        con4.aw = new AccountAgingChartController.AccountWrapper(tempAcc1.Id);              
        con4.getUnBilledChart();
        con4.getUnBilledBucket();
        
        AccountAgingChartController con5 = new AccountAgingChartController(controller);
        con5.aw = new AccountAgingChartController.AccountWrapper(tempAcc.Id);
        con5.aw.age0 = 1; 
        con5.aw.age1 = 1; 
        con5.aw.age2 = 1; 
        con5.aw.age3 = 1; 
        con5.aw.age4 = 1;        
        con5.aw.age5 = 1; 
        txList = DataGenerator.transactionInsert(tempAcc,1); 
        con5.getUnBilledChart();
        con5.getUnBilledBucket();
        
        AccountAgingChartController con6 = new AccountAgingChartController(controller);

        con6.aw = new AccountAgingChartController.AccountWrapper(tempAcc1.Id);                      
        con6.getUnBilledBucket();
        
        Test.stopTest();
        
    }
}