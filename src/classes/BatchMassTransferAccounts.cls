/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   batch job to Mass Transfer Account.
 */

global class BatchMassTransferAccounts implements Database.Batchable<SObject>, Database.Stateful{
    /* Query that will get the list of objects to maintain the batches*/
    global String query;
    global String toUserId;
    // globall Constructor which will take the query string as argument
    global BatchMassTransferAccounts(String whereCondtion,String fromUser,String toUser) {
      //  String namespace =   (UtilityController.getAkritivPackageNameSpace()=='')?'':(UtilityController.getAkritivPackageNameSpace()+'__');
        String namespace = '';
        toUserId = toUser;
       
       /**start Namespace for migrating to unmanaged package  **/
        if ( UtilityController.isMultiCurrency() ) {
             if(whereCondtion != '')
                 this.query = 'select CurrencyIsoCode ,Id,ownerId from Account where '+ whereCondtion +' AND ownerId = \''+ fromUser +'\'';
             else
                 this.query = 'select CurrencyIsoCode ,Id,ownerId from Account where ownerId = \''+ fromUser +'\'';
            

        } else {
             if(whereCondtion != '')
                 this.query = 'select Id,ownerId from Account where '+ whereCondtion +' AND ownerId = \''+ fromUser +'\'';
             else
                 this.query = 'select Id,ownerId from Account where ownerId = \''+ fromUser +'\'';
        }

       
        
    }
    // start method implementation of Bachable
    // Return Type - QueryLocator method in Database class
    // Parameters - BC Database.Batchablecontext 
    global Database.Querylocator start(Database.BatchableContext BC) {
        
        return Database.getQueryLocator(query);
    }
    
    // execute method implementation this will set the transaction balance to zero
    // Return Type - Void 
    // Parameter - BC Database.Batchablecontext scope List<Sobject>
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        List<Account>accList = (List<Account>) scope;
        for(Account acc: accList) {
            acc.ownerId = toUserId; 
        }
        update accList;
    }

    // finish method implementation of  database.batchable
    // Return Type - Void 
    // Parameter BC Database.Batchablecontext
    global void finish(Database.BatchableContext BC) { 
    }
}