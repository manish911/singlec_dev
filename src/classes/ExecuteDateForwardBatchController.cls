/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This is class used for Execute batch jobs refresh Batchjobs
 *          and date forward Execute Batchjobs.
 */
public with sharing class ExecuteDateForwardBatchController {

    public Boolean isCompleted { get; set; }
    public ID batchprocessid {get; set; }
    public AsyncApexJob aaj {get; set; }
    public Integer progress {get; set; }
    public Integer daysToForward {get; set; }
    
    public ExecuteDateForwardBatchController() {
        progress = 0;
        isCompleted = true;
		aaj = new AsyncApexJob();
       
        List<BatchJobLog__c> entry = [Select id, name, last_run_date__c from batchjoblog__c where name ='DateForwardBatchJob' order by last_run_date__c desc Limit 1];
        Integer i = 0;
        if (entry.size()>0 && entry.get(0).last_run_date__c != null) {
            i = entry.get(0).last_run_date__c.daysBetween(Date.today());
        }
        daysToForward = i;
    }
    // method is used to execute batch update 
    // return type : Pagereference
    public Pagereference executeBatchUpdate() {
        try {
            progress = 0;
            isCompleted = false;
            DateForwardBatchJob bca = new DateForwardBatchJob();

            bca.daysToForward = daysToForward;

            batchprocessid = Database.executeBatch(bca);
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Batch successfully updated'));
        }
        catch (Exception e) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Error:'+e.getMessage()));
        }
        return null;
    }
    // method is used to refresh Job status 
    // return type : Pagereference
    public Pagereference refreshJobStatus() {
        if(batchprocessid != null)
            aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid ];
            
        if(aaj != null && aaj.TotalJobItems != null && aaj.TotalJobItems != 0 && aaj.JobItemsProcessed != null) {
            
                progress = Integer.valueOf(aaj.JobItemsProcessed*100/aaj.TotalJobItems);
        }

        if(progress == 100)
            isCompleted = true;

        return null;
    }
}