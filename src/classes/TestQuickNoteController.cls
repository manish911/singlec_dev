/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for QuickNoteController
 */

@isTest
private class TestQuickNoteController {

    // To test the ui on the sidebar component
    static testmethod void runTest1() {
        Id currentUserId = UserInfo.getUserId();

        QuickNoteController controller = new QuickNoteController();

        // create some instances of user_wall_message__c using this controller
        controller.prepareNewMessage();
        controller.selectedToUserIds = new String[] {currentUserId};
        controller.newOrReplyMsg.Message_Body__c = 'TestMessage';
        controller.createWallMessage();
        /*User u = [Select id from user where id = '005A0000004z6og'];
        System.RunAs(u){
            controller.markMessageVisited();
        }
        */
        //
        //controller.newOrReplyMsg.Add_To_Note__c=true;
        //controller.newOrReplyMsg.Related_Objects__c='testtest';
        //controller.createWallMessage();
        controller.markMsgVisitedPageId = 'pg:frm:someId:0:markReadMsgId';
        controller.markMessageVisited();
        /*User u = [Select id from user where id = '005A0000004z6og'];
        System.RunAs(u){
            controller.markMessageVisited();
        }*/

        controller.addTaskPageId = 'pg:frm:someId:0:addTaskPgId';
        controller.addMessageTask();
        system.assertEquals(controller.addTaskPageId,'pg:frm:someId:0:addTaskPgId');
        controller.replyToPageId = 'pg:frm:someId:0:replyMsgPgId';
        controller.prepareReplyMessage();
        system.assertEquals(controller.replyToPageId,'pg:frm:someId:0:replyMsgPgId');

        controller.getAllUsers();
        controller.getMessagesRefreshed();
        controller.cancel();
    }

    // To test the send Im button functionality on the account detail page
    static testmethod void runTest2() {
        Id currentUserId = UserInfo.getUserId();

        Account acc = DataGenerator.accountInsert();

        ApexPages.currentPage().getParameters().put('objIds', acc.Id);
        ApexPages.currentPage().getParameters().put('objtype', 'Account');

        QuickNoteController controller = new QuickNoteController();
        controller.prepareObjectIM();

        controller.selectedToUserIds = new String[] {currentUserId};
        controller.newOrReplyMsg.Message_Body__c = 'TestMessage';
        controller.createWallMessage();

        controller.addTaskPageId = 'pg:frm:someId:0:addTaskPgId';
        controller.addMessageTask();
        system.assertEquals(controller.addTaskPageId,'pg:frm:someId:0:addTaskPgId');
        controller.msgPageToDeleteId= 'pg:frm:someId:0:deleteMsgPgId';
        controller.deleteWallMessage();

        controller.saveIMMessage();
    }

    // To test the send Im button functionality on the transaction detail page
    static testmethod void runTest3() {
        Id currentUserId = UserInfo.getUserId();

        Account acc = DataGenerator.accountInsert();

        List<Transaction__c> tx = DataGenerator.transactionInsert(acc);

        ApexPages.currentPage().getParameters().put('objIds', tx[0].Id);
        ApexPages.currentPage().getParameters().put('objtype', 'Transaction__c');

        QuickNoteController controller = new QuickNoteController();
        controller.prepareObjectIM();

        controller.selectedToUserIds = new String[] {currentUserId};
        controller.newOrReplyMsg.Message_Body__c = 'TestMessage';
        controller.createWallMessage();

        controller.addTaskPageId = 'pg:frm:someId:0:addTaskPgId';
        controller.addMessageTask();
        system.assertEquals(controller.addTaskPageId,'pg:frm:someId:0:addTaskPgId');
        controller.msgPageToDeleteId= 'pg:frm:someId:0:deleteMsgPgId';
        controller.deleteWallMessage();
        string s=controller.getQuickNoteTitleMsg();
        controller.saveIMMessage();
    }
   
}