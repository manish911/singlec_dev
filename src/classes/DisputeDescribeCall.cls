/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class  generates metadata of Dispute object.
 */

global with sharing class DisputeDescribeCall {

	static Map<String,FieldDescibe> objectDetails = new Map<String,FieldDescibe>();
	static Set<String> existingFields;
	static Set<String> dpFields;

	static {

		Map<String, Schema.SObjectField> FieldsMap = Schema.SObjectType.Dispute__c.fields.getMap();
		existingFields = FieldsMap.keySet();

		dpFields = new Set<String>();
		//exclude system fields for dispute and name field
		for(String fieldName : existingFields)
		{
			if( fieldName !='CreatedById'
			    && fieldName !='CreatedDate' && fieldName !='LastModifiedById'
			    && fieldName !='LastModifiedDate' && fieldName != 'SystemModstamp'
			    && fieldName !='CurrencyIsoCode' &&  fieldName !='id'
			    &&  fieldName !='lastactivitydate' &&  fieldName !='isdeleted'
			    &&  fieldName !='ConnectionReceivedId' &&  fieldName !='ConnectionSentId'
			    && (!(fieldName.contains('without_sign')))
			    )
			{

				dpFields.add(fieldName);
			}
		}

		for(String nextField : existingFields ) {

			Schema.SObjectField field = FieldsMap.get(nextField);
			Schema.DescribeFieldResult fieldDesc = field.getDescribe();

			FieldDescibe fieldDescribeObj = new FieldDescibe();
			fieldDescribeObj.fieldName = nextField;
			fieldDescribeObj.fieldLabel = fieldDesc.getLabel();
			fieldDescribeObj.fieldType =  fieldDesc.getType();
			fieldDescribeObj.fieldNamespaceName =  fieldDesc.getName();

			objectDetails.put(nextField,fieldDescribeObj);

		}
	}
	
	// method is used to get field data 
	// return type : FieldDescribe
	// parameters : fieldName - String
	global static FieldDescibe getFieldData(String fieldName){

		if(objectDetails.get(fieldName.toLowerCase()) !=null) {
			return objectDetails.get(fieldName.toLowerCase());
		}
		else
			return null;
	}
	
	// method is used to get all field 
	// return type : Set<String>
	global Static Set<String> getAllFields(){
		return dpFields;
	}

	global class FieldDescibe {

		global String fieldName {get; set; }
		global String fieldLabel {get; set; }
		global Schema.Displaytype fieldType {get; set; }
		global String fieldNamespaceName {get; set; }

	}

}