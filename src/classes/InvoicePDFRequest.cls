/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: InvoicePDFRequest class called in ContactCustomerController.
 *        Used for displaying the list of
 *        selected transaction in template selected by User
 */
public with sharing class InvoicePDFRequest {

	public static String REQUEST_TYPE_EMAIL = 'Email Template';
	public static String REQUEST_TYPE_VF = 'Visualforce Page';
	public static String REQUEST_TYPE_URL = 'URL';
	public static String REQUEST_TYPE_HTML = 'HTML';
	public static String REQUEST_TYPE_CUSTOM = 'Custom Template';
	public static String KEY_FOR_CONTENTS = 'KEY_FOR_CONTENTS';

	public String requestType {get; set; }
	public String requestValue {get; set; }
	public String accountId {get; set; }
	public String transctionKey {get; set; }
	public String fileName {get; set; }
	public Boolean bundled {get; set; }
	public Map      < String, String > params = new Map<String, String >();

}