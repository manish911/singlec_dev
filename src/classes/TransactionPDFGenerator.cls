/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class creates PDF  for selected Transactions
 */

global virtual class TransactionPDFGenerator {

    global Boolean showDetails {get; set; }
    global Boolean hasErrorMesssages {get; set; }
    global String accountId {get; set; }
    global String orgLogo {get; set; }
    global String txsAdditionalFilter {get; set; }
    global String lineItemAdditionalFilter {get; set; }

    global TransactionPDFGenerator() {
        try {

            SysConfig_Singlec__c sysConfObj = new  SysConfig_Singlec__c();

            if(ConfigUtilController.getSystemConfigObj() !=null) {
                sysConfObj = ConfigUtilController.getSystemConfigObj();
                if(sysConfObj.Org_Logo__c  !=null && sysConfObj.Org_Logo__c !='') {
                    orgLogo = sysConfObj.Org_Logo__c;
                }
            }

        }
        catch(Exception ex) {
            hasErrorMesssages = true;
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Exception occured while processing your request. Please contact your support team.\nError Detail:'+ex.getMessage()));
        }
    }
    //method to get List Of Transactions
    //return type : List<Transaction__c>
    // parameters : txIds - String[]
    global List<Transaction__c> getLstOfTxs(String[] txIds){

        List<Transaction__c> lstTransactions = new List<Transaction__c>();

        if( txIds!=null && txIds.size()>0 ) {

            String queryWithTxFields='select ';

            //get tx fields
            for(String fieldName : TransactionDescribeCall.getAllFields())
            {
                queryWithTxFields = queryWithTxFields + fieldName + ',';
            }

            queryWithTxFields = queryWithTxFields.substring(0,queryWithTxFields.length()-1);
            queryWithTxFields = queryWithTxFields + ' from Transaction__c where Transaction_Key__c != null and Id in:txIds ';
            if(txsAdditionalFilter !=null && txsAdditionalFilter!='') {
                queryWithTxFields = queryWithTxFields + ' ' +  string.escapeSingleQuotes(txsAdditionalFilter);
            }
            lstTransactions = Database.query(queryWithTxFields);
        }
        return lstTransactions;

    }

    //method to get Map Of Transactions With Line Items
    //return type : Map<ID,List<Line_Item__c>>
    // parameters : txIds - String[]
    global Map<ID,List<Line_Item__c>>  getMapOfTxsWithLineItems(String[] txIds){

        Map<ID,List<Line_Item__c>> mapTrasactionLineItems =  new Map<ID,List<Line_Item__c> >();
        if( txIds!=null && txIds.size()>0 ) {
            Set<ID> setTrasactionID =  new Set<ID>();
            for(Transaction__c nextTransObj : getLstOfTxs(txIds)) {
                setTrasactionID.add(nextTransObj.id);
            }

            String queryWithTxLineItem ='select ';
            //get line item fields
            for(String fieldName : TransactionDescribeCall.getAllLineItemFields()) {
                queryWithTxLineItem = queryWithTxLineItem +  fieldName + ',';
            }

            queryWithTxLineItem = queryWithTxLineItem.substring(0,queryWithTxLineItem.length()-1);

            queryWithTxLineItem = queryWithTxLineItem + ' from Line_Item__c where Transaction__c in : setTrasactionID ';

            if(lineItemAdditionalFilter !=null && lineItemAdditionalFilter !='') {
                queryWithTxLineItem = queryWithTxLineItem +  ' ' + lineItemAdditionalFilter;
            }
            //queryWithTxLineItem = queryWithTxLineItem + ' Limit 1000';
            //no limit to collection, query limit imposed
            queryWithTxLineItem = queryWithTxLineItem + ' Limit 10000';

            queryWithTxLineItem = String.escapeSingleQuotes(queryWithTxLineItem);
            for(Line_Item__c lineObj : Database.query(queryWithTxLineItem) ) {

                if(mapTrasactionLineItems.containsKey(lineObj.Transaction__c)) {
                    mapTrasactionLineItems.get(lineObj.Transaction__c).add(lineObj);
                }
                else {
                    List<Line_Item__c> lstLineIteams = new List<Line_Item__c>();
                    lstLineIteams.add(lineObj);
                    mapTrasactionLineItems.put(lineObj.Transaction__c,lstLineIteams);
                }
            }

        }
        return mapTrasactionLineItems;
    }

}