/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class is used to purge the all the data that is considered as garbage in the org.
            It will delete transactions, line Items, notes, attachments with certain criteria.
*/
global class BatchPurgeDataJob implements Database.Batchable<Sobject> {
    
      global Decimal trxage = ConfigUtilController.getTransactionAge();
      global Decimal daysPast;

    global BatchPurgeDataJob(Decimal ctrxage){
        if(ctrxage != null ){
            daysPast = ctrxage ;
        }
        else{
            daysPast = trxage;
        }
    }
    global Database.queryLocator start(Database.BatchableContext bc){
       // 
       // date d = date.today() - (date)close_date__c;   
       
       date d = date.today() -  (Integer)dayspast;
       //String temp = 'close_date__c <=' + d;
       List<String> datetimestamp = (d + ' ').split(' ');
       List<String> dateStamp = datetimestamp.get(0).split('-');

       String dateformatstamp = dateStamp.get(0) + '-' + dateStamp.get(1) + '-' + dateStamp.get(2);

       String query = 'select Id,Create_date__c, Due_Date__c, close_date__c, Balance__c from Transaction__c where Transaction_Key__c != null and Balance__c=0 and close_date__c < = ' + dateformatstamp + ' order by Batch_Number__c';
       
     //   date d = date.today() + (Integer)dayspast;
     //   String query = 'select Id,Create_date__c, Due_Date__c, close_date__c, Balance__c from Transaction__c where Transaction_Key__c != null and Balance__c=0 and close_date__c != null order by Batch_Number__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Sobject> scope) {
        List<Transaction__c> txList = (List<Transaction__c>)scope;
        Set<Id> txListToDelete = new Set<Id>();
        Set<Id> lineItemsToDelete = new Set<Id>();
        Set<Id> AttachmentsToDelete = new Set<Id>();
        
        
        Integer invage=0;
        for(Transaction__c tx : txList) {
            invage= tx.Close_Date__c.daysBetween(Date.Today());
           // if(invage != null && trxage != null && trxage <= invage){
               if(invage != null && daysPast != null && daysPast <= invage){
               txListToDelete.add(tx.Id);
            }
        }
        
        // Delete line Items for transaction Deleted
        Boolean allLineItemsDeleted = false;
        while(!allLineItemsDeleted) {
            List<Line_Item__c> lineItemsLst = [select Id from Line_Item__c where Line_Key__c !=  null and Transaction__c in:txListToDelete limit 10000];
            if(lineItemsLst.size()>0)
                delete lineItemsLst;
            else
                allLineItemsDeleted = true;
        }

        // Delete disputes for transaction Deleted
        Boolean AllDisputesDeleted = false;
        while(!AllDisputesDeleted) {
            List<Dispute__c> disputesLst = [select Id from Dispute__c where Account__c != null and Transaction__c in:txListToDelete limit 10000];
            if(disputesLst.size()>0)
                delete disputesLst;
            else
                AllDisputesDeleted = true;
        }

        // Delete all related attachments
        Boolean AllAttachDeleted = false;
        while(!AllAttachDeleted) {
            List<Attachment> attachLst = [select Id from Attachment where ParentId in:txListToDelete limit 10000];
            if(attachLst.size()>0)
                delete attachLst;
            else
                AllAttachDeleted = true;
        }

        // Delete transaction list
        if(txListToDelete.size()>0)
            delete [select Id from Transaction__c where id in : txListToDelete ];
    }

    global void finish(Database.BatchableContext bc){
        // also delete line items with criteria
        // Line.Transaction.Balance = 0 & (Today() - Transaction.close_date) > 7
        Date Days7BeforeToday = Date.today()-7;
        Boolean allLineItemsDeleted = false;
        while(!allLineItemsDeleted) {
            //changed limit from 1K to 10K , limit of sql query
            List<Line_Item__c> liList = [select Id from Line_Item__c where Line_Key__c !=  null and Transaction__r.Balance__c=0 and Transaction__r.Close_Date__c < : Days7BeforeToday limit 10000];
            if(liList.size()>0)
                delete liList;
            else
                allLineItemsDeleted = true;
        }
    }
}