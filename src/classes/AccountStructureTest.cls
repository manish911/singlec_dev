/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This is class AccountStructureTest.
 */

@istest(seeAllData=true)
public with sharing class AccountStructureTest{
    
    static testMethod void TestIt() {
    
        Account tempAccount = AccountStructure.accountInsert();
        list<User> listu1 = [Select id from user where Profile.name='Custom: Support Profile' and isActive=true limit 1];
        User u1=listu1.get(0);
        
        System.RunAs(u1){
            Account tempAccount1 = AccountStructure.accountInsert();
        }
        tempAccount.Age0__c=5;
       // tempAccount.Total_AR__c=25600;
        //tempAccount.Total_Past_Due__c=150;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
       // tempAccount.Percent_Credit_Utilized__c=5;
        update tempAccount;
        
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = AccountStructure.transactionInsert(tempAccount);
        List<Transaction__c> txList1 = new List<Transaction__c>();
        System.RunAs(u1){
            txList1 = AccountStructure.transactionInsert(tempAccount);
        }
      
        Account tempAccountc = AccountStructure.accountInsert();
        System.assertEquals(tempAccountc.Name, 'testAccount');
        tempAccountc.Parent = tempAccount;
       // tempAccount.Total_AR__c=25600;
        //tempAccount.Total_Past_Due__c=150;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
        //tempAccount.Percent_Credit_Utilized__c=5;
        update tempAccountc;
        
        List<Transaction__c> txList2 = new List<Transaction__c>();
        txList2 = AccountStructure.transactionInsert(tempAccountc);
        
        Test.startTest();
        
        AccountStructure controller = new AccountStructure();  
        
        //List<ObjectStructureMap> osm = controller.getObjectStructure();
        
        controller.setcurrentId(tempAccount.id);
        controller.formatObjectStructure(tempAccount.id);
        controller.GetTopElement(tempAccount.id);
        controller.selectedAccountHierarchy(tempAccount.id);
        List<String> nodeSortList = new List<String>{};
        controller.isLastNode(nodeSortList);
        controller.getObjectStructure();
        controller.objstruMap.getnodeId();
        
        controller.objstruMap.getlevelFlag();
        controller.objstruMap.getcloseFlag();
        controller.objstruMap.getnodeType();
        controller.objstruMap.getcurrentNode();
        controller.objstruMap.getaccount();
        controller.objstruMap.setnodeId('node');
        controller.objstruMap.setriskrating('');
        controller.objstruMap.getriskrating();
        controller.objstruMap.setcreditriskCategoryScore(1);
        controller.objstruMap.getcreditriskCategoryScore();                
        System.assertEquals(controller.objstruMap.getnodeId(),'node');
        controller.objstruMap.setlevelFlag(true);
        controller.objstruMap.setlcloseFlag(true);
        controller.objstruMap.setcurrentNode(true);
        controller.objstruMap.setnodeType('node2');
        controller.objstruMap.setaccount(tempAccount);
        controller.addChildAccount();
        Test.stopTest(); 
    }
    //----------------------
    
        static testMethod void CoverageTestIt() {
    
        Account tempAccount = AccountStructure.accountInsert();
        tempAccount.Age0__c=5;
       // tempAccount.Total_AR__c=25600;
        //tempAccount.Total_Past_Due__c=150;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
       // tempAccount.Percent_Credit_Utilized__c=5;
        update tempAccount;
        
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = AccountStructure.transactionInsert(tempAccount);
      
      
        Account tempAccountc = AccountStructure.accountInsert();
        System.assertEquals(tempAccountc.Name, 'testAccount');
        tempAccountc.Parent = tempAccount;
       // tempAccount.Total_AR__c=25600;
        //tempAccount.Total_Past_Due__c=150;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
        //tempAccount.Percent_Credit_Utilized__c=5;
        update tempAccountc;
        
        List<Transaction__c> txList2 = new List<Transaction__c>();
        txList2 = AccountStructure.transactionInsert(tempAccountc);
        
        Test.startTest();
        
        list<User> listu = [Select id from user where Profile.name='System Administrator' and isActive=true limit 1];
        User u=listu.get(0);
        
        System.RunAs(u){
            TreeFieldConfiguration__c mcs = TreeFieldConfiguration__c.getInstance('1-30');
            System.debug('===mcs1==='+mcs);
            mcs.CustomField__c = 'Auto_Dunning_Status__c';
            mcs.Name = 'Auto Dunning';
            mcs.Summary_Field__c = true;
            update mcs;
            
            System.debug('===mcs2==='+mcs);
            
            TreeFieldConfiguration__c mcsObj = new TreeFieldConfiguration__c();
            mcsObj.CustomField__c = 'Last_Contact_Type__c';
            mcsObj.Name = 'Last Contact Type';
            mcsObj.Order__c = 10;
            mcs.Summary_Field__c = true;
            insert mcsObj;
            
        }
        
        AccountStructure controller = new AccountStructure();  
        
        //List<ObjectStructureMap> osm = controller.getObjectStructure();
        
        
         
        controller.setcurrentId(tempAccount.id);
        
            controller.getObjectStructure();
        TreeFieldConfiguration__c mcs1 = TreeFieldConfiguration__c.getInstance('Auto Dunning');
         System.debug('===mcs3==='+mcs1);
        
        Test.stopTest(); 
    }
    
    //--------------------
     
    /*
    * @Chintan
    */
    static testmethod void testIt2(){
            
        Account tempAccount = AccountStructure.accountInsert();
        tempAccount.Age0__c=5;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
        update tempAccount;
        
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = AccountStructure.transactionInsert(tempAccount);
      
        Account tempAccountc = AccountStructure.accountInsert();
        System.assertEquals(tempAccountc.Name, 'testAccount');
        tempAccountc.Parent = tempAccount;
        tempAccount.Credit_Limit__c=150.10;
        tempAccount.DPD_Mean__c=200.10;
        update tempAccountc;
        
        List<Transaction__c> txList2 = new List<Transaction__c>();
        txList2 = AccountStructure.transactionInsert(tempAccountc);

        TreeFieldConfiguration__c treeConfigObj = new TreeFieldConfiguration__c();
        treeConfigObj.Summary_Field__c = false;
        treeConfigObj.isAverage__c = false;
        treeConfigObj.isMax__c = false;
        treeConfigObj.isMIn__c = false;
        List<TreeFieldConfiguration__c> lstTreeConfig = new List<TreeFieldConfiguration__c>();
        lstTreeConfig.add(treeConfigObj);
        
        Test.startTest();
        
        AccountStructure controller = new AccountStructure();  
        
        //List<ObjectStructureMap> osm = controller.getObjectStructure();
        controller.currentId = null;
        controller.setcurrentId(tempAccount.id);
        controller.formatObjectStructure(tempAccount.id);
        controller.GetTopElement(tempAccount.id);
        controller.selectedAccountHierarchy(tempAccount.id);
        List<String> nodeSortList = new List<String>{};
        controller.isLastNode(nodeSortList);
        controller.getObjectStructure();
        controller.objstruMap.getnodeId();
        
        controller.objstruMap.getlevelFlag();
        controller.objstruMap.getcloseFlag();
        controller.objstruMap.getnodeType();
        controller.objstruMap.getcurrentNode();
        controller.objstruMap.getaccount();
        controller.objstruMap.setnodeId('node');
        controller.objstruMap.setriskrating('');
        controller.objstruMap.getriskrating();
        controller.objstruMap.setcreditriskCategoryScore(1);
        controller.objstruMap.getcreditriskCategoryScore();                
        System.assertEquals(controller.objstruMap.getnodeId(),'node');
        controller.objstruMap.setlevelFlag(true);
        controller.objstruMap.setlcloseFlag(true);
        controller.objstruMap.setcurrentNode(true);
        controller.objstruMap.setnodeType('node2');
        controller.objstruMap.setaccount(tempAccount);
        controller.addChildAccount();
        Test.stopTest(); 
    } 
  //--------------------------------------------------------------     
    
    
     
}