/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is Used for Custom Setting for administration.
 */
public with sharing class CustomSettingAdministrationController
{

    public SysConfig_Singlec__c sysConfObj {get; set; }
    public Risk_Configuration_Singlec__c riskConfObj {get; set; }

    public CustomSettingAdministrationController()
    {
        sysConfObj =  SysConfig_Singlec__c.getOrgDefaults();
        riskConfObj = Risk_Configuration_Singlec__c.getOrgDefaults();
    }
    // Method to save modified Custom Setting SysConfig_Singlec__c
    // Return Type - PageReference
    // No parameters are passed in this method 
    public PageReference saveSysConfig()
    {
        if(sysConfObj != null)
        {        
//========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.SysConfig_Singlec__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
//========================================================================================
            upsert sysConfObj;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Configuration_has_been_modified);
            ApexPages.addMessage(myMsg);
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.Label_You_should_first_make_system_conf);
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    // Method to save modified Custom Setting RiskConfig_Singlec__c
    // Return Type - PageReference
    // No parameters are passed in this method 
    public PageReference saveRiskConfig()
    {
        if(riskConfObj != null)
        {
        
//========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Risk_Configuration_Singlec__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Insufficient access'));
               return null;
              }
          }
        }
//========================================================================================
            upsert riskConfObj;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, Label.Label_Configuration_has_been_modified);
            ApexPages.addMessage(myMsg);
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.Label_You_should_first_make_risk_conf);
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
}