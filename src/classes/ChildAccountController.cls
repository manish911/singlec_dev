public with sharing class ChildAccountController{
public Id AccountId;
String accQuery = '';
String namespace='';
public string searchText{get;set;}
public List<Account> accList{get;set;}

public Temp_Object_Holder__c txIdsConfig{get;set;}
public List<AccLabelListWithAttributes> lstOfLabels {get; set; }
public List<wrapperAccountList> wrapList {get; set; }
public List<List<wrapperAccountList>> wrapList2 {get; set; }
public List<TxListWithAttributes> tempList = new List<TxListWithAttributes>{};
 public ApexPages.StandardSetController con {get; set; }
 public List<SelectOption> recPerPageOptions {get; set; }
 public String recPerPage {get; set; }
 public Integer totalTransRec {get; set; }
 public String displayTotalRec {get; set; }
 public String strTotal {get; set; }
 public Integer pgNumber {get; set; }
  private final integer listLimit;

public static String comparedField {get; set;}
public static String sortOrder {
        get {
            if(sortOrder== null)
                sortOrder='asc';
            return sortOrder;
        }
        set;
    }
    
   

    public childAccountController(){
       searchText = '';
       pgNumber = 1;
       listLimit = 999;
       if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != ''){
            AccountId = ApexPages.currentPage().getParameters().get('id');
       }
        accList  = new List<Account>();
        
            recPerPageOptions =  new  List<SelectOption>();
            SelectOption newOption1 = new SelectOption('500','500');
            SelectOption newOption2 = new SelectOption('1000','1000');
            SelectOption newOption3 = new SelectOption('2000','2000');
        //    SelectOption newOption4 = new SelectOption('200','200');
           
            recPerPageOptions.add(newOption1);
            recPerPageOptions.add(newOption2);
            recPerPageOptions.add(newOption3);
            recPerPage = '500';

        List<AccountStructure.ObjectStructureMap> osMapList = new List<AccountStructure.ObjectStructureMap>();
        AccountStructure ast = new AccountStructure();
        osMapList = ast.selectedAccountHierarchy(AccountId);
        for(AccountStructure.ObjectStructureMap asOsMap : osMapList){
            //accIdList.add(asOsMap.account.Id);
            accQuery = accQuery + '\'' + asOsMap.account.Id + '\'' + ',';
            system.debug('--------asOsMap.account.Name-------------->'+asOsMap.account.Name);
            system.debug('--------asOsMap.account.Id-------------->'+asOsMap.account.Id);
        } 
        
        if(accQuery != null && accQuery != ''){
            accQuery = accQuery.substring(0,accQuery.length()-1);
            accQuery = ' ' + namespace + 'Id in (' + accQuery + ') ';
        }
        
        getSelectedFieldsLabel(TransactionUtil.getDefaultChildAccountListView());
   
        fillWrapper();
        system.debug('--accQuery --'+accQuery);

    }
    
    public void doSearch(){
        system.debug('---searchText----'+searchText);
        fillWrapper();
    } 
    
      //pagination start here
    public void changeRecPerPage(){
        strTotal='';
         if(con== null)
            return;
        
        if( con.getResultSize() > 0) {
            con.setPageSize(Integer.valueOf(recPerPage));
            first();
        }
    }
    
    // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    set;
  }
  
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    fillwrapper();
  }
  
  // runs the actual query
  /* public void runQuery() {

    try {
     contacts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
    }*/
  
    public String pageDetail {
        get {
            Integer totalPages = 1;
            if(con== null)
                return '';
            
            system.debug('--totalTransRec-'+totalTransRec);
            system.debug('--recPerPage-'+recPerPage);
            if(totalTransRec !=0 && totalTransRec != null) {
                totalPages =  totalTransRec/Integer.valueOf(recPerPage);
                if( math.mod(totalTransRec,Integer.valueOf(recPerPage) )!= 0)
                {
                    totalPages =totalTransRec/Integer.valueOf(recPerPage) + 1;
                }
            }
            return 'Page ' + pageNumber +  ' Of ' + totalPages + '';

        }
        set;
    }

    public void next() {
        strTotal ='';
        if(con== null)
            return;
        con.next();
        getWrapList();
    }
    public Boolean hasNext {
        get {
            if(con== null)
                return false;
            
            return con.getHasNext();

        }
        set;
    }

    public Boolean hasPrevious {
        get {
            if(con== null)
                return false;
                
            return con.getHasPrevious();
        }
        set;
    }
    public Integer pageNumber {
        get {
            if(con== null)
                return 0;
            
            return con.getPageNumber();
        }
        set;
    }
    public void first() {
        strTotal ='';
        if(con== null)
            return;
        
        con.first();
        getWrapList();
    }
    public void last() {
        strTotal ='';
        if(con== null)
            return;
        
        con.last();
        getWrapList();
    }
    public void previous() {
        strTotal ='';
        if(con== null)
            return;
        con.previous();
        getWrapList();
    }
   
    public void fillwrapper(){

            
             String str = '';
            
                 for(String nextStr : TransactionUtil.getDefaultChildAccountListView()) {
                    //add extra field if field type Schema.DisplayType.REFERENCE exist
                    AccountDescribeCall.FieldDescibe fieldObj1 = AccountDescribeCall.getFieldData(nextStr);
                    if(fieldObj1 !=null && fieldObj1.fieldType == Schema.DisplayType.REFERENCE ) {
                        String refFieldName = nextStr.subString(0,nextStr.length()-1)+ 'r.Name';
                        str =str +  + ',' + refFieldName ;
                    }
                    else{
    
                        str =str + ',' +  nextStr ;
                    }
                }
                String accQuery1 = accQuery;
                if(searchText != null && searchText != ''){
                    accQuery1 = accQuery + ' and name Like '+'\''+ searchText.trim() + '%' +'\'';
                }
               
                String SOQLQuery = 'Select Id ' + str + ' From Account where ' +accQuery1 + ' order by ' + sortField + ' ' + sortDir + ' limit 9999';
                
                system.debug('---SOQLQuery ---'+SOQLQuery );
                
                con = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQuery ));
                
                system.debug('---con.getResultSize();----'+con.getResultSize());
               totalTransRec = 0;
                 displayTotalRec = string.valueOf(totalTransRec);
               if( con.getResultSize() > 0) {
                totalTransRec = con.getResultSize();
                if(totalTransRec >2000) {
                    displayTotalRec ='2000+';
                }else{
                    displayTotalRec = string.valueOf(totalTransRec);
                }
                
               
                con.setPageSize(Integer.valueOf(recPerPage));
               
               con.setPageNumber(pgNumber);
          }   
          
          getWrapList();                
                     
             
    }
    
        public void getWrapList() {
        Integer counter = 0;
             Integer loopCount = 0;
                        wrapList = new List<wrapperAccountList>();
                        wrapList2 = new List<List<wrapperAccountList>>();
                        
            for (Account transObj : (List<Account>)con.getRecords()){
                  tempList = new List<TxListWithAttributes>();
                 
                   for(String field : TransactionUtil.getDefaultChildAccountListView()){  
    
                       AccountDescribeCall.FieldDescibe fieldObj = AccountDescribeCall.getFieldData(field);
        
                       if(field =='name') {
                         
                         String  txno = String.ValueOf(transObj.get(field));
                        //   String stemp = EncodingUtil.urlEncode('../'+ transObj.Id, 'UTF-8');
                          //   String txLink  = '<a href="/' + transObj.Id + '"'  + ' style="text-decoration: underline;cursor: pointer; cursor: hand;" title="Open Transaction" target="_blank">' + txno + '</a>';
                         String txLink = '<a href="' + '/'+ transObj.Id + '"' + ' style="text-decoration: underline;cursor: pointer; cursor: hand;" title="+Label.Label_Open_Transaction+" target="_blank">' + txno + '</a>';
                         tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),txLink));
                         
                       }
        
                       else if(fieldObj.fieldType ==Schema.DisplayType.DATE) {
                           if(transObj.get(field) !=null ) {
                               Date dtVar =(Date) transObj.get(field);
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),dtVar.format()));
        
                           }else{
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' '));
        
                           }
                       }
                       else if(fieldObj.fieldType ==Schema.DisplayType.CURRENCY) {
                           String formattedAmt = '';
                           Double amnt = (Double)transObj.get(field);
                           if(amnt !=null) {
        
                               //@TODAO : Bhavik : Ref : On-Demand Multi-Currency
                               String currencyIsoCode = ( String )(UtilityController.isMultiCurrency() ? transObj.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode());
                               formattedAmt = '  '+ UtilityController.formatDoubleValue(currencyIsoCode, amnt);
                           }
                           tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),formattedAmt));
        
                       }
                       else if(fieldObj.fieldType == Schema.DisplayType.REFERENCE) {
                           if(transObj.get(field) !=null ) {
                               String refFieldName = field.subString(0,field.length()-1) + 'r';
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),String.valueOf(transObj.getSobject(refFieldName).get('name'))));
                           }else{
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' '));
                           }
                       }
                       else
                       {
                           if(transObj.get(field)!=null)
                           {
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),String.ValueOf(transObj.get(field))));
                           }
                           else
                           {
                               tempList.add(new TxListWithAttributes(String.valueOf(fieldObj.fieldType),' ' ));
        
                           }
                        }          
                 }
                 wrapperAccountList wrapObj = new wrapperAccountList(transObj, tempList);
                 
                 if(counter < listLimit){
                           
             wrapList.add(wrapObj);
                        counter++;
                        }ELSE{
                            loopCount++;wrapList2.add(wrapList);wrapList= new List<wrapperAccountList>();
                             
                             wrapList.add(wrapObj);
                            counter = 0;
                        }
             
             
               }
               
                         if(counter > 0){
                    loopCount++;
                    wrapList2.add(wrapList);
                }
            }          
    
    
    public Pagereference submit(){
    
            Set<Id> txsIds = getSelectedAccounts();
    system.debug('--txsIds ---'+txsIds);
        txIdsConfig = new Temp_Object_Holder__c();
        txIdsConfig.key__c = Userinfo.getUserName()+Datetime.now();
                String txIdsStr = '';

         for(Id txId : txsIds)
            txIdsStr = txIdsStr + ',' +txId;
            txIdsConfig.value__c = txIdsStr;

        
        //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Temp_Object_Holder__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
            System.debug('=======fieldToCheck======'+fieldToCheck);
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              System.debug('=======fieldToCheck2======'+fieldToCheck);
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                System.debug('=======fieldToCheck in======'+fieldToCheck);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        
        insert (txIdsConfig);
        
        return null;
        // return new pagereference('/AccountDetailPage?id='+AccountId+'&txIds='+txIdsConfig.key__c + ' &sfdc.override=1');
        
       
    }
    
      public Set<Id> getSelectedAccounts() {
        Set<Id> selectedTxs = new Set<Id>();
        if( wrapList !=null &&  wrapList.size() > 0) {
            for(wrapperAccountList transWrapper : wrapList) {
                if(transWrapper.selected)
                    selectedTxs.add(transWrapper.transId);
            }

        }
        return selectedTxs;

    }
    
      public void getSelectedFieldsLabel(List<String> fieldsList)
      {
        lstOfLabels = new List<AccLabelListWithAttributes>();
        
        system.debug('--fieldsList---'+fieldsList);

        //fetch field label through field name
        for(String fieldName : fieldsList)
        {
            AccountDescribeCall.FieldDescibe fieldObj = AccountDescribeCall.getFieldData(fieldName);
            lstOfLabels.add(new AccLabelListWithAttributes(String.valueOf(fieldObj.fieldType),fieldObj.fieldLabel,fieldName ));

        }

    }
    
 
    
        public class AccLabelListWithAttributes {
        transient public String txId {get; set; }
        public String fieldType {get; set; }
        public String fieldValue {get; set; }
        public String fieldName {get; set; }
        public AccLabelListWithAttributes (String fieldType,String fieldValue,String fieldName){
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
            this.fieldName = fieldName;
        }
    }
    
       //inner class for customize columns of transaction objects
    public class wrapperAccountList {
        public Boolean selected {get; set; }
        transient public List<TxListWithAttributes> txValues {get; set; }
        public String transId {get; set; }
        transient public String fieldType {get; set; }
        transient public String txBalance {get; set; }
        transient public Account txObject {get; set; }
        public wrapperAccountList(Account txObj, List<TxListWithAttributes> txValues){
            this.txValues = new List<TxListWithAttributes>();
            this.txValues = txValues;
            this.txObject = new Account();
            this.txObject = txObj;
            transId = txObj.Id;
            this.selected = false;
            this.fieldType = fieldType;
          //  this.txBalance = txBalanceVal;
        }
    }

    public class TxListWithAttributes {
        transient public String txId {get; set; }
        transient public String fieldType {get; set; }
        transient public String fieldValue {get; set; }
        public TxListWithAttributes (String fieldType,String fieldValue){
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
        }
    }
   
   static testmethod void testcase(){

        Test.startTest();
        
        Account acc = Datagenerator.accountInsert();
        
                ApexPages.currentPage().getParameters().put('id',acc.Id);
                        SysConfig_Singlec__c sic = new SysConfig_Singlec__c();
                        sic.Is_Multi_Currency__c = false;
                        sic.Billing_Statement_PDF_page__c = 'test';
                        sic.Billing_PDF_Rows_In_First_Page__c = 10;
                        sic.Billing_PDF_Rows_in_Subsequent_Pages__c = 1;
                        sic.Billing_Statement_Excel_Template__c = 'SYSTEM_BillingStatementExcel';
                        sic.Billing_Statement_PDF_page__c = 'test';
                        sic.Billing_Statement_Pdf_Template__c = 'SYSTEM_BillingStatementPDF';
                        sic.IconsetURL__c = 'test';
                        sic.Invoice_PDF_Url_Prefix__c = 'test';
                        sic.Default_Child_Account_List_View__c = 'Name,AccountNumber,Last_Contact_Date__c,Total_AR__c';
                         sic.Display_Child_Account_Invoice__c = true;
                        
                        upsert sic;
                        System.assertEquals(sic.Billing_Statement_Excel_Template__c,'SYSTEM_BillingStatementExcel');
                        TreeFieldConfiguration__c tfc = new TreeFieldConfiguration__c();
                        tfc.name = 'test';
                        tfc.CustomField__c = 'Total_AR__c';
                        upsert tfc;
        
        ChildAccountController cac = new ChildAccountController();
        cac.submit();
        cac.doSearch();
        cac.changeRecPerPage();
        cac.next();
        cac.first();
        cac.last();
        cac.previous();


        
       Test.stopTest();
        
    } 
   
}