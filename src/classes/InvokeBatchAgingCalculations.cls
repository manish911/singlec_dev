/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: InvokeBatchAgingCalculation class object of
 *        BatchAgingCalculations class is created
 *        so execution of batches are carried out in this class.
 */
public class InvokeBatchAgingCalculations {
      // Methods calls class BatchAgingCalculation and starts Batch aging calculation for transaction whose Batch Number is specified   
     //  Return Type - void
    //   No parameters are passed in this method
    public static void startBatch(){
        BatchAgingCalculations bac = new BatchAgingCalculations();
        ID batchprocessid = Database.executeBatch(bac);
    }
    
        // Methods calls class BatchADPDCalculation and starts Batch ADPD calculation for transaction whose Batch Number is specified   
    //  Return Type - void
    //   No parameters are passed in this method
    public static void startADPDBatch(){
        BatchADPDCalculations badpd = new BatchADPDCalculations();
        ID batchprocessid2 = Database.executeBatch(badpd);
    }
    
    public static void startBatchSourcesys(String sourcesystem){
        BatchAgingCalculations bac = new BatchAgingCalculations(sourcesystem);
        ID batchprocessid = Database.executeBatch(bac);
    }
    
        // Methods calls class BatchADPDCalculation and starts Batch ADPD calculation for transaction whose Batch Number is specified   
    //  Return Type - void
    //   No parameters are passed in this method
    public static void startADPDBatchSourcesys(String sourcesystem){
        BatchADPDCalculations badpd = new BatchADPDCalculations(sourcesystem);
        ID batchprocessid2 = Database.executeBatch(badpd);
    }
    
    // Methods calls class BatchAgingCalculation and starts Batch aging calculation for transaction whose Batch Number is not specified
    // Return Type - void
    // No parameters are passed in this method
    public static void runWithoutBatch(){
        BatchAgingCalculations bca = new BatchAgingCalculations();
        Database.executeBatch(bca);
    }
}