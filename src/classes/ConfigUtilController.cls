/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Application wide utility controller for configuration.
 */
public with sharing class ConfigUtilController {
    // Fetches value of Customer Logo field in SysConfig_Singlec__c custom Setting 
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getCustomerLogoUrl() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String url='';
        if(sysConfObj!=null) {
            if(sysConfObj.Org_Logo__c !=null) {
                url = sysConfObj.Org_Logo__c;
            }
        }
        return url;
    }
    // Fetches value of Currency Code set in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getOrgCurrencyCode() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String code='';
        if(sysConfObj!=null) {
            if(sysConfObj.Default_Currency__c !=null) {
                code = sysConfObj.Default_Currency__c;
            }
        }
        return code;
    }
    
       // Fetches value of Account Field specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No parameters are passed in this method , Used for Closing the task on account level.
    
      public static String getAccountFieldForClosing() {
       
         SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
         String customField='';
         if(sysConfObj!=null) {
             if(sysConfObj.Account_Field__c !=null) {
                 customField = sysConfObj.Account_Field__c;
             }
         }
         return customField;
    }
    
    // Fetches value of Namespace specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getNamespace() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String namespace ='';
        if(sysConfObj!=null) {
            if(sysConfObj.NameSpace__c !=null) {
                namespace = sysConfObj.NameSpace__c;
            }
        }
        return namespace;
    }
    
    // Returns custom field of Contact which contains fax domain in System_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static String getContactFaxField(){
         SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
         String customField='';
         if(sysConfObj!=null) {
             if(sysConfObj.ContactFaxField__c !=null) {
                 customField = sysConfObj.ContactFaxField__c;
             }
         }
         return customField;
    }
    
    
    // Fetches the template Specified BillingStatementPdfTemplate in SysConfig_Singlec__c Custom Setting
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getBillingStatementPdfTemplate() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();

        String templateName='';

        if(sysConfObj!=null) {
            if(sysConfObj.Billing_Statement_Pdf_Template__c !=null) {
                templateName = sysConfObj.Billing_Statement_Pdf_Template__c;
            }
        }
        return templateName;
    }
    // Fetches the template specified in BillingStatementExcelTemplate in SysConfig_Singlec__c Custom Setting
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getBillingStatementExcelTemplate() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String templateName='';

        if(sysConfObj!=null) {
            if(sysConfObj.Billing_Statement_Excel_Template__c !=null) {
                templateName = sysConfObj.Billing_Statement_Excel_Template__c;
            }
        }
        return templateName;
    }
    
    public static String getDunningTemplate(){
        
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String dunningTemplateName='';
        
        if(sysConfObj != null){
            if(sysConfObj.Dunning_Template__c != null){
                dunningTemplateName = sysConfObj.Dunning_Template__c;
            }    
        }
        
        return dunningTemplateName;    
    }
    // Fetches the PDF page which is specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBillingStatementPdfPage() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String pageName ='';

        if(sysConfObj!=null) {
            if(sysConfObj.Billing_Statement_Pdf_Page__c !=null) {
                pageName = sysConfObj.Billing_Statement_Pdf_Page__c;
            }
        }
        return pageName;
    }
    // Fetches the EXCEL page which is specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBillingStatementExcelPage() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String pageName='';

        if(sysConfObj!=null) {
            if(sysConfObj.Billing_Statement_Excel_Page__c !=null) {
                pageName = sysConfObj.Billing_Statement_Excel_Page__c;
            }
        }
        return pageName;
    }
    // Fetches the EmailID which is specified in BatchJobNotificationEmailId  in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBatchJobNotificationEmailId() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String emailId = 'admin@akritiv.com';

        if(sysConfObj!=null) {
            if(sysConfObj.Batch_Job_Notification_Email__c !=null) {
                emailId = sysConfObj.Batch_Job_Notification_Email__c;
            }
        }
        return emailId;
    }
    
    // Fetches the EmailIDs which is specified in BatchJobNotificationEmailIds  in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No Parameters are passed in this method  
    public static String getBatchJobNotificationEmailIds() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String emailIds = 'admin@akritiv.com';

        if(sysConfObj!=null) {
            if(sysConfObj.Batch_Job_Notification_Emails__c !=null) {
                emailIds = sysConfObj.Batch_Job_Notification_Emails__c;
            }
        }
        return emailIds;
    }
    
    // Fetches the value of EnableInvoiceDetailPDF which is specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - Boolean 
    // No Parameters are passed in this method  
    public static boolean isEnableInvoiceDetailPDF() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        boolean isInvoicePDFOptionEnabled = false;
        if(sysConfObj!=null) {
            if(sysConfObj.EnableInvoiceDetailPDF__c !=null) {
                isInvoicePDFOptionEnabled = sysConfObj.EnableInvoiceDetailPDF__c;
            }
        }
        return isInvoicePDFOptionEnabled;
    }
        
    // Fetches Default values specified in SysConfig_Singlec__C Custom Setting
    // Return Type - SysConfig_Singlec__C 
    // No parameters are passed in this method 
    public static SysConfig_Singlec__c getSystemConfigObj() {
        if(SysConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(SysConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return SysConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }

        return SysConfig_Singlec__c.getOrgDefaults();
    }

    //To get organisation config object for field merging that will be used in email templates.
    public static OrganizationConfig_Singlec__c getOrganizationConfigObj() {
        if(OrganizationConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return OrganizationConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(OrganizationConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return OrganizationConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }

        return OrganizationConfig_Singlec__c.getOrgDefaults();
    }

    //To get Batch Job Configuration object for size of batch.
    public static BatchJobConfiguration_Singlec__c getBatchJobConfigObj() {

        return BatchJobConfiguration_Singlec__c.getOrgDefaults();
    }
    //To get Bucket config object for field merging that will be used in email templates.
    public static BucketConfig_Singlec__c getAgingBucketConfigObj() {
        if(BucketConfig_Singlec__c.getInstance(Userinfo.getUserid()) != null) {
            return BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }
        else if(BucketConfig_Singlec__c.getInstance(Userinfo.getProfileId()) != null) {
            return BucketConfig_Singlec__c.getInstance(Userinfo.getUserid());
        }

        return BucketConfig_Singlec__c.getOrgDefaults();
    }
    
    
    
    public static Boolean isBucketOnBase(){
        
        return getSystemConfigObj().BucketsOnBase__c;
    }
    
    public static Boolean useNativeNote(){
        
        return getSystemConfigObj().Use_Native_Note__c;
    }
    public static Boolean captureNoteOnTransaction(){
        
        return getSystemConfigObj().Capture_Notes_On_Transaction__c;
    }
    public static Boolean useReasonCode(){
        
        return getSystemConfigObj().Use_Reason_Code__c;
    }
    
    //To get Risk config object for field merging that will be used in email templates.
    public static Risk_Configuration_Singlec__c getRisksConfigObj() {
        Risk_Configuration_Singlec__c rc = Risk_Configuration_Singlec__c.getOrgDefaults();

        return rc;
    }
    // batch capacity
    public static String getBatchCapacity(){
        return getBatchJobConfigObj().Capacity__c;

    }
    // Batch Auto Close Ratio
    public static Double  getBatchAutoCloseRiskRatio(){
        return getBatchJobConfigObj().Auto_Close_Risk_Ratio__c;
    }
    // Batch Auto Close Email Service 
    public static String getBatchAutoCloseEmailService(){
        return getBatchJobConfigObj().Auto_Close_Email_Service__c;

    }
    
    public static String getBatchAutoCloseExternalEmailNotificationService(){
        return getBatchJobConfigObj().Custom_Email_Service__c;
       
    }
    
    // Fetches value of Default List View Fields specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getDefaultListViewFields() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String defaultlist ='';
        if(sysConfObj!=null) {
            if(sysConfObj.Default_List_View_Fields__c!=null) {
                defaultlist = sysConfObj.Default_List_View_Fields__c;
            }
        }
        return defaultlist;
    }
    
      public static String getDefaultChildAccountListView() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String defaultlist ='';
        if(sysConfObj!=null) {
            if(sysConfObj.Default_Child_Account_List_View__c !=null) {
                defaultlist = sysConfObj.Default_Child_Account_List_View__c;
            }
        }
        return defaultlist;
    }
    
    
    
     
    // Fetches value of Default List View Fields specified in SysConfig_Singlec__c Custom Setting 
    // Return Type - String 
    // No parameters are passed in this method 
    public static String getDefaultDisputeListViewFields() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String defaultlist ='';
        if(sysConfObj!=null) {
            if(sysConfObj.Default_Dispute_List_View_Fields__c!=null) {
                defaultlist = sysConfObj.Default_Dispute_List_View_Fields__c;
            }
        }
        return defaultlist;
    }
    
    
    
    //* ----------------- Email Template Configuration -----------
    // Returns default  value of Email_Template_Filter_Configuration__c Custom Setting 
    // Return Type Email_Template_Filter_Configuration__c
    // No parameters are passed in this method 
    public static Email_Template_Filter_Configuration__c getEmailTemplateFilterConfigurationObj() {

        return Email_Template_Filter_Configuration__c.getOrgDefaults();
    }
    // Returns Contact Customer specified in  Email_Template_Filter_Configuration__c Custom Setting 
    // Return Type Email_Template_Filter_Configuration__c
    // No parameters are passed in this method 
    public static String getForContactCustomer() {
        return getEmailTemplateFilterConfigurationObj().For_Contact_Customer__c;
    }
    // Returns Dispute specified in  Email_Template_Filter_Configuration__c Custom Setting 
    // Return Type Email_Template_Filter_Configuration__c
    // No parameters are passed in this method 
    public static String getForDispute() {
        return getEmailTemplateFilterConfigurationObj().For_Dispute__c;
    }
    // Returns InterFaxProvider specified in  Email_Template_Filter_Configuration__c Custom Setting 
    // Return Type Email_Template_Filter_Configuration__c
    // No parameters are passed in this method 
    public static String getForPromiseToPay() {
        return getEmailTemplateFilterConfigurationObj().For_Promise_to_Pay__c;
    }
    
    // Returns Locale Supported in Email_Template_Filter_Configuration__c Custom Setting 
    // Return Type Email_Template_Filter_Configuration__c
    // No parameters are passed in this method 
    public static boolean getLocaleSupported() {
        return getEmailTemplateFilterConfigurationObj().Locale_Supported__c;
    }
    
    //* ----------------- Data Purage Settings -----------
    // Returns default  value of SysConfig_Singlec__c Custom Setting 
    // Return Type SysConfig_Singlec__c
    // No parameters are passed in this method 




    
    // Returns EndDate specified in SysConfig_Singlec__c Custom Setting 
    // Return Type SysConfig_Singlec__c
    // No parameters are passed in this method 
    public static Decimal getTransactionAge() {
        return getSystemConfigObj().Transaction_Age__c;
    }
    
    
    //* ----------- Use of External PDf web service ----------------
    // Returns Default value of PDF endpoint Custom setting
    
       
    public static String getHost() {
        
        return getSystemConfigObj().Host__c;
    }
    
    public static String getURI() {
        
        return getSystemConfigObj().URI__c;
    }
    
    public static String getPort() {
        
        return getSystemConfigObj().Port__c;
    }
    
    public static String getKeyParameter() {
        
        return getSystemConfigObj().Key_Parameter__c;
    }
    
   public static Boolean getUseExternalPDF(){
        return getSystemConfigObj().Use_External_PDF__c;
   }
   
     
    //for Payment configuration
    
    public static String getPaymentMethodPage() {
        
        return getSystemConfigObj().Payment_Page__c;
    }
    
    // Get Payment Credit Limit
    public static double getPaymentCreditLimit() {
        if(getSystemConfigObj().Payment_Credit_Limit__c != null)
            return getSystemConfigObj().Payment_Credit_Limit__c*100;
        else
            return null;
    }
    public static boolean isShowChildTrxn() {
        return getSystemConfigObj().Show_Child_Transaction__c;
    }
    public static boolean getPaymentMethodEnable() {
        return getSystemConfigObj().Enable_Payment__c;
    }
    // Decide whether to show child account's transaction or not
    
     public static boolean getDisplayChildAccountInvoice() {
        return getSystemConfigObj().Display_Child_Account_Invoice__c;
    }
      public static boolean getMarkReserveEnable() {
        return getSystemConfigObj().MarkReserveForMultipleTransaction__c;
    }
    
     public static String getDefaultRelatedRecordPage() {
        
        return getSystemConfigObj().DefaultRecord_Page__c;
    }
    //* ----------------- Inter Fax Settings -----------
    // Returns default  value of Fax_Configuration__c Custom Setting 
    // Return Type InterFAXConfig__c
    // No parameters are passed in this method 
    public static Fax_Configuration__c getFaxConfigurationObj() {

        return Fax_Configuration__c.getOrgDefaults();
    }
    // Returns InterFaxUserName specified in  Fax_Configuration__c Custom Setting 
    // Return Type InterFAXConfig__c
    // No parameters are passed in this method 
    public static String getFaxUsername() {
        return getFaxConfigurationObj().Fax_Username__c;
    }
    // Returns InterFaxPassword specified in  Fax_Configuration__c Custom Setting 
    // Return Type InterFAXConfig__c
    // No parameters are passed in this method 
    /*public static String getFaxPassword() {
        String finalPassword = '';
        String strkey = '0fHwmuoFcUhq6vcUb2POJITcn2cZVV3up11aZn8yOT0=';
        
        Blob cryptoKey = EncodingUtil.base64Decode(strkey);
        
        if(getFaxConfigurationObj().Fax_Password__c != null){
            String encodedFaxPassword =  getFaxConfigurationObj().Fax_Password__c;
            Blob encryptedFaxPassword= EncodingUtil.base64Decode( encodedFaxPassword );
            
            Blob decryptedData  = Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedFaxPassword);
            
            finalPassword = decryptedData.toString();
        }
        
        return  finalPassword;
    }*/
    //New method added as on 12 June 2013
     public static String getFaxPassword() {
     
   

        // Get Crypto Key
        //Crypto_Configuration__c sysConfObj = Crypto_Configuration__c.getInstance(Userinfo.getUserid());

        String EncodedBlobCryptoKey = 'rU7WA2ENzuZ9Lhcwj57HQw=='; //sysConfObj.CryptoKey__c;
        
        blob cryptoKey = EncodingUtil.base64Decode(EncodedBlobCryptoKey);
        if(getFaxConfigurationObj().Fax_Password__c != null && getFaxConfigurationObj().Fax_Password__c != ''){
            blob decodedFaxPassword = EncodingUtil.base64Decode(getFaxConfigurationObj().Fax_Password__c);   
    
            // Decrypt Data
            blob decryptFaxPassword = Crypto.decryptWithManagedIV('AES128', cryptoKey, decodedFaxPassword );
           // finalPassword = decryptFaxPassword.toString();
    
            if(decryptFaxPassword != null)
            {
               return decryptFaxPassword.toString();
            }
        }
        
        return '';
     }
    
    
    // Returns InterFaxProvider specified in  Fax_Configuration__c Custom Setting 
    // Return Type Fax_Configuration__c
    // No parameters are passed in this method 
    public static String getFaxProvider() {
        return getFaxConfigurationObj().Fax_Provider__c;
    }
    
    // Returns FaxProvider specified in  Fax_Configuration__c Custom Setting 
    // Return Type Fax_Configuration__c
    // No parameters are passed in this method 
    public static String getFaxEmailService() {
        return getFaxConfigurationObj().Fax_Email_Service__c;
    }
     
     
    // -------------- Trigger Conguration ------------------
    // Returns default  value of Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static Trigger_Configuration__c getTriggerConfigObj() {


        if(Trigger_Configuration__c.getInstance(Userinfo.getUserid()) != null) {
            return Trigger_Configuration__c.getInstance(Userinfo.getUserid());
        }
        else if(Trigger_Configuration__c.getInstance(Userinfo.getProfileId()) != null) {
            return Trigger_Configuration__c.getInstance(Userinfo.getUserid());
        }

        
        return Trigger_Configuration__c.getOrgDefaults();
    }
    
    // Returns After Change Account Owner specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getAfterChangeAccountOwner() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().After_Change_Account_Owner__c;
            
        } else {
        
        
            return false;
        }
        
    }
    // Returns After Create PaymentLine specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getAfterCreatePaymentLine() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().After_Create_PaymentLine__c;
            
        } else {
        
        
            return false;
        }
        
    }
    // Returns After Insert Full DataLoad Batch specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getAfterInsertFullDataLoadBatch() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().After_Insert_Full_DataLoad_Batch__c;
            
        } else {
        
        
            return false;
        }
        
    }
   
    
    // Returns Before Insert UserPreference specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getBeforeInsertUserPreference() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Before_Insert_UserPreference__c;
            
        } else {
        
        
            return false;
        }
        
    }
    // Returns Handle Reserve Transaction Trigger specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getHandleReserveTransaction() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Handle_Reserve_Transaction_Trigger__c;
            
        } else {
        
        
            return false;
        }
        
    }
    // Returns Manage Account Key Trigger specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getManageAccountKey() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Manage_Account_Key_Trigger__c;
            
        } else {
        
        
            return false;
        }
        
    }
    // Returns Update Account ContactLog Details specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getUpdateAccountContactLogDetails() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Update_Account_ContactLog_Details__c;
            
        } else {
        
        
            return false;
        }
        
    }
    
    
    
     
    // Returns Update Transaction Disputed Amount specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getUpdateTransactionDisputedAmount() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Update_Transaction_Disputed_Amount__c;
        } else {
        
        
            return false;
        }
        
    }
    // Returns Zero Balance Transaction Trigger specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getZeroBalanceTransactionTrigger() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Zero_Balance_Transaction_Trigger__c;
        } else {
        
        
            return false;
        }
        
    }
    // Returns Note On Broken Promise specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    public static boolean getCreateNoteOnBrokenPromiseTrigger() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Create_Note_On_BrokenPromise__c;
        } else {
        
        
            return false;
        }
        
    }
     // Returns Zero AR Account Trigger specified in Trigger_Configuration__c Custom Setting 
    // Return Type Trigger_Configuration__c
    // No parameters are passed in this method 
    
    public static boolean getZeroZeroARAccountTrigger() {
    
        if ( getTriggerConfigObj() != null ) {        
        
            return getTriggerConfigObj().Zero_AR_Account_Trigger__c;
        } else {
        
        
            return false;
        }
        
    }
  
    //--------------------------------------------------------------------------------------//
    //  AGING BUCKET CONFIGURATION
    //--------------------------------------------------------------------------------------//
    public class AgingBucket {
        public String label {get; set; }
        public Integer minRange {get; set; }
        public Integer maxRange {get; set; }

        public AgingBucket(String l, Integer min, Integer max) {
            this.label = l;
            this.minRange = min;
            this.maxRange = max;
        }
    }
    // Fetches values of Buckets in Accounts and stores it in BucketConfig_Singlec__c Custom Setting
    // Return Type - Aging Bucket 
    // Parameter - age Integer 
    public static AgingBucket getBucketByAge(Integer age) {
        BucketConfig_Singlec__c abc = getAgingBucketConfigObj();
        
        if(abc != null) {
            if(age == 0 ) //2147483647
                return new AgingBucket('Current',-2147483646,0);
            else if(age == 5)
                return new AgingBucket('Over ' + abc.Age4__c.intValue(), (abc.Age4__c).intValue()+1,2147483647);
            else if(age == 1)
                return new AgingBucket('1-'+abc.Age1__c.intValue(), 1, abc.Age1__c.intValue());
            else if(age == 2)
                return new AgingBucket(abc.Age1__c.intValue()+1+'-'+abc.Age2__c.intValue(), abc.Age1__c.intValue()+1, abc.Age2__c.intValue());
            else if(age == 3)
                return new AgingBucket(abc.Age2__c.intValue()+1+'-'+abc.Age3__c.intValue(), abc.Age2__c.intValue()+1, abc.Age3__c.intValue());
            else if(age == 4)
                return new AgingBucket(abc.Age3__c.intValue()+1+'-'+abc.Age4__c.intValue(), abc.Age3__c.intValue()+1, abc.Age4__c.intValue());
        }
          return null;
    }
       
       
    //--------------------------------------------------------------------------------------//
    //  TEST METHODS SECTION
    //--------------------------------------------------------------------------------------//
    public static testmethod void runAllTest() {
        test.startTest();
        SysConfig_Singlec__c sysConfig = SysConfig_Singlec__c.getOrgDefaults();
       
        if(sysConfig != null) {
            sysConfig.Billing_Statement_Pdf_Template__c = 'TestPdfTemplateName';
            sysConfig.Billing_Statement_Excel_Template__c = 'TestExcelTemplateName';
            sysConfig.Org_Logo__c = 'Some Url to image';
            sysConfig.Billing_Statement_PDF_page__c = 'Test';
            sysConfig.NameSpace__c = 'testName';
            sysConfig.Default_Currency__c = '$';
            sysConfig.Billing_Statement_PDF_page__c = 'testPdfPage';
            sysConfig.Billing_Statement_Excel_page__c = 'testExcelPage';
            sysConfig.Batch_Job_Notification_Email__c = 'test@test.com';
            update sysConfig;
        }

        if(sysConfig != null) {
            String excelTemplate = ConfigUtilController.getBillingStatementExcelTemplate();
            String pdfTemplate = ConfigUtilController.getBillingStatementPdfTemplate();
            String orgLogo = ConfigUtilController.getCustomerLogoUrl();
            String nameSpace = ConfigUtilController.getNamespace();
            String pdfPage = ConfigUtilController.getBillingStatementPdfPage();
            String excelPage = ConfigUtilController.getBillingStatementExcelPage();
            String notificationEmail = ConfigUtilController.getBatchJobNotificationEmailId();
            String orgCurrency = ConfigUtilController.getOrgCurrencyCode();

            String confaxfield = ConfigUtilController.getContactFaxField();            
            String accfield= ConfigUtilController.getAccountFieldForClosing();
            String dunnTemplate= ConfigUtilController.getDunningTemplate();
            boolean isbucket= ConfigUtilController.isBucketOnBase();
            boolean capturenote= ConfigUtilController.captureNoteOnTransaction();
            boolean usenativenote= ConfigUtilController.useNativeNote();
            boolean usereasoncode= ConfigUtilController.useReasonCode();
            String defaultlist= ConfigUtilController.getDefaultListViewFields();
            String defaultchildlist= ConfigUtilController.getDefaultChildAccountListView();
            String defaultdiputelist= ConfigUtilController.getDefaultDisputeListViewFields();
            Decimal tranage= ConfigUtilController.getTransactionAge();
            String host= ConfigUtilController.getHost();
            String url= ConfigUtilController.getURI();
            String port= ConfigUtilController.getPort();
            String keyparameter= ConfigUtilController.getKeyParameter();
            boolean useextpdf= ConfigUtilController.getUseExternalPDF();            
            String paymethod= ConfigUtilController.getPaymentMethodPage();
            double paycrdlmt= ConfigUtilController.getPaymentCreditLimit();
            boolean paymthenbl= ConfigUtilController.getPaymentMethodEnable();
            boolean mrkrsrvenbl= ConfigUtilController.getMarkReserveEnable();
            boolean dispacctinv= ConfigUtilController.getDisplayChildAccountInvoice();
            String defaultrelatedrecord= ConfigUtilController.getDefaultRelatedRecordPage();
            
                        
            System.assertEquals('testName', nameSpace);
            System.assertEquals('test@test.com', notificationEmail);
            System.assertEquals('testExcelPage', excelPage);
            System.assertEquals('testPdfPage', pdfPage);
            System.assertEquals('Some Url to image', orgLogo);
            System.assertEquals('TestPdfTemplateName', pdfTemplate);
            System.assertEquals('TestExcelTemplateName', excelTemplate);
        }

        Risk_Configuration_Singlec__c riskOrg = Risk_Configuration_Singlec__c.getOrgDefaults(); {
            if(riskOrg != null) {
                System.assert(ConfigUtilController.getRisksConfigObj() != null);
            }
        }

        OrganizationConfig_Singlec__c orgConfig = OrganizationConfig_Singlec__c.getOrgDefaults();
        if(orgConfig != null) {
            System.assert(ConfigUtilController.getOrganizationConfigObj() != null );
        }

        BatchJobConfiguration_Singlec__c batchJobConfig = BatchJobConfiguration_Singlec__c.getOrgDefaults();
        if(batchJobConfig != null) {
            System.assert(ConfigUtilController.getBatchJobConfigObj() != null );
        }

        BucketConfig_Singlec__c bucketConfig = BucketConfig_Singlec__c.getOrgDefaults();
        if(bucketConfig != null) {
            System.assert(ConfigUtilController.getAgingBucketConfigObj() != null );
        }
        
        Email_Template_Filter_Configuration__c emailconf = getEmailTemplateFilterConfigurationObj();
        if(emailconf != null){
            boolean localsupported= ConfigUtilController.getLocaleSupported();
            
        }
        
        Fax_Configuration__c faxconf = Fax_Configuration__c.getOrgDefaults();
        if(faxconf != null){
            String faxuser= ConfigUtilController.getFaxUsername();
            String faxpwd= ConfigUtilController.getFaxPassword();
            String faxpwdprovider= ConfigUtilController.getFaxProvider();
            String faxemailservice= ConfigUtilController.getFaxEmailService();
        }
        
        Trigger_Configuration__c trgconf = Trigger_Configuration__c.getOrgDefaults();
        if(trgconf != null){
            boolean afterchange= ConfigUtilController.getAfterChangeAccountOwner();
            boolean aftercreate= ConfigUtilController.getAfterCreatePaymentLine();
            boolean afterfillload= ConfigUtilController.getAfterInsertFullDataLoadBatch();
            boolean beforeinsert= ConfigUtilController.getBeforeInsertUserPreference();
            boolean handlereserve= ConfigUtilController.getHandleReserveTransaction();
            boolean manageaccountkey= ConfigUtilController.getManageAccountKey();
            boolean updatecontact= ConfigUtilController.getUpdateAccountContactLogDetails();
            boolean updatetrx= ConfigUtilController.getUpdateTransactionDisputedAmount();
            boolean zerobal= ConfigUtilController.getZeroBalanceTransactionTrigger();
            boolean createnote= ConfigUtilController.getCreateNoteOnBrokenPromiseTrigger();
            boolean zeroar= ConfigUtilController.getZeroZeroARAccountTrigger();
        }

        System.assertEquals(ConfigUtilController.getBucketByAge(0).maxRange, 0);
        ConfigUtilController.getBucketByAge(1);
        ConfigUtilController.getBucketByAge(2);
        ConfigUtilController.getBucketByAge(3);
        ConfigUtilController.getBucketByAge(4);
        System.assertEquals(ConfigUtilController.getBucketByAge(5).maxRange, 2147483647);
        
        ConfigUtilController.getBatchCapacity();
        ConfigUtilController.getBatchAutoCloseRiskRatio();
        ConfigUtilController.getBatchAutoCloseEmailService();
        
        ConfigUtilController.getForContactCustomer();
        ConfigUtilController.getForDispute();
        ConfigUtilController.getForPromiseToPay();
       
        ConfigUtilController.getBatchJobNotificationEmailIds();
        ConfigUtilController.isEnableInvoiceDetailPDF();
        
        ConfigUtilController.getBatchAutoCloseExternalEmailNotificationService();
       
       
        test.stopTest();
    }
    
    public static Boolean captureNoteOnLineItem(){
        
        return getSystemConfigObj().Capture_Notes_On_Line_Item__c;
    }
    
    /*
     *    Fetches value of Default List View Fields specified in SysConfig_Singlec__c Custom Setting for Dispute Line
     *    Return Type - String 
     *    No parameters are passed in this method 
     */
    public static String getDefaultDisputeLinesListViewFields() {
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String defaultlist ='';
        if(sysConfObj!=null) {
            if(sysConfObj.Default_Dispute_Lines_List_View_Fields__c!=null) {
                defaultlist = sysConfObj.Default_Dispute_Lines_List_View_Fields__c;
            }
        }
        return defaultlist;
    }
}