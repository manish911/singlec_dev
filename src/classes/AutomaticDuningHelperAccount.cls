public with sharing class AutomaticDuningHelperAccount{
    public Integer i { get;set;}
    public Integer e { get;set;}
    public String status { get;set;}
    public boolean buttonDisabled {get;set;}
    public List < Account > accIds = null;
    
    public Boolean isExternalPDF {get; set; }
    String baseUrl {get; set; }
     
    String packageHostAddress {get; set; }
    public List < Account > processedList  { get;set;}
    public List < Account > errorList { get;set;}
    public String key {get;set;} 
    public Temp_Object_Holder__c pdfConfig;
    public String selectedLocale = null; 
    public String location { get; set; }
    public Set < Id > processids { get;set; }
    private SysConfig_Singlec__c sysConfig {get;set;}
    
    public AutomaticDuningHelperAccount(){
    
        i = 0;
        e = 0;
        buttonDisabled = false;
        status = 'Not Started';
        processids = new Set < Id >();
    sysConfig = ConfigUtilController.getSystemConfigObj();
        processedList  = new List < Account >();
        errorList = new List<Account >();
        packageHostAddress = ApexPages.currentPage().getHeaders().get('host');
        
        location = ApexPages.currentPage().getParameters().get('location');
    }
    
     public void prepareAccounts() {
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Do_Not_Close_Window_Message)); 
    
        
            key =  ApexPages.currentPage().getparameters().get('key');
            if(key != '' && key != null){
                key = key.trim();
                
               List < Temp_Object_Holder__c> temp = [ select Id, Value__c,key__c from Temp_Object_Holder__c where key__c = : key];
                String accstr = '';
                if(temp.size() > 0){
                    accstr = temp.get(0).Value__c;
                }
                String  [] accstrarray = accstr.split(','); 
                
                accIds = [ select Id,name, account_key__c from Account where Id in:accstrarray  ];
                
                for ( Account a : accIds ) {
                    a.Auto_Dunning_Status__c = 'In Progress';
                }
                
                //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
                
                upsert accIds ;
            }
        
        }
           
    
    
    public void save() {
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Re_Evaluation_Process_Completed_Message));
        
        key = ApexPages.currentPage().getparameters().get('key');
        key = key.trim();

        List < Temp_Object_Holder__c> temp = [ select Id, Value__c,key__c from Temp_Object_Holder__c where key__c = : key];
               
        String accstr = temp.get(0).Value__c;

        String  [] accstrarray = accstr.split(','); 
        
        accIds = [ select Id,name, Auto_Dunning_Status__c from Account where Id in:accstrarray  ];
      
        for ( Account a : accIds ) {
           
            a.Auto_Dunning_Status__c = 'Not Started';
            a.Auto_Dunning_Error__c = '';
        }
        
  //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Insufficient access'));
               return;
              }
          }
        }
  //========================================================================================
        
        upsert accIds ;
    }
    
      
    public void execute() {
        buttonDisabled = true;
        List<EmailTemplate> billingTemplateList = new List<EmailTemplate>();
        
        status = 'In Progress';
       
        List <Account> alst = [select Id,name, account_key__c,Total_Ar__c,Total_Disputed__c,Dunning_Error_Fix_URL__c, Lead_Tx__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingCountry, ShippingCity, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity, AccountNumber, Auto_Dunning_Status__c,Locale__c From Account where Auto_Dunning_Status__c = 'In Progress' and account_key__c != null  limit 1];
      
        if ( alst.size() == 0 ){
        
            //stop
            buttonDisabled = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Auto_Dunning_Is_Completed)); 
        
            status = 'Completed';
            return;
        }
        i++;
        Account acc = alst.get(0);
        
        Decimal totalUndisputed = 0;
        totalUndisputed = acc.Total_Ar__c - acc.Total_Disputed__c;
        
        if ( totalUndisputed <= 0 ){
        
            acc.Auto_Dunning_Status__c = 'Auto Dunning Error';
            //log error : account not apecified
            acc.Auto_Dunning_Error__c = Label.Error_Account_s_Undisputed_Balance_Is_Less_Then_Or_Equal_to_Zero;
            //TODO : put dynamic host address
            acc.Dunning_Error_Fix_URL__c = 'https://akritiv.'+ packageHostAddress +'/apex/AccountDetailPage?id='+acc.id;
            
          //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
            
            update acc;

            errorList.add(acc);
            
            return;
        }  
        
        List < Contact > cons = [ select Id,Email,Locale__c from Contact where AccountId =: acc.Id and Default__c = True ];
        
        if ( cons.size() == 0 ){
        
            acc.Auto_Dunning_Status__c = 'Auto Dunning Error';
            //log error : account not apecified
            acc.Auto_Dunning_Error__c = Label.Error_Accounts_default_contact_is_not_specified;
            
            //TODO : put dynamic host address
            acc.Dunning_Error_Fix_URL__c = 'https://akritiv.'+ packageHostAddress +'/apex/AccountDetailPage?id='+acc.id;
            
            //log error : missing account default contact
            
            //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
            
            
            update acc;
            
            errorList.add(acc);
            
            return;
        }
        
        Contact con = cons.get(0) ;
        
        //GET  ( TRNASACTION )INVOICE DETIAL FROM OLDES INVOICE ID
        
      
        List <Transaction__c> txlst = [Select a.Shipment_Method__c, 
                                        a.Ship_To_State__c, 
                                        a.Ship_To_Postal_Code__c, 
                                        a.Ship_To_Number__c, 
                                        a.Ship_To_Name__c, 
                                        a.Ship_To_Country__c,
                                        a.Ship_To_City__c, 
                                        a.Ship_To_Address__c, 
                                        a.Bill_To_State__c, 
                                        a.Bill_To_Postal_Code__c, 
                                        a.Bill_To_Number__c, 
                                        a.Bill_To_Name__c, 
                                        a.Bill_To_Country__c, 
                                        a.Bill_To_City__c, 
                                        a.Bill_To_Address__c, 
                                        a.Account__c, 
                                        a.Days_Past_Due__c, 
                                        a.Balance__c,a.Name
                                        From Transaction__c a 
                                        where a.Account__c =: acc.Id and a.Days_Past_Due__c > 30 and a.Balance__c > 0 ];
        
        //TODO : check for total balance > 0
        Decimal TotalBalance = 0;
        if(txlst.size() != 0 ){
            for(Transaction__c trx : txlst){
                TotalBalance = TotalBalance + trx.Balance__c;
            }
        }
        
        
         if(TotalBalance < 0){
            
            acc.Auto_Dunning_Status__c = 'Auto Dunning Error';
            //log error : Total Balance is < 0
            acc.Auto_Dunning_Error__c = Label.Error_Total_Balance_is_0;
            
            //TODO : put dynamic host address
            acc.Dunning_Error_Fix_URL__c = 'https://akritiv.'+ packageHostAddress +'/apex/AccountDetailPage?id='+acc.id;
            
            //log error : missing account default contact
            
            //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
            
            update acc;
            
            errorList.add(acc);
            
            return;
        }
        
        if ( txlst.size() == 0 ){
            //stop
            acc.Auto_Dunning_Status__c = 'Completed';
            
            //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
        
            update acc;
            
            processedList.add( acc );
           
            return;
        }
        
        String Invoicenums ='';
        for ( Transaction__c tx : txlst  ) {
            invoiceNums = invoiceNums + ','+tx.Name;
        }
        if(invoiceNums.length() > 0 ){
            invoiceNums = invoiceNums.substring(1, invoiceNums.length()- 1 );
        }
        selectedLocale = con.Locale__c;
        
        if(selectedLocale == null || selectedLocale == ''){
            selectedLocale = acc.Locale__c;
        }
        //Template name from Task Subject
        
        //Template folder from default contact
            
          String dunningTemplateName = ConfigUtilController.getDunningTemplate();
          
          if(dunningTemplateName != null && dunningTemplateName != ''){
              if(sysConfig.is_Akritiv_Org__c){  
                    billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null and Folder.Name like 'Akritiv%'  and IsActive=true and Name=:dunningTemplateName];
              }else{
                  if(selectedLocale != null && selectedLocale != '' ){
                        billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null and Folder.Name=:selectedLocale  and IsActive=true and Name=:dunningTemplateName];
                   } else{ 
                        billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null and Folder.Name != '' and Name =: dunningTemplateName];
                  }              
              }
          }
          if(billingTemplateList.size() == 0){
             
                acc.Auto_Dunning_Status__c = 'Auto Dunning Error';
              
                acc.Auto_Dunning_Error__c = Label.Error_Dunning_Email_Template_Not_Found;
                
            //TODO : put dynamic host address
            
           errorList.add(acc);
           
           //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
           
            update acc;
            return;
        
          }
          if(billingTemplateList.size() > 0){
          
            String  emailTemplateBody = billingTemplateList.get(0).HtmlValue;     
            //--email mechanism with mandrill--start-----
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            MandrillEmailService objMandrill = new MandrillEmailService();
                       
            mail.setSubject(billingTemplateList.get(0).Subject);
            objMandrill.strSubject = billingTemplateList.get(0).Subject;
            
             //--email mechanism with mandrill--end-----                
            GenerateTemplateOutput output = new GenerateTemplateOutput();
    
            emailTemplateBody = replaceMergeFields(acc , txlst ,con , output, emailTemplateBody);
                
            if(con.Email != '' && con.Email != null )
            {
            //--email mechanism with mandrill--start-----                
            
                mail.setToAddresses( new String []{con.Email}); 
                objMandrill.strToEmails = new String []{con.Email};  
                
             //--email mechanism with mandrill--end-----                   
            }else{
                acc.Auto_Dunning_Status__c = 'Auto Dunning Error';
                //log error : Total Balance is < 0
                acc.Auto_Dunning_Error__c = Label.Error_Accounts_s_Default_Contact_Email_Not_Found;
                
                //TODO : put dynamic host address
                acc.Dunning_Error_Fix_URL__c = 'https://' + packageHostAddress+'/'+con.id;
                
                //log error : missing account default contact
                
                //==========================================================================================  
                     list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
                   Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
                   for (String fieldToCheck : lstFields) {
                      // Check if the user has create access on the each field
                      if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                         if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return;
                          }
                       }
                    }
                //========================================================================================
                
                update acc;
                            
                errorList.add(acc);
                
                return;
             }   
          //--email mechanism with mandrill--start-----
            Messaging.EmailFileAttachment[] attachments = new Messaging.EmailFileAttachment[] {};
            List<MandrillEmailService.clsMandrillAttachments> lstMandrillAttachments = new List<MandrillEmailService.clsMandrillAttachments>(); 
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            MandrillEmailService.clsMandrillAttachments objMandrillAttachments = new MandrillEmailService.clsMandrillAttachments();
                    
            //--email mechanism with mandrill--end-----
                
            isExternalPDF = ConfigUtilController.getUseExternalPDF();
            String showDetailsParam = '&showDetails=false';
            baseUrl = packageHostAddress ;
            Blob b = null;
            String st = null;
            if(baseUrl !=null && baseUrl.trim()!='') {
                if(isExternalPDF){
                  //  String pageurlref ='https://' +  baseUrl + '/apex/GetExternalPDF?key='+hiddenKey+showDetailsParam + '&listview='+ selectedList + '&accId='+acc.Id+'&orderby=due_Date__c&ordertype=asc&tconid='+con.Id+'&templateid='+billingTemplateList.get(0).id; 
                   // Pagereference pageref =new PageReference(pageurlref);   
                   /* b = ExternalPDFGetway.getAllOpenInvoice( invoiceNums );
                    if(b!=null){
                        efa.setFileName('BillingStatement.pdf');
                        efa.setBody(b); 
                        attachments.add(efa);
                        mail.setFileAttachments(attachments);
                    }*/
                }else{
                    Pagereference pageref = new Pagereference( 'https://' + baseUrl+ '/apex/GenerateStatementPage?accId='+ acc.Id+ '&txIds='+ 'notrx' + '&rdrPdf=PDF&template=StatPDF&orderby=Balance__c&ordertype=DESC&sendallinvoice=1&txidlist=null');
                    b = pageref.getContentAsPDF();
                    efa.setFileName('BillingStatement.pdf');
                    efa.setBody(b);
                    attachments.add(efa);

                    //--email mechanism with mandrill--start-----
                    
                    mail.setFileAttachments(attachments);
                    objMandrill.mailAttachment = lstMandrillAttachments;
                    
                    //--email mechanism with mandrill--end-----                 
                }
            } 
          
            //--email mechanism with mandrill--start-----
            
            mail.setHtmlBody(emailTemplateBody);
            objMandrill.strHtmlBody = emailTemplateBody;
            SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
            boolean us = seo.Use_Mandrill_Email_Service__c;
          
            If(us){
                objMandrill.SendEmail();
                  }
                  Else{
                  Messaging.sendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                  }
            //--email mechanism with mandrill--end-----            
            acc.Auto_Dunning_Status__c = 'Completed';
            
            //==========================================================================================  
             list<String> lstFields = new list<String>{'Dunning_Error_Fix_URL__c','Auto_Dunning_Error__c','Auto_Dunning_Status__c','Lead_Tx__c','ShippingStreet','ShippingState','ShippingPostalCode','ShippingCountry','ShippingCity','BillingStreet','BillingState','BillingPostalCode','BillingCountry','BillingCity','AccountNumber','Notes__c','Locale__c'};
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                    return;
                  }
               }
            }
        //========================================================================================
            
            
            update acc;
        
            processedList.add( acc );
            
            e++;
        }
    }    
    
  
    
    public Pagereference stop() {
         buttonDisabled  = false;
         i = 0;
         return null;    
    }
    
    
    private static String replaceMergeFields( Account acc , List<Transaction__c> txs , Contact c, GenerateTemplateOutput output, String emailTemplateStr)
    {
        String mergedBody;
        
        List<List<Transaction__c>> txListOfList = new List<List<Transaction__c>>();
        
      //  List<Transaction__c> txList = new List <Transaction__c>();
        
     //   txList.add( tx  );
        txListOfList.add( txs );
        //??no user id
        mergedBody = output.processTemplateBody(acc.Id, c.Id, '', emailTemplateStr,'');
        // replace the summary field
        if(mergedBody.contains('{!Transactions.Summary}'))
            mergedBody = output.processTransactionSummaryMergeField(mergedBody, 'Default', TransactionsCollection.getTransactionIdsListOfList(new TransactionsCollection(txListOfList)), acc.Id,'', 'Due_Date__c', 'DESC');

        if(mergedBody.contains('{!User.Name}')) {
            mergedBody   =  mergedBody.replace('{!User.Name}', UserInfo.getUserName());
        }
        
       // String renderAsPDF = 'PDF';
       // mergedBody = output.processTransactionSummaryMergeFieldSendBillingStat(mergedBody, 'Default', true, acc.Id, '',renderAsPDF);
        return mergedBody;
    }
    
    
}