/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for SetPortalAccountTab
 */
@isTest
private class TestSetPortalAccountTab
{
    static testMethod void testSetPortalAccountTab()
    {
        Account accWithuser;
        Contact conWithUser;
        User adminUser = DevUtilityController.getSystemAdministrator();
        if(adminUser != null)
        {
            System.runAs(adminUser)
            {
                //Create a new instance of Account Object with test values
                accWithuser = new Account(Name = 'TestAccountName' );
                accWithuser.Account_Key__c = '4679801242352354';

                //Insert the object virtually
                insert accWithuser;

                //Create a new instance of the Contact with test values
                conWithUser = new Contact(LastName = 'Test',AccountId = accWithuser.Id, email='standarduser@testorg.com');

                //Insert the object virtually
                insert conWithUser;

            }
        }

        //List for getting the profile
        List<Profile> profile = [select id from profile where name like 'Akritiv Customer Portal User' LIMIT 1];

        //Create a new instance of the User with test values
        User portalUser;

        // Fetch the default available CEO role
        UserRole testRole =
        [select id from UserRole where name = 'CEO' LIMIT 1];

        System.assertNotEquals(null, testRole);
        //User adminUser = DevUtilityController.getSystemAdministrator();
        if(profile.size() > 0)
        {
            portalUser = new User(alias = 'standt', email = 'test@9876test.com',
                                  emailencodingkey = 'UTF-8', lastname = 'TestLasName', languagelocalekey = 'en_US',
                                  localesidkey = 'en_US', profileid = profile.get(0).id, contactId = conWithUser.id,
                                  timezonesidkey = 'America/Los_Angeles', username = Math.random() + '@test.com');
            insert portalUser;

            ApexPages.currentPage().getParameters().put('accId',accWithuser.Id);

            System.RunAs(portalUser)
            {
                SetPortalAccountTab controller = new SetPortalAccountTab();
                controller.goToAccountDetailPage();
            }
        }
    }
}