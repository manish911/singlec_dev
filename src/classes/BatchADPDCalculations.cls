/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for calculating Average DPD values.
 */
global class BatchADPDCalculations implements Database.Batchable<SObject>, Database.Stateful {

     public AccountWrapper aw;
     String email;
     public Data_Load_Batch__c batchInfo ;
     global Id batchInfoId ;  
     public RunningStandardDevCalc stdDevCalAll = new RunningStandardDevCalc ();
     global String SourceSys;
     
     public BatchADPDCalculations(){
     }
     
     public BatchADPDCalculations (String SourceSystem){
        Sourcesys = SourceSystem;
     }

    //@TODO : Bhavik : Ref - On-Demand Multi-Currency
    global database.querylocator start(Database.BatchableContext bc){
        /**start Namespace for migrating to unmanaged package  **/
        String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        //String namespace = ' ';
        // Check for multi currency
        String sQuery = '';
        if ( UtilityController.isMultiCurrency() ) {

            sQuery = 'select  CurrencyIsoCode, '+namespace+'Account__c, '+namespace+'Account__r.CurrencyIsoCode, '+namespace+'Base_Balance__c,  '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        } else {

            sQuery = 'select   '+namespace+'Account__c,  '+namespace+'Base_Balance__c,  '+namespace+'Balance__c, '+namespace+'Days_Past_Due__c,';

        }
        sQuery += ' '+namespace+'Due_Date__c, '+namespace+'Promised_Amount__c, '+namespace+'Promise_Date__c, '+namespace+'Disputed_Amount__c, '+namespace+'Undisputed_Balance__c';
        sQuery += ' from '+namespace+'Transaction__c';
        sQuery += ' where '+namespace+'Account__c !=null and '+namespace+'Balance__c != null and '+namespace+'Due_Date__c != null ';

        if(Sourcesys != null && Sourcesys != ''){
            sQuery += ' and ' + namespace+'Source_System__c =: Sourcesys';
        }
        
        SysConfig_Singlec__c sysconfig  = ConfigUtilController.getSystemConfigObj();
        
        // Concider aging filter from System configuration in where clause if any.
        
        if ( sysconfig.Aging_Filter__c != null &&  sysconfig.Aging_Filter__c.trim() != '') {

            sQuery += ' and ' + sysconfig.Aging_Filter__c;
        }
        sQuery += '  order by '+namespace+'Account__c , '+namespace+'Days_Past_Due__c DESC';
        
        // Update batch info record
        
        if ( batchInfoId  != null ) {
           //* batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c, d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c  From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo.Aging_Job_Start_Time__c = System.now();
            
        }
        return Database.getQueryLocator(sQuery);
    }
    
    // Start calculation for Aging  
    // Return Type - Void 
    // Parameter - BC Database.Batchablecontext, sObjs List<sObject>
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
        Boolean isupdate = false;

        List<Account> accountsToUpdate = new List<Account>();
        for(Sobject txSobject : sObjs) {
            Transaction__c tx = (Transaction__c) txSobject;
            if(tx.Account__c != null && tx.Due_Date__c!= null && tx.Balance__c != null) {
                if(aw == null) {
                    aw = new AccountWrapper(tx.Account__c);
                    addDetails(tx);

                }
                else if(aw.accountId == tx.Account__c) {
                    addDetails(tx);
                }
                else {
                    if(aw != null && aw.accountId != null) {
                        Account acc = new Account(Id = aw.accountId);
                        // Set the oldest invoice on account
                        acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
                        // Set the High past due invoice 
                        acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;
                        
                        // Set the avg DPD on account 
                        if(aw.stdDevCal.getMean() != null)
                            acc.DPD_Mean__c = aw.stdDevCal.getMean();
                            
                        //  Set the variance in DPD on account
                        if(aw.stdDevCal.getStandardDeviation() != null)
                            acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();
                        
                        // Set DPD on account which is same as of oldest invoice(if exists) else set to 0.
                            
                        acc.DPD__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Days_Past_Due__c : 0;
                        accountsToUpdate.add(acc); 

                        aw = new AccountWrapper(tx.Account__c);
                        addDetails(tx);
                    }
                }
            }
        }

        try {
            if(accountsToUpdate.size()>0)
                update accountsToUpdate;
        } catch(Exception e){
            throw e;
        }
    }

    global void addDetails(Transaction__c tx){

        // to calculate avg pastdue date
        if(tx.Days_Past_Due__c !=null) {
            aw.stdDevCal.push(tx.Days_Past_Due__c);
        }

       // if BucketOnBase is true then calculate on  base balance else on balance
        if( ConfigUtilController.isBucketOnBase()){
            // Find oldest invoice ( The invoice having smallest due date )
            if(tx.Base_Balance__c >0 && tx.Due_Date__c !=null && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) && (tx.Disputed_Amount__c == null || tx.Disputed_Amount__c == 0) ) {
                if(aw.leadPastdueTx == null)
                    aw.leadPastdueTx = tx;
                else {
                    if(tx.Due_Date__c < aw.leadPastdueTx.Due_Date__c)
                        aw.leadPastdueTx = tx;
                    else if(tx.Due_Date__c == aw.leadPastdueTx.Due_Date__c && tx.Base_Balance__c > aw.leadPastdueTx.Base_Balance__c)
                        aw.leadPastdueTx = tx;
                }
            }
    
            // find the higest past due invoice
            // Broken promise will also be a part of calculations (promise date < today)
            if(tx.Base_Balance__c >0 && tx.Undisputed_Balance__c != null && tx.Due_Date__c < Date.today() && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) ) {
                if(aw.highPastdueTx == null)
                    aw.highPastdueTx = tx;
                else {
                    if(tx.Undisputed_Balance__c > aw.highPastdueTx.Undisputed_Balance__c )
                        aw.highPastdueTx = tx;
                }
            }
        }
        else{
        
             // Find oldest invoice
            if(tx.Balance__c >0 && tx.Due_Date__c !=null && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) && (tx.Disputed_Amount__c == null || tx.Disputed_Amount__c == 0) ) {
                if(aw.leadPastdueTx == null)
                    aw.leadPastdueTx = tx;
                else {
                    if(tx.Due_Date__c < aw.leadPastdueTx.Due_Date__c)
                        aw.leadPastdueTx = tx;
                    else if(tx.Due_Date__c == aw.leadPastdueTx.Due_Date__c && tx.Balance__c > aw.leadPastdueTx.Balance__c)
                        aw.leadPastdueTx = tx;
                }
            }
    
            // find the higest past due invoice
            // Broken promise will also be a part of calculations (promise date < today)
            if(tx.Balance__c >0 && tx.Undisputed_Balance__c != null && tx.Due_Date__c < Date.today() && (tx.Promise_Date__c < Date.today() || tx.Promised_Amount__c == 0 || tx.Promised_Amount__c == null) ) {
                if(aw.highPastdueTx == null)
                    aw.highPastdueTx = tx;
                else {
                    if(tx.Undisputed_Balance__c > aw.highPastdueTx.Undisputed_Balance__c )
                        aw.highPastdueTx = tx;
                }
            }
        }
    }
    // Methods assins value to fields in tarnsaction the values which are calculated 
    // Return Type - Void 
    // Parameter - bc Database.Batchablecontext
    global void finish(Database.BatchableContext bc){
        if(aw != null && aw.accountId != null) {
            Account acc = new Account(Id = aw.accountId);
           
            // Set the oldest invoice on account
            acc.Lead_Tx__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Id : null;
            
            // Set high past due invoice on account
            acc.High_Past_Due_Tx__c = aw.highPastdueTx != null ? aw.highPastdueTx.Id : null;
            
            // Set Avg DPD on account
            if(aw.stdDevCal.getMean() != null)
                acc.DPD_Mean__c = aw.stdDevCal.getMean();
            // Set Variance in DPD on account
            if(aw.stdDevCal.getStandardDeviation() != null)
                acc.DPD_Std_Dev__c  = aw.stdDevCal.getStandardDeviation();
            
            // Set DPD on account which is same as of oldest invoice(if exists) else set to 0.
               
            acc.DPD__c = aw.leadPastdueTx != null ? aw.leadPastdueTx.Days_Past_Due__c : 0;
            update acc;
        }
        if (batchInfo != null ) {
        
            // Update the batch info record
            batchInfo.Aging_Job_End_Time__c = System.now();
            batchInfo.Aging_Job_Status__c = 'Done';
            update batchInfo;
            
            SysConfig_Singlec__c confog = ConfigUtilController.getSystemConfigObj();
            boolean triggeractive =  confog.PopulateTaskWithARDetail__c;
            
            if(triggeractive){
            
                String toAddress = ConfigUtilController.getBatchAutoCloseEmailService();
                    
                //notify system of this finish
                if (toAddress != null ) {
                //--email mechanism with mandrill--start-----
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    MandrillEmailService objMandrill = new MandrillEmailService();

   
                    
                    mail.setToAddresses(new String[] {toAddress});
                    objMandrill.strToEmails = new String[] {toAddress};
                    
 
                
                    mail.setReplyTo(toAddress);
                    objMandrill.strReplyTo=toAddress;

                    mail.setSenderDisplayName('Batch Processing :');
                    objMandrill.strFromName = 'Batch Processing :';
                   //--email mechanism with mandrill--end-----
                    String mailBodyText = '';
                    
                //--email mechanism with mandrill--start-----                    
                    mail.setSubject('Akritiv Task Detail');
                    objMandrill.strSubject ='Akritiv Task Detail';
                    
                    
                    
                    
                    
                     if ( batchInfo != null  ){
                          
                        mailBodyText ='<END>'+batchInfo.Batch_Number__c+''+Userinfo.getOrganizationId()+''+batchInfo.Source_System__c+''+Userinfo.getOrganizationId()+''+batchInfo.Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                  
    
                     } else {
            
                        mailBodyText ='<END>'+batchInfo.Batch_Number__c+''+Userinfo.getOrganizationId()+''+batchInfo.Source_System__c+''+Userinfo.getOrganizationId()+''+batchInfo.Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                    
    
                     }             
            
                     mail.setPlainTextBody(mailBodyText);
               
                     //--email mechanism with mandrill--start-----              
                 SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                 boolean us = seo.Use_Mandrill_Email_Service__c;
                 if(us){
                    objMandrill.SendEmail();
                 }
                 else{
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     }
                  
                     
                     //--email mechanism with mandrill--end-----
                 }
                
            }
            else{
                
                //notify external sysem of this finish
                String toAddress = ConfigUtilController.getBatchAutoCloseExternalEmailNotificationService();
                    
                //notify system of this finish
                if (toAddress != null ) {
                
                    //--email mechanism with mandrill--start-----               
                
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    MandrillEmailService objMandrill = new MandrillEmailService();
                    
                    
                    
                    mail.setToAddresses(new String[] {toAddress});
                    objMandrill.strToEmails = new String[] {toAddress};
                    

                    mail.setReplyTo(toAddress);
                    objMandrill.strReplyTo=toAddress;

                    mail.setSenderDisplayName('Batch Processing :');
                    objMandrill.strFromName = 'Batch Processing :';
                    //--email mechanism with mandrill--end-----  
                    
                    // Set subject
                  //  mail.setSubject( 'Akritiv ADPD Process');
                   
                  //--email mechanism with mandrill--start-----   
                    mail.setSubject('Akritiv Close Process');
                    objMandrill.strSubject = 'Akritiv Close Process';                    
                  //--email mechanism with mandrill--end-----  
                    // Set body
                    String mailBodyText ='<END>'+batchInfo.Batch_Number__c+''+Userinfo.getOrganizationId()+''+batchInfo.Source_System__c+''+Userinfo.getOrganizationId()+''+batchInfo.Id+'<END>';                   
                    
            
                    mail.setPlainTextBody(mailBodyText);
               
               //--email mechanism with mandrill--start-----               
               
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    objMandrill.SendEmail();
                    
                    //--email mechanism with mandrill--end-----               
                 }
             }
        }
    }

    global class AccountWrapper {
        public ID accountId {get; set; }
       
        public Transaction__c highPastdueTx {get; set; }
        public Transaction__c leadPastdueTx {get; set; }
        public RunningStandardDevCalc stdDevCal {get; set; }

        global AccountWrapper(Id accId) {
            this.accountId = accId;
            stdDevCal = new RunningStandardDevCalc();
            
        }
    }

    /*
     * Class to calculate standard deviation accurately in one pass.
     */
    public class RunningStandardDevCalc {
        public Integer n;
        private Double oldM, newM, oldS, newS;

        public RunningStandardDevCalc() {
            n=0;
        }
        //   Method to clear to all data before Standard Deviation Calculation
        //   Return Type - Void 
       //    No parameters are passed in this method
        public void Clear() {
            n = 0;
        }
        // Method use for calculating value of average Due Date for Billed and Unbilled transaction
        // Return Type - Void 
        // Parameter - x Double 
        public void push(Decimal x){
            n++;
            if (n == 1) {
                oldM = newM = x;
                oldS = 0.0;
            }
            else {
                newM = oldM + (x - oldM)/n;
                newS = oldS + (x - oldM)*(x - newM);
                // set up for next iteration
                oldM = newM;
                oldS = newS;
            }
        }
        // Returns the value of variable n which is currently set after push method is called
        // Return Type - Integer 
        // No parameter are passed in this method
        public Integer getNumDataValues() {
            return n;
        }
        // Returns value of newly calculated mean in push method
        // Return Type - Double 
        // No parameters are passed in htis method
        public Double getMean() {
            return (n > 0) ? newM : 0.0;
        }
        // Returns the value of Variance calculated on the Due Date  
        // Return Type - Double
        // No parameters passed in this method 
        public Double getVariance() {
            return ( (n > 1) ? newS/(n - 1) : 0.0 );
        }
        // Returns the Average Standard Dveiation for current Due Date of that Account
        // Return Type - Double 
        // No Parameters are parameters are passed in this method 
        public Double getStandardDeviation() {
            return Math.sqrt( getVariance() );
        }
    }

     

}