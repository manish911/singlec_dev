/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for EditContactController
 */
@isTest
private class TestEditContactController
{
    static testMethod void testEditContactController()
    {
        Test.startTest();

        EditContactController controller = new EditContactController();

        Contact conObj = new Contact();
        conObj.lastName = 'Test';
        insert conObj;

        ApexPages.currentPage().getParameters().put('Id',conObj.Id);

        EditContactController controller1 = new EditContactController();
        System.assertEquals(controller1.updateContact(), null);
        System.assertEquals(conObj.lastName, 'Test');

        Test.stopTest();
    }
}