/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for TestCloseDispute.
 */
@isTest
private class TestCloseDispute {

    static testMethod void testCloseDispute()
    {
        List<Transaction__c> txList = new List<Transaction__c>();

        //Insert Account
        Account tempAccount = DataGenerator.accountInsert();

        //List of Tx on Account
        txList = DataGenerator.transactionInsert(tempAccount);

        List<Dispute__c> dispList = new List<Dispute__c>();

        //List of Dispute on Account with Tx
        dispList = DataGenerator.disputeInsert(tempAccount, txList);

        Id disputeId = dispList[0].ID;

        // set the current page parameters
        ApexPages.currentPage().getParameters().put('objIds',disputeId);
        ApexPages.currentPage().getParameters().put('objtype','Dispute__c');

        Test.startTest();

        CloseDisputeController cont = new CloseDisputeController();

        System.assertNotEquals(null, cont.resolutionCodes);

        cont.closeDispute();
        List<Dispute__c> disputeToBeClosedList = [Select id, Name, Account__r.Id, Account__r.Name, Type__c, Transaction__c, amount__c, balance__c, resolution_code__c, status__c,close_date__c from Dispute__c where id=:disputeId];

        System.assertEquals(1, disputeToBeClosedList.size());

        Test.stopTest();
    }
}