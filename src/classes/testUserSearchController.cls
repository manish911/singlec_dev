/* * $Author: Minkesh Patel 
$ * $Revision:  1.0 
Note: Code is written to just show the working demo. Can be optimized for best use. 
*/

 
@isTest
private class testUserSearchController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        UserSearchController usc = new UserSearchController();
        User u = [select Name from user limit 1];
        usc.userName = '  '+u.Name+'   ';
        system.assert(u != null);
        usc.searchUser();
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        UserSearchController usc = new UserSearchController();
        system.assertEquals(usc.userName,null);
        usc.searchUser();
    }
}