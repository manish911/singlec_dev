/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 **************************************************************************

   Name    :   TestPortalGenerateTransactionDispute

   Author  :   Bhavik Patel

   Date    :   May 16, 2010

   Usage   :   Test class for PortalGenerateTransactionDispute.

   History :   1.0

 ****************************************************************************/
@isTest
private class TestPortalGenerateTransactionDispute {
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test

        Account tempAccount = DataGenerator.accountInsert();

        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(tempAccount);
        String customViewName = AkritivConstants.CONST_SYSTEM_PDF_EXCEL_LIST;
        String renderAsPDF = 'PDF';
        String contactNote = 'Test Note';
        // get comma separated ids of transaction to create a config object value

        String txIds = '';
        Integer c = 0;
        String lastTxId = null;
        for(Transaction__c tx : txList)
        {
            if ( c == 0 ) {

                txIds +=tx.Id;
            } else {

                txIds += ','+tx.Id;
            }
            if ( c == txList.size()-1) {

                lastTxId = tx.Id;
            }
            c++;

        }

        ApexPages.currentPage().getParameters().put('txid',txIds);
        //start test
        Test.startTest();

        PortalGenerateTransactionDisputes pgtd = new PortalGenerateTransactionDisputes();
        pgtd.contactNote = contactNote;
        List<PortalGenerateTransactionDisputes.DisputeWithTransaction> disputesWithTxList = pgtd.disputesWithTxList;
        
        // System.assertEquals(disputesWithTxList.size(), 0);

        System.assertEquals(pgtd.selectedList,'Default');
        System.assertEquals(pgtd.contactNoteTitle,AkritivConstants.NOTE_DISPUTE);
        Integer size = disputesWithTxList.size();

        for(PortalGenerateTransactionDisputes.DisputeWithTransaction tx : disputesWithTxList)
        {
            tx.selected = false;
           
        }

        //  System.assertEquals(pgtd.getSelectedTxs().size(), 0);

        pgtd.execute();
        System.assertEquals(pgtd.disputesCreated, true);

        Pagereference pagePref = pgtd.goBackToAccount();
        

        // System.assertEquals(pagePref.getUrl(), '/'+lastTxId);
    } 
    
    static testMethod void case2() {
        // TO DO: implement unit test
        
        PortalGenerateTransactionDisputes pgtd = new PortalGenerateTransactionDisputes();
       // List<PortalGenerateTransactionDisputes.DisputeWithTransaction> disputesWithTxList = new List<PortalGenerateTransactionDisputes.DisputeWithTransaction>();

        Account tempAccount = DataGenerator.accountInsert();
        Account tempAccount1 = DataGenerator.accountInsert();
    
        List<Transaction__c> txList = new List<Transaction__c>();
        
        Transaction__c tx1 = DataGenerator.transactionInsert(tempAccount).get(0);
        Transaction__c tx2 = DataGenerator.transactionInsert(tempAccount1).get(0);
        
        txList.add(tx1);
        txList.add(tx2);
        
      
        String customViewName = AkritivConstants.CONST_SYSTEM_PDF_EXCEL_LIST;
        String renderAsPDF = 'PDF';
        String contactNote = 'Test Note';
        
        // get comma separated ids of transaction to create a config object value

        String txIds = '';
        Integer c = 0;
        String lastTxId = null;
        for(Transaction__c tx : txList)
        {
        
            PortalGenerateTransactionDisputes.DisputeWithTransaction dwt = new PortalGenerateTransactionDisputes.DisputeWithTransaction(tx);
            dwt.setDisputeAmount(Double.ValueOf(tx.Amount__c) );
            dwt.selected =true;
             
             pgtd.accountId= dwt.dispute.Account__c;
            pgtd.disputesWithTxList.add(dwt);
            
           
        
            if ( c == 0 ) {

                txIds +=tx.Id;
            } else {

                txIds += ','+tx.Id;
            }
            if ( c == txList.size()-1) {

                lastTxId = tx.Id;
            }
            c++;

        }

        ApexPages.currentPage().getParameters().put('txid',txIds);
        //start test
        Test.startTest();
        
        pgtd.contactNote = contactNote;
              
        
        // System.assertEquals(disputesWithTxList.size(), 0);

        System.assertEquals(pgtd.selectedList,'Default');
        System.assertEquals(pgtd.contactNoteTitle,AkritivConstants.NOTE_DISPUTE);
        Integer size = pgtd.disputesWithTxList.size();
        
        
        
        pgtd.contactNoteTitle = 'this is note title';
        
                           
         // System.assertEquals(pgtd.getSelectedTxs().size(), 0);
        
        pgtd.execute();
        
       
        System.assertEquals(pgtd.disputesCreated, true);

        Pagereference pagePref = pgtd.goBackToAccount();
        

        // System.assertEquals(pagePref.getUrl(), '/'+lastTxId);
    }
}