/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for TransactionUtil.
 */
@isTest
private class TestTransactionUtil {
    static testMethod void runTest() {

        Test.startTest();

        System.assertEquals(1, TransactionUtil.getStatusNumber('Current'));
        System.assertEquals(2, TransactionUtil.getStatusNumber('Reminder'));
        System.assertEquals(3, TransactionUtil.getStatusNumber('Past-due'));
        System.assertEquals(4, TransactionUtil.getStatusNumber('Serious'));
        System.assertEquals(5, TransactionUtil.getStatusNumber('Collection'));
        System.assertEquals(6, TransactionUtil.getStatusNumber('Disputed'));
        System.assertEquals(0, TransactionUtil.getStatusNumber('--none--'));
        //System.assertEquals(TransactionUtil.getDefaultListView().size(), 7);
        System.assertEquals(TransactionUtil.getDefaultListView().size(), 6);
        System.assertNotEquals(TransactionUtil.getDefaultDisputeListView().size(), 11);
        list<string> s=TransactionUtil.getDefaultChildAccountListView();
        Test.stopTest();
    }
}