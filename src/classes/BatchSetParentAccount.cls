/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */

global class BatchSetParentAccount implements Database.Batchable<SObject>, Database.Stateful {
	// Returns Name space for Parent Account 
	// Return Type - Database.Querylocator
	// Parameter - bc Database.Batchablecontext
	global database.querylocator start(Database.BatchableContext bc){
		/**start Namespace for migrating to unmanaged package  **/
		//String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
		String namespace = '';
		return Database.getQueryLocator('Select Id, '+namespace+'Account_Key__c, '+namespace+'Parent_Account_Key__c from Account');
		/**end Namespace for migrating to unmanaged package  **/
	}
	// Set Parent Accouint of Account
	// Return Type - Void 
	// Parameters - bc Database.Batchablecontext, sObjs List<sObject> 
	global void execute(Database.BatchableContext bc, List<sObject> sObjs){

        // simply update will also set the parent account for all the accounts using the trigger setParentAccount on update
        // finally update the list/batch of accounts [sobjs]
        update sObjs;
    }
    
    global void finish(Database.BatchableContext bc){
    }
}