/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for TransactionsCollection
 */
@isTest
private class TestTransactionsCollection
{
    static testMethod void testTransactionsCollection()
    {
        // create an test account
        Account acc = DataGenerator.accountInsert();

        // create a list of transaction related to the test account
        List<Transaction__c> txList = DataGenerator.transactionInsert(acc);

        List<List<Transaction__c>> txList2 = new List<List<Transaction__c>>();

        List<Id> listId = new List<Id>();

        //Getting List Of Tx Ids
        for(Integer temp = 0; temp < txList.size(); temp++)
        {
            listId.add(txList[temp].Id);
        }

        //Getting List Of List Of Tx
        txList2.add(txList);

        Test.startTest();

        //Constructor 1
        TransactionsCollection controller = new TransactionsCollection(txList2);

        controller.getTransactionsListOfList();

        List<List<Id>> idList1 = new List<List<Id>>();
        idList1 = TransactionsCollection.getTransactionIdsListOfList(controller);
        System.assertEquals(idList1.size(), 1);

        List<List<Id>> idList2 = new List<List<Id>>();
        idList2 = TransactionsCollection.getTransactionIdsListOfList(listId);
        System.assertEquals(idList2.size(), 1);

        //Constructor 2
        TransactionsCollection controller2 = new TransactionsCollection(txList);
        String tempStr1 = TransactionsCollection.getCommaSeparatedTxIds(txList);
        String tempStr2 = TransactionsCollection.getCommaSeparatedTxIds(listId);

        String tempStr3 = '';
        for(Transaction__c tx : txList)
        {
            tempStr3 += tx.id+',';
        }

        String tempstr4 = '';
        for(String txId : listId)
        {
            tempstr4 += txId+',';
        }

        System.assertEquals(tempStr2, tempstr4);

        Test.stopTest();
    }
}