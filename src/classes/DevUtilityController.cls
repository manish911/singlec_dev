/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This is class used for getting system administrator utility.
 */
public class DevUtilityController
{
    // method is used to get System Administrator 
    // return type : User
    public static User getSystemAdministrator()
    {
        // Fetch the profile record for System Administrator
        List<Profile> testProfile =new List<Profile>([select id
                               from profile where name like '%System Administrator%' LIMIT 1]);

        System.assertNotEquals(null, testProfile);

        // Fetch the default available CEO role
        List<UserRole> testRole = new  List<UserRole>([select id from UserRole where name = 'CEO' LIMIT 1]);

        System.assertNotEquals(null, testRole.get(0));

        // Create a new instance of user Object with test values
        // This is to create a user with a role who can act as portal account owner
        User adminUser = new User(alias = 'standt', email = 'test@987123436.com',
                                  emailencodingkey = 'UTF-8',
                                  lastname = 'TestLasName',
                                  languagelocalekey = 'en_US',
                                  localesidkey = 'en_US',
                                  profileid = testProfile.get(0).id,
                                  timezonesidkey = 'America/Los_Angeles',
                                  username = 'test@987123436.com');
        
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.User.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert adminUser;
        adminUser.userroleid = testRole.get(0).id;
        
        //===============================================================  
             list<String> lstFields = new list<String>{'userroleid'};
           Map<String,Schema.SObjectField> m1 = Schema.SObjectType.User.fields.getMap();
           for (String fieldToCheck : lstFields) {

              // Check if the user has create access on the each field
                if(!m1.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m1.get(fieldToCheck).getDescribe().isUpdateable()) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return null;
                  }
                }
            }
        //========================================================================================
        update adminUser;
        return adminUser;
    }
}