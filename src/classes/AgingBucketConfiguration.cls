/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Use to edit Field Labels of Account age Fields.
 */

public with sharing class AgingBucketConfiguration {
public String styleVar {get; set; }
public List<AgingBucketWrapper> buckets {get; set; }
public List<AgingBucketWrapper> basebuckets {get; set; }
    
    //constructor to set configure the profie  
    public AgingBucketConfiguration (){
        string profileType = '';
        List<User> lstProfile = null;
        lstProfile = [SELECT Profile.Name FROM User where Id = :UserInfo.getUserId()];
        profileType = lstProfile[0].Profile.Name;
        
    // accourding to the profile selection get current org configuration settings are use
    
        if(profileType =='System Administrator')
        {
            getCurrentConfig();
            
        }else {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,Label.Access_Denied);
            ApexPages.addMessage(msg);
        }
    }
    // Method is used for getting Current Config and add value into bucket
    // param - no parametar
    // return - void
    private void getCurrentConfig(){
        buckets = new List<AgingBucketWrapper>();
        for(Integer i=0; i<6; i++) {
            ConfigUtilController.AgingBucket newBuck = ConfigUtilController.getBucketByAge(i);
            String tempStr = ''+newBuck.maxRange;
            buckets.add(new AgingBucketWrapper(i, ConfigUtilController.getBucketByAge(i), tempStr, i==0 ? true : i==5 ? true : false));
        }
    }
     

    public class AgingBucketWrapper {
        public ConfigUtilController.AgingBucket bucket {get; set; }
        public String maxRange2 {get; set; }
        public Boolean maxRangeDisabled {get; set; }
        public Integer index {get; set; }

        // Constructor to set buckets value like maxRange, bucket, maxRangeDisable and index value
        // param - Integer i, , ConfigUtilController.AgingBucket b, String range, Boolean maxDisabled
        public AgingBucketWrapper(Integer i, ConfigUtilController.AgingBucket b, String range, Boolean maxDisabled) {

            this.maxRange2 = range;
            this.bucket = b;
            this.maxRangeDisabled = maxDisabled;
            this.index = i+1;
        }
    }
    // Method is used to update the bucketconfiguration value
    // return - void 
    public void save(){
        BucketConfig_Singlec__c bucketConfig = ConfigUtilController.getAgingBucketConfigObj();
        
        if(bucketConfig != null) {
            // set the new bucket age value and update it 
            bucketConfig.Age1__c = Integer.valueOf(buckets.get(1).maxRange2);
            bucketConfig.Age2__c = Integer.valueOf(buckets.get(2).maxRange2);
            bucketConfig.Age3__c = Integer.valueOf(buckets.get(3).maxRange2);
            bucketConfig.Age4__c = Integer.valueOf(buckets.get(4).maxRange2);
            
             //==========================================================================================  
         list<String> lstFields = new list<String>{'Age1__c','Age2__c','Age3__c','Age4__c'};
       Map<String,Schema.SObjectField> m = Schema.SObjectType.BucketConfig_Singlec__c.fields.getMap();
       for (String fieldToCheck : lstFields) {
          // Check if the user has create access on the each field
             if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
         
        }
        //========================================================================================
            
            update bucketConfig;

            getCurrentConfig();
        }
        
        

    }

    //reset to default custom conig
    public void resetToDefault(){
       

    }

}