/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   : Generates the Line Item for objects like Dispute, Payment,
 *           Payment Line for selected Transaction on TransactionDetail Page
 */

//Apex Class for gat transcationlist of an account
public with sharing class RelatedLineItemList
{
	public List<List<LineitemWrapper>> lineitemlist {get; set; }
	public List<Line_Item__c> templineItem {get; set; }
	public Dispute__c dispute {get; set; }
	public String LineitemId {get; set; }
	public string TransId {get; set; }

	Transaction__c controllertrans;
	//Property to hold tatal transaction balance for account
	public Decimal totalBalance {get; set; }

	public RelatedLineItemList(ApexPages.StandardController controller) {
		Dispute__c dispute = new Dispute__c();
		controllertrans = (Transaction__c) controller.getRecord();
		Integer i =0;
		totalBalance = 0;

		lineitemlist = new List<List<LineitemWrapper>>();
		lineitemlist.add(new List<LineitemWrapper>());
		TransId = ApexPages.currentPage().getParameters().get('Id');
		for(Line_item__c lineitem :[select Id,Name,Transaction__c,Line_Key__c,Item_Number__c,Line_Total__c,description__c from Line_Item__c where Transaction__c =: controllertrans.Id limit 1000])
		{

			if(i == 1000)
			{
				lineitemlist.add(new List<LineitemWrapper>());
				i = 0;
			}
			lineitemlist[lineitemlist.size()-1].add(new LineitemWrapper(lineitem,transId));
			i++;
		}
	}
	// method is used to get Line item Object List
	// return type : List<LineitemWrapper> 
	public List<LineitemWrapper> getLineitemObjList()
	{
		return lineitemlist[0];
	}

	public String transNoteId {get; set; }
	public String noteBody {get; set; }
	// method is used to add balance
	// return type : void
	public void addBalance()
	{
		//Set totalBalance inital
		totalBalance= 0;

		if(lineitemlist != null && lineitemlist.size() > 0 && lineitemlist[0].size() > 0)
		{

			//Loop to get selected  suppliers ids
			for(LineitemWrapper lineitemWrapper1 : getLineitemObjList())
			{
				if(lineitemWrapper1.selected == true)
				{
					if(totalBalance == 0)
						totalBalance = lineitemWrapper1.newLineItem.Line_Total__c;

					else
						totalBalance = totalBalance + lineitemWrapper1.newLineItem.Line_Total__c;

				}
			}

		}
	}

	public class LineitemWrapper
	{
		public boolean selected {get; set; }
		public Line_Item__c newLineItem {get; set; }
		public string transId1 {get; set; }

		//Constructor
		public LineitemWrapper(Line_Item__c line, String transactionId)
		{
			newLineItem = line;
			transId1 = transactionId;

		}
	}
}