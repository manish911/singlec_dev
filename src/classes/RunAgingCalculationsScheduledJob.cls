/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Schedulabel job to run aging calculations for all account in the system.
 */
global class RunAgingCalculationsScheduledJob implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
        Integer agingBatchSize = ( batchConfigObj!= null && batchConfigObj.Aging_Batch_Size__c != null && batchConfigObj.Aging_Batch_Size__c > 0 && batchConfigObj.Aging_Batch_Size__c < 201) ? batchConfigObj.Aging_Batch_Size__c.intValue() : 200;
        BatchAgingCalculations bca = new BatchAgingCalculations();
        Database.executeBatch(bca, agingBatchSize);
    }
}