/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for SetParentAccount.
 */
@isTest
private class TestSetParentAccount {

    static testMethod void myUnitTest() {
        // create an test account
        Account acc1 = new Account();
        acc1.Name = 'TestAccount1';
        acc1.AccountNumber = '7777';
        acc1.Account_Key__c = '7777';
        insert acc1;
        system.assertEquals(acc1.Account_Key__c,'7777');

        // create an test account
        Account acc2 = new Account();
        acc2.Name = 'TestAccount2';
        acc2.AccountNumber = '2222';
        acc2.Account_Key__c = '2222';
        insert acc2;
        system.assertEquals(acc2.Account_Key__c,'2222');
		
        // create an test account
        Account acc3 = new Account();
        acc3.Name = 'TestAccount3';
        acc3.AccountNumber = '3333';
        acc3.Account_Key__c = '3333';
        insert acc3;
        system.assertEquals(acc3.Account_Key__c,'3333');
    }
}