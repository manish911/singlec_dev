public class ConsoleCommonSendMsgController{

    public ConsoleCommonSendMsgController(){
    if(ApexPages.currentPage().getParameters().get('contactmethod') == 'phone'){
    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, Label.Label_Call_has_been_set_up)); 
    }
    else if(ApexPages.currentPage().getParameters().get('contactmethod') == 'print'){
     Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, Label.Label_Process_completed_message)); 
    }
    else {
        Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, ApexPages.currentPage().getParameters().get('contactmethod')+Label.Label_Sent_Now_you_can_close_this_window));
     }
    }
    
    static testMethod void testmethod1() {               
        
         Test.startTest();
         ApexPages.currentPage().getParameters().put('contactmethod','phone');
         System.assertEquals(ApexPages.currentPage().getParameters().get('contactmethod'),'phone');
         ConsoleCommonSendMsgController controller= new ConsoleCommonSendMsgController();
         Test.stopTest();
    }
        static testMethod void testmethod2() {               
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('contactmethod','print');
        System.assertEquals(ApexPages.currentPage().getParameters().get('contactmethod'),'print');
        ConsoleCommonSendMsgController controller= new ConsoleCommonSendMsgController();
        Test.stopTest();
    }
    
        static testMethod void testmethod3() {               
        
        Test.startTest();
         System.assertEquals(ApexPages.currentPage().getParameters().get('contactmethod'),null);
        //('---asseert---'+  ApexPages.currentPage().getParameters().get('contactmethod'));
        ConsoleCommonSendMsgController controller= new ConsoleCommonSendMsgController();
        Test.stopTest();
    }
    
}