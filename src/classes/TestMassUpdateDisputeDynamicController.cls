/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for MassUpdateDisputeDynamicController.
 */
 
@isTest
private class TestMassUpdateDisputeDynamicController{
    static testMethod void TestUpdateDispute(){
        Integer tempVar=5;
        //Trigger_Configuration__c objtrg = Trigger_Configuration__c.getOrgDefaults();
        //objtrg.Auto_Generate_Account_Key__c = false;
        //upsert objtrg;
        
        SysConfig_Singlec__c sysconfig = SysConfig_Singlec__c.getOrgDefaults();
        sysconfig.Billing_Statement_PDF_page__c = 'test';
        upsert sysconfig;
              
        Account tempAcc = new Account();
        tempAcc = insertAccount();
        
        List<Dispute__c> disList = new List<Dispute__c>();
        disList = insertDispute(tempAcc, tempVar);
   
        // get comma separated ids of Task to create a config object value
        String disIds = '';
        
        for(Dispute__c dis : disList)
        {
            disIds += ','+dis.Id;
        }
            
        Temp_Object_Holder__c TempObj = new Temp_Object_Holder__c();
        TempObj.Key__c = Userinfo.getUserName()+Datetime.now();
        TempObj.value__c = disIds;
        insert TempObj;
        system.debug('----TempObj----' + TempObj );
            
        // Passing selected list 
        ApexPages.currentPage().getParameters().put('key',TempObj.Key__c);
        Test.startTest();
        MassUpdateDisputeDynamicController updateDisobj = new MassUpdateDisputeDynamicController();
        updateDisobj.massUpdate();
        Test.stopTest();  
    }
    
    public static Account insertAccount(){
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
        insert accObj;
        return accObj;
    }
    
    /*public static Escalation_Matrix__c insertEscalationMatrix(){
        //Escalation_Matrix__c matrixObj = new Escalation_Matrix__c();
        //matrixObj.ownerId = UserInfo.getUserId();
        matrixObj.Dispute_Action_Owner__c= 'test@abc.com';
        insert matrixObj;
        return matrixObj;
    }*/
    
    public static List<Dispute__c> insertDispute(Account acc,Integer Count){
        List<Dispute__c> disObjList = new List<Dispute__c>();
        //Escalation_Matrix__c matrixObj = insertEscalationMatrix();
        for(integer i=0;i<count;i++){
            Dispute__c disObj = new Dispute__c();
            disObj.Account__c = acc.Id;
            disObj.Notes__c = 'Test Done';
            //disObj.Escalation_1__c = matrixObj.Id;
            disObjList.add(disObj);
        }
        insert disObjList;
        return disObjList;
    }
}