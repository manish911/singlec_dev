/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: Used for displaying and adding notes in the Related Transaction
 *        tab in AccountDetailPage in the list of transactions.
 */
public with sharing class NotesController {

    public Account controllerAccount {get; set; }

    public String entityId  {get; set; }

    public String accountId {get; set; }

    public boolean showDisputes {get; set; }

    public boolean showTransactionNotes {get; set; }

    public boolean showAccountNotes {get; set; }

    public boolean showZeroBalanceTxNotes {get; set; }

    public boolean showAttachments {get; set; }
     public List<Note> notelt {get; set; }

    //Search Text
    public string searchText {get; set; }

    private List<List<NotesWrapper>> notesList = new List<List<NotesWrapper>>();

    public boolean renderNotesList {get; set; }

    //Counter for List
    public integer listIndex =0;

    public Set<Id> txSet1 = new Set<Id>();
    public Set<Id> txSet2 = new Set<Id>();
    public Set<Id> txSet3 = new Set<Id>();
    public Set<Id> txSet4 = new Set<Id>();
    public Set<Id> txSet5 = new Set<Id>();
    public Set<Id> txSet6 = new Set<Id>();
    public Set<Id> txSet7 = new Set<Id>();
    public Set<Id> txSet8 = new Set<Id>();
    public Set<Id> txSet9 = new Set<Id>();
    public Set<Id> txSet10 = new Set<Id>();
    public Set<Id> txSet11 = new Set<Id>();

    private static final String SQL_FROM = 'Select n.Title, n.SystemModstamp, n.ParentId, n.OwnerId, n.LastModifiedDate, n.LastModifiedById, n.IsPrivate, n.IsDeleted, n.Id, n.CreatedDate, n.CreatedById, n.Body From Note n ';

    private static final String SQL_FROM_ATTACHMENT = 'Select n.Name, n.SystemModstamp, n.ParentId, n.OwnerId, n.LastModifiedDate, n.LastModifiedById, n.IsPrivate, n.IsDeleted, n.Id, n.CreatedDate, n.CreatedById From Attachment n ';

    private static String SQL_WHERE = ' where ';
    private static String SQL_ORDERBY = ' order by n.LastModifiedDate desc limit 999';

    //Tx related
    public Transaction__c controllerTx {get; set; }
    public String transactionId {get; set; }
    public Boolean isTransactionLevelOperation {get; set; }
    
    List<User_Preference__c> userPref {get; set; }
    
    public NotesController()
    {
        String idStr = ApexPages.currentPage().getParameters().get('id');

        String searchParam = ApexPages.currentPage().getParameters().get('txtSearch2');

        if(idStr != null){
            idStr = String.escapeSingleQuotes(idStr);
            entityId = idStr;
        }    
        //account id always starts with 001, any better way to identify??
        if(entityId.startsWith('001') && entityId != null)
        {
            accountId = entityId;
            isTransactionLevelOperation = false;
            showAccountNotes = true;
            showTransactionNotes = false;
        }
        else
        {
            transactionId = entityId;
            isTransactionLevelOperation = true;
            showAccountNotes = false;
            showTransactionNotes = true;
        }

        showZeroBalanceTxNotes = false;
        showDisputes = false;

        showAttachments = true;

        if(searchParam != null)
        {
            searchText = searchParam;
            showTransactionNotes = true;
            //setNotesList();
        }

       // get default setting from user preference
        userPref = new List<User_Preference__c>();
        userPref = [select Name,SortingFieldListView__c,SortListViewOrderType__c,SelectedListView__c,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        Decimal selectedTab = 0;
        
        if(userPref.size() > 0 ) {
             selectedTab = userPref[0].SelectedTab__c;
        }
        if(isTransactionLevelOperation){
            setNotesList();
         }
        String notetbsal = ApexPages.currentPage().getParameters().get('tabsel');
            
        if(notetbsal == '4' || selectedTab == 4)
            setNotesList();
    }
    // Method for retriving the list of all Transaction for Account
    // Returns List of Invoice for in Set for an Account
    // Parameters : parentNumber - String
    private List<Set<Id>> getParentIdsListOfSet(String parentNumber)
    {
        List<Set<Id>> txListOfSet = new List<Set<Id>>();
        if(parentNumber == null )
            return txListOfSet;

        String sqlQuery = 'Select tx.id, tx.Name from Transaction__c tx where tx.Name = :parentNumber ';

        List<Transaction__c> tx = null;
        if(!isTransactionLevelOperation)
        {
            
            sqlQuery += ' and account__c = :accountId ';
        }
        else
        { //account id is null in case of tx level operation
            sqlQuery += ' and id = :transactionId';
        }

        if(showTransactionNotes && (!showZeroBalanceTxNotes))
        {
            sqlQuery += ' AND tx.Balance__c !=0';
        }
        if(showZeroBalanceTxNotes && (!showTransactionNotes))
        {
            sqlQuery += ' AND tx.Balance__c =0';
        }

        for(List<Transaction__c> txList : Database.query(sqlQuery))
        {
            Set<Id> idList = null;

            if(txList !=null &&  txList.size() > 0)
            {
                idList = new Set<Id>();

                for(Transaction__c trans : txList)
                {
                    idList.add(trans.Id);
                }

                txListOfSet.add(idList);
            }

        }

        return txListOfSet;
    }
    // Method for get parent 
    // Returns Id  
    // Parameter : parentNumber - String
    private Id getParent(String parentNumber)
    {
        Id parentId = null;
        if(parentNumber != null )
        {
            List<Transaction__c> tx = null;
            if(!isTransactionLevelOperation)
            {
                tx = [Select Id from Transaction__c where Name = :parentNumber and account__c = :accountId limit 1];
            }
            else
            { //account id is null in case of tx level operation
                tx = [Select Id from Transaction__c where Name = :parentNumber and id = :transactionId];
            }

            if(tx != null && tx.size() > 0) {
                parentId = tx.get(0).Id;
            }
            else {
                List<Dispute__c> dp = [Select Id from Dispute__c where Name = :parentNumber and account__c = :accountId limit 1];
                if(dp != null && dp.size() > 0)
                {
                    parentId = dp.get(0).Id;
                }
            }
        }
        return parentId;
    }
    // Method returns List of Account along with Parent Entity
    // Return Type - List of Mapped Account ID along with Parent Entity
    // No Parameters are passed in this method
    private List<Map<Id,ParentEntity>> getAccountListOfMap()
    {
        List<Map<Id,ParentEntity>> accListOfMap = new List<Map<Id,ParentEntity>> ();

        Map<Id, ParentEntity> accIdsMap = new Map<Id, ParentEntity>();

        //  try{
        List<Account> acc  =new List<Account>([Select Id, Name, AccountNumber from Account where id= :accountId]);
        ParentEntity entity = new ParentEntity();
        entity.entityId = accountId;
        entity.entityName = 'Account Number';
        entity.entityValue = acc.get(0).AccountNumber;
        accIdsMap.put(accountId, entity);
        accListOfMap.add(accIdsMap);
        //}catch(QueryException e)
        //{
        //}
        return accListOfMap;
    }
    // Method returns List of Transaction along with Parent Entity
    // Return Type - List of Mapped ATransaction ID along with Parent Entity
    // No Parameters are passed in this method 
    private List<Map<Id,ParentEntity>> getTxListOfMap()
    {
        List<Map<Id,ParentEntity>> txListOfMap = new List<Map<Id,ParentEntity>> ();

        Map<Id, ParentEntity> txIdsMap = new Map<Id, ParentEntity>();

        //  try{
        if(transactionId != null && transactionId != ''){
        List<Transaction__c> tx =new List<Transaction__c>([Select tx.id, tx.Name from Transaction__c tx where tx.id= :transactionId]);
        ParentEntity entity = new ParentEntity();
        entity.entityId = transactionId;
        entity.entityName = 'Transaction Number';
        entity.entityValue = tx.get(0).Name;
        txIdsMap.put(transactionId, entity);

        txListOfMap.add(txIdsMap);
        }
        // }catch(QueryException e)
        //{
        //}
        return txListOfMap;
    }
    // method is used to set notes List
    // return type : PageReference
    public PageReference setNotesList()
    {

        List<Set<Id>> parentEntityListOfSet = new List<Set<Id>>();
        List<Map<Id,ParentEntity>> parentEntityListOfMap =  new List<Map<Id,ParentEntity>> ();
        notesList.clear();
        notesList.add(new List<NotesWrapper>());

        //try
        //{
        if(!isTransactionLevelOperation)
        {
            if((!showAccountNotes) && (!showTransactionNotes) && (!showZeroBalanceTxNotes) && (!showDisputes))
            {
                return null;
            }
            if(showAccountNotes)
            {
                Set<Id> accountSet = new Set<Id>();
                accountSet.add(accountId);
                parentEntityListOfSet.add(accountSet);
                List<Map<Id,ParentEntity>> accMapList = getAccountListOfMap();
                parentEntityListOfMap.addAll(accMapList);
            }
            if(showTransactionNotes || showZeroBalanceTxNotes)
            {
                List<Set<Id>> txListOfSet = getTransactionIdsListOfSet();
                parentEntityListOfSet.addAll(txListOfSet);
                List<Map<Id,ParentEntity>> listMap = getTransactionsListOfMap();
                parentEntityListOfMap.addAll(listMap);
            }
        }
        else
        {
            if((!showTransactionNotes) && (!showDisputes))
                return null;

            if(showTransactionNotes)
            {
                Set<Id> txSet = new Set<Id>();
                txSet.add(transactionId);
                parentEntityListOfSet.add(txSet);

                List<Map<Id,ParentEntity>> txMapList = getTxListOfMap();
                parentEntityListOfMap.addAll(txMapList);

            }
        }

        //disputes is common both with tx and acc
        if(showDisputes)
        {
            List<Set<Id>> disputeListOfSet = getDisputeIdsListOfSet();
            List<Map<Id,ParentEntity>> disputeMap = getDisputesListOfMap();

            parentEntityListOfSet.addAll(disputeListOfSet);
            parentEntityListOfMap.addAll(disputeMap);
        }

        /*}catch(Exception e)
           {
           }*/

        // case where no tx with zero balance found and account and dispute filter is unchecked
        if(parentEntityListOfSet == null || parentEntityListOfSet.size() == 0)
        {
            return null;
        }

        //Integer i = 0;
        listIndex = 0;

        Id parentIds = null;
        if(searchText!=null) {

            if(searchText !='')
            {
                //String str = searchText.trim() ;

                //searchText = str;
            }

            searchText = string.escapeSingleQuotes(searchText);
            List<Set<Id>> parentIdListOfSet = getTransactionIdsListOfSet();
            parentIds = getParent(searchText);
        }

        String searchText =  getSearchText();

        String soqlwhere = '';
        printMap(parentEntityListOfMap);
        printSet(parentEntityListOfSet);

        if(parentEntityListOfSet != null && parentEntityListOfSet.size() > 0)
        {
            soqlwhere = getWhereClause(parentEntityListOfSet);
        }

        //String EXCLUDE_CLAUSE = '';

        //  try {

        //include clause
        String INCLUDE_CLAUSE = '';
        String INCLUDE_CLAUSE_ATTACHMENT = '';

        if(searchText != '')
        {
               searchText ='%'+searchText +'%'; 
            INCLUDE_CLAUSE += ' AND (n.title Like :searchText ';
            INCLUDE_CLAUSE_ATTACHMENT += ' AND (n.name Like :searchText ';

            if(parentIds != null)
            {
                INCLUDE_CLAUSE += ' OR n.parentId = :parentIds ';
                INCLUDE_CLAUSE_ATTACHMENT += ' OR n.parentId = :parentIds ';
            }
        }

        if(INCLUDE_CLAUSE != '')
        {
            INCLUDE_CLAUSE += ' )';
        }

        if(INCLUDE_CLAUSE_ATTACHMENT != '')
        {
            INCLUDE_CLAUSE_ATTACHMENT += ' )';
        }

        String whereClause = SQL_WHERE;
        Integer i = 0;
        if(showAttachments)
        {
            whereClause += soqlWhere + INCLUDE_CLAUSE_ATTACHMENT;
            String sqlQueryForAttachment =  string.escapeSingleQuotes(SQL_FROM_ATTACHMENT) +  string.escapeSingleQuotes(whereClause) +  string.escapeSingleQuotes(SQL_ORDERBY);

            for(List<Attachment> attachments :  Database.query(sqlQueryForAttachment))
            {
                for(Attachment attachmentObj : attachments)
                {
                    NotesWrapper wrapper = getNoteWrapper(attachmentObj, parentEntityListOfMap);
                    if(i == 20)
                    {
                        notesList.add(new List<NotesWrapper>());
                        i=0;
                    }

                    notesList[notesList.size()-1].add(wrapper);
                    i++;
                }
            }
        }

        whereClause =SQL_WHERE;
        whereClause += soqlWhere + INCLUDE_CLAUSE;
        String sqlQuery =  string.escapeSingleQuotes(SQL_FROM) +  string.escapeSingleQuotes(whereClause) +  string.escapeSingleQuotes(SQL_ORDERBY);

        i = 0;
        for(List<Note> notes :  Database.query(sqlQuery))
        {
            for(Note cnote : notes)
            {
                if(cnote.body !=null && cnote.body.length()>300){
                       
                       cnote.body = cnote.body.SubString(0,300)+'....';
                     
                    }
                    
                
                NotesWrapper wrapper = getNoteWrapper(cnote, parentEntityListOfMap);
                if(i == 20)
                {
                    notesList.add(new List<NotesWrapper>());
                    i=0;
                }

                notesList[notesList.size()-1].add(wrapper);
                i++;
            }
        }

    
        return null;
    }
    // Provides Searching facility in notes tab in Account Detail Page
    // Return Type - String 
    // No parameters are passed in this Method
    private String getSearchText()
    {
        String searchTemp = searchText;
        if(searchTemp == null ||  searchTemp.trim() == '') {
            searchTemp = '%';
        }
        else
        {
            if(searchTemp.contains('*'))
                searchTemp = searchTemp.replace('*','%');
            else
                searchTemp = '%' + searchTemp + '%';
        }
        return searchTemp;
    }
    // Adding Notes on selected Transaction ID
    // Return Type - NotesWrapper
    // Parameters - cnote Note, listmap List<Map<Id,ParentEntity>>
    private NotesWrapper getNoteWrapper(Note cnote, List<Map<Id,ParentEntity>> listMap )
    {
        NotesWrapper wrapper = new NotesWrapper();
        //in all the available maps search the parent of the given note and then pick the parent information
        for(Map<Id,ParentEntity> txMap : listMap )
        {
            if(cnote.title != null && txMap.containsKey(cnote.parentId))
            {
                //get the ParentEntity corresponding to the note
                NoteAttachmentObj obj =  new NoteAttachmentObj();
                obj.id = cnote.Id; obj.name = cnote.Title; obj.body = cnote.Body;
                obj.lastModifiedDate = String.valueOf(cnote.LastModifiedDate).split(' ')[0];
                obj.isNote= true;

                wrapper = new NotesWrapper(obj,txMap.get(cnote.parentId));
            }
        }
        return wrapper;
    }
    // Adding attachment notes on selected Transaction ID
    // Return Type - NotesWrapper
    // Parameters - attachmentobj Attachment , listmap List<Map<Id,ParentEntity>>
    private NotesWrapper getNoteWrapper(Attachment attachmentobj, List<Map<Id,ParentEntity>> listMap )
    {
        NotesWrapper wrapper = new NotesWrapper();
        //in all the available maps search the parent of the given note and then pick the parent information
        for(Map<Id,ParentEntity> txMap : listMap )
        {
            if(attachmentobj.name != null && txMap.containsKey(attachmentobj.parentId))
            {
                //get the ParentEntity corresponding to the note
                NoteAttachmentObj obj =  new NoteAttachmentObj(attachmentobj.Id, attachmentobj.Name, attachmentobj.LastModifiedDate);
                obj.isNote = false;
                wrapper = new NotesWrapper(obj,txMap.get(attachmentobj.parentId));
            }
        }
        return wrapper;
    }
    // method is used to Print set
    // return type : void 
    // parameters : txList - List<Set<Id>>
    private void printSet(List<Set<Id>> txList)
    {
        if(txList != null) {
            for(Set<Id> setx : txList)
            {
                for(Id sid : setx)
                {
                }
            }
        }
    }
    // method is used to Print Map
    // return type : void 
    // parameters : listMap - List<Map<Id,ParentEntity>>
    private void printMap(List<Map<Id,ParentEntity>> listMap )
    {
        for(Map<Id,ParentEntity> txMap : listMap )
        {
        }
    }
    // Adding list of Transaction in different sets which are created 
    // Return Type - String
    // Parameter - txListOfSet List<Set<Id>>
    public String getWhereClause(List<Set<Id>> txListOfSet)
    {
        String soqlwhere = '';

        if(txListOfSet == null || txListOfSet.size() == 0 ) return soqlwhere;

        Integer index = 0;

        Integer listSize = txListOfSet.size();

        txSet1 = new Set<Id>(); txSet2= new Set<Id>(); txSet3 = new Set<Id>();
        txSet4 = new Set<Id>(); txSet5 = new Set<Id>(); txSet6 = new Set<Id>();
        txSet7 = new Set<Id>(); txSet8 = new Set<Id>(); txSet9 = new Set<Id>(); txSet10 = new Set<Id>();

        if(index <= listSize-1)
        {
            txSet1.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' (n.ParentId IN :txSet1';

        }
        if(index <= listSize-1)
        {
            txSet2.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet2';
        }
        if(index <= listSize-1)
        {
            txSet3.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet3';
        }
        if(index <= listSize-1)
        {
            txSet4.addAll(txListOfSet.get(index));
            index++;

            soqlwhere += ' OR n.ParentId IN :txSet4';
        }
        if(index <= listSize-1)
        {
            txSet5.addAll(txListOfSet.get(index));
            index++;

            soqlwhere += ' OR n.ParentId IN :txSet5';
        }
        if(index <= listSize-1)
        {
            txSet6.addAll(txListOfSet.get(index));
            index++;

            soqlwhere += ' OR n.ParentId IN :txSet6';

        }
        if(index <= listSize-1)
        {
            txSet7.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet7';
        }

        if(index <= listSize-1)
        {
            txSet8.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet8';
        }

        if(index <= listSize-1)
        {
            txSet9.addAll(txListOfSet.get(index));

            index++;
            soqlwhere += ' OR n.ParentId IN :txSet9';
        }

        if(index <= listSize-1)
        {
            txSet10.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet10';
        }

        if(index <= listSize-1)
        {
            txSet11.addAll(txListOfSet.get(index));
            index++;
            soqlwhere += ' OR n.ParentId IN :txSet11';
        }

        soqlwhere += ' )';

        return soqlwhere;
    }
    // Returns list of all notes which are created
    // Return Type - List of NoteWrapper
    // No Parameter are passed 
    public List<NotesWrapper> getNotesList()
    {
        if(notesList != null && notesList.size() > 0)
            return notesList[listIndex];
        
        return new List<NotesWrapper>();
    }

    public Boolean hasPreviousNotes {
        get {
            Boolean val = (listIndex > 0);

            return val;
        }
        set;
    }

    public Boolean hasNextNotes {
        get {
            Boolean val =  listIndex < (notesList.size()-1);

            return val;
        }
        set;
    }

    //Decrements the ListIndex for paging
    public PageReference getPreviousNotes()
    {
        if(listIndex > 0)
            listIndex--;

        return null;
    }

    //Increments the ListIndex for paging
    public PageReference getNextNotes()
    {
        if(listIndex < notesList.size()-1)
            listIndex++;
        //return new Pagereference('/apex/NotesPage?accId='+controllerAccount.id);
        return null;
    }
    // Method for returning list of ID of Transactions
    // Return Type - List of Id in Set for Transaction
    // No Parameters are passed in this method
    private List<Set<Id>> getTransactionIdsListOfSet()
    {
        List<Set<Id>> txListOfSet = new List<Set<Id>>();
       
        String sqlQuery = 'Select tx.id, tx.Name from Transaction__c tx where tx.account__c = :accountId';

        if(showTransactionNotes && (!showZeroBalanceTxNotes))
        {
            sqlQuery += ' AND tx.Balance__c !=0';
        }
        if(showZeroBalanceTxNotes && (!showTransactionNotes))
        {
            sqlQuery += ' AND tx.Balance__c =0';
        }
        if(showZeroBalanceTxNotes && showTransactionNotes)
        {
            //show all
        }
        //try {

        /*if(showZeroBalanceTxNotes)
           {
                sqlQuery = 'Select tx.id, tx.Name from Transaction__c tx where tx.account__c = :accountId AND tx.Balance__c = 0';
           }*/

        for(List<Transaction__c> txList : Database.query(sqlQuery))
        {

            Set<Id> idList = null;

            if(txList !=null &&  txList.size() > 0)
            {
                idList = new Set<Id>();

                for(Transaction__c tx : txList)
                {
                    idList.add(tx.Id);
                }

                txListOfSet.add(idList);
            }

        }

        return txListOfSet;
    }
    // Returns List of all Mapped Transaction along with Parent Entity
    // Return Type - List of all Transaction Mapped with their Parent Entity
    // No Parameters are passed in this method 
    private List<Map<Id,ParentEntity>> getTransactionsListOfMap()
    {
        List<Map<Id,ParentEntity>> txListOfMap = new List<Map<Id,ParentEntity>> ();

        String sqlQuery = 'Select tx.id, tx.Name from Transaction__c tx where tx.account__c = :accountId';

        if(showTransactionNotes && (!showZeroBalanceTxNotes))
        {
            sqlQuery += ' AND tx.Balance__c !=0';
        }
        if(showZeroBalanceTxNotes && (!showTransactionNotes))
        {
            sqlQuery += ' AND tx.Balance__c =0';
        }
        if(showZeroBalanceTxNotes && showTransactionNotes)
        {
            //show all
        }
        
        for(List<Transaction__c> txList : Database.query(sqlQuery))
        {

            Map<Id, ParentEntity> transactionIdsMap = null;

            if(txList !=null &&  txList.size() > 0)
            {
                transactionIdsMap = new Map<Id,ParentEntity>();

                for(Transaction__c tx : txList)
                {
                    ParentEntity entity = new ParentEntity();
                    entity.entityId = tx.Id;
                    entity.entityName = 'Transaction Number';
                    entity.entityValue = tx.Name;
                    transactionIdsMap.put(tx.Id, entity);
                }
                txListOfMap.add(transactionIdsMap);
            }
        }
    
        return txListOfMap;
    }
    // Returns list of Disputes along with their mapped Parent Entity
    // Return Type - List of Disputes along with mapped Parent Entity
    // No parameters are passed in this method
    private List<Map<Id,ParentEntity>> getDisputesListOfMap()
    {
        List<Map<Id,ParentEntity>> dpListOfMap = new List<Map<Id,ParentEntity>> ();
        String dbQuery = null;

        if(isTransactionLevelOperation) {
            dbQuery = 'Select d.id, d.Name, d.Account__r.AccountNumber, d.amount__c from Dispute__c d where d.transaction__c = :transactionId';
        }
        else {
            dbQuery = 'Select d.id, d.Name, d.Account__r.AccountNumber, d.amount__c from Dispute__c d where d.account__c = :accountId';
        }

        for(List<Dispute__c> dpList : Database.query(dbQuery))
        {
            Map<Id, ParentEntity> dpIdsMap = null;

            if(dpList !=null &&  dpList.size() > 0)
            {
                dpIdsMap = new Map<Id,ParentEntity>();

                for(Dispute__c d : dpList)
                {
                    ParentEntity entity = new ParentEntity();
                    entity.entityId = d.Id;
                    entity.entityName = 'Dispute Number';
                    entity.entityValue = d.Name;

                    dpIdsMap.put(d.Id, entity);
                }
                dpListOfMap.add(dpIdsMap);
            }
        }

        return dpListOfMap;
    }
    // Returns list of Id for all disputes created in a Set
    // Return Type - List of Ids for all disputes in Set
    // No parameters are passed in this method
    private List<Set<Id>> getDisputeIdsListOfSet()
    {
        List<Set<Id>> disputeIdsListOfSet = new List<Set<Id>>();

        String dbQuery = null;
        if(isTransactionLevelOperation) {
            dbQuery = 'Select d.id, d.Name, d.Account__r.AccountNumber, d.amount__c from Dispute__c d where d.transaction__c = :transactionId';
        }
        else {
            dbQuery = 'Select d.id, d.Name, d.Account__r.AccountNumber, d.amount__c from Dispute__c d where d.account__c = :accountId';
        }

        for(List<Dispute__c> dpList : Database.query(dbQuery))
        {
            Set<Id> idList = null;

            if(dpList !=null &&  dpList.size() > 0)
            {
                idList = new Set<Id>();

                for(Dispute__c dp : dpList)
                {
                    idList.add(dp.Id);
                }
                disputeIdsListOfSet.add(idList);
            }

        }

        return disputeIdsListOfSet;
    }
    
    public class NotesWrapper
    {
        //public Boolean selected {get;set;}
        public NoteAttachmentObj note {get; set; }
        public ParentEntity entity {get; set; }

        public NotesWrapper()
        {
        }
        public NotesWrapper(NoteAttachmentObj n, ParentEntity pe)
        {
            this.note = n;
            this.entity = pe;
        }

    }

    public class ParentEntity
    {
        public Id entityId {get; set; }
        public String entityName {get; set; }
        public String entityValue {get; set; }

        public ParentEntity()
        {
            this.entityName = 'Number';
            this.entityValue = '';

        }

        public ParentEntity(Id entityId, String label, String value)
        {
            this.entityId = entityId;
            this.entityName =  label;
            this.entityValue = value;
        }
    }

    public class NoteAttachmentObj
    {
        public Id id {get; set; }
        public String name {get; set; }
        public String lastModifiedDate {get; set; }
        public String body {get; set; }
        public Boolean isNote {get; set; }

        public NoteAttachmentObj()
        {

        }
        public NoteAttachmentObj(Id id, String name, datetime d)
        {
            this.id = id;
            this.name = name;
            this.lastModifiedDate = String.valueOf(d).split(' ')[0];

            //tempList.add(String.valueOf(createdDate).split(' ')[0]);

        }

    }

}