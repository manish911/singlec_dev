/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is Test class for AkritivConstantsTest.
 */

@istest(seeAllData = true)
private class AkritivConstantsTest{
 
    static testMethod void testConstant(){    
        AkritivConstants obj = new AkritivConstants();
        System.assertNotEquals('',AkritivConstants.CURRENCY_BRL_CODE);
    }
    
}