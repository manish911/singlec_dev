/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
 
 /*
     Test class for Mass update Transactions functionality.
*/
@isTest
public class TestMassUpdateTransactionController{
    
    
    public static testMethod void testCase() {
        
        Integer tempVar = 5;
        
        // Create Account  
        Account tempAccount = new Account();
        tempAccount = accountInsert();
        
        System.assertEquals(tempAccount.name,'testAccount');
        
        // Create Transactions
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = listTransactionInsert(tempAccount,tempVar);

        // get comma separated ids of transaction to create a config object value
        String txIds = '';

        for(Transaction__c tx : txList)
        {
            txIds += ','+tx.Id;
        }
        
        // create a config object for comma separated tx ids.
        Temp_Object_Holder__c Config = new Temp_Object_Holder__c();
        Config.Key__c = Userinfo.getUserName()+Datetime.now();
        Config.value__c = txIds;
        insert Config;

        // Passing selected list 
        ApexPages.currentPage().getParameters().put('key',Config.Key__c);
        
        MassUpdateTransactionController robj = new MassUpdateTransactionController();
                      
        Test.startTest();
        // robj.transList = akritiv__Transaction__c ;       
        robj.massUpdate();
        
        robj.getTotalBalance();
        
        Test.stopTest();
    }
    
    // method is used for account insert 
    // return type : Account
    public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
        
        insert accObj;
        return accObj;
    }
    
    public static List<Transaction__c> listTransactionInsert(Account acc, Integer count){

        //create transaction on Account

        List<Transaction__c> txObjList = new List<Transaction__c>();
        system.debug('-------- acc..:: '+acc);
        Account tempAcc = acc;
        system.debug('-------- tempAcc ..:: '+tempAcc );

        for(Integer i=0; i<count; i++){
        
            Transaction__c txObj = new Transaction__c();
            system.debug('-------- tempAcc.ID ..:: '+tempAcc.Id);
            txObj.Account__c = tempAcc.Id;
            txObj.Amount__c = 12312 + i;
            txObj.Balance__c = 6336 + i;
            txObj.Base_Balance__c  = 6336 + i;
            txObj.Name = 'testno' + i;
            txObj.Create_Date__c = Date.today().addMonths(-7);
            txObj.Due_Date__c = Date.today().addMonths(-6);
            txObj.Disputed_Amount__c = 0;
            txObj.Promised_Amount__c = 100;
            txObj.Promise_Date__c = Date.today().addMonths(+1);
            txObj.Batch_Number__c = 'SAP20130802';
            txObj.Source_System__c = 'SAP';
            txObj.Transaction_Key__c =  ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue(); 
            txObj.Notes__c = 'Testing Note';
            txObjList.add(txObj);
        }
        
        system.debug('-------- Trx List..:: '+txObjList);
        insert txObjList;

        return txObjList;
    }
    
}