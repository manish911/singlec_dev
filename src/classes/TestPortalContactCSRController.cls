/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for PortalContactCSRController
 */
@isTest
private with sharing class TestPortalContactCSRController
{

    static testMethod void testPortalContactCSRController()
    {

        Account accWithuser;
        Dispute__c dispObj;
        Contact conWithUser;

        User adminUser = DevUtilityController.getSystemAdministrator();
        

        Test.startTest();

        if(adminUser != null)
        {
            System.runAs(adminUser)
            {
                //Create a new instance of Account Object with test values
                accWithuser = DataGenerator.accountInsert();
                

                //Create a new instance of the Contact with test values
                conWithUser = DataGenerator.contactInsert(accWithuser);

                List<Dispute__c> tempDispList = new List<Dispute__c>();
                tempDispList = DataGenerator.disputeInsert(accWithuser, null);

                dispObj = tempDispList[0];

            }
        }

        //List for getting the profile
        List<Profile> profile =
        [select id from profile where name like 'Akritiv Customer Portal User' LIMIT 1];

        //Create a new instance of the User with test values
        User portalUser;
         
       /* List<User> newUser = new List<User>();
        newUser = [select email from User where Id =: accWithuser.ownerId]; 
        System.assertEquals(newUser[0], 'abc');*/
        
        if(profile.size() > 0)
        {

            portalUser = new User(alias = 'standt', email = 'test@9876test.com',
                                  emailencodingkey = 'UTF-8', lastname = 'TestLasName', languagelocalekey = 'en_US',
                                  localesidkey = 'en_US', profileid = profile.get(0).id, contactId = conWithUser.id,
                                  timezonesidkey = 'America/Los_Angeles', username = Math.random() + '@test.com');
            insert portalUser;

            System.assertNotEquals(portalUser, null);

            System.RunAs(portalUser)
            {
                PortalContactCSRController controller = new PortalContactCSRController();
                controller.emailsubject = 'test';
                controller.defaultMail = portalUser.email;
                
                controller.cancel();
                controller.redirectToEmailPage();
                
            }
        }
        Test.stopTest();
    }
    
     static testMethod void testPortalContactCSRController1()
    {

        Account accWithuser;
        Dispute__c dispObj;
        Contact conWithUser;

        User adminUser = DevUtilityController.getSystemAdministrator();
        

        Test.startTest();

        if(adminUser != null)
        {
            System.runAs(adminUser)
            {
                //Create a new instance of Account Object with test values
                accWithuser = DataGenerator.accountInsert();
                

                //Create a new instance of the Contact with test values
                conWithUser = DataGenerator.contactInsert(accWithuser);

                List<Dispute__c> tempDispList = new List<Dispute__c>();
                tempDispList = DataGenerator.disputeInsert(accWithuser, null);

                dispObj = tempDispList[0];

            }
        }

        //List for getting the profile
        List<Profile> profile =
        [select id from profile where name like 'Akritiv Customer Portal User' LIMIT 1];

        //Create a new instance of the User with test values
        User portalUser;
         
       /* List<User> newUser = new List<User>();
        newUser = [select email from User where Id =: accWithuser.ownerId]; 
        System.assertEquals(newUser[0], 'abc');*/
        
        if(profile.size() > 0)
        {

            portalUser = new User(alias = 'standt', email = 'test@9876test.com',
                                  emailencodingkey = 'UTF-8', lastname = 'TestLasName', languagelocalekey = 'en_US',
                                  localesidkey = 'en_US', profileid = profile.get(0).id, contactId = conWithUser.id,
                                  timezonesidkey = 'America/Los_Angeles', username = Math.random() + '@test.com');
            insert portalUser;
            
            //accWithuser.ownerId = portalUser.Id;

            System.assertNotEquals(portalUser, null);
            

            System.RunAs(portalUser)
            {
                PortalContactCSRController controller = new PortalContactCSRController();
                controller.emailsubject = '';
                controller.defaultMail = portalUser.email;
                
                controller.cancel();
                controller.redirectToEmailPage();
                
            }
        }
        Test.stopTest();
    }

}