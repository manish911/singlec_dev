/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for DataPurgeScheduledJob.
 */
@isTest
private class TestDataPurgeJob {
    static testMethod void testDataPurgeScheduledJob() {

        test.starttest();

        DataPurgeJob datajobObj = new DataPurgeJob();

        String sch = '0 0 23 * * ?';
        system.assertEquals(sch,'0 0 23 * * ?');
        system.schedule('Contract Creates', sch, datajobObj);
        
        test.stopTest();
    }
}