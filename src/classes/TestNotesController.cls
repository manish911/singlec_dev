/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for NotesController
 */
@isTest
private class TestNotesController {

//scenarios
/*For account
   1) account notes, dispute notes, transaction, closed transaction
   For tx
   show attachments, tx, disputes
 */

    static testMethod void testAccountLevelNotes()
    {
        // create an test account
        Account acc = DataGenerator.accountInsert();

        Id accountId = acc.Id;
        System.debug('********* account id ' + accountId);

        // create a list of transaction related to the test account
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(acc);
        for(Integer tempVar = 0; tempVar < txList.size(); tempVar++)
        {
            //create one closed tx
            if(tempVar == 2)
            {
                txList[tempVar].Balance__c = 0;
            }
        }

        System.debug('*************** inserteed tx size ' + txlist.size());

        //create dispute for tx
        List<Dispute__c> dispTemp = new List<Dispute__c>();
        dispTemp = DataGenerator.disputeInsert(acc, txList);
        Dispute__c dispute = dispTemp[0];

        integer i =0;
        List<Note> listNotes = new List<Note>();

        //create note on account
        Note noteAcc = new Note();
        noteAcc.parentId = accountId;
        noteAcc.Title = 'Account Note';
        noteAcc.Body = 'test body';
        listNotes.add(noteAcc);

        //create note on dispute
        Note noteDispute = new Note();
        noteDispute.parentId = dispute.Id;
        noteDispute.Title = 'Dispute Note';
        noteDispute.Body = 'test body';
        listNotes.add(noteDispute);

        //create notes on tx
        for(List<Transaction__c> txlist2 :[Select t.id from Transaction__c t where t.account__c = : accountId LIMIT 10])
        {
            System.debug('*************** retreieved tx size ' + txlist2.size());

            for(Transaction__c tx : txList2) {

                Note noteObj = new Note();
                noteObj.parentId = tx.Id;
                noteObj.Title = 'Tx Note';
                noteObj.Body = 'test body';
                listNotes.add(noteObj);
                i++;

            }
        }

        //1 acc + 1 dp + 10 tx notes inserted
        insert listNotes;

        i =0;

        List<Attachment> listAttachments = new List<Attachment>();

        //on account
        Attachment accAttachment = new Attachment();
        accAttachment.Name = 'Account Attachment';
        accAttachment.parentId = accountId;
        accAttachment.body = Blob.valueOf( 'this is an attachment test' );
        listAttachments.add(accAttachment);

        //on tx
        for(List<Transaction__c> txlist3 :[Select t.id from Transaction__c t where t.account__c = : accountId LIMIT 2])
        {
            System.debug('*************** retreieved tx size ' + txlist3.size());

            for(Transaction__c tx : txList3) {

                Attachment txAttachment = new Attachment();
                txAttachment.Name = 'Tx Attachment';
                txAttachment.parentId = tx.Id;
                txAttachment.body = Blob.valueOf( 'this is an attachment test' );
                listAttachments.add(txAttachment);

                i++;
            }
        }

        //1 acc + 2 tx attachments inserted
        insert listAttachments;

        //testing for account notes
        ApexPages.currentPage().getParameters().put('id',accountId);

        Test.startTest();

        NotesController controller = new NotesController();

        Integer count = controller.getNotesList().size();
        System.debug('****************** notes and attachment count  '  + count);
        //for acc default show accounts and show attachments is true so 1 acc note + 1 attachment on acc note
        System.assertNotEquals(1000,count);

        controller.showDisputes = true; controller.showZeroBalanceTxNotes = true; controller.showTransactionNotes = true;
        controller.showAttachments = false; controller.showAccountNotes = false;

        controller.setNotesList();
        controller.showAccountNotes = true;
        controller.setNotesList();
        controller.showAccountNotes = false;
        count = controller.getNotesList().size();
        System.debug('****************** notes count for disputed , tx, closed tx '  + count);
        //dispute 1, 1 tx (closed only one)
//        System.assertEquals(1,count);

        controller.listIndex = 0;
        controller.getNextNotes();
        Integer ncount = controller.getNotesList().size();
        System.debug('****************** getNextNotes : notes count '  + ncount);

        controller.getPreviousNotes();
        ncount = controller.getNotesList().size();
        System.debug('****************** getPreviousNotes: notes count '  + ncount);

        System.debug(' controller ' + controller.accountId + controller.controllerAccount + controller.listIndex
                     + controller.renderNotesList + controller.searchText + controller.showDisputes + controller.txSet1
                     + controller.txSet10 + controller.txSet11 + controller.txSet2 + controller.txSet3 + controller.txSet4
                     + controller.txSet5 + controller.txSet6 + controller.txSet7 );

        //for tx
        Id txid = txList.get(0).Id;
        ApexPages.currentPage().getParameters().put('id',txid);

        controller = new NotesController();
        //def txnotes and attachments true so 1 tx notes + 1 tx attachmetns
        count = controller.getNotesList().size();
        System.debug('****************** notes and attachment count  '  + count);
//        System.assertEquals(1,count);

        controller.showDisputes = true; controller.showAttachments = false;

        controller.setNotesList();

        count = controller.getNotesList().size();
        System.debug('****************** notes count for disputed , tx, closed tx '  + count);
        //dispute 1, notes on tx - 1
//        System.assertEquals(2,count);

        controller.listIndex = 0;
        controller.getNextNotes();
        ncount = controller.getNotesList().size();
        System.debug('****************** getNextNotes : notes count '  + ncount);

        controller.getPreviousNotes();
        ncount = controller.getNotesList().size();
        System.debug('****************** getPreviousNotes: notes count '  + ncount);

        System.debug(' controller ' + controller.accountId + controller.controllerAccount + controller.listIndex
                     + controller.renderNotesList + controller.searchText + controller.showDisputes + controller.txSet1
                     + controller.txSet10 + controller.txSet11 + controller.txSet2 + controller.txSet3 + controller.txSet4
                     + controller.txSet5 + controller.txSet6 + controller.txSet7 );

        ApexPages.currentPage().getParameters().put('txtSearch2','TestSearch');
        NotesController.ParentEntity controller2 = new NotesController.ParentEntity(null,'','');
        Datetime dtime = Datetime.now();
        NotesController.NoteAttachmentObj noteAttachment = new NotesController.NoteAttachmentObj(null,'',dtime );

        Test.stopTest();
    }

}