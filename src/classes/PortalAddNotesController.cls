/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is used when any Portal User logs in the system
 *        and add notes.
 */
public with sharing class PortalAddNotesController
{
    public List<User> currUser;
    List<Account> acc;
    public Note newNote {get; set; }
    public String noteTitle {get; set; }

    public PortalAddNotesController(ApexPages.StandardController controller)
    {
        currUser = new List<User>([select name,Id, ContactId, Contact.AccountId from User where id=: Userinfo.getUserId()]);
        acc =new List<Account>([select name,Id, ownerId from Account where id=: currUser.get(0).Contact.AccountId]);
        newNote = new Note();
    }
    // This method add notes on Tasks for Portal User
    // Return Type - PageRefernce
    // No Parameters are passed in this method.
    public pageReference SaveNew()
    {

        if(noteTitle == null || noteTitle.trim() == '')
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.Message_Empty_Title);
            ApexPages.addMessage(myMsg);
            return null;
        }
        else
        {
            newNote.Title = 'Portal : ' + noteTitle;
            newNote.Body = newNote.Body + '\n' +  currUser.get(0).Name;
            newNote.ParentId = ApexPages.currentPage().getParameters().get('id');
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert newNote;

            Task newTask = new Task();
            newTask.Subject =  'Portal : ' + noteTitle;
            newTask.OwnerId = acc.get(0).ownerId;
            newTask.Description = newNote.Body;
            //==========================================================================================  
       Map<String,Schema.SObjectField> m1 = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m1.keyset()) {
          // Check if the user has create access on the each field
          if (m1.get(fieldToCheck).getDescribe().isCustom() && !m1.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m1.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert newTask;

            return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        }

        //return null;
    }
}