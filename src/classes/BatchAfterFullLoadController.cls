/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Batch job that set the transaction balance to zero
 */
global class BatchAfterFullLoadController implements Database.Batchable<SObject>, Database.Stateful, Schedulable {
    /* Query that will get the list of objects to maintain the batches*/
    global String query;
    global Double totalBalanceZeroedOut;
    global Double totalBaseBalanceZeroedOut; 
    global Integer totalTransactionsProcessed;
    global String batchNumber;
    global String sourceSystem;
    global Id batchInfoId;
    public List<Data_Load_Batch__c> batchInfo;
    public String orgCurrencyCode;
    
    global Boolean validBatchNumber = false; 
    

    // global Constructor which will take the query string as argument
    
    global BatchAfterFullLoadController(String batchNo, String sourceSys) {
        String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        //String namespace = '';
        totalBalanceZeroedOut = 0.0;
        totalBaseBalanceZeroedOut = 0.0;
        totalTransactionsProcessed = 0;
        // Set the batch number and source system 
        batchNumber = batchNo.trim();
        sourceSystem = sourceSys;
     
        Trigger_Configuration__c systconfig = Trigger_Configuration__c.getOrgDefaults();
        systconfig.Zero_Balance_Transaction_Trigger__c = true;
        update systconfig;     
       
          List<Transaction__c> txList =[select id From Transaction__c where Batch_Number__c=:batchNumber limit 1] ;
          
          if(txList.size()>0 ){

        /**start Namespace for migrating to unmanaged package  **/
        // check for multi currency from custom setting ( System config )
        if ( UtilityController.isMultiCurrency() ) {
            // Get the currency code
            this.query = 'select CurrencyIsoCode ,'+namespace+'Balance__c, '+namespace+'Base_Balance__c, '+namespace+'Source_System__c, '+namespace+'Batch_Number__c from '+namespace+'Transaction__c where ';

        } else {

            this.query = 'select '+namespace+'Balance__c, '+namespace+'Base_Balance__c, '+namespace+'Source_System__c, '+namespace+'Batch_Number__c from '+namespace+'Transaction__c where ' ;
        }
        // Check for source system ( Fetch only those record which matching the source system and batch number )
        this.query = this.query  + (sourceSystem == null ? namespace+'Source_System__c = null' : namespace+'Source_System__c =\''+ sourceSystem +'\'');
        this.query = this.query + ' and ' + (batchNumber == null ? namespace+'Batch_Number__c = null' : namespace+'Batch_Number__c !=\''+ batchNumber +'\'');
        this.query = this.query + ' and Transaction_Key__c != null';
         
          // Check for BucketonBase in custom setting ( System config) 
         if( ConfigUtilController.isBucketOnBase()){
             
             this.query = this.query + ' and ('+namespace+'Base_Balance__c != 0 or ' +namespace+ 'Balance__c != 0 ) ';
         }else{
             this.query = this.query + ' and '+namespace+'Balance__c != 0 ';
         }
         
        validBatchNumber = true;
         }else {
        
        // Retrive the email address from custom setting to send notification mail ( System config batch job notification emails )
        String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
        String address = ConfigUtilController.getBatchJobNotificationEmailId();
        String[] toAddress  = addresses.split(','); 
        
        //--email mechanism with mandrill--start-----
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        MandrillEmailService objMandrill = new MandrillEmailService();

        
        mail.setToAddresses(toAddress);
        objMandrill.strToEmails = (toAddress);

                //--email mechanism with mandrill--end-----
                
        mail.setReplyTo(address);
        objMandrill.strReplyTo = address;

        mail.setSenderDisplayName('Batch Processing');
        objMandrill.strFromName = 'Batch Processing';
         //--email mechanism with mandrill--end-----
                 
        //--email mechanism with mandrill--start-----
        mail.setSubject('Aborting Batch Job Process Due to Invalid Batch Number for: '+ Userinfo.getOrganizationName());       
        objMandrill.strSubject = 'Aborting Batch Job Process Due to Invalid Batch Number for: '+ Userinfo.getOrganizationName();

        //--email mechanism with mandrill--end-----
        
        String mailBodyText = 'Aborting Closing Process due to Invalid Batch Number';
        mail.setPlainTextBody(mailBodyText);
        
        //--email mechanism with mandrill--start-----
         SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                 boolean us = seo.Use_Mandrill_Email_Service__c;
                 if(us){
                          objMandrill.SendEmail();
                       }
                 else{
                         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
                       }

        //--email mechanism with mandrill--end-----
        } 
    }
    global BatchAfterFullLoadController(){

    }
    // start method implementation of Bachable
    // Return Type - QueryLocator method in Database class
    // Parameters - BC Database.Batchablecontext 
    global Database.Querylocator start(Database.BatchableContext BC) {
        
         if ( batchInfoId  != null ) {
           if(validBatchNumber){
                batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c,  d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c, d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
                batchInfo.get(0).Batch_Job_Start_Time__c = System.now();
            }else{
            //*batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c,  d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c,  d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c, d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo.get(0).Batch_Job_Start_Time__c = System.now();
            batchInfo.get(0).Batch_Job_Status__c = 'Abortted';
            update batchInfo;
           }
        }
        return Database.getQueryLocator(query);
    }

    // execute method implementation this will set the transaction balance to zero
    // Return Type - Void 
    // Parameter - BC Database.Batchablecontext scope List<Sobject>
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        List<Transaction__c> txList = (List<Transaction__c>)scope;
        Set<Id> txIds = new set<Id>();
        for(Transaction__c tx : txList) {
            txIds.add(tx.Id);
        }

        List<Transaction__c> batchTxs = new List<Transaction__c>();
        Boolean bucketOnBase = ConfigUtilController.isBucketOnBase();
        
        String Query1 = '';
         boolean isMulitCurrency = UtilityController.isMultiCurrency();
        
        if(isMulitCurrency){
            Query1 = 'select Balance__c,CurrencyIsoCode ,Base_Balance__c from Transaction__c';
        }
        else{
            Query1  = 'select Balance__c,Base_Balance__c from Transaction__c';
        }
        
        
         if(bucketOnBase){
               Query1  = Query1   + ' where (Balance__c != 0 or Base_Balance__c != 0)  and Id in: txIds ';
               batchTxs  =  Database.query(Query1);
         }
         else{
              Query1  = Query1   + ' where Balance__c != 0  and Id in: txIds';
              batchTxs  =  Database.query(Query1);
        }
        
        integer txCount = 0;
        
       if(isMulitCurrency){
            // Get Currency information
           String SOQLQuery = 'Select c.IsoCode , c.ConversionRate From CurrencyType c where c.IsCorporate = true';
    
           List < Sobject > currencyTypes = Database.query(SOQLQuery);
            
                   
              for ( Sobject currencyType :  currencyTypes ) {
    
                    orgCurrencyCode = ( String )currencyType.get('IsoCode');
              }
        }
        else{
            orgCurrencyCode = UtilityController.getSingleCurrencyOrgCurrencyCode();
        }
      //  String orgCurrencyCode = (String)(UtilityController.isMultiCurrency() ? currencyTypes.IsoCode : UtilityController.getSingleCurrencyOrgCurrencyCode()); 
      

     //   double convertedTxBalance = (tx.Balance__c * getConversionRate(orgCurrencyCode))/getConversionRate(txCurrencyCode);
      String txCurrencyCode;
        
        try {
            // Fetch all tx whose batch# is not match with batch# of this batch info record
            for(Transaction__c tx : batchTxs) {
             txCurrencyCode = (String)(isMulitCurrency ? tx.get('CurrencyIsoCode') : orgCurrencyCode);
                if(bucketOnBase){
                     if(tx.Base_Balance__c != null){
                         // Calculate total balance which is zeroed out.
                         totalBalanceZeroedOut += tx.Base_Balance__c;
                     }
                }
                else{
                 if(UtilityController.isMultiCurrency())
                     totalBalanceZeroedOut += (tx.Balance__c * getConversionRate(orgCurrencyCode))/getConversionRate(txCurrencyCode);
                 else
                     totalBalanceZeroedOut += tx.Balance__c;
                  
                }
              
                // Set balance and base balance to zero of transaction whose batch# doesn't match with latest batch#(   
                tx.Base_Balance__c = 0;
               
                tx.Balance__c = 0;
                txCount++;
            }

            update batchTxs;

        } catch(Exception e) {
            throw e;
        }
        totalTransactionsProcessed +=txCount;
    }


   public double getConversionRate(String inputCurrencyCode) {

        //@TODO : Bhavik : Ref - On-Demand Multi-Currency

        return UtilityController.getConversionRate(inputCurrencyCode);
    }
    
    // finish method implementation of  database.batchable
    // Return Type - Void 
    // Parameter BC Database.Batchablecontext
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = null;
        if(BC != null)
        {
            a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                 from AsyncApexJob where Id =:BC.getJobId()];
        }
        Integer openTxLeft = [select count() from Transaction__c where (Transaction_Key__c <> null and Source_System__c= :sourceSystem  and Batch_Number__c!= :batchNumber and Balance__c !=0)];
      
       
       Data_Load_Batch__c batchInfo = null;
       
       if( batchInfoId != null ) {
            
            batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c , d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c, d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c,d.Balance_Closed__c,d.Total_Open_Records__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
        
        }
        // Retrive the email address from custom setting to send notification mail ( System config batch job notification emails )
        
        String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
        String address = ConfigUtilController.getBatchJobNotificationEmailId();
        String[] toAddress  = addresses.split(',');

      // String currencyIsoCode = AkritivConstants.CURRENCY_USD_CODE;
        
    //    String currencyIsoCode = (String)(UtilityController.isMultiCurrency() ? currencyTypes.IsoCode : UtilityController.getSingleCurrencyOrgCurrencyCode()); 
        
        String totalBalanceZeroedOuts = UtilityController.formatDoubleValue(orgCurrencyCode, totalBalanceZeroedOut);

//--email mechanism with mandrill--start-----
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        MandrillEmailService objMandrill = new MandrillEmailService();
  

        
        mail.setToAddresses(toAddress);
        objMandrill.strToEmails = (toAddress);
        

               
        mail.setReplyTo(address);
        objMandrill.strReplyTo = address;

        mail.setSenderDisplayName('Batch Processing');
        objMandrill.strFromName = 'Batch Processing';
                 //--email mechanism with mandrill--end-----
                 
  //--email mechanism with mandrill--start-----         
        mail.setSubject('Batch Job Finished - Closing Transactions on organization: '+ Userinfo.getOrganizationName());
        objMandrill.strSubject = 'Batch Job Finished - Closing Transactions on organization: '+ Userinfo.getOrganizationName();
          //--email mechanism with mandrill--end----- 
          // Send the mail with SYSTEM BatchJob Notification as body      
        String notificationTemplateName = 'SYSTEM BatchJob Notification';
        
        SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
        List<EmailTemplate> template = new List<EmailTemplate>();
        if(sysConfig.is_Akritiv_Org__c){
            template = [ Select e.Name, e.HtmlValue From EmailTemplate e where Folder.Name like 'Akritiv%' and e.Name =:notificationTemplateName ];
        }else{
            template = [ Select e.Name, e.HtmlValue From EmailTemplate e where e.Name =:notificationTemplateName ];
        }
        
        String templateBody;
        if(template.size() > 0){
            templateBody = template.get(0).HtmlValue;
        }else{
            templateBody = '';
        }
       if ( a != null ) {
       
           templateBody = templateBody.replace('{!totalbatchprocess}', a.TotalJobItems+'');
            templateBody = templateBody.replace('{!failures}', a.NumberOfErrors+'');
       }
        // Put the value in the template 
        templateBody = templateBody.replace('{!notificationdate}', Date.today()+'');
       
        templateBody = templateBody.replace('{!closetransaction}',totalTransactionsProcessed+'' );
        templateBody = templateBody.replace('{!totalbalancezeroedout}',totalBalanceZeroedOuts+'');
        templateBody = templateBody.replace('{!totaltransactionfailed}',openTxLeft+'' );
        templateBody = templateBody.replace('{!query}',query );
        templateBody = templateBody.replace('{!batchnumber}',batchNumber );
        templateBody = templateBody.replace('{!sourcesystem}',sourceSystem);
        templateBody = templateBody.replace('{!organizationname}',Userinfo.getOrganizationName() );
      
       //--email mechanism with mandrill--start-----
               
        mail.setHtmlBody(templateBody);
        objMandrill.strHtmlBody = templateBody;
       
      
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        objMandrill.SendEmail();
        //--email mechanism with mandrill--end-----
        
        
        if (batchInfo != null ) {
            // Update the batch info record with detail 
            batchInfo.Batch_Job_End_Time__c = System.now();
            batchInfo.Batch_Job_Status__c = 'Done';
            batchInfo.Balance_Closed__c = Double.valueOf(totalBalanceZeroedOut);
            batchInfo.Total_Open_Records__c = openTxLeft;
            update batchInfo;
        }
         //notify system of this finish
         // send mail to the email address defined in custom seting( Batch job configuration) which is email address of email service (Data Load Batch Job Kick Off)
        toAddress =new String[] {ConfigUtilController.getBatchAutoCloseEmailService()} ;
                
        //notify system of this finish
       if(a != null && (a.TotalJobItems - a.JobItemsProcessed) == 0){
            
            mail = new Messaging.SingleEmailMessage();
            
            //--email mechanism with mandrill--start-----
            mail.setToAddresses(toAddress);
            objMandrill.strToEmails = (toAddress);  
            
            mail.setReplyTo(address);
            objMandrill.strReplyTo = address;
 
            mail.setSenderDisplayName('Batch Processing :');
            objMandrill.strFromName = 'Batch Processing :';
            //--email mechanism with mandrill--end-----
            // Set subject
           
           //--email mechanism with mandrill--start-----
            mail.setSubject( 'Aging Process');
            objMandrill.strSubject = 'Aging Process';
            
            //--email mechanism with mandrill--end-----
            String mailBodyText = '';
            if ( batchInfo != null  ){
                // Set body value
                mailBodyText ='<END>'+batchNumber+''+Userinfo.getOrganizationId()+''+sourceSystem+''+Userinfo.getOrganizationId()+''+batchInfo.Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                     
    
            } else {
                // Set body value
                mailBodyText ='<END>'+batchNumber+''+Userinfo.getOrganizationId()+''+sourceSystem+''+Userinfo.getOrganizationId()+''+'NOBATCHINFO'+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';                       
    
            }
            
    
            mail.setPlainTextBody(mailBodyText);
       
       //--email mechanism with mandrill--start-----
            SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                 boolean us = seo.Use_Mandrill_Email_Service__c;
                 if(us){
                      objMandrill.SendEmail();
                       }
                 else{
                      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     }

            
            //--email mechanism with mandrill--end-----
      }  
    }

    global void execute(SchedulableContext sc) {
        Integer dataLoadBatchSize = 200;
        BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
        if(batchConfigObj != null)
        {
            dataLoadBatchSize = (batchConfigObj.DataLoad_Batch_Size__c != null && batchConfigObj.DataLoad_Batch_Size__c > 0 && batchConfigObj.DataLoad_Batch_Size__c < 201 ? batchConfigObj.DataLoad_Batch_Size__c.intValue() : 200);
        }

        //get max batch num and it's source system
        try {

            List<Transaction__c> tx = [ Select a.Transaction_Key__c, a.Source_System__c, a.Batch_Number__c From Transaction__c a where Transaction_Key__c != null and Source_System__c  <> null and Source_System__c <>'' and Batch_Number__c  <> null and Batch_Number__c <>'' order by Batch_Number__c desc limit 1];
            if(tx.size()>0){
                BatchAfterFullLoadController bafl = new BatchAfterFullLoadController(tx.get(0).Batch_Number__c, tx.get(0).Source_System__c);
                Database.executeBatch(bafl, dataLoadBatchSize);
            }

        } catch( Exception e ) {

            
        }

    }
}