/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Class provides the following functionality
            Closes the given dispute, sets the status to closed and marks closed date to today.
            It updates the resolution code with the value provided by user. A task and a note is
            created with proper comments.
            (The balance of the dispute becomes 0 on closure and balance formula field does that.)
            Also once dispute is closed "closed" button is changed to "Reopen".
            If user click on "Reopen", user should be warned with message
            "Are you sure you want to reopen dispute #blah with balance <amount>."
            Dispute status should change to "Reopened" (add status  if not available)
            and Balance should change to Amount.
 */
public with sharing class CloseDisputeController {
    public String objectIds {get; set; }
    public String objectType {get; set; }
    public String selectedResolutionCode {get; set; }
    public String noteTitle {get; set; }
    public String noteBody {get; set; }
    public Integer activityRating {get; set; }
    public String selectedValue {get; set; }

    // default constructor for the class
    public CloseDisputeController() {
        if(Apexpages.currentPage().getParameters().get('objIds') != null && Apexpages.currentPage().getParameters().get('objIds') != ''){
            Id objIdParam = String.escapeSingleQuotes( Apexpages.currentPage().getParameters().get('objIds'));
            objectIds = objIdParam;
        }
        objectType =Apexpages.currentPage().getParameters().get('objtype');
        selectedValue =Apexpages.currentPage().getParameters().get('selectedRc');
        String currentUserId = UserInfo.getUserId();
        noteTitle = 'Resolution:'; noteBody = '';
        activityRating = UtilityController.getRatingStarNumber(AkritivConstants.DEFAULT_ACTIVITY_RATING);
        selectedResolutionCode = selectedValue;
    }

    // property to get the dispute types select options
    public List<SelectOption> resolutionCodes {
        get {
            if(resolutionCodes == null) {
                resolutionCodes = new List<SelectOption>();
                Schema.DescribeFieldResult fieldResult = Dispute__c.Resolution_Code__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple) {
                    resolutionCodes.add(new SelectOption(f.getValue(),f.getLabel()));
                }
            }

            return resolutionCodes;
        }
        set;
    }
    // Change status of dispute to Close 
    // Return Type - PageReference 
    // No parameters are passed in this method  
    public Pagereference closeDispute()
    {

        List<Dispute__c> disputeToBeClosedList = [Select id, Name, Account__r.Id, Account__r.Name, Type__c, Transaction__c, amount__c, balance__c, resolution_code__c, status__c,close_date__c from Dispute__c where id=:objectIds];
        Dispute__c disputeToBeClosed = null;
      
        if(disputeToBeClosedList != null && disputeToBeClosedList.size() ==1 )
        {
            disputeToBeClosed = disputeToBeClosedList.get(0);
        }

        disputeToBeClosed.resolution_code__c = selectedResolutionCode;
        disputeToBeClosed.Status__c ='Closed';
        disputeToBeClosed.close_date__c = Date.today();
        
        //==========================================================================================  
             list<String> lstFields = new list<String>{'Type__c','resolution_code__c','status__c','close_date__c'};
           Map<String,Schema.SObjectField> m1 = Schema.SObjectType.Dispute__c.fields.getMap();
           for (String fieldToCheck : lstFields) {
              // Check if the user has create access on the each field
              if(!m1.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m1.get(fieldToCheck).getDescribe().isUpdateable()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'+fieldToCheck));
                    return null;
                  }
               }
            }
        //========================================================================================
        
        update disputeToBeClosed;

        Id transactionId = disputeToBeClosed.Transaction__c;
        String txName =  null;

        List<Transaction__c> txList = null;
        if(transactionId != null )
        {
            txList = [Select id, Name from Transaction__c where id =:transactionId];
            txName = txList.get(0).Name;
        }

        Note n = new Note();
        n.title = noteTitle;
        n.body = noteBody;
        n.ParentId = disputeToBeClosed.id;
        
        //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
            System.debug('=======fieldToCheck======'+fieldToCheck);
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              System.debug('=======fieldToCheck2======'+fieldToCheck);
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                System.debug('=======fieldToCheck in======'+fieldToCheck);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        
        insert n;

        String userName = UserInfo.getUserName();
        String comments = 'Dispute <' + disputeToBeClosed.Name + '>, <' + disputeToBeClosed.Type__c + '>'
                          + ' in amount of <' + disputeToBeClosed.amount__c + '> closed by <'+ userName + '> ';

        if(txName != null)
        {
            comments += ' for Transaction <' +  txList.get(0).Name + '>';
        }
        else
        {
            comments += ' for Account <' +  disputeToBeClosed.Account__r.Name + '>';
        }

        ContactLogEntry entry = new ContactLogEntry(null, disputeToBeClosed.account__r.Id, null, AkritivConstants.CONTACT_TYPE_DISPUTE, AkritivConstants.CONTACT_TYPE_OTHER, 'Dispute Closed',
                                                    noteBody, AkritivConstants.CATEGORY_CONTACT, Userinfo.getUserId(), AkritivConstants.TASK_COMPLETED,ContactLogEntry.PRIORITY_DEFAULT, '',comments, UtilityController.getRatingLabel(activityRating));

        entry.store();

        return null;

    }

}