/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
public with sharing class ExtendedConsoleController
{
    // Method to get and store Task key
    // param - no parameter
    // return - void
    public void load() {

        taskkey = Apexpages.currentPage().getParameters().get('taskkey');
    }
    public String taskkey {get; set;}

    static testMethod void testController() {
        // TO DO: implement unit test

        Test.startTest();
        Apexpages.currentPage().getParameters().put('taskkey','test');
        ExtendedConsoleController c = new ExtendedConsoleController();
        c.load();
                        
        System.assertEquals(c.taskkey,'test' );

        Test.stopTest();
    }
}