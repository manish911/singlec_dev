public class ViewLineItems{
 
    //default page size
    private static final Integer PAGE_SIZE = 450;
     public List<DynamiclineitemFields> fieldWithApiList {get;set;} 
     
    //pagination information
    public Integer page{get;set;}
    public Integer totalRecords{get;set;}
    public Integer totalPages{get;set;}
    public Integer startIdx{get;set;}
    public Integer endIdx{get;set;}
    public String accountId{get; set;}
    public String lstaccountIds{get; set;}
    public String accountName{get;set;}
    public List<String> listAccountID{get;set;}
    public String[] csvAccIDs = new List<String>();
    public String selectedAccIDs{get;set;}
    public integer count{get;set;}
    Public List<Integer> lstInt{get;set;}
    public Boolean isParentCalled{get;set;}
    public Boolean testing{get;set;}
    public String selectedLineItemIds{get;set;}
    public String tempKey{get;set;}
    public Boolean tempKeyBoolean{get;set;}
    //public List<FiltersWrapper> lstFiltersWrapper{get;set;}
    private String strFilterCriteria;
    public String ObjName{get;set;}
    public Temp_Object_Holder__c objtemp{get;set;}
    public Boolean recordSelected{get;set;}
    public String selectedID{get;set;}
//    public Map<String, Boolean> lineSelectedMap {get;set;}
    public String lineSelectedMap {get;set;}
    public String selectedLines {get;set;}
    /*
    * set controller
    */
    public List<CCWRow> tRecords{get;set;}

    /*
    * constructor
    */
    public ViewLineItems(){
        ObjName= 'Line_Item__c';
        objtemp = new Temp_Object_Holder__c();   
       // objtemp.value__c = ' Disputed_Status__c = \'NO\' ';     
        tempKeyBoolean = false;
        selectedLineItemIds = '';
        lineSelectedMap = '';
//        lineSelectedMap = new Map<String, Boolean>();
        recordSelected = false;
        selectedID = '';
        tempKey = '';
        selectedLines = '';
        //strFilterCriteria= '';
        listAccountID = new List<String>();
        isParentCalled = true;
        lstInt = new List<Integer>();
        for (Integer i =0;i<=3;i++){
            lstInt.add(i);
        }
        tempKey = UserInfo.getUserId()+ DateTime.now();
        tempKey = tempKey.replaceAll( '\\s+', '');
        System.debug(' ### Constructor Temp Key : '+tempKey);
 /*       
        if(selectedAccIDs != null) {
            csvAccIDs = selectedAccIDs.split(','); 
        }Else{*/
            accountId = ApexPages.currentPage().getParameters().get('accid');
   //         csvAccIDs = accountId.split(',');
  //      }
  //      System.debug('######## Account ID : '+csvAccIDs);
        
        listAccountID.add(accountId);
 /*       FOR(Integer i=0; i<csvAccIDs.size(); i++){
            listAccountID.add(csvAccIDs[i]);
            System.debug('## Acc ID : '+csvAccIDs[i]);
        }*/
        System.debug(' $$$$$ Account List SIze : '+listAccountID.size());
        accountName = ApexPages.currentPage().getParameters().get('accName');
        //init variable
        this.tRecords = new List<CCWRow>();
        //set initial page
        this.page = 1;
        //load records
        getLineItems();
        
        /*lstFiltersWrapper = new List<FiltersWrapper>();
        FiltersWrapper obj1 = new FiltersWrapper();
        FiltersWrapper obj2 = new FiltersWrapper();
        FiltersWrapper obj3 = new FiltersWrapper();
        FiltersWrapper obj4 = new FiltersWrapper();
        lstFiltersWrapper.add(obj1);
        lstFiltersWrapper.add(obj2);
        lstFiltersWrapper.add(obj4);
        lstFiltersWrapper.add(obj4); */      
        
         Map<String,String> customFieldDataTypeMap = new Map<String,String>();
        Map<String,String> customFieldLabelMap = new Map<String,String>();
        
        Schema.Sobjecttype objType1 = Line_Item__c.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType1.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
        sortSelectOption.sort();
        String fieldType;
        String fieldName;
        String fieldLabel;
        for(String a : sortSelectOption){
            Schema.Sobjectfield field = fieldMap.get(a);
            fieldName = field.getDescribe().getName().toLowerCase();
            fieldType = field.getDescribe().getType().name();
            fieldLabel = field.getDescribe().getLabel();
            customFieldDataTypeMap.put(fieldName,fieldType);
            customFieldLabelMap.put(fieldName,fieldLabel);
        }
        
        System.debug('=========customFieldLabelMap====='+customFieldLabelMap);
        System.debug('=========customFieldDataTypeMap====='+customFieldDataTypeMap);
        
        SysConfig_Singlec__c sysConfig = [SELECT Id, View_Line_Items__c FROM SysConfig_Singlec__c limit 1];
        string disputeFieldList = sysConfig.View_Line_Items__c;
        
        List<String> disputeField= new list<String>();
        disputeField = disputeFieldList.split(','); 
        String objtype='Dispute_Lines__c';
        fieldWithApiList = new List<DynamiclineitemFields>();
        System.debug('####$$$$ Dispute Field : '+disputeField);
        for(Integer i=0;i<(disputeField.size());i++){
            //System.debug('----temp---'+disputeField.get(i)+'----Field Label---'+Schema.getGlobalDescribe().get(type).getDescribe().fields.getMap().get(disputeField.get(i)).getDescribe().getLabel());
           
            if(disputeField != null && disputeField.get(i) != null){
                System.debug('===disputeField.get==='+disputeField.get(i));
                System.debug('=========customFieldLabelMap====='+customFieldLabelMap.get(disputeField.get(i).toLowerCase()));
                //disputField.fieldName = customFieldLabelMap.get(disputeField.get(i).toLowerCase());//Schema.getGlobalDescribe().get(objtype).getDescribe().fields.getMap().get(disputeField.get(i)).getDescribe().getLabel();
                //disputField.fieldApiName = disputeField.get(i);
                fieldWithApiList.add(new DynamiclineitemFields(customFieldLabelMap.get(disputeField.get(i).toLowerCase()),disputeField.get(i)));
            }
        }        
    }

    /*
    * advance to next page
    */
    public void doNext(){
        if(getHasNext()){
            this.page++;
//            selectedLines += lineSelectedMap;
            getLineItems();
        }
    }

    /*
    * advance to previous page
    */
    public void doPrevious(){
        if(getHasPrevious()){
            this.page--;
            getLineItems();
        }
    }

    /*
    * returns whether the previous page exists
    */
    public Boolean getHasPrevious(){
        if(this.page>1){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    * returns whether the next page exists
    */
    public Boolean getHasNext(){
        if(this.page<this.totalPages){
            return true;
        }
        else{
            return false;
        }
    }
    /*
    * return current page of records
    */
    public void getLineItems(){
        strFilterCriteria = '';
        //calculate range of records for capture
        this.startIdx = (this.page-1)*PAGE_SIZE;
        this.endIdx = this.page*PAGE_SIZE;
        this.totalRecords = 0;
        //clear container for records displayed
        this.tRecords.clear();
        //cycle through
        System.debug('Calling query : '+listAccountID);
        System.debug('Variable value:-------' + strFilterCriteria); 
        if(String.IsNotBlank(objtemp.value__c)){
        
         strFilterCriteria = ' And ' + objtemp.value__c; 
        } 
        
    /*    if(String.IsNotBlank(objtemp.value__c)){
               System.debug('======objtemp ====='+objtemp.value__c); 
               if(objtemp.value__c.equalsIgnorecase('Disputed_Status__c = \'YES\'') || objtemp.value__c.equalsIgnorecase('Disputed_Status__c <> \'NO\'') || objtemp.value__c.equalsIgnorecase('Disputed_Status__c Like \'%YES%\'')){
                   //continue;
               }else{
                   strFilterCriteria = ' And ' + objtemp.value__c;               
                   strFilterCriteria = strFilterCriteria.replace('Disputed_Status__c = \'YES\'','');  
                   strFilterCriteria = strFilterCriteria.replace('Disputed_Status__c <> \'NO\'','');
                   strFilterCriteria = strFilterCriteria.replace('Disputed_Status__c Like \'%YES%\'','');              
               }                              
        }
          */               
        //List<Line_Item__c> listLineItems = [Select Account_Name__c,Name,Billing_Period_Start__c,Billing_Period_End__c,akritiv__Description__c,Line_Total_Oper__c,Charge_Type_Code__c from Line_Item__c where  akritiv__Transaction__r.akritiv__Account__c IN :listAccountID and Open_Amount_Oper__c <> 0 and akritiv__Line_Total__c <> 0 order by Line_Total_Oper__c desc limit 50000];
        SysConfig_Singlec__c sysConfig = [SELECT Id, View_Line_Items__c FROM SysConfig_Singlec__c limit 1];
      //  string disputeFieldList = sysConfig.View_Line_Items__c;
        String fieldsName = sysConfig.View_Line_Items__c;
        String strQuery = 'Select '+fieldsName +' from Line_Item__c where  Account__r.id IN :listAccountID and Line_Total__c <> 0 ' +  strFilterCriteria + ' order by account__r.name ASC limit 50000';
        System.debug('Variable strQuery value:-------' + strQuery ); 
        //System.debug(' ##### List of Line Items Size : '+listLineItems.size());
        for(Line_Item__c c : Database.query(strQuery )){
            //capture records within the target range
            if(this.totalRecords>=this.startIdx && this.totalRecords<this.endIdx){
                this.tRecords.add( new CCWRow(c, false) );
            }
            //count the total number of records
            this.totalRecords++;
        }
        //calculate total pages
        Decimal pages = Decimal.valueOf(this.totalRecords);
        pages = pages.divide(Decimal.valueOf(PAGE_SIZE), 2);
        this.totalPages = (Integer)pages.round(System.RoundingMode.CEILING);
        //adjust start index e.g. 1, 11, 21, 31
        this.startIdx++;
        //adjust end index
        if(this.endIdx>this.totalRecords){
            this.endIdx = this.totalRecords;
        }
        //objtemp = new Temp_Object_Holder__c();
    }
    
    /*
    * helper class that represents a row
    */
    public with sharing class CCWRow{
        public Line_Item__c tContact{get;set;}
        public Boolean IsSelected{get;set;}
        public CCWRow(Line_Item__c c, Boolean s){
            this.tContact=c;
            this.IsSelected=s;
        }
    }

    public PageReference callParentPageFunction(){
        System.debug(' Function is used for setting parameter from javascript...!!'+selectedAccIDs);
        listAccountID = new List<String>();
        isParentCalled = true;
        if(selectedAccIDs != null) {
            csvAccIDs = selectedAccIDs.split(','); 
        }
        
        FOR(Integer i=0; i<csvAccIDs.size(); i++){
            listAccountID.add(csvAccIDs[i]);
            System.debug('## Acc ID : '+csvAccIDs[i]);
        }
        getLineItems();  
        return null;      
    }
    
    public PageReference addTempHolder(){
        System.debug('Adding temp object entery...!!'+selectedLineItemIds);
        tempKeyBoolean = true;
        System.debug('#### Temp Key : '+tempKey);
        Temp_Object_Holder__c config = new  Temp_Object_Holder__c();
        config.key__c = tempKey;
        config.value__c = selectedLineItemIds;
        insert config;
        System.debug('####### Value : '+selectedLineItemIds);
        return null;
    }
           
    public pageReference applyFilters(){
        getLineItems();
        return null;
    }
    
    public void maintainSelRecords(){
        System.debug('#### Maintain Selected Records : '+lineSelectedMap+' and selected Line Item : '+selectedID);
        selectedLines += lineSelectedMap;
        System.debug('Selected Lines - '+selectedLines);
    }
    
    public class DynamiclineitemFields{
        public String fieldName{get;set;}
        public String fieldApiName{get;set;}  
        
        public DynamiclineitemFields(String fieldName,String fieldApiName){
            this.fieldName = fieldName;   
            this.fieldApiName = fieldApiName;    
        }      
    }
     /*   
      //Fetch records for the search criteria
    public List<selectOption> getLineItemsFields() {
        List<selectOption> lstoptions = new List<selectOption>();                
        Schema.Sobjecttype objType = Line_Item__c.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
        sortSelectOption.sort();        
        lstoptions.add(new SelectOption('None','--None--'));
        for(String a : sortSelectOption) {
            Schema.Sobjectfield field = fieldMap.get(a);            
            lstoptions.add(new selectOption(field.getDescribe().getName(),field.getDescribe().getLabel()));
        }        
        return lstoptions;
    }
 
   
    public List<selectOption> getOperatorList() {
        List<selectOption> lstoptions = new List<selectOption>();                 
        lstoptions.add(new SelectOption('None','--None--'));
        lstoptions.add(new SelectOption('=','equals'));
        lstoptions.add(new SelectOption('!=','not equals to'));
        lstoptions.add(new SelectOption('<','less than')); 
        lstoptions.add(new SelectOption('>','greater than'));
        lstoptions.add(new SelectOption('<=','less or equal'));
        lstoptions.add(new SelectOption('>=','greater or equal'));
        lstoptions.add(new SelectOption('like \'%','contains'));
        lstoptions.add(new SelectOption('like \'','starts with'));
        return lstoptions;
    }
     public class FiltersWrapper {
        public List<SelectOption> listCustomField{get;set;}
        public List<SelectOption> listFilterOperator{get;set;}
        public String selectedCustomField {get;set;}
        public String selectedFilterOperator {get;set;}
        public String selectedFilterValue {get;set;}
  
        public FiltersWrapper(){
            listFilterOperator = new List<SelectOption>();    
            listCustomField = new List<SelectOption>();
                                            
            listFilterOperator.add(new SelectOption('None','--None--'));
            listFilterOperator.add(new SelectOption('=','equals'));
            listFilterOperator.add(new SelectOption('!=','not equals to'));
            listFilterOperator.add(new SelectOption('<','less than')); 
            listFilterOperator.add(new SelectOption('>','greater than'));
            listFilterOperator.add(new SelectOption('<=','less or equal'));
            listFilterOperator.add(new SelectOption('>=','greater or equal'));
            listFilterOperator.add(new SelectOption('like \'%','contains'));
            listFilterOperator.add(new SelectOption('like \'','starts with'));

            Schema.Sobjecttype objType = Line_Item__c.getSObjectType();
            Schema.Describesobjectresult sobjRes = objType.getDescribe();
            Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
            List<String> sortSelectOption = new List<String>(fieldMap.keyset());
            sortSelectOption.sort();
            listCustomField.add(new SelectOption('None','--None--'));
            for(String a : sortSelectOption) {
                Schema.Sobjectfield field = fieldMap.get(a);
                String fieldLabel = field.getDescribe().getLabel();
                String fieldName = field.getDescribe().getName();                
                listCustomField.add(new SelectOption(fieldName,fieldLabel));                
            }
            listCustomField.add(new SelectOption('CurrencyIsoCode','CurrencyIsoCode'));
        }
    }*/     
}