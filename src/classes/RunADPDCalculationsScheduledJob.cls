/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Schedulabel job to run DPD Calculation for all account in the system.
 */
global class RunADPDCalculationsScheduledJob implements Schedulable {

    global void execute(SchedulableContext sc) {
        
        BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
        Integer agingBatchSize1 = ( batchConfigObj!= null && batchConfigObj.Aging_Batch_Size__c != null && batchConfigObj.Aging_Batch_Size__c > 0 && batchConfigObj.Aging_Batch_Size__c < 201) ? batchConfigObj.Aging_Batch_Size__c.intValue() : 200;
        BatchADPDCalculations badpd = new BatchADPDCalculations();
        Database.executeBatch(badpd , agingBatchSize1);
    }
    
    static testMethod void myUnitTest() {
        test.starttest();

        RunADPDCalculationsScheduledJob runagingcalcjobObj = new RunADPDCalculationsScheduledJob ();

        String sch = '0 0 23 * * ?';

        system.schedule('Contract Creates', sch, runagingcalcjobObj);
        
        system.assertEquals(sch,'0 0 23 * * ?');

        test.stopTest();
    }
    
}