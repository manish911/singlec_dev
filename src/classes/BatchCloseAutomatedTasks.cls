/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for closing old Batch Automated Task.
 */
global class BatchCloseAutomatedTasks implements Database.Batchable<SObject>, Schedulable {

    SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
        String BccEmailAdd = sysConfig.ArchivalBccEmailAddress__c;  
          Boolean CheckDefaultContacts;
       
    // List to hold the Automated Task ids that need to be closed
    List<Id> automatedTasksToClose = new List<Id>();

    //billing related fields
    String accountId = null;
    String accountOwnerId = null;
    String selectedLocale = '';
    String emailTargetToContactId = null;
    // by default contact method
    String contactMethod = 'Email';
    String Uid='';
    String username ='';
    String email='';
    String ContactName='';
    String textTemplateBodyTemp ='';
    String fromEmail='';
    boolean attachPDF = true;
    public String billingStatementPdf = 'Billing_Statement.pdf';
    Public boolean DisplayChildAccountInvoice;
    String selectedTemplateId ='';
    String emailSubject = '';
    String emailTemplateBody = '';
    String selectedList  {get; set;}
    //global List<String> DefaultEmails = new List<String>();
    global Set<String> DefaultEmails = new Set<String>();
    global List<Id> SingleDefaultEmailId = new List<Id>();
    global List<String> ll =new list<String>();
    global Map<Id,Set<String>> accConEmailMap = new Map<Id, Set<String>>();
    global Map<Id,Set<String>> accConMap = new Map<Id, Set<String>>();
    global List<task> tskToinsert = new List<Task>();
    global List<task> taskToCloseNew = new List<Task>();
    global  List < Id > acIds = new List < Id >();
    global list<Account> pcAccount =new list<Account>();
    global List<Boolean> boolFieldTrue= new List<Boolean>();
    global List<Boolean> boolFieldFalse= new List<Boolean>();
    TransactionsCollection txCollectionObj = null;
    global Set<Id> IdChildInvoice= new Set<Id>();
    global Set<Id> IdNoChildInvoice= new Set<Id>();
    global Map<Id,Account> mapaccIdAccountForchild = new Map<Id,Account>();
    
    global BatchCloseAutomatedTasks(){
        
       
        
    }
    
    // start method that rerurn the query locator object
    // with all the active activityTransaction records
    global database.querylocator start(Database.BatchableContext bc){
        init();
        // get all automated and tasks that are due
        String automatedTasksFilter = 'Auto%';
        String sQuery = 'Select t.Who.Email, t.Who.LastName, ';
        sQuery += ' t.WhoId, t.What.Name, t.WhatId, t.Subject,';
        sQuery += ' t.Status, t.Owner.Name, t.Owner.Email,  t.OwnerId, t.IsClosed, t.ActivityDate ';
        sQuery += ' From Task t where t.subject like \''+ automatedTasksFilter +'\'';
        sQuery += ' and t.ActivityDate <= Today';
        sQuery += ' and t.status !=\'Completed\'';
       // sQuery += ' and t.status !=\'Auto Dunning Error\'';
        
        
        return Database.getQueryLocator( sQuery );
    }

    global void init()
    {
        SysConfig_Singlec__c sysConfObj = null;
        if(ConfigUtilController.getSystemConfigObj() !=null) {
            sysConfObj = ConfigUtilController.getSystemConfigObj();
            if(sysConfObj.Billing_Statement__c  !=null && sysConfObj.Billing_Statement__c !='') {
                String billState = sysConfObj.Billing_Statement__c;
                billingStatementPdf = billState + '.pdf';
            }
        }

       //get list views
        String strTrans ='Transaction__c';
        String listviewName =AkritivConstants.CONST_SYSTEM_PDF_EXCEL_LIST;
        List<Custom_List_View__c> listViewObj = new  List<Custom_List_View__c>();
        listViewObj = [select Name,Object_Fields__c from Custom_List_View__c where ListView_For_Object__c =: strTrans and name =:listviewName Limit 1  ];
        if(listViewObj.size()>0)
            selectedList= listViewObj[0].Id;
        else{
            selectedList='Default';
        }
    }

    //need following for send billing
    //account id, emailTargetToContactId, emailTemplateId
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
    
    System.debug('#### E X E C U T E Method called : ' +CheckDefaultContacts);
        // iterate over all the due automated tasks in batches
        List<Task> tasksToClose = new List<Task>();
        //List < Id > acIds = new List < Id >();
        Map <Id,  Account > accs =null;
        Map < Id, Contact > acContactMap = new Map < Id, Contact >();
        Map < Id, Contact > acContactMapFax = new Map < Id, Contact >();
        Map<Id,Account> MapAccoutUpdate = new Map<Id,Account> ();
        set<Id> accIDsUp = new set<Id>();
         
    
        
        Map < String , OrgWideEmailAddress  > orgwdEmailMap = new Map < String , OrgWideEmailAddress >();
        List < String> taskOwnerEmails = new List< String>();
        List<Id> taskListToFax = new List<Id>();
        List<Id> taskListToMail = new List<Id>();
        
      
        for (Task automatedTask : (List<Task>)sObjs) {
            acIds.add( automatedTask.WhatId );
            system.debug('====acIds' +acIds + '' +acIds.size());
            taskOwnerEmails.add(automatedTask.Owner.Email );
        }
        
        List <OrgWideEmailAddress > orgwdadds =  [Select Id,Address,DisplayName from OrgWideEmailAddress where Address in: taskOwnerEmails ]; 
        
        for ( OrgWideEmailAddress  orwdadd : orgwdadds  ){
            orgwdEmailMap.put( orwdadd.Address , orwdadd );       
        }
        
        List < Account > AccountToUpdate = new List< Account >();
        
        List < Task > tasktoInsert = new List< Task >();
        List < Task > errTasktoupdate = new List< Task >();
        List < Task > errTasktoupdate1 = new List< Task >();
        accs = new Map < Id, Account >([ select Id, OwnerId, Auto_Dunning_Enabled__c,Locale__c,Last_Contact_Date__c,Last_Contact_Method__c,Last_Contact_Type__c,Notes__c from Account where Id in : acIds]);
        
        List < Account > acu = new List< Account >([select id, Last_Contact_Date__c,Notes__c from Account where Id in : acIds ]);
        Integer currentDunningDays = 0;
        //JIRA #AKTPROD-106 : By Kruti Tandel
          //***date format ex. 02-Nov-2012*******// 
                String temp1 = DateTime.now().format('MMM');
                String temp2 = String.valueOf(DateTime.now().day());
                String temp3 = String.valueOf(DateTime.now().year());
                
                if(temp2.length() == 1 ){
                temp2 = '0'+ temp2;
                }
                 
                String temp4 = temp2 + '-' + temp1 + '-' + temp3;
       
        for(Account acs : acu){
            if(acs.Last_Contact_Date__c != null){
                currentDunningDays =acs.Last_Contact_Date__c.daysBetween(Date.Today());
                if(currentDunningDays != null && currentDunningDays <= 10 ){
                    acIds.add(acs.Id);
                    acs.Last_Contact_Date__c = Date.Today();
                    //AccountToUpdate.add(acs);
                }
            }else{
                acIds.add(acs.Id);
                acs.Last_Contact_Date__c = Date.Today();
               // AccountToUpdate.add(acs);
            }
            
            mapaccIdAccountForchild.put(acs.id,acs);
        }       
        system.debug('CheckDefaultContacts1' +CheckDefaultContacts);
       CheckDefaultContacts = SysConfig.EmailSendToAllDefaultContacts__c;
       Set<String> conEmailAddresses = new Set<String>();
       List < Contact > contacts = new List < Contact >();
        try {
            contacts = [ select Id,lastname, Email, Fax, AccountId,Locale__c from Contact where AccountId in:acIds AND Default__c =: TRUE order by lastname desc];
        } catch ( Exception e ) {

        }
                      
        if(contacts.size() > 0){
         system.debug('###### contacts' +contacts.size() +' '+contacts);
            for(Contact contact : contacts){
                
                if(contact.Email != null && contact.Email != ''){    
                    acContactMap.put( contact.AccountId, contact );
                    IF(accConEmailMap.containsKey(contact.accountId)){
                        conEmailAddresses = accConEmailMap.get(contact.accountId);
                        conEmailAddresses.add(contact.Email);
                        accConEmailMap.put(contact.AccountId, conEmailAddresses);
                        accConMap.put(contact.id,conEmailAddresses);
                    }ELSE{
                        conEmailAddresses = new Set<String>();
                        conEmailAddresses.add(contact.Email);
                        accConEmailMap.put(contact.accountId, conEmailAddresses);
                        accConMap.put(contact.id,conEmailAddresses);
                    }
                }
                else if(contact.FAX != null && contact.FAX != '') {
                
                    acContactMapFax.put( contact.AccountId, contact );
                }else{
                    continue;
                }
            }
           
            //List < ContactLogEntry > logEntries = new List < ContactLogEntry > ();
            for(Task automatedTask : (List<Task>)sObjs)
            {
             textTemplateBodyTemp ='';
                username = automatedTask.Owner.Name;  
              // phone=    automatedTask.Owner.direct_line__c;  
               // fax = automatedTask.Owner.fax;         
                if ( username == null ) {    
                    username = '';
                }
                
                emailTargetToContactId = automatedTask.WhoId;
              //  Uid=automatedTask.owner.id;
                accountId = automatedTask.WhatId;
                system.debug('&&&&' +accountId);
    
                Account acc = accs.get( accountId );
                if(acc != null)
                    accountOwnerId = acc.OwnerId;
                else
                    continue;
        
                Boolean autodunningenabled = accs.get( accountId ).Auto_Dunning_Enabled__c;
                system.debug('###### autodunningenabled '+autodunningenabled );
                if ( !autodunningenabled ) {   
                    continue;
                }             
               
                           
                system.debug('###### emailTargetToContactId '+emailTargetToContactId );
                if (emailTargetToContactId == null || emailTargetToContactId == '') {
                    
                        Contact contact = new Contact();
                        if(acContactMap.size() > 0){
                            contact = acContactMap.get(accountId);
                            system.debug('###### contact '+contact);
                        }
                        if( contact != null && contact.Locale__c != null && contact.Locale__c != ''){
                            selectedLocale = contact.Locale__c;
                        }                    
                    
                    if(selectedLocale == null || selectedLocale == ''){
                        selectedLocale = acc.Locale__c;
                    }
                    system.debug('aaaaa' +contact);
                       
                   if ( contact == null || contact.Email == null  ) {   
                        //create new task to manage contacts    
                     
                        Task t = automatedTask ;
                       // t.OwnerId = accs.get( accountId ).OwnerId;
                        t.Auto_Dunning_Error__c = 'Accounts\'s Default Contact Email Not Found.';
                        t.status = 'Completed';
                        t.Dunning_Status__c = 'Auto Dunning Error';
                        errTasktoupdate.add(t);
                        Database.update(errTasktoupdate); 
                        errTasktoupdate.clear();
                        continue;
                                system.debug('bbbbb' +tasktoInsert);
                    }else {
                    
                        emailTargetToContactId = contact.Id;   
                        Uid=automatedTask.ownerid; 
                      
                    } 
    
                }
              
                setTemplateBody( automatedTask.Subject, selectedLocale);
                boolean emailSuccess = false;
                String[] arr=new String[]{};
                if(automatedTask.Owner.Email != null &&  automatedTask.Owner.Email != ''){
                    if( (orgwdEmailMap != null && orgwdEmailMap.size() > 0 && orgwdEmailMap.get( automatedTask.Owner.Email ) != null ) && orgwdEmailMap.get( automatedTask.Owner.Email ).Id != null ){
                    
                   //Check whether email need to send all default contacts  
                  
                      IF(CheckDefaultContacts)
                      {
                                              
                        emailSuccess = SendToAllDefault(accConEmailMap.get(automatedTask.WhatId));                                              
                      }
                     else
                      {                       
                        emailSuccess = sendEmail( orgwdEmailMap.get( automatedTask.Owner.Email ).Id );
                      }
                    }
                    else{
                    system.debug('###org wide else loop');
                   //Close the tasks if owner email not found in Org Wide Emails
                    Task tsk1 = automatedTask ;
                    tsk1.status = 'Completed';
                    tsk1.Type = 'Email';
                    tsk1.whoid=emailTargetToContactId ;
                    tsk1.Description= 'Email not sent since user is not registered in Org wide email address';
                    tsk1.Dunning_Status__c = 'Done';
                    taskToCloseNew.add(tsk1); 
                    Database.update(taskToCloseNew);
                    taskToCloseNew.clear();
                    
                    } 
                    
                    
                }
             
                if(emailSuccess)
                {
                    /*JIRA #AKTPROD-106 : By Kruti Tandel
                    Created map and set for updating account's fields */ 
                    String textTemplateBodyNew = UtilityController.replaceHTMLTags(textTemplateBodyTemp);
                    
                    String s1,s3,s4;
                    for(integer i=textTemplateBodyNew .indexOf('{!'); i<=textTemplateBodyNew .lastIndexOf('}'); i++){
                    s1=textTemplateBodyNew .substringBetween('{','}');
                    s3=textTemplateBodyNew .remove(s1);
                    s4=s3.remove('{}');
                    textTemplateBodyNew =s4.remove('&nbsp;');
                    
                   }
                 
                    automatedTask.status = 'Completed';
                    automatedTask.Type = 'Email';
                    automatedTask.whoid=emailTargetToContactId ;
                    automatedTask.Description= textTemplateBodyNew ;
                    automatedTask.Dunning_Status__c = 'Done';
                    automatedTask.Notes__c = temp4 +' by '+UserInfo.getName()+' - '+automatedTask.subject+' - '+'The Statement sent to the customer via Dunning' ;
                    tasksToClose.add(automatedTask);
                    if(accIDsUp.contains(acc.Id)){
                   
                        if(MapAccoutUpdate.get(acc.Id).Notes__c != null){
                        String note = (String)MapAccoutUpdate.get(acc.Id).Notes__c;
                        acc.Notes__c = temp4 +' by '+UserInfo.getName()+' - '+automatedTask.subject+' - '+'The Statement sent to the customer via Dunning' +'\n'+'\n'+note ;
                        }else{
                          acc.Notes__c =   temp4 +' by '+UserInfo.getName()+' - '+automatedTask.subject+' - '+'The Statement sent to the customer via Dunning';
                        }
                        MapAccoutUpdate.put(acc.Id,acc);
                    }else{
                        acc.Last_Contact_Date__c = date.today();
                        acc.Last_Contact_Method__c = 'Email';
                        acc.Last_Contact_Type__c = 'Dunning';
                         if(acc.Notes__c != null){
                            acc.Notes__c =temp4 +' by '+UserInfo.getName()+' - '+automatedTask.subject+' - '+'The Statement sent to the customer via Dunning'+'\n'+'\n'+acc.Notes__c;
                        }else{
                             acc.Notes__c =temp4 +' by '+UserInfo.getName()+' - '+automatedTask.subject+' - '+'The Statement sent to the customer via Dunning';
                         } 
                        accIDsUp.add(acc.Id);
                        MapAccoutUpdate.put(acc.Id,acc);
                    }
                    
                  List < List < Transaction__c > > txListOfList = txCollectionObj.getTransactionsListOfList();
                    List < Transaction__c > txList = new List < Transaction__c >();
                    if ( txListOfList != null && txListOfList.size()> 0 ) {
    
                        txList = txListOfList.get ( 0 );
                    }
                  //  generateActivityTransaction(automatedTask.id,txList);            
                }
            }
         }
         else{
         if(contacts.size() == 0){
       
          for(Task automatedTask : (List<Task>)sObjs) {    
                        Task t = automatedTask ;
                       // t.OwnerId = accs.get( accountId ).OwnerId;
                        t.status = 'Completed';
                        t.Auto_Dunning_Error__c = 'Accounts\'s Default Contact Not Found.';
                        t.Dunning_Status__c = 'Auto Dunning Error';
                        errTasktoupdate1.add(t);
                        }
                        database.update(errTasktoupdate1); 
                        errTasktoupdate1.clear();
                      } 
             return;
         }  
 
               AccountToUpdate = (List<account>) MapAccoutUpdate.values();
          
        //do it in finish method???????????
      /*  if(errTasktoupdate != null && errTasktoupdate.size() >0)
            update errTasktoupdate;*/
            system.debug('############### tasksToClose'+tasksToClose);
         if(tasksToClose != null && tasksToClose.size() >0)
            update tasksToClose;
        system.debug('############### tasktoInsert'+tasktoInsert);
        if(tasktoInsert != null && tasktoInsert.size() >0)
            insert tasktoInsert;
        system.debug('############### AccountToUpdate'+AccountToUpdate);
        if(AccountToUpdate != null && AccountToUpdate.size() >0)
            update AccountToUpdate;
        if(taskToCloseNew!= null && taskToCloseNew.size() >0)
            update taskToCloseNew;
            tasksToClose.clear();
            tasktoInsert.clear();
            AccountToUpdate.clear();
    }

    public void generateActivityTransaction(Id taskId, List<Transaction__c> transactions) {
                    
                    if(taskId != null) {
                        Activity_Transaction__c activityTx = new Activity_Transaction__c();
                        activityTx.Task_Id__c = taskId;
                        activityTx.Transaction_ids__c = TransactionsCollection.getCommaSeparatedTxIds(transactions);
                       insert activityTx;
                }
              }
    
    
    
    private void setTemplateBody( String automatedTaskSubject, String selectedLocale)
    {
        System.debug('#####$$$ Set Template body method is called...!!!');
        String selectedTemplateId = '';
        EmailTemplate selected = null;
        
        SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
        List<EmailTemplate> billingTemplateList = new  List<EmailTemplate>();
        if(sysConfig.is_Akritiv_Org__c){
            billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null and Folder.Name like 'Akritiv%' and Name=:automatedTaskSubject AND IsActive =: TRUE];        
        }else{
            if(selectedLocale != null && selectedLocale != '' ){
                billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null and Folder.Name=:selectedLocale  and Name=:automatedTaskSubject AND IsActive =: TRUE];
            }else{
                billingTemplateList = [select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Name=:automatedTaskSubject AND IsActive =: TRUE];
            } 
        }
     
        if(billingTemplateList != null && billingTemplateList.size() > 0){
            selected =billingTemplateList.get(0);
            selectedTemplateId = billingTemplateList.get(0).Id;
        }
            
        if(selected != null && selectedTemplateId != ''){   
            // check the type of email template and set the body of current email accordingly
            if(selected.TemplateType=='text')  {
                if(selected.body != null)
                    emailTemplateBody = selected.Body.replaceAll('\n','<br/>');
                else
                    emailTemplateBody = '';
            }
            else if(selected.TemplateType=='custom'){
                if(selected.HtmlValue != null)
                    emailTemplateBody = selected.HtmlValue;
                else
                    emailTemplateBody = '';
                
            }
            
            GenerateTemplateOutput output = new GenerateTemplateOutput();
             // set the email subject by the subject of the email template
            emailSubject = selected.Subject;
            emailSubject = replaceMergeFields(output, emailSubject);
           emailTemplateBody = replaceMergeFields(output, emailTemplateBody);
             system.debug('-------emailTemplateBodyOutput  ---'+emailTemplateBody  );
    
            // set the email subject by the subject of the email template
           // emailSubject = selected.Subject;
            //emailSubject = replaceMergeFields(output, emailSubject);
            
        }else{
            emailTemplateBody = '';
            emailSubject = '';
           
        }
    }

    global TransactionsCollection getTransactionsCollectionObject( String id ) {
        return txCollectionObj = new TransactionsCollection(getSelectedTransactions(id));
    }

    public Double totalBillingBal {get; set; }
    global List<List<Transaction__c>> getSelectedTransactions( String id) {

        List<List<Transaction__c>> txListOfList = new List<List<Transaction__c>>();
        totalBillingBal = 0;
    
     // String txSelectQuery = 'select Id, Name, Create_Date__c, So_Number__c, CreatedDate, Days_Past_Due__c, Po_Number__c, Due_Date__c, Balance__c, Amount__c  from Transaction__c where Account__c=: id';
       String txSelectQuery='';
        pcAccount=[select id,name from account where id in:acIds];
        integer i;
        
       DisplayChildAccountInvoice = sysConfig.Display_Child_Account_Invoice__c;

        
      if(DisplayChildAccountInvoice)   
            {       
             txSelectQuery = 'select Id, Name, Create_Date__c, So_Number__c, CreatedDate, Days_Past_Due__c, Po_Number__c, Due_Date__c, Balance__c, Amount__c  from Transaction__c where (Account__c=: id OR Account__r.parentid= : id ) ';
            }
      else
        {
           txSelectQuery = 'select Id, Name, Create_Date__c, So_Number__c, CreatedDate, Days_Past_Due__c, Po_Number__c, Due_Date__c, Balance__c, Amount__c  from Transaction__c where Account__c=: id';

        }


        
        
        
      // system.debug('aaaaaaaa'+txSelectQuery);
       
        //txSelectQuery = txSelectQuery + ' and (Disputed_Amount__c <= 0 OR Disputed_Amount__c = NULL)';
        
        txSelectQuery = txSelectQuery + ' and Balance__c != 0 and Balance__c != null ';


      //  txSelectQuery = txSelectQuery + ' and (Days_Past_Due__c >0 or Days_Past_Due__c = null)';

      //  txSelectQuery = txSelectQuery + ' and (Balance__c > 0 OR Balance__c = NULL)';

       SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
         //String BccEmailAdd = sysConfig .ArchivalBccEmailAddress__c;
        String invoicefilter =  sysConfig.Auto_Dunning_Invoice_Filter__c == null ?'':sysConfig.Auto_Dunning_Invoice_Filter__c ;                        

        system.debug('-------invoiceOrderBy[0] ---'+invoicefilter );
     //   string[] invoiceOrderBy = invoicefilter.split('order by'); 
        
       
        
        if ( invoicefilter != null &&  invoicefilter.trim() != '') {

            txSelectQuery  += ' ' + invoicefilter +' LIMIT 9999';
        }else{
            txSelectQuery  += ' ORDER BY Due_Date__c ASC LIMIT 9999';
        }
      
                     system.debug('*** txSelectQuery1 ' + txSelectQuery );

   

        // add the filter to get only the open transactions
       // txSelectQuery = txSelectQuery + ' and Balance__c != 0 and Balance__c != null ORDER BY Due_Date__c ASC LIMIT 9999';
        
        for(List<Transaction__c> txList : Database.query(txSelectQuery))
            txListOfList.add(txList);
        
        for(List<Transaction__c> txList : txListOfList) {
            for(Transaction__c tx : txList)
                totalBillingBal = totalBillingBal + (tx.Balance__c != null ? tx.Balance__c : 0);
        }
        system.debug('*** txListOfList' +txListOfList);
        return txListOfList;
    }
    

    private String replaceMergeFields(GenerateTemplateOutput output, String emailTemplateStr)
    {
        SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
        String invoicefilter =  sysConfig.Auto_Dunning_Invoice_Filter__c == null ?'':sysConfig.Auto_Dunning_Invoice_Filter__c ;                        
        string[] invoiceOrderBy = invoicefilter.split('order by'); 
        string[] invoiceOrderByNew = invoiceOrderBy[1].split(' '); 
        String mergedBody;
        //??no user id
        mergedBody = output.processTemplateBody(accountId, emailTargetToContactId, Uid, emailTemplateStr,'');
        textTemplateBodyTemp = mergedBody ;
        // replace the summary field
      // system.debug('***Acccc' +accountID);
       //  if(mergedBody.contains('{!Transactions.TotalBalance}'))  
          //  mergedBody = output.processTransactionSummaryMergeField(mergedBody, selectedList, TransactionsCollection.getTransactionIdsListOfList(getTransactionsCollectionObject(accountId)), accountId,'','','');
        if(mergedBody.contains('{!Transactions.Summary}')){
            mergedBody = output.processTransactionSummaryMergeField(mergedBody, selectedList, TransactionsCollection.getTransactionIdsListOfList(getTransactionsCollectionObject(accountId)), accountId,'', invoiceOrderByNew[0], invoiceOrderByNew[1] );
           //textTemplateBodyTemp = mergedBody ;
           }
        if(mergedBody.contains('{!User.Name}')) {

            mergedBody   =  mergedBody.replace('{!User.Name}', username);
        }
      
        return mergedBody;
    }

 


    global Boolean validateEmail() {
        return (emailTargetToContactId != null && emailTargetToContactId !='');
    }
    global Boolean sendEmail() {
        
       

        // Flag that will set to true if all executes successfully
        Boolean success = false;
        

        try {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setHtmlBody(emailTemplateBody);
            mail.setSubject(emailSubject);
            mail.setTargetObjectId(emailTargetToContactId);
            mail.setSaveAsActivity(false);
           
            
            if( fromEmail != null) {

                mail.setReplyTo(fromEmail);
               
            }
            //mail.setBccSender(true);
            Messaging.sendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});

            for ( Messaging.sendEmailResult result : results ) {
                if ( !result.isSuccess () ) {
                    
                    success = false;
                }
            }
            success = true;
        }
        catch(Exception ex) {
            
            success = false;
        }

        return success;
    }
      
     
     
 public Boolean sendEmail( Id orgwideemailaddress ) {
     
        if(!validateEmail()) {
            return false;
        }
       //system.debug('***********************************');
        // Flag that will set to true if all executes successfully
        Boolean success = false;
        
        if((emailTemplateBody != null && emailTemplateBody.trim() != '') && (emailSubject != null && emailSubject.trim() != '')){
            try {
            system.debug('eeeeeeeee' +emailTargetToContactId);
                ll.add(BccEmailAdd);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setHtmlBody(emailTemplateBody);
                mail.setSubject(emailSubject);
                mail.setTargetObjectId(emailTargetToContactId);
                mail.setSaveAsActivity(false);
                mail.setOrgWideEmailAddressId(orgwideemailaddress );
                mail.setBccAddresses(ll);
                
                Messaging.sendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
                for ( Messaging.sendEmailResult result : results ) {
                    if ( !result.isSuccess () ) {
                      system.debug('***********************************');  
                        success = false;
                    }
                }
                success = true;
    
            }
            catch(Exception ex) {
                system.debug('***********************************else');  
                success = false;
            }
        
      }
        return success;
        
    }

//Send email to all Default Contacts
  public boolean SendToAllDefault(Set<String> con)
      {
      
                  List<String> toAddressesList = new List<String>();
                  toAddressesList.addAll(con);
                  System.debug('*****' +con+ ' ' +con.size());
                  ll.add(BccEmailAdd);
                  Boolean success = false;
                  Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();                            
                  mail.setHtmlBody(emailTemplateBody);
                  mail.setSubject(emailSubject);                                      
                            
                  mail.setToAddresses(toAddressesList);
                  mail.setBccAddresses(ll);
                 // mails.add(mailToAll);
                  Messaging.sendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                    for(Messaging.sendEmailResult result : results){
                    IF(!result.isSuccess()){
                        success = false;
                    }
                }
                success = true;
                return success;
      }
      
      
      

    global void finish(Database.BatchableContext bc){
        
    }
    
    global void execute(SchedulableContext sc) {
        // call the data purge batch job
        BatchCloseAutomatedTasks bafl = new BatchCloseAutomatedTasks();
        Database.executeBatch(bafl,1);
    }
}