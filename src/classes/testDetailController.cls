@isTest 
private class testDetailController{
    static testMethod void testCase1() 
    {    
        
      
        
        
        Account accObj = new Account() ;
        accObj = DataGenerator.accountInsert();
        List<Transaction__c> txList = new List<Transaction__c>();
        txList = DataGenerator.transactionInsert(accObj);
        Task tsk = DataGenerator.taskInsert(accObj);
        Temp_Object_Holder__c temp = new Temp_Object_Holder__c();
        temp.value__c = tsk.Id;
        String key = Userinfo.getUserName()+Datetime.now();

       // temp.key__c = key;
        
        
        insert temp;
        
        
        
        ApexPages.currentPage().getparameters().put('key',temp.key__c);
        Task t = new Task();
        t.WhatId = accObj.Id;
        t.status = 'Not Started';
        t.OwnerId = Userinfo.getUserId();
        insert t;
        Task t1 = new Task();
        t1.WhatId = accObj.Id;
        t1.status = 'Not Started';
        t1.OwnerId = Userinfo.getUserId();
        insert t1;
        
       
        
        DetailController dc = new DetailController();
        
        dc.notetitle = 'test note';
        dc.taskIds.add(t.Id);
        dc.taskIds.add(t1.Id);
        
       
        system.assertEquals(dc.notetitle,'test note');
        dc.prepareAccounts();
        dc.prepareselected();
        dc.gettskkeyval();
        dc.getAccountNotes();
        dc.getTransactionsNotes();
        dc.getListContents();
        dc.next();
        dc.completenext();
        dc.updateNote();
        dc.updateNotetrx();
        
        
        
       /* String temp1 = '1';
        system.assertEquals(temp1,'1');*/
    }
    
     static testMethod void testCase2() 
    {    
    
        /*akritiv__Trigger_Configuration__c triggerObj = new akritiv__Trigger_Configuration__c();
        triggerObj.akritiv__Zero_AR_Account_Trigger__c = false;
        triggerObj.akritiv__Manage_Account_Key_Trigger__c = false;
        triggerObj.akritiv__Zero_Balance_Transaction_Trigger__c = false;
        triggerObj.akritiv__Handle_Reserve_Transaction_Trigger__c = false;
        triggerObj.akritiv__Create_Note_On_BrokenPromise__c = false;
        upsert triggerObj; */
    
        DetailController dc = new DetailController();
        dc.notetitletrx = 'test';
        dc.noteBodytrx = 'test';
        Account accObj = new Account() ;
        accObj = DataGenerator.accountInsert();
     
        List<Transaction__c> txList = new List<Transaction__c>();
       
        
        txList = DataGenerator.transactionInsert(accObj);
         accObj.Lead_Tx__c = txList.get(0).Id;
        accObj.High_Past_Due_Tx__c = txList.get(0).Id;
        update accObj;
           Note n = new Note();
        n.title = 'test';
        n.body = 'test';
        n.parentId = txList.get(0).Id;
        insert n;
        Task tsk = DataGenerator.taskInsert(accObj);
        Temp_Object_Holder__c temp = new Temp_Object_Holder__c();
        temp.value__c = tsk.Id;
        String key = Userinfo.getUserName()+Datetime.now();

        temp.key__c = key;
        
        
        insert temp;
        ApexPages.currentPage().getparameters().put('key',temp.key__c);
        Task t = new Task();
        t.WhatId = accObj.Id;
        t.status = 'Not Started';
        t.OwnerId = Userinfo.getUserId();
        insert t;
        Task t1 = new Task();
        t1.WhatId = accObj.Id;
        t1.status = 'Not Started';
        t1.OwnerId = Userinfo.getUserId();
        insert t1;
        dc = new DetailController();
        dc.notetitletrx = 'test';
        dc.noteBodytrx = 'test';
        dc.notetitle = 'test note';
        dc.taskIds.add(t.Id);
        dc.taskIds.add(t1.Id);
        
        system.assertEquals(dc.notetitle,'test note');
        dc.prepareAccounts();
        dc.prepareselected();
        dc.gettskkeyval();
        dc.getAccountNotes();
        dc.getTransactionsNotes();
        dc.getListContents();
        dc.next();
        dc.completenext();
        dc.updateNote();
        dc.updateNotetrx();
        
       
    }
}