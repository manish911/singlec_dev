/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Wrapper Class to construct Task object for various activities
            like contact customer,promise, dispute etc.
 */
public class ContactLogEntry
{
    public Id contactId {get; set; }
    public Id accountId {get; set; }
    public Set<Id> accountIds {get; set; }
    public Id userId {get; set; }
    public List<Transaction__c> transactionIds {get; set; }
    public String contactType {get; set; }
    public String contactMethod {get; set; }
    public String subject {get; set; }
    public String notes {get; set; }
    public String comments {get; set; }
    public String category {get; set; }
    public String priority {get; set; }
    public String status {get; set; }
    public String callResult {get; set; }
    public String activityRating {get; set; }
    public Date activityDate {get; set; }
    public String accountOwnerId {get; set; }
    public List<Line_Item__c> lineItemList {get;set;}
    Public String ccAddress {get;set;}
    Public String bccAddress {get;set;}

    public static final String PRIORITY_DEFAULT='Normal';
    public static final String STATUS_DEFAULT='Completed';
    
    // Param - contactId, accountId, transactionIdList, contactType, contactMethod, contactMethod, subject, notes, category, userId, status, priority, emailText 
    // set cont log entry details
    public ContactLogEntry(Id contactId, Id accountId, List<Transaction__c> transactionIdList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String emailText)

    {

        validate(contactId, accountId, userId);
        if(contactId != null)
        this.contactId = contactId;
        this.accountId  = accountId;
        this.userId = userId;
        this.transactionIds = transactionIdList;
        this.contactMethod = contactMethod;
        this.contactType = contactType;
        this.subject = subject;
        this.notes = notes;
        this.category = category;
        if(priority == null)
        {
            this.priority = PRIORITY_DEFAULT;
        }

        this.priority = priority;

        if(status == null)
        {
            this.status = STATUS_DEFAULT;
        }

        this.status = status;
        this.comments = emailText.trim();
        system.debug('comments from First----' + this.comments);
    }
    // Created contractor for accountlist's arguments
    //JIRA Issue #AKTPROD-133
    //By Kruti Tandel 
     public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Transaction__c> transactionIdList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String emailText)

    {
        for(Id AccId:accountIdList){
            validate(contactId, AccId, userId);
        }
        if(contactId != null)
        this.contactId = contactId;
        this.accountIds  = accountIdList;
        this.userId = userId;
        this.transactionIds = transactionIdList;
        this.contactMethod = contactMethod;
        this.contactType = contactType;
        this.subject = subject;
        this.notes = notes;
        this.category = category;
        if(priority == null)
        {
            this.priority = PRIORITY_DEFAULT;
        }

        this.priority = priority;

        if(status == null)
        {
            this.status = STATUS_DEFAULT;
        }

        this.status = status;
        this.comments = emailText.trim();
        system.debug('comments From second----' + this.comments);
    }

    //AKTPROD-489 ---------------- Stat Here ---------------->>
    //Created Constructor with one more Parameter which is EmailTemplatebody which is Edited by user
    //save this Edited email template body in to task's comment
    
        public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Transaction__c> transactionIdList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String emailText, 
                           String EmailTemplateBody, boolean IsEditedBody)

        {
            for(Id AccId:accountIdList){
                validate(contactId, AccId, userId);
            }
            if(contactId != null)
            this.contactId = contactId;
            this.accountIds  = accountIdList;
            this.userId = userId;
            this.transactionIds = transactionIdList;
            this.contactMethod = contactMethod;
            this.contactType = contactType;
            this.subject = subject;
            this.notes = notes;
            this.category = category;
            if(priority == null)
            {
                this.priority = PRIORITY_DEFAULT;
            }
    
            this.priority = priority;
    
            if(status == null)
            {
                this.status = STATUS_DEFAULT;
            }
    
            this.status = status;
            this.comments = EmailTemplateBody.trim();
            system.debug('comments From Newly Created Constructor----' + this.comments);
        }    
     
    
    //AKTPROD-489 ---------------- End Here ---------------->>

    public ContactLogEntry(Id contactId, Id accountId, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText)
    {

        this(contactId, accountId, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );

        this.callResult = callResult;
    }
     public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText)
    {

        this(contactId,accountIdList, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
       // this.accountIds = accountIdList;
        this.callResult = callResult;
    }

    public ContactLogEntry(Id contactId, Id accountId, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, String rating )
    {

        this(contactId, accountId, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityRating = rating;
    }
    
    
    public ContactLogEntry(Id contactId, set<Id> accountListId, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, String rating )
    {

        this(contactId, accountListId, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityRating = rating;
    }

    public ContactLogEntry(Id contactId, Id accountId, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, String rating, String accountOwnerId)
    {

        this(contactId, accountId, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityRating = rating;
        this.accountOwnerId = accountOwnerId;
    }

    public ContactLogEntry(Id contactId, Id accountId, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, Date activityDate, String rating) {
       
        this(contactId, accountId, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityDate = activityDate;
        this.activityRating = rating;
    }
     public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Transaction__c> transactionList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, Date activityDate, String rating) {
       
        this(contactId, accountIdList, transactionList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityDate = activityDate;
        this.activityRating = rating;
    }
    
    public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Line_Item__c> lineList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String emailText){
        for(Id AccId:accountIdList){
            validate(contactId, AccId, userId);
        }
        if(contactId != null)
        this.contactId = contactId;
        this.accountIds  = accountIdList;
        this.userId = userId;
        this.lineItemList = lineList;
        this.contactMethod = contactMethod;
        this.contactType = contactType;
        this.subject = subject;
        this.notes = notes;
        this.category = category;
        if(priority == null)
        {
            this.priority = PRIORITY_DEFAULT;
        }

        this.priority = priority;

        if(status == null)
        {
            this.status = STATUS_DEFAULT;
        }

        this.status = status;
        this.comments = emailText.trim();
        system.debug('comments From Third----' + this.comments);
    }
    
    
    
    
    
    public ContactLogEntry(Id contactId, Set<Id> accountIdList, List<Line_Item__c> lineList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, Date activityDate, String rating){
        this(contactId, accountIdList, lineList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText);
        this.callResult = callResult;
        this.activityDate = activityDate;
        this.activityRating = rating;
        System.debug('#### Contact Log Enter - User ID 1 : '+userId);
    }
    
    public ContactLogEntry(Id contactId, set<Id> accountListId, List<Line_Item__c> lineList,
                           String contactType, String contactMethod, String subject, String notes,
                           String category, Id userId, String status, String priority, String callResult,String emailText, String rating )
    {

        this(contactId, accountListId, lineList,contactType, contactMethod, subject, notes, category, userId, status, priority, emailText );
        this.callResult = callResult;
        this.activityRating = rating;
    }

    private void validate(Id contactId, Id accountId, Id userId)
    {
        if(accountId == null || userId == null)
        {
            throw new InvalidDataException('Invalid Contact Log Data, Either contactId, accountId or '+
                                           'userId is missing.');
        }
    }
    // Store 
    public void store()
    {
        ContactLogGenerator.getInstance().generateContactLogEntry(this);
    }
    
    // call generateTaskListLogEntries method for creating tasks in single entery.
    //JIRA Issue #AKTPROD-133
    //By Kruti Tandel 
    public void storeMultiple()
    {
        ContactLogGenerator.getInstance().generateTaskListLogEntries(this);
    }

}