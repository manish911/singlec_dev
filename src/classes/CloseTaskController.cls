public with sharing class CloseTaskController {
    public String location { get; set; }
    public CloseTaskController(){
        location =  ApexPages.currentPage().getparameters().get('location');
    }
    
    public pageReference closeTask(){
        List<Task> updateTaskList = new List<Task>() ;
        List<Task> taskIds = new List<Task>();
        
        String key,loc;        
        
            if(ApexPages.currentPage().getparameters().get('key') != null){
            
                key =  ApexPages.currentPage().getparameters().get('key');
                loc =  ApexPages.currentPage().getparameters().get('location');
                key = key.trim();
                loc = key.trim();
                
                Temp_Object_Holder__c temp = [ select Id, Value__c from Temp_Object_Holder__c where key__c = : key];
                                                
                String taskstr = temp.Value__c;
    
                String  [] taskstrarray = taskstr.split(','); 
                
                taskIds = [ SELECT ID, Status FROM Task WHERE Id in:taskstrarray ORDER BY ID  ];
                 
                try {                        
                    for ( Task t : taskIds  ) {                
                        t.Status = 'Completed';
                        updateTaskList.add(t);
                    }  
                    if ( updateTaskList != null ){   
                        //==========================================================================================  
                             list<String> lstFields = new list<String>{'Status'};
                           Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
                           for (String fieldToCheck : lstFields) {
                              // Check if the user has create access on the each field
                              if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                                 if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                            'Insufficient access'));
                                    return null;
                                  }
                               }
                            }
                        //========================================================================================       
                        Update updateTaskList;                                           
                    }
                    return null;
                }
                catch(Exception ex) {     
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Label_Tasks_Are_Not_Close_Due_To + Label.Label_Exception + ex.getMessage()));  
                    return null;         
                }                 
            }
            return null;
    }    
    public void shwfirtMsg(){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Are_you_sure_you_want_to_close_selected_tasks));
    }
    
    static testMethod void testCase1()
    {        
            String key;
            String loc;
            CloseTaskController cts = new CloseTaskController();      
            Account acc = CloseTaskController.accountInsert();     
            System.assertEquals('testAccount', acc.name); 
            Task tsk = CloseTaskController.taskInsert(acc);       
            Temp_Object_Holder__c toh = CloseTaskController.tempObjectInsert(tsk);  
            loc = toh.Value__c;
            key = toh.key__c;
            ApexPages.currentPage().getParameters().put('key',key);    
            ApexPages.currentPage().getParameters().put('location',loc); 
            cts.closeTask();
            cts.shwfirtMsg();
    }   
    public static Temp_Object_Holder__c tempObjectInsert(Task ts){
        String txIds= ts.Id;
       
        String key = Userinfo.getUserName()+Datetime.now();
        Temp_Object_Holder__c txIdsConfig = new Temp_Object_Holder__c();
        txIdsConfig.key__c = key;
        txIdsConfig.value__c = txIds;
        
        insert (txIdsConfig);
        
        return txIdsConfig;
    
    }  
    public static Task taskInsert( Account acc) {
        //create a Task
        Task taskObj = new Task();
        //taskObj.WhatId = acc.Id;
        taskObj.ActivityDate  = Date.today()-2;
        taskObj.Subject= 'DemoBillPDF';
        taskObj.Dunning_Status__c = 'In Progress';
        taskObj.Status = 'In Progress';
        insert taskObj;
        return taskObj;
    }  
    public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =
                ''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
        insert accObj;
        return accObj;
    }   
}