/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : An apex page controller that exposes the site forgot password
 *         functionality
 */
public class ForgotPasswordController {
    public String username {get; set; }

    public ForgotPasswordController() {
    }
    // Provides functionality to user to remember his old password
    // Return Type - Pagereference
    // No parameters are passed in this method 
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.ForgotPasswordConfirm;
        pr.setRedirect(true);

        if (success) {
            return pr;
        }
        return null;
    }

    public static testMethod void testForgotPasswordController() {

        Test.startTest();

        // Instantiate a new controller with all parameters in the page
        ForgotPasswordController controller = new ForgotPasswordController();
        controller.username = 'test@salesforce.com';

        System.assertEquals(controller.forgotPassword(),null);
        System.assertEquals(controller.username, 'test@salesforce.com');

        Test.stopTest();
    }
}