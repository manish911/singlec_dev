/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
   *Usage   :   The class is used to update fields of dispute.This fields have to be retrieved dynamically using fieldset
 */
public Class MassUpdateDisputeDynamicController{
    public list<String> SelectedFields {get;set;}
    public Dispute__c disList{get;set;}
    String fields;
    public Boolean isDisabledUpdateBtn{get;set;}
    public String PARAMNAME {get;set;}
    
    public MassUpdateDisputeDynamicController(){
        isDisabledUpdateBtn = false;
        SelectedFields = new list<String>();
        PARAMNAME =   ApexPages.currentPage().getParameters().get('location');
        disList= new Dispute__c();
    }
    
    public void massUpdate() {
        String tempObjKey = ApexPages.currentPage().getParameters().get('key');
        if(tempObjKey != null && !tempObjKey.trim().equals('')) {
            Temp_Object_Holder__c idsObj = [ Select value__c from Temp_Object_Holder__c where Key__c = :tempObjKey.trim() LIMIT 1];
             
            if(idsObj != null && idsObj.value__c != null && !idsObj.value__c.equals('')) {
             
            try {
                List<Schema.FieldSetMember> fieldSet = SObjectType.Dispute__c.FieldSets.Dispute_Update_FieldSet.getFields();
                List<String> ids = idsObj.value__c.trim().split(',');
                String selectQry = 'Select Id,';
                for(Schema.FieldSetMember field : fieldSet) {
                    selectQry += String.valueOf(field.getFieldPath()) + ',';
                }
                selectQry = selectQry.subString(0,selectQry.length() - 1);
                selectQry += ' From Dispute__c Where Id in (' ;
                for(String id : ids) {
                    selectQry += '\'' + id + '\',';
                }
                selectQry = selectQry.subString(0,selectQry.length() - 1);
                selectQry += ')' ;
                List<Dispute__c> dis = Database.query(selectQry);
                system.debug('--------qry---------'+selectQry);
                String noteHeader = Datetime.now().format('dd-MMM-yyyy') + ' by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                
                for(Schema.FieldSetMember field : fieldSet) {
                    //to check value is enter or not
                    system.debug('---------disList.get(field.getFieldPath())-----'+disList.get(field.getFieldPath()));
                    if(disList.get(field.getFieldPath()) != null && !String.valueOf(disList.get(field.getFieldPath())).trim().equals('')) {                
                        system.debug('--------------------dis----------------'+dis.size());
                        for(Dispute__c t : dis) {
                            if(field.getLabel().equalsIgnoreCase('Notes')) {
                                if(t.get(field.getFieldPath()) != null) {
                                    system.debug('----in---if');
                                    t.put(field.getFieldPath(),noteHeader + '\n' + disList.get(field.getFieldPath()) + '\n\n'+String.valueOf(t.get(field.getFieldPath()))); 
                
                                }         
                                else {
                                    t.put(field.getFieldPath(), noteHeader + '\n' + disList.get(field.getFieldPath()) + '\n'); 
                                }
                            } 
                            else {
                                t.put(field.getFieldPath(), disList.get(field.getFieldPath()));
                            }
                        }
                     }
                }
                if(dis.size() > 0 ) {
                    update dis;
                    isDisabledUpdateBtn = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Selected Disputes updated successfully'));
                } 
                else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Update of selected disputes failed.Please try again'));
                }          
            } 
            catch (Exception e) {
                System.debug('Exception : ' + e);
            }
            }
        }
    }
}