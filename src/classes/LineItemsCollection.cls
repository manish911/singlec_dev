/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Class used to hold the collection of transaction objects.
*/

global class LineItemsCollection{
    /* List of list of transaction*/
    global List<List<Line_Item__c>> listOfListOfLineItems {get; set;}

    // constructor1
    global LineItemsCollection(List<List<Line_Item__c>> lstLstLines) {
        this.listOfListOfLineItems = lstLstLines;
    }

    // constructor2
    global LineItemsCollection(List<Line_Item__c> lstLines) {
        if(lstLines != null) {
            this.listOfListOfLineItems = new List<List<Line_Item__c>>();
            this.listOfListOfLineItems.add(lstLines);
        }
    }

    /* return listOfListOfLineItems property */
    global List<List<Line_Item__c>> getLineItemsListOfList() {
        return this.listOfListOfLineItems;
    }
    // method is used to get TransactionIds List Of List
    // return type : List<List<Id>>
    // parameters : LineItemCollection - LineItemsCollection
    global static List<List<Id>> getLineItemIdsListOfList(LineItemsCollection txCollection){
        if(txCollection == null){
            return null;
        }
        List<List<Id>> lineItemIdListOfList = new List<List<Id>>();
        if(txCollection != null) {
            for(List<Line_Item__c> lineItemList : txCollection.getLineItemsListOfList()) {
                List<Id> txIdList = new List<Id>();
                for(Line_Item__c tx : lineItemList) {
                    txIdList.add(tx.Id);
                }
                lineItemIdListOfList.add(txIdList);
            }
        }
        return lineItemIdListOfList;
    }
    
   /* 
    *    method is used to get TransactionIds List Of List
    *    return type : List<List<Id>>
    *    parameters : lineItemIdsList - List < Id >
    */
    global static List<List<Id>> getTransactionIdsListOfList(List<Id> lineItemIdsList) {
        if(lineItemIdsList == null){
            return null;
        }
        
        List<List<Id>> lineItemIdsListOfList;
        if( lineItemIdsList.size() > 0 && lineItemIdsList != null) {
            lineItemIdsListOfList = new List<List<Id>>();
            lineItemIdsListOfList.add(lineItemIdsList);
        }

        return lineItemIdsListOfList;
    }
    // method is used to get Comma Separated lineItemIds
    // return type : String
    // parameters : lineItemIdsList - List < Id >
    global static String getCommaSeparatedlineItemIds(List<Line_Item__c> lineItemList) {
        String lineItemIds = '';
        if(lineItemList != null){
            for(Line_Item__c tx : lineItemList)
                lineItemIds += tx.id+',';
        }
        return lineItemIds;
    }
    // method is used to getCommaSeparatedlineItemIds
    // return type : String
    // parameters : lineItemIdsList - List < Id > 
    global static String getCommaSeparatedlineItemIds(List<Id> lineItemIdsList) {
        String lineItemIds = '';
        if(lineItemIdsList != null) {
            for(String txId : lineItemIdsList)
                lineItemIds += txId+',';
        }
        return lineItemIds;
    }
}