public with sharing class ChildAccountController2
{
        public list<account> listChildAcc00{set;get;}
        public list<wrapperacc> listChildAcc{set;get;}
        String accid;
         
        
        public ChildAccountController2()
        {
            listChildAcc=new list<wrapperacc>();
            if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != '')
            {
                accid = ApexPages.currentPage().getParameters().get('id');
            }
           
            listChildAcc00 = [select id, Name, Site, BillingAddress, Phone, Type, Owner.name from Account where Parentid =:accid];
            for(account i:listChildAcc00)
            {
                listChildAcc.add(new wrapperacc(i));
            }

        }
        
      
        
        
        public class wrapperacc
        
        {
            public boolean isselected {set;get;}
            public account accrec {set;get;}
            public wrapperacc(account a)
            {
              isselected = false;
              
              accrec = a;
            }   
        
        }
        
        

}