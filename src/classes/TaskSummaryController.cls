public with sharing class TaskSummaryController {

    public Integer openTasks { get; set; }
    
    public Integer calls { get; set; }
    
    public Integer emails { get; set; }
    
    public Integer fax { get; set; }

    public Integer high { get; set; }
    
    public Integer normal { get; set; }
    
    public Integer low { get; set; }
    
    public Id userId { get; set ; }
    
    public Integer notstarted { get; set; }
    
    public Integer inprogress { get; set; }
    
    public Temp_Object_Holder__c temp {get;set;}
    
    public List < Task > allTasks = new List < Task >();
    
    public String consoleurl {get;set;}
    
    public String packageHostAddress {get;set;}
    
    Public String s {get;set;}
    
    public TaskSummaryController  (){
        
        packageHostAddress = ApexPages.currentPage().getHeaders().get('host');
    
    }
    
    
    public void summarise() {
        
        openTasks  = 0;
        calls  = 0;
        userId = UserInfo.getUserId();
        high = 0;
        allTasks = [ select Id , Subject ,Contact_Method__c, Type , Status , Priority , OwnerId from Task where Status != 'Completed' and OwnerId =:userId and WhatId != null ];
        openTasks  = allTasks.size();
        normal = 0;
        notstarted = 0;
        inprogress= 0;
        low=0;
        fax = 0;
        emails = 0;
        
        s = '';
        for ( Task t : allTasks ){
        
            if ( s ==''){
            
                s = s + t.id;
            } else {
                
                s = s +','+t.id;
            }
            if ( t.Contact_Method__c == 'Phone')calls++;
            if ( t.Contact_Method__c== 'Email')emails++;
            if ( t.Priority == 'High')high ++;
            if ( t.Priority == 'Normal')normal++;
            if ( t.Priority == 'Law')low++;
            
            
            if ( t.Status == 'Not Started')notstarted++;
            if ( t.Status == 'In Progress')inprogress++;
        }
                
        
        consoleurl = 'https://'+packageHostAddress+'/apex/DetailPage';
                   
        
    }

    public static testmethod void teste1(){
        String str = 'test';
        TaskSummaryController  tsc = new TaskSummaryController();  
        tsc.summarise();
        System.assertEquals(str ,'test');
    }
}