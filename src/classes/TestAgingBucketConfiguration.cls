/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Unit tests for testing AgingBucketConfiguration apex class
 */
public class TestAgingBucketConfiguration {

    static testMethod void Test_AgingBucketConfiguration(){

        Test.startTest();

        AgingBucketConfiguration controller = new AgingBucketConfiguration();

        controller.save();
        controller.resetToDefault();

        System.assertEquals(controller.buckets.size(), 6);
        System.assertEquals(controller.buckets[0].maxRangeDisabled, true);
        System.assertEquals(controller.buckets[5].maxRangeDisabled, true);
        System.assertEquals(controller.buckets[0].maxRange2, '0');
        System.assertEquals(controller.buckets[5].maxRange2, '2147483647');

        Test.stopTest();

    }
}