public class ShowAccountforLineItems{
 
    //default page size
    private static final Integer PAGE_SIZE = 1000;
    //pagination information
    public Integer page{get;set;}
    public Integer totalRecords{get;set;}
    public Integer totalPages{get;set;}
    public Integer startIdx{get;set;}
    public Integer endIdx{get;set;}
    public id accountId{get; set;}
    public String accountName{get;set;}
    public String strProtocol{get;set;}
    public string accIdCommaSepareted{get;set;}
    public String selectedAccIDs{get;set;}
     
    /*
    * set controller
    */
    public List<CCWRow> tRecords{get;set;}
    /*
    * constructor
    */
    
    public Pagereference VFpage(){
    
    return null;
    
    }

public ShowAccountforLineItems()
{
    accIdCommaSepareted = '';
    accountId = ApexPages.currentPage().getParameters().get('id');
    accountName = ApexPages.currentPage().getParameters().get('accName');
    strProtocol = ApexPages.currentPage().getParameters().get('strProtocol');
    System.currentPageReference().getParameters().put('strProtocol', strProtocol);
    //init variable
    this.tRecords = new List<CCWRow>();
    //set initial page
    this.page = 1;
    //load records
    getContacts();
}
/*
* advance to next page
*/
public void doNext(){
if(getHasNext()){
this.page++;
getContacts();
}
 
}
/*
* advance to previous page
*/
public void doPrevious(){
if(getHasPrevious()){
this.page--;
getContacts();
}
}
/*
* returns whether the previous page exists
*/
public Boolean getHasPrevious(){
if(this.page>1){
return true;
}
else{
return false;
}
}
/*
* returns whether the next page exists
*/
public Boolean getHasNext(){
if(this.page<this.totalPages){
return true;
}
else{
return false;
}
}
    /*
    * return current page of records
    */
    public void getContacts(){
        //calculate range of records for capture
        this.startIdx = (this.page-1)*PAGE_SIZE;
        this.endIdx = this.page*PAGE_SIZE;
        this.totalRecords = 0;
        //clear container for records displayed
        this.tRecords.clear();
        //cycle through
        for(Account c : [Select Name,Account_Key__c,Parent.Name,Base_Total_AR__c,Base_Total_Past_Due__c from Account where Total_AR__c <> 0 and   (Id = :accountId OR  ParentId = :accountId   ) order by Name ASC limit 50000]){
            //capture records within the target range
            if(this.totalRecords>=this.startIdx && this.totalRecords<this.endIdx){
                this.tRecords.add( new CCWRow(c, false) );
                accIdCommaSepareted = accIdCommaSepareted + ',' + c.ID;
            }
            //count the total number of records
            this.totalRecords++;
        }
        //calculate total pages
        Decimal pages = Decimal.valueOf(this.totalRecords);
        pages = pages.divide(Decimal.valueOf(PAGE_SIZE), 2);
        this.totalPages = (Integer)pages.round(System.RoundingMode.CEILING);
        //adjust start index e.g. 1, 11, 21, 31
        this.startIdx++;
        //adjust end index
        if(this.endIdx>this.totalRecords){
            this.endIdx = this.totalRecords;
        }
    }
    /*
    * helper class that represents a row
    */
    public with sharing class CCWRow{
        public Account tContact{get;set;}
        public Boolean IsSelected{get;set;}
        public CCWRow(Account c, Boolean s){
            this.tContact=c;
            this.IsSelected=s;
        }
    }
    
    /*
     * Function is called from Page : ShowAccountForLineItems
    */
    public PageReference callParentPageFunction(){
        System.debug('#### Selected Account IDs : '+selectedAccIDs);
        return new PageReference('/apex/TestPagination?accid='+selectedAccIDs);
    }
}