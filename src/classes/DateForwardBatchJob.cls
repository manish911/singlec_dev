/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for Adding days into the day past due.
 */
global class DateForwardBatchJob implements Database.Batchable<SObject>, Database.Stateful {
    public Integer daysToForward {get; set; }
    public String query {get; set; }
    
    // method is used to start batchable context 
    // return type : database.querylocator
    // parameters : bc - Database.BatchableContext
    global database.querylocator start(Database.BatchableContext bc){
        /**start Namespace for migrating to unmanaged package  **/
        //String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        String namespace = '';
        query = 'Select '+namespace+'Due_Date__c, '+namespace+'Create_Date__c, '+namespace+'Close_Date__c, '+namespace+'Business_Unit__c, '+namespace+'Batch_Number__c, '+namespace+'Account__c From '+namespace+'Transaction__c where '+namespace+'Account__c !=null';
        // query = 'Select  Due_Date__c,  Create_Date__c,  Close_Date__c,  Business_Unit__c,  Batch_Number__c,  Account__c From  Transaction__c where  Account__c !=null';

        /**end Namespace for migrating to unmanaged package  **/
        if(daysToForward == null)
            daysToForward = 0;

        return Database.getQueryLocator(query);
    }
    
    // method is used to execute batchable context 
    // return type : void
    // parameters : bc - Database.BatchableContext , sObjs - List< sObject >
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
        List<Transaction__c> txToUpdate =  new List<Transaction__c>();
        for(Sobject txSobject : sObjs) {
            Transaction__c tx = (Transaction__c) txSobject;
            addDaysToForward(tx);

            txToUpdate.add(tx);
        }
        if(txToUpdate.size() > 0)
        {
            update txToUpdate;
        }
    }
    
    // method is used to add days to forward into days pass due 
    // return type : void
    // parameters : tx - Transaction__c
    private void addDaysToForward(Transaction__c tx)
    {
        if(tx.Create_Date__c != null)
            tx.Create_Date__c= tx.Create_Date__c + daysToforward;

        if(tx.Close_Date__c != null)
            tx.Close_Date__c= tx.Close_Date__c + daysToforward;

        if(tx.Due_Date__c != null)
            tx.Due_Date__c = tx.Due_Date__c + daysToforward;

    }

    // finish method implementation of database.batchable
    // return type : void 
    // parameters : BC : Database.Batchablecontext
    global void finish(Database.BatchableContext BC) {
        try {
            BatchJobLog__c logEntry = new BatchJobLog__c();
            logEntry.Name= 'DateForwardBatchJob';
            logEntry.Last_Run_Date__c = Date.today();

            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.BatchJobLog__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
            insert logEntry;

            List<BatchJobLog__c> entry = new List<BatchJobLog__c>([Select id, name, last_run_date__c from batchjoblog__c where name ='DateForwardBatchJob' order by last_run_date__c desc limit 1]);

        } catch(QueryException e)
        {
            throw e;
        }
    }

}