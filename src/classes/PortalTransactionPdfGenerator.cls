/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: Creates PDF for selected transaction for Portal User.
 */

public with sharing class PortalTransactionPdfGenerator {
    public List<User> currUser;
    String AccId;
    public String pageContent {get; set; }
    String defaultPdfPage ='TransactionPDFGenerator';
    String txId;
    Transaction__c controllerTx;
    String InvoicePDFPrefix {get; set; }
    Boolean pdfPageOutSidePackage = false;

    public PortalTransactionPdfGenerator(ApexPages.StandardController controller){
        String id = ApexPages.currentPage().getParameters().get('id');

        controllerTx = (Transaction__c) controller.getRecord();
        currUser =new List<User>([select Id, ContactId, Contact.AccountId from User where id=: Userinfo.getUserId()]);
        if(id != null)
        {
            txId = id;
        }
        else
        {
            txId = controllerTx.Id;
        }
        if(currUser.get(0).Contact != null)
            AccId = currUser.get(0).Contact.AccountId;

        
        //not a protal user
        if(AccId == null) {
            Transaction__c txobj= [Select Account__c from Transaction__c where id=:txId];
            AccId = txobj.Account__c;
        }
        
        SysConfig_Singlec__c sysConfObj = new  SysConfig_Singlec__c();
        if(ConfigUtilController.getSystemConfigObj() !=null) {
            sysConfObj = ConfigUtilController.getSystemConfigObj();
            if(sysConfObj.Contact_Customer_Pdf__c !=null && sysConfObj.Contact_Customer_Pdf__c !='') {
                defaultPdfPage = sysConfObj.Contact_Customer_Pdf__c;
                
                pdfPageOutSidePackage =true;
            }
            if(sysConfObj.Invoice_PDF_Url_Prefix__c  !=null && sysConfObj.Invoice_PDF_Url_Prefix__c !='') {
                InvoicePDFPrefix = sysConfObj.Invoice_PDF_Url_Prefix__c;
            }
        }

    }
    // method is used to get transaction pdf
    // return type : PageReference
    public PageReference getTxPdf(){
        String strTrans ='Transaction__c';
        String listviewName =AkritivConstants.CONST_SYSTEM_PORTAL_PDF;
        String selectedListView ='default';
        String packageHostAddress='';
        String baseUrl='';
        List<Custom_List_View__c> listObj = new  List<Custom_List_View__c>();
        listObj = [select Name,Object_Fields__c from Custom_List_View__c where Is_Global__c =true  and  ListView_For_Object__c =: strTrans and name =:listviewName Limit 1  ];
        if(listObj.size()>0)
            selectedListView = listObj[0].Id;

        if(ApexPages.currentPage().getHeaders().get('host') !=null)
            packageHostAddress = ApexPages.currentPage().getHeaders().get('host');

        if(ApexPages.currentPage().getHeaders().get('host') !=null)
            packageHostAddress = ApexPages.currentPage().getHeaders().get('host');

        if(pdfPageOutSidePackage ==true) {
            baseUrl = InvoicePDFPrefix;
        }
        else{
            baseUrl = packageHostAddress;
        }
        if(baseUrl !=null && baseUrl.trim()!='' && defaultPdfPage !=null && defaultPdfPage.trim() !='') {
            return new Pagereference('https://' + baseUrl + '/apex/' + defaultPdfPage + '?listview=' + selectedListView + '&accId='+  AccId + '&txid='+  txId + '&showDetails=true');
        }else{
            return null;
        }
    }

}