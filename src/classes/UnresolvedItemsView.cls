public class UnresolvedItemsView{
    
    public UnresolvedItemsView(ApexPages.StandardController controller){
    
    }
    
    public List<Task> getUnresolvedTasksList(){
        List<Task> taskList = new List<Task>();
        taskList = [Select Subject,Status,Type From Task where Subject LIKE 'Unresolved Email%']; 
        return taskList;
    }
}