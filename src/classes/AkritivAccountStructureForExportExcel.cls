/**
* To adapt this to anouther Object simply search for "Change" to go to the places 
* where the sObject and query must be changed
*/
public with sharing class AkritivAccountStructureForExportExcel{

    //Declare variables
    public String currentId;
    public Transient List<List<ObjectStructureMap>> allAsm;
    public Transient Map<String, ObjectStructureMap> masm;      
    private Transient String commaSeperatedFields{get;set;}
    public Transient List<TreeFieldConfiguration__c> treeConfigList{get;set;}
    public Transient List<totalfieldWrapper> totalOfFields1{get;set;}
    private final integer listLimit;

    /* Contructor
    */
    public AkritivAccountStructureForExportExcel() {
        listLimit = 999;
        totalOfFields1 = new List<totalfieldWrapper>();
        totalOfFields1.clear();
        treeConfigList = [select Name,CustomField__c,isAverage__c,isCount__c,isMax__c,Order__c,isMin__c, Summary_Field__c  from TreeFieldConfiguration__c order by Order__c limit 100];
        if(treeConfigList.size()>0){
        for(TreeFieldConfiguration__c tfc : treeConfigList){
            if(commaSeperatedFields == null){
                commaSeperatedFields = tfc.CustomField__c;
            }else{
                commaSeperatedFields =  commaSeperatedFields + ',' + tfc.CustomField__c;
            }
        }
       }else{
           commaSeperatedFields =  'Total_AR__c';
       }
        this.allAsm = new List<List<ObjectStructureMap>>{};
        this.masm = new Map<String, ObjectStructureMap>{};

        if ( currentId == null ) {
            currentId = System.currentPageReference().getParameters().get( 'id' );
        }
        
        System.assertNotEquals( currentId, null, 'sObject ID must be provided' );
    }

    /**
    * Allow page to set the current ID
    */
    public void setcurrentId( String cid ){
        currentId = cid;
    }   
 
    public List<List<ObjectStructureMap>> getObjectStructure(){              
        allAsm.clear();                    
        totalOfFields1.clear();
        allAsm = formatObjectStructure( CurrentId );
        Schema.Sobjecttype objType = Account.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        
        System.debug(' ##### all ASM Size : '+allAsm.size());
                
        Integer flag = 0;
        boolean temp = true;
        Integer j = 0;
        Double Max;
        Double MIn;
        For(List<ObjectStructureMap> asm : allAsm){
            for(ObjectStructureMap asm1 : asm){
                totalfieldWrapper tfw;
                totalfieldWrapper tfw1;
                Integer i = 0; 
                Integer blank = 0;
                j++;
                asm1.objList = new List<totalfieldWrapper>();
                String fieldType;
                for(TreeFieldConfiguration__c tfc : treeConfigList){

                    Schema.Sobjectfield field = fieldMap.get(tfc.CustomField__c);
                    fieldType = field.getDescribe().getType().name();
                    tfw = new totalfieldWrapper();
                    if(fieldType.equals('CURRENCY') || fieldType.equals('NUMBER') || fieldType.equals('DOUBLE') || fieldType.equals('PERCENT')){
                        tfw.accNumber = (Double)asm1.Account.get(tfc.CustomField__c);
                        if(tfw.accNumber == null){
                            tfw.accNumber = 0.0;
                        }
                        asm1.objList.add(tfw);
                    }else if(fieldType.equals('STRING')){
                        tfw.accStr = (String)asm1.Account.get(tfc.CustomField__c);
                        if(tfw.accStr == null){
                            tfw.accStr = '-';
                        }
                        asm1.objList.add(tfw);
                    } /*else if(fieldType.equals('PICKLIST')){
                        tfw.accStr = (String)asm1.Account.get(tfc.CustomField__c);
                        if(tfw.accStr == null){
                            tfw.accStr = '-';
                        }
                        asm1.objList.add(tfw);
                    }else if(fieldType.equals('BOOLEAN')){
                        tfw.accboolean= String.ValueOf(asm1.Account.get(tfc.CustomField__c));
                        if(tfw.accboolean== null){
                            tfw.accboolean= 'false';
                        }
                        asm1.objList.add(tfw);
                    }else if(fieldType.equals('DATETIME')){
                        tfw.accStr = String.valueOf(asm1.Account.get(tfc.CustomField__c));
                        if(tfw.accStr == null){
                            tfw.accStr= '-';
                        }
                        asm1.objList.add(tfw);
                    }else if(fieldType.equals('DATE')){
                        tfw.accStr = String.valueOf(asm1.Account.get(tfc.CustomField__c));                   
                        if(tfw.accStr == null){
                            tfw.accStr= '-';
                        }else{
                            Date dt = date.valueOf(tfw.accStr);
                            tfw.accStr = dt.format();                        
                        }
                        asm1.objList.add(tfw);
                    }*/
                                                    
                      tfw1 = new totalfieldWrapper(tfw);
                    
                    
                    if(tfc.Summary_Field__c == true){
                        if(flag > 0){
    
                            temp = false;
                            if(fieldType.equals('STRING')  ){
                                tfw1.accStr = '-';
                                totalOfFields1.set(i,tfw1);
                            }
                            else{
                                
                                    tfw1.accNumber =  (Double)tfw1.accNumber + (Double)totalOfFields1.get(i).accNumber;
                                    totalOfFields1.set(i,tfw1);
                                
                            }                          
                        }else{
                            if(fieldType.equals('STRING')){
                                tfw1.accStr = '-';
                                totalOfFields1.add(tfw1);
                            }
                            else{                            
                                totalOfFields1.add(tfw1);                           
                            }                           
                        }
                    }
                    else{
                        if(flag > 0){
                                tfw1.accStr = '-';
                                totalOfFields1.set(blank,tfw1);
                        }else{
                            if(fieldType.equals('STRING')){
                                tfw1.accStr = '-';
                                totalOfFields1.add(tfw1);
                            }
                            else{
                                totalOfFields1.add(tfw1);
                            }
                        }
                    }                          
    
                    i++;
                    blank++;
                 }       
                 flag++;             
            }
        }
        return allAsm;
    }
  
    /**
    * Query Account from top down to build the ObjectStructureMap
    * @param currentId
    * @return asm
    */
    public List<List<ObjectStructureMap>> formatObjectStructure( String currentId ){
        Transient  List<List<ObjectStructureMap>> asm = new List<List<ObjectStructureMap>>{};
        Transient  List<ObjectStructureMap> osmTemp = new List<ObjectStructureMap>();        
        masm.clear();
     
        List<ID> currentParent      = new List<ID>{};
        Map<ID, String> nodeList    = new Map<ID, String>{};
        List<String> nodeSortList   = new List<String>{};
        String nodeId               = '0';
        Integer level               = 0;
        Boolean endOfStructure      = false;
        Integer counter = 0;
        Integer loopCount = 0;
        //Find highest level obejct in the structure
        currentParent.add( GetTopElement(currentId) );         
        Integer i = 0;
        String query;
        while(!endOfStructure){
            
             if(UtilityController.isMultiCurrency()){
           if( level == 0 ){
                query = 'SELECT ParentId, CurrencyIsoCode, Name, Id,'+String.escapeSingleQuotes(commaSeperatedFields)+' FROM Account WHERE id IN : CurrentParent ORDER BY Name ASC';
            } 
            else {
                query = 'SELECT ParentId,CurrencyIsoCode, Name, Id,'+String.escapeSingleQuotes(commaSeperatedFields)+' FROM Account WHERE ParentID IN : CurrentParent ORDER BY Name ASC';
             }
          }else{
            if( level == 0 ){
                query = 'SELECT ParentId, Name, Id,'+String.escapeSingleQuotes(commaSeperatedFields)+' FROM Account WHERE id IN : CurrentParent ORDER BY Name ASC';
            } 
            else {
                query = 'SELECT ParentId, Name, Id,'+String.escapeSingleQuotes(commaSeperatedFields)+' FROM Account WHERE ParentID IN : CurrentParent ORDER BY Name ASC';
             }            
           }                              
                     
            i = 0;
            List<Account> listOfAcc = Database.query(query);
            System.debug(' Account list size : '+listOfAcc.size());
            for(Account a : listOfAcc){
                    if(i < 10){
                        nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.00000'+String.valueOf( i ) : String.valueOf( i );
                    }
                    else if(i < 100){
                        nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.0000'+String.valueOf( i ) : String.valueOf( i );
                    }
                    else if(i < 1000){
                        nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.000'+String.valueOf( i ) : String.valueOf( i );
                    }
                    else{
                        nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.00'+String.valueOf( i ) : String.valueOf( i );
                    }
                    masm.put( NodeID, new ObjectStructureMap( nodeID, false, a ) );
                    if(i==0) currentParent.clear();
                    currentParent.add( a.id );
                    nodeList.put( a.id,nodeId );    
                    i++;
                    nodeSortList.add( nodeId );
            }
            if(i==0){ 
                endOfStructure = true;
            }
            else{
                level++;
            }
        }                            
        
        //Account structure must now be formatted
        NodeSortList.sort();
        i = 0;
        for(i = 0; i < NodeSortList.size(); i++){
  
            List<String> cnl = new List<String> {};      
            String cn   = NodeSortList[i];
            cnl         = cn.split( '\\.', -1 );

            ObjectStructureMap tasm = masm.get( cn );            
            if ( tasm.account.id == currentId ) {
                tasm.currentNode = true;
            }            
 
           if(counter < listLimit){
               osmTemp.add(tasm);
               counter++;
           }else{
               loopCount++;
               asm.add(osmTemp);
               osmTemp = new List<ObjectStructureMap>{};              
               osmTemp.add(tasm);
               counter = 0;
           }
        }
        if(counter > 0){
            loopCount++;
            asm.add(osmTemp);
        }
        return asm;
    }                
    
    /**
    * Find the tom most element in Heirarchy  
    * @return objId
    */
    public String GetTopElement( String objId ){
      
        Boolean top = false;
        while ( !top ) {
            //Change below
           Account a = [ Select a.Id, a.ParentId From Account a where a.Id =: objId limit 1 ];
          
            if ( a.ParentID != null ) {
                objId = a.ParentID;
            }
            else {
                top = true;
            }
        }
        return objId ;
    }
    
    /**
    * Wrapper class
    */
     public with sharing class totalfieldWrapper
    {
        public transient String accStr{get;set;}
        public transient Double accNumber{get;set;}
        public transient String currencyIsoCode{get;set;}
        public transient String accboolean{get;set;}
       
        public totalfieldWrapper(){
            accStr = null;
            accNumber = null;
            currencyIsoCode = null;
            accboolean= null;
            
        }
        public totalfieldWrapper(totalfieldWrapper tw){
            this.accStr = tw.accStr;
            this.accNumber = tw.accNumber;
            this.currencyIsoCode = tw.currencyIsoCode;
            this.accboolean= tw.accboolean;
            
        }
    }
    public with sharing class ObjectStructureMap{

        public transient String nodeId;
        public transient String parentId;        
        public transient Boolean currentNode;
        Public transient List<totalfieldWrapper> objList {get;set;}       
        /**
        * @Change this to your sObject
        */
        Public ObjectStructureMap(){
              objList = new List<totalfieldWrapper>(); 
        } 
        
        public Account account;        
        public String getnodeId() { return nodeId; }
        public String getParentId(){ return parentId;}       
        public Boolean getcurrentNode() { return currentNode; }
        public Account getaccount() { return account; }        
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }
        public void setParentId(String p){ this.parentId = p;}        
        public void setaccount( Account a ) { this.account = a; }

        public ObjectStructureMap( String nodeId, Boolean currentNode, Account a ){            
            this.nodeId         = nodeId;            
            this.currentNode    = currentNode;
            this.account = a;
            this.parentId = a.ParentId;
        }
    }
    
    public PageReference exportToExcel(){
        System.debug('##### EXPORT TO EXCEL method is called...!!!');
        PageReference pr = new PageReference('/apex/ExportAkritivAccountHierarchyPageNew');
        return pr;
    } 
}