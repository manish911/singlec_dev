global class FaxFeedBackListener implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          
          String myPlainText = '';
          String messagId= '';
          try
          {     
                
                XMLDom xmld = new XMLDom(email.plainTextBody);
                
                XMLDom.Element txid = xmld.getElementByTagName('TransactionID'); 
                
                
                
                
                String  transactionId = txid.getValue('TransactionID');
                System.debug('------MessageId ' + transactionId );
                XMLDom.Element statusel = xmld.getElementByTagName('Status'); 
                
                String status = statusel.getValue('Status');
                System.debug('------Status ' + status );
                
                List<Task> tsk = [select Id,Status,CallDisposition from Task where CallDisposition =: transactionId  and Status = 'Sending FAX'];
                system.debug('-----tsk ------>'+tsk.size());
                if(tsk.size() > 0){
                 
                    Task t = tsk.get(0);   
                    if ( '0'.equals( status ) ) {
                    
                            t.status = 'FAX Sent';
                    } else {
                    
                            t.status = 'Fax Failed';
                    
                    }
                    
                    //==========================================================================================  
                    list<String> lstFields = new list<String>{'status'};
                       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
                       for (String fieldToCheck : lstFields) {
            
                          // Check if the user has create access on the each field
                            if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                             if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
            
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                        'Insufficient access'));
                               return null;
                              }
                            }
                        }
                    //========================================================================================
                    
                    update t;
                 }   
                
               
          }
          catch (System.StringException e)
          {
               myPlainText = email.plainTextBody;
               System.debug('========' + e);
          }
          return result;
      }
      @isTest
      public static void test1(){
            FaxFeedBackListener ffl = new FaxFeedBackListener();
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Task objTask = new Task();
            objTask.CallDisposition = '221969703';
            objTask.Subject = 'Call';
            objTask.Priority = 'Normal';
            objTask.Status = 'Sending Fax';
            insert objTask;
            
            email.plainTextBody  = '<?xml version="1.0" encoding="utf-8" ?>'+
'<OutgoingFax version="2.0">'+
'<TransactionID>221969703</TransactionID>'+
'<RemoteCSID>12673817259</RemoteCSID>'+
'<Pages>2</Pages>'+
'<Status>0</Status>'+
'<Subject></Subject>'+
'<FaxNumber>0012673817259</FaxNumber>'+
'<Duration>74</Duration>'+
'<SubmitTime>2011/07/26 03:52:38</SubmitTime>'+
'<CompletionTime>2011/07/26 03:53:01</CompletionTime>'+
'<Contact></Contact>'+
'</OutgoingFax>';
          
          System.assertNotEquals('',email.plainTextBody);
          
          Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
          ffl.handleInboundEmail(email,envelope);
      }
      
  }