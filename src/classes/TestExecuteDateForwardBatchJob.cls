/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for ExecuteDateForwardBatchJob
 */
@isTest
private class TestExecuteDateForwardBatchJob {

	static testMethod void runTest() {
		Account acc = DataGenerator.accountInsert();

		List<Transaction__c> txList = new List<Transaction__c>();
		txList = DataGenerator.transactionInsert(acc);

		Test.startTest();

		DateForwardBatchJob dateForwardJob = new DateForwardBatchJob();
		String query = 'select Account__c, Batch_Number__c, Business_Unit__c, Create_Date__c, Close_Date__c, Due_Date__c from Transaction__c t where Account__c != null LIMIT 200';
		dateForwardJob.query = query;
		dateForwardJob.daysToForward = 1;

		ID batchprocessid = Database.executeBatch(dateForwardJob);
		
		System.assertNotEquals(null, batchprocessid);

		dateForwardJob.execute(null,(List<SObject>)txList);
		dateForwardJob.finish(null);

		System.assertEquals(dateForwardJob.daysToForward, 1);
		System.assertEquals(dateForwardJob.query, query);

		Test.stopTest();
	}
}