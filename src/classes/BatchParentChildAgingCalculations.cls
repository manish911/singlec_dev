/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for calculating Batch aging values.
 */
global class BatchParentChildAgingCalculations implements Database.Batchable<SObject>, Database.Stateful {

    Set<Id> accountIds;
    List<Account> lstAccount;
    Double age0;
    Double age1;
    Double age2;
    Double age3;
    Double age4;
    Double age5;

    Double BaseTotalAR = 0.0000;
    Double BaseTotalPastDue = 0.0000;
    
    Double PCAge0;
    Double PCAge1;
    Double PCAge2;
    Double PCAge3;
    Double PCAge4;
    Double PCAge5;

    Double PCBaseTotalAR = 0.0000;
    Double PCBaseTotalPastDue = 0.0000;
    
    List<Account> lstAccount1;
    List<Account> lstAccount2;
    
    Account objAccount1;
    Account objAccount2;
    //ParentId =null
       String sQuery ='';

   
       global database.querylocator start(Database.BatchableContext bc){        
       sQuery = 'Select Id, Age0__c, Age1__c, Age2__c, Age3__c, Age4__c, Age5__c,Base_Total_AR__c,Base_Total_Past_Due__c ,' ;
       sQuery = sQuery + '  PC_Current__c,PC_1_30__c, PC_31_60__c, PC_61_90__c, PC_91_180__c, PC_Over_180__c,';
       sQuery = sQuery + '  PC_Total_AR_Local__c,PC_Total_Past_Due_Local__c ';
       sQuery = sQuery + 'from Account ' ;
     

        system.debug('GetQuery' + sQuery );
        return Database.getQueryLocator(sQuery);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
        lstAccount = new List<Account>(); 
        accountIds = new Set<Id>();  
          
        for(Sobject accSobject : sObjs) { 
            
            system.debug('Account' + accSobject );
            age0 = 0.0000;
            age1 = 0.0000;
            age2 = 0.0000;
            age3 = 0.0000;
            age4 = 0.0000;
            age5 = 0.0000;   
        
            BaseTotalAR = 0.0000;
            BaseTotalPastDue = 0.0000;
            
            PCAge0 = 0.0000;
            PCAge1 = 0.0000;
            PCAge2 = 0.0000;
            PCAge3 = 0.0000;
            PCAge4 = 0.0000;
            PCAge5 = 0.0000;
          
            PCBaseTotalAR = 0.0000;    
            PCBaseTotalPastDue = 0.0000;
           
            
            Account acctemp = (Account)accSobject;
            
            AggregateResult[] results = getChildAccountAgging(acctemp.Id);
            
            system.debug('---------------Result----------------'+results);
            
            
            if (results != null && results.size() > 0){
            
            
                for(AggregateResult ar : results) {
                  
                    
             age0 = acctemp.Age0__c!= null ? acctemp.Age0__c : 0.0000;                                                           
                if((Double)ar.get('expr0')!= null){
                  if(PCAge0 == 0)
                       { 
                        PCAge0 = PCAge0 +  age0 + (Double)ar.get('expr0');
                       }
                    else
                         {
                      PCAge0 = PCAge0 + (Double)ar.get('expr0');
                          }
                     }
                                         
                 age1 = acctemp.Age1__c!= null ? acctemp.Age1__c : 0.0000;
                 
                    if((Double)ar.get('expr1')!= null){
                  if(  PCAge1 == 0)
                   {
                    PCAge1  =  PCAge1   +  age1 + (Double)ar.get('expr1');
                   } 
                      else
                      {
                      PCAge1  =  PCAge1   + (Double)ar.get('expr1');
                      }
                   
                   }
                   

                   
                    age2 = acctemp.Age2__c != null ? acctemp.Age2__c : 0.0000;
                                        if((Double)ar.get('expr2')!= null){
                                           if(  PCAge2 == 0) {
                  PCAge2 = PCAge2 + age2 + (Double)ar.get('expr2'); }else
                      {
                      PCAge2  =  PCAge2   + (Double)ar.get('expr2');
                      } }
                    
                                                   
                    
                    age3 = acctemp.Age3__c != null ? acctemp.Age3__c :0.0000;
                                                            if((Double)ar.get('expr3')!= null){
                                     if(  PCAge3 == 0) {
                        
                     PCAge3 = PCAge3   + age3 +  (Double)ar.get('expr3');}
                     else {               
                          PCAge3 =  PCAge3  + (Double)ar.get('expr3');
                              }}
                    
                    
                    age4 = acctemp.Age4__c != null ? acctemp.Age4__c : 0.0000;
                                    if((Double)ar.get('expr4')!= null){
                                    if(PCAge4 == 0) {
                     PCAge4 = PCAge4 + age4  + (Double)ar.get('expr4'); } else 
                     
                     {  PCAge4  =  PCAge4   + (Double)ar.get('expr4');
                        }}
                    
                    
                    age5 = acctemp.Age5__c != null ? acctemp.Age5__c : 0.0000;
                    if((Double)ar.get('expr5')!= null){
                    if(PCAge5 == 0){      
                    PCAge5 = PCAge5 + age5 +  (Double)ar.get('expr5'); } 
                    else 
                    {  PCAge5  =  PCAge5  + (Double)ar.get('expr5');
                      }}
                    
                    BaseTotalAR = acctemp.Base_Total_AR__c!= null ? acctemp.Base_Total_AR__c : 0.0000;
                 
                    if((Double)ar.get('expr7')!= null){
                    if(  PCBaseTotalAR == 0)
                   {
                    PCBaseTotalAR  =  PCBaseTotalAR  +  BaseTotalAR + (Double)ar.get('expr6');
                   } 
                      else
                      {
                      PCBaseTotalAR  =  PCBaseTotalAR   + (Double)ar.get('expr6');
                      }
                   
                   }
                   
                   BaseTotalPastDue = acctemp.Base_Total_Past_Due__c!= null ? acctemp.Base_Total_Past_Due__c : 0.0000;
                 
                    if((Double)ar.get('expr7')!= null){
                  if(  PCBaseTotalPastDue == 0)
                   {
                    PCBaseTotalPastDue  =  PCBaseTotalPastDue  +  BaseTotalPastDue + (Double)ar.get('expr7');
                   } 
                      else
                      {
                      PCBaseTotalPastDue =  PCBaseTotalPastDue   + (Double)ar.get('expr7');
                      }
                   
                   }
                
                }
                acctemp.PC_Current__c=PCAge0 != null ?PCAge0 : 0.0;
                acctemp.PC_1_30__c = PCAge1 != null ?PCAge1 : 0.0;
                acctemp.PC_31_60__c = PCAge2 != null ?PCAge2 : 0.0;
                acctemp.PC_61_90__c = PCAge3 != null ?PCAge3 : 0.0;
                acctemp.PC_91_180__c = PCAge4 != null ?PCAge4 : 0.0;
                acctemp.PC_Over_180__c = PCAge5 != null ?PCAge5 : 0.0;
                   
                acctemp.PC_Total_AR_Local__c = PCBaseTotalAR != null ?PCBaseTotalAR : 0.0;
                acctemp.PC_Total_Past_Due_Local__c =PCBaseTotalPastDue != null ?PCBaseTotalPastDue : 0.0;
               
            }
            
            else
            
            {
               acctemp.PC_Current__c=0.0;
                acctemp.PC_1_30__c = 0.0;
                acctemp.PC_31_60__c = 0.0;
                acctemp.PC_61_90__c = 0.0;
                acctemp.PC_91_180__c = 0.0;
                acctemp.PC_Over_180__c = 0.0;
                   
                acctemp.PC_Total_AR_Local__c = 0.0;
                acctemp.PC_Total_Past_Due_Local__c =0.0;

            }
                                    
            lstAccount.add(acctemp);
        }
        
        update lstAccount;
    } 
    private AggregateResult[] getChildAccountAgging(Id accId) {
    
      
   
         AggregateResult[] groupedResults = [select ParentId,SUM(Age0__c),SUM(Age1__c),SUM(Age2__c),SUM(Age3__c),
                                           SUM(Age4__c),SUM(Age5__c), SUM(Base_Total_AR__c),SUM(Base_Total_Past_Due__c)
                                           From Account Where ( ParentId = : accid and Base_Total_AR__c !=null and Base_Total_Past_Due__c !=null) group by ParentId ];                               
                                           
                                                                                              
                                          
        return groupedResults;
    }

    global void finish(Database.BatchableContext bc){
               
    }
}