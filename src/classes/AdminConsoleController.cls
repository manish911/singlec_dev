/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is used for Displaying Links in the admin panel.
 */
public with sharing class AdminConsoleController {
    // Runs the test method for this class and generates average code coverage
    
   
    public String invoicetemplate {
        get {
            return UtilityController.getUrlForInvoiceTemplates();
        } 
        set; 
    }
    
    public String TrxObjUrl {
        get {
            return UtilityController.getUrlForTransactionObject();
        } 
        set; 
    }
    
    public String DisObjUrl {
        get {
            return UtilityController.getUrlForDisputeObject();
        } 
        set; 
    }
    
    public String LineItemObjUrl {
        get {
            return UtilityController.getUrlForLineItemObject();
        } 
        set; 
    }
   
   
    
}