/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   The class provide the functionality to send messages
 *             to internal users of the system.
 */
public with sharing class QuickNoteController {
    public List<Quick_Note__c> wallMessages {get; set; }
    public List<UserWallMsgWrapper> wallWrapperMsgs {get; set; }
    public Id currentUserId {get; set; }
    public Quick_Note__c newOrReplyMsg {get; set; }
    public Boolean showMasseges {get; set; }
    public Boolean isReplyMsg {get; set; }
    public String[] selectedToUserIds {get; set; }
    public String objectIds;
    public String objectType;
    public Integer totalUnreadMessage {get; set; }

    // default constructor for the class
    public QuickNoteController() {
        objectIds =Apexpages.currentPage().getParameters().get('objIds');
        objectType =Apexpages.currentPage().getParameters().get('objtype');
        if(objectType != null)
            objectType = String.escapeSingleQuotes(objectType);

        selectedToUserIds = new String[] {};
        showMasseges = true;
        currentUserId = UserInfo.getUserId();
        createWallMessagesWrapperList();
    }

    // will get the latest messages from the system
    // return type : void 
    public void createWallMessagesWrapperList() {
        totalUnreadMessage = 0;
        wallMessages = [select Id, From_User__c, From_User__r.Name, isVisited__c, To_User__c,To_User__r.Name, Related_Objects__c, Related_Object_Type__c, Message_Body__c, isReply__c, Add_To_Note__c, CreatedDate from Quick_Note__c
                        where (To_User__c=:currentUserId) or (From_User__c=:currentUserId and isReply__c = true)
                        order by createdDate desc limit 10];

        wallWrapperMsgs = new List<UserWallMsgWrapper>();
        for(Quick_Note__c wm : wallMessages) {
            if(wm.isVisited__c == false)
                totalUnreadMessage += 1;

            wallWrapperMsgs.add(new UserWallMsgWrapper(wm));
        }
    }

    public String replyToPageId {get; set; }
    // function to prepare the reply message to an user
    // return type : Pagereference 
    public Pagereference prepareReplyMessage() {
        Integer i = getRepeatIndexFromPageId(replyToPageId);
        UserWallMsgWrapper wallMsg = wallWrapperMsgs.get(i);
        isReplyMsg = true;
        showMasseges = false;

        newOrReplyMsg = new Quick_Note__c();
        newOrReplyMsg.From_User__c = currentUserId;
        newOrReplyMsg.To_User__c = wallMsg.fromUserId;

        selectedToUserIds.clear();
        selectedToUserIds.add(wallMsg.fromUserId);

        newOrReplyMsg.isReply__c = true;
        newOrReplyMsg.Related_Object_Type__c = wallMsg.relatedObjType;
        newOrReplyMsg.Related_Objects__c = wallMsg.relatedObjectsStr;
        return null;
    }

    public String markMsgVisitedPageId {get; set; }
    // function to mark the messages as visted
    // return type : Pagereference
    public Pagereference markMessageVisited() {
        Integer i = getRepeatIndexFromPageId(markMsgVisitedPageId);
        UserWallMsgWrapper wallMsg = wallWrapperMsgs.get(i);

        Quick_Note__c m = new Quick_Note__c(id=wallMsg.msgId);
        m.isVisited__c = true;
        //==========================================================================================  
        list<String> lstFields = new list<String>{'isVisited__c'};
           Map<String,Schema.SObjectField> m1 = Schema.SObjectType.Quick_Note__c.fields.getMap();
           for (String fieldToCheck : lstFields) {

              // Check if the user has create access on the each field
                if(!m1.get(fieldToCheck).getDescribe().isCalculated()){
                 if (!m1.get(fieldToCheck).getDescribe().isUpdateable()) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                   return null;
                  }
                }
            }
        //========================================================================================
        update m;

        return null;
    }

    // function to prepare a new message which will further be send to an user
    // return type : Pagereference
    public Pagereference prepareNewMessage() {
        isReplyMsg = false;
        showMasseges = false;
        selectedToUserIds.clear();
        newOrReplyMsg = new Quick_Note__c();
        newOrReplyMsg.From_User__c = currentUserId;
        return null;
    }

    // insert and save the reply or new message to user(s)
    // return type : Pagereference
    public Pagereference createWallMessage() {
        List<Quick_Note__c> msgLst = new List<Quick_Note__c>();
        List<Note> notesList = new List<Note>();

        // if there is multiple to users are selected
        for(String toUser : selectedToUserIds) {
            if(newOrReplyMsg != null) {
                Quick_Note__c m = new Quick_Note__c();
                m.From_User__c = currentUserId;
                m.To_User__c = toUser;
                m.isReply__c = newOrReplyMsg.isReply__c;
                m.Related_Object_Type__c = newOrReplyMsg.Related_Object_Type__c;
                m.Related_Objects__c = newOrReplyMsg.Related_Objects__c;
                m.message_body__c = newOrReplyMsg.message_body__c;
                msgLst.add(m);
            }
        }

        if(newOrReplyMsg.Add_To_Note__c && newOrReplyMsg.Related_Objects__c != null) {
            for(String relatedObjId : newOrReplyMsg.Related_Objects__c.split(',')) {
                if(relatedObjId != null && relatedObjId != '') {
                    Note n = new Note();
                    String title= AkritivConstants.NOTE_QUICKNOTE + (newOrReplyMsg.message_body__c!= null ? (newOrReplyMsg.message_body__c.length()>75 ? newOrReplyMsg.message_body__c.substring(0,70) : newOrReplyMsg.message_body__c.substring(0,newOrReplyMsg.message_body__c.length())) : '');
                    n.Title = title+'...';
                    n.ParentId = relatedObjId;
                    n.Body = newOrReplyMsg.message_body__c;
                    notesList.add(n);
                }
            }
        }

        if(msgLst.size()>0){
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Quick_Note__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert msgLst;
            }

        if(notesList.size()>0){
         //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert notesList;
        }
        createWallMessagesWrapperList();
        showMasseges = true;
        return null;
    }

    //@TODO Birju
    // method is used to get Quick Note Title Msg
    // return type : String
    public String getQuickNoteTitleMsg() {
        if(isReplyMsg == true) {
            return 'Reply Message';
        }
        else {
            return 'New Message';
        }

    } //end 

    public String addTaskPageId {get; set; }
    // function to add the message as a task.
    // return type : Pagereference
    public Pagereference addMessageTask() {
        Integer i = getRepeatIndexFromPageId(addTaskPageId);
        UserWallMsgWrapper wallMsg = wallWrapperMsgs.get(i);

        List<Task> tasksToCreate = new List<Task>();

        String subject = '';
        if( wallMsg.msgBody != null && wallMsg.msgBody.length()>75)
            subject = wallMsg.msgBody.substring(0,75);
        else
            subject = wallMsg.msgBody;

        if(wallMsg.relatedObjList != null && wallMsg.relatedObjList.size()>0) {
            for(RelatedObject relObj : wallMsg.relatedObjList) {
                Task t = new Task();
                t.Subject = AkritivConstants.NOTE_QUICKNOTE + subject;
                t.OwnerId = currentUserId;
                t.ActivityDate = Date.today()+1;
                t.WhatId = relObj.objId;
                tasksToCreate.add(t);
            }
        }
        else {
            Task t = new Task();
            t.Subject = AkritivConstants.NOTE_QUICKNOTE + subject;
            t.OwnerId = currentUserId;
            t.ActivityDate = Date.today()+1;
            tasksToCreate.add(t);
        }

    
        if(tasksToCreate.size()>0){
            //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
            insert tasksToCreate;
            }

        return null;
    }

    public String msgPageToDeleteId {get; set; }
    // function to delete a message from user's wall
    // return type : Pagereference
    public Pagereference deleteWallMessage() {
        Integer i = getRepeatIndexFromPageId(msgPageToDeleteId);

        Id msgId = wallWrapperMsgs.get(i).msgId;
        if(msgId != null){
         
         //===================================================================================
                if (!Quick_Note__c.sObjectType.getDescribe().isDeletable()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                    'Insufficient access'));
                            return null;
                  }
         //===================================================================================
             
            delete (new Quick_Note__c(id=msgId));
        }
        wallWrapperMsgs.remove(i);
        if(totalUnreadMessage>0)
            totalUnreadMessage -= 1;
        return null;
    }

    // property used to get all the users and to show them on the page as select list.
    // return type : List<Selectoption>
    public List<Selectoption> getAllUsers() {
        List<Selectoption> users = new List<Selectoption>();
        for(User u : [select id, Name from user where Name != null order by Name])
            users.add(new Selectoption(u.id,u.name));

        return users;
    }

    // makes a cancel operation
    // return type : Pagereference
    public Pagereference cancel() {
        showMasseges = true;
        return null;
    }

    // get the repeat index of element's id on the page to identify which message to reply,to add task, to delete or to mark it is read
    // return type : Integer
    public Integer getRepeatIndexFromPageId(String pgId) {
        String[] tokens = pgId.split(':');
        return Integer.valueOf(tokens[tokens.size()-2]);
    }

    // refresh the message window
    // method is called to get check the new message arrived in the system.
    // return type : Pagereference
    public Pagereference getMessagesRefreshed() {
        createWallMessagesWrapperList();
        return null;
    }

    //------------------------------------------------------------------------------------------------
    //  CODE WILL BE CALLED FROM IM BUTTON ON DETAIL PAGES
    //------------------------------------------------------------------------------------------------
    public List<RelatedObject> imRelatedObjects {get; set; }
    // method is used to prepare Object IM
    // return type : Pagereference
    public Pagereference prepareObjectIM() {
        newOrReplyMsg = new Quick_Note__c();
        newOrReplyMsg.From_User__c = currentUserId;

        if(objectType!= null && objectIds != null) {
            newOrReplyMsg.Related_Objects__c = objectIds;
            newOrReplyMsg.Related_Object_Type__c = objectType;

            String[] relatedObjIds = objectIds.split(',');
            imRelatedObjects = new List<RelatedObject>();

            String queryStr;
            if('Dispute__c'.equalsIgnoreCase(objectType) || 'Transaction__c'.equalsIgnoreCase(objectType)) {
                if('Dispute__c'.equalsIgnoreCase(objectType))
                    queryStr = 'Select Id,Name, Account__r.OwnerId from Dispute__c where id in : relatedObjIds';
                else
                    queryStr = 'Select Id,Name, Account__r.OwnerId from Transaction__c where id in : relatedObjIds';
            }
            else if('Account'.equalsIgnoreCase(objectType))
                queryStr = 'Select Id,Name, OwnerId from Account where id in : relatedObjIds';

            for(Sobject s : Database.query(queryStr)) {
                imRelatedObjects.add(new RelatedObject(s));

                if('Dispute__c'.equalsIgnoreCase(objectType) || 'Transaction__c'.equalsIgnoreCase(objectType))
                    newOrReplyMsg.To_User__c = String.valueOf(s.getSObject('Account__r').get('OwnerId'));

                else if('Account'.equalsIgnoreCase(objectType))
                    newOrReplyMsg.To_User__c = String.valueOf(s.get('OwnerId'));
            }
            selectedToUserIds.clear();
            selectedToUserIds.add(newOrReplyMsg.To_User__c);
        }
        return null;
    }
    // method is used to save IM Message
    // return type : Pagereference
    public Pagereference saveIMMessage() {
        createWallMessage();

        return null;
    }
    //------------------------------------------------------------------------------------------------
    //  WRAPPER CLASSES
    //------------------------------------------------------------------------------------------------
    class UserWallMsgWrapper {
        public String msgId {get; set; }
        public String fromUserId {get; set; }
        public String fromUserName {get; set; }
        public String replyToUserId {get; set; }
        public String replyToUserName {get; set; }
        public String msgBody {get; set; }
        public Boolean isReply {get; set; }
        public List<RelatedObject> relatedObjList {get; set; }
        public String relatedObjectsStr;
        public String relatedObjType {get; set; }
        public String createdDateStr {get; set; }
        public Boolean isVisited {get; set; }
        public UserWallMsgWrapper(Quick_Note__c msg) {
            relatedObjList = new List<RelatedObject>();
            this.msgId = msg.Id;
            this.fromUserId = msg.From_User__c;
            this.fromUserName = msg.From_User__r.Name;
            this.msgBody = msg.Message_Body__c;
            this.isReply = msg.isReply__c;
            this.relatedObjType= msg.Related_Object_Type__c;
            this.relatedObjectsStr = msg.Related_Objects__c;
            this.createdDateStr = msg.createdDate.format('hh:mm a');
            this.isVisited = msg.isVisited__c;

            if(this.fromUserId == Userinfo.getUserid() && this.isReply==true) {
                this.replyToUserId = msg.To_User__c;
                this.replyToUserName = msg.To_User__r.Name;
            }

            if(msg.Related_Objects__c!=null) {
                String[] relatedObjIds = msg.Related_Objects__c.split(',');
                String objType = String.escapeSingleQuotes(msg.Related_Object_Type__c);

                for(Sobject s : Database.query('Select Id,Name from '+objType+' where id in : relatedObjIds')) {
                    relatedObjList.add(new RelatedObject(s));
                }
            }
        }

    }

    class RelatedObject {
        public String objId {get; set; }
        public String objName {get; set; }

        public RelatedObject(Sobject s) {
            this.objId = String.valueOf(s.get('Id'));
            this.objName = String.valueOf(s.get('Name'));
        }
    }
}