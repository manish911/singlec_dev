/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is responsible for fetching the tasks of
 *        Portal Users
 */
public with sharing class PortalContactCSRTasksController {

	public String accountId {get; set; }
	public Account acc;
	List <Task> taskList;

	public PortalContactCSRTasksController(ApexPages.StandardController controller)
	{
		acc = (Account) controller.getRecord();
		accountId = acc.Id;

		taskList = [Select t.whatId,t.Subject, t.Description From Task t where t.subject like
		            '%Portal%' and t.whatId = :accountId];
		for(Task t : taskList)
		{
	
		}

	}
	// method is used to get Task List
	// return type : List<Task>
	public List<Task> getTaskList()
	{
		return taskList;
	}

}