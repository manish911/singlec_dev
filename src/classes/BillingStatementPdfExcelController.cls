/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   # Class used to generate the pdf/excel datasheet for billing statement with a configurable template
               # template will be used to generate the content of the pdf or excel
 */
public class BillingStatementPdfExcelController {
    private List<List<Object>> txListofList;
    private String txCustomViewName;
    private String accountId;
    private String contactId;
    private EmailTemplate statementPDFTemplate;
    public String renderAsPdf {get; set; }
    private List<List<Id>> txIdsListOfList;
    private GenerateTemplateOutput output;
    public String configObjStr {get; set; }
    // Default constructor for the class
    public BillingStatementPdfExcelController() {
        accountId = ApexPages.currentPage().getParameters().get('aid');
        contactId = ApexPages.currentPage().getParameters().get('cid');
        txCustomViewName = ApexPages.currentPage().getParameters().get('lvname');
        renderAsPDF = ApexPages.currentPage().getParameters().get('renderAsPdf');

        // Get list of transaction ids using the temorary object which has key equals to key given in the parameter
        configObjStr = ApexPages.currentPage().getParameters().get('txidconfigkey');
        output = new GenerateTemplateOutput();
    }

    public List<List<Id>> getTxIdsListOfList() {
        txIdsListOfList = new List<List<Id>>();
        List<Temp_Object_Holder__c> tempObjList = new List<Temp_Object_Holder__c>();
        Integer cnt = [Select count() from Temp_Object_Holder__c where key__c = :configObjStr];

        if(configObjStr != null) {
            tempObjList = [select value__c from Temp_Object_Holder__c where key__c = :configObjStr ];
        }

        if(tempObjList != null && tempObjList.size() > 0) {
            for(Temp_Object_Holder__c temp : tempObjList) {
                List<Id> txIdsList = new List<Id>();
                if(temp!= null && temp.value__c != null ) {
                    for( String txId : temp.value__c.split(',') )
                        txIdsList.add(txId);
                }
                txIdsListOfList.add(txIdsList);
            }
        }
        else {
            txIdsListOfList = null;
        }

        return txIdsListOfList;
    }
    // method will return the merged body for the configurable pdf template
    // Return Type - String 
    // No parameters are passed in this method 
    public String getStatementTemplateStr() {
        String configTemplateKeyName;
        try {
            configTemplateKeyName = (renderAsPDF=='PDF' ? ConfigUtilController.getBillingStatementPdfTemplate() : ConfigUtilController.getBillingStatementExcelTemplate());
        }
        catch(Exception e) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Message_Set_Configuration_Template +'\n'+Label.Label_Error_Colon+e.getMessage()));
            return null;
        }
        
        Map<Id,EmailTemplate> allTemplates;
        SysConfig_Singlec__c sysConfig = ConfigUtilController.getSystemConfigObj();
        if(sysConfig.is_Akritiv_Org__c){
            allTemplates = new Map<Id,EmailTemplate>([select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate WHERE Folder.Name like 'Akritiv%' order by Name]); //where TemplateType='text'
        }else{
            allTemplates = new Map<Id,EmailTemplate>([select id, Name, FolderId, Subject, HtmlValue, TemplateType, Body from EmailTemplate where Folder.Name != null order by Name ]); //where TemplateType='text'
        }
        for(Id templateId : allTemplates.keySet()) {
            EmailTemplate et = allTemplates.get(templateId);

            if(et.name == configTemplateKeyName)
                statementPDFTemplate = et;
        }

        if(statementPDFTemplate == null) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Message_Set_Configuration_Template));
            return null;
        }

        String templateBody = statementPDFTemplate.HtmlValue;
        templateBody = replaceMergeFields(templateBody);

        return templateBody;
    }

    // param  : templateStr - string to be merged
    // return : will return the input string after replacing all the merge fields
    public String replaceMergeFields(String templateStr) {
        GenerateTemplateOutput output = new GenerateTemplateOutput();
        String mergedBody = output.processTemplateBody(accountId, contactId, Userinfo.getUserId(), templateStr,'');
        mergedBody = output.processTransactionSummaryMergeField(mergedBody, txCustomViewName,  getTxIdsListOfList(), accountId,'');
        return mergedBody;
    }
}