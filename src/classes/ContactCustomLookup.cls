/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : Apex Class for contact custom lookup
 */
public with sharing class ContactCustomLookup
{
    public String FaxServiceType{get;set;} 
        
    public String contactFaxField{get;set;}
    
    //accountId for filtering the contacts
    private string accountId {get; set; }

    //Contact Ctrl ID - The selected values will be set in this control on the opener page
    public string contactCtrlID {get; set; }

    //Check for functionality the Contact lookup is used for
    private string usedFor {get; set; }

    //to, cc or bcc
    public String addressType {get; set; }

    //Email,Phone,Fax
    public String contactMethod {get; set; }

    //Search Text
    public string searchText {get; set; }

    //Decides rendering of new contact section on the contact lookup
    public boolean renderNewContactSection {get; set; }

    //Decides rendering of checkboxes column on the contact lookup
    public boolean renderCheckBoxes {get; set; }

    //Decides rendering of Contact list on the contact lookup
    public boolean renderContactList {get; set; }

    // Holds newly added contact
    public Contact newContact {get; set; }

    //custom list controller that provides pagination and sorting
    public ApexPages.StandardSetController con {get; set; }
    
    //List of contacts to be displayed with checkboxes
    public List<ContactWrapper> contactList = new List<ContactWrapper>();

    public String selectedContacts {get; set; }

    public Integer totalContactsRec {get { if ( totalContactsRec == null ) return 0;return totalContactsRec ; } set; }

    public String selectedContactIdsStr {get; set; }

    public String contactMethodAddressStr {get; set; }

    public List<SelectOption> recPerPageOptions {get; set; }

    public String recPerPage {get; set; }

    public boolean updatedDefaultValue {get; set; }

    public String contactIdToBeEdited {get; set; }

    // Holds edited contact info
    //public Contact contactToBeUpdated{get;set;}
    public String updatedContactName {get; set; }

    //Constructor
    public ContactCustomLookup(ApexPages.StandardController controller)
    {   
        contactFaxField = ConfigUtilController.getContactFaxField();
        if(contactFaxField != null){
            FaxServiceType = 'contactFaxField';
        }else{
            FaxServiceType = 'interFax';
        }
        if(ApexPages.currentPage().getParameters().get('AccountId')!=null)
            accountId = ApexPages.currentPage().getParameters().get('AccountId');
        else accountId = '';
        if(ApexPages.currentPage().getParameters().get('CtrlId')!=null)
            contactCtrlID = ApexPages.currentPage().getParameters().get('CtrlId');
        else contactCtrlID ='';
        if(ApexPages.currentPage().getParameters().get('UseFor')!=null)
            usedFor = ApexPages.currentPage().getParameters().get('UseFor');
        else usedFor ='';
        if(ApexPages.currentPage().getParameters().get('addressType')!=null)
            addressType = ApexPages.currentPage().getParameters().get('addressType');
        else
            addressType = '';
        if(ApexPages.currentPage().getParameters().get('contactMethod')!=null)
            contactMethod  = ApexPages.currentPage().getParameters().get('contactMethod');
        else
            contactMethod  = '';

        selectedContactIdsStr = '';
        selectedContactIdsStr = getSelectedContactIdsString();

        //initilize selected records per page options
        recPerPageOptions =  new  List<SelectOption>();

        //@todo this shud nt be hard coded , recperpage shud come from some property file
        SelectOption newOption1 = new SelectOption('20','20');
        SelectOption newOption2 = new SelectOption('50','50');
        SelectOption newOption3 = new SelectOption('100','100');
        SelectOption newOption4 = new SelectOption('200','200');
        recPerPageOptions.add(newOption1);
        recPerPageOptions.add(newOption2);
        recPerPageOptions.add(newOption3);
        recPerPageOptions.add(newOption4);

        recPerPage = '20';

        if(usedFor == 'Contact' )
        {
            setContactListForAccount();
        }
        //By default do not display the new contact section, display the contact list
        renderNewContactSection = false;
        renderContactList = true;

        if('TO'.equalsIgnoreCase(addressType))
        {
            renderCheckBoxes = false;
        }
        else
        {
            renderCheckBoxes = true;
        }

        contactIdToBeEdited = '';
        updatedContactName = '';

    }

    public PageReference updateContact()
    {
        
        PageReference pageRef = ApexPages.currentPage();

        String urlx = pageRef.getUrl();
        
        String namespace = ConfigUtilController.getNamespace();

        // the url needs namespace as the page will be redirected back from native edit contact page.
        String url ='';
        if((namespace != null) && (!(namespace.trim().equals(''))))
        {
            url = '/apex/' + namespace + '__ContactLookUpView?usefor=Contact&AccountID='+ accountId
                  + '&ctrlid=' + contactCtrlID + '&addressType=' + addressType + '&contactMethod=' + contactMethod;
        }
        else
        {
            url = '/apex/ContactLookUpView?usefor=Contact&AccountID='+ accountId
                  + '&ctrlid=' + contactCtrlID + '&addressType=' + addressType + '&contactMethod=' + contactMethod;
        }

        

        //return new Pagereference('/apex/EditContact?id='+contactIdToBeEdited);

        String urlEncodedTimestamp = EncodingUtil.urlEncode(url, 'UTF-8');

        return new Pagereference('/'+contactIdToBeEdited + '/e?retURL='+urlEncodedTimestamp);
    }
    //this will retrieve contact info from the selected contact ids.
    public void processContacts()
    {
        List<ContactWrapper> selectedContacts = new List<ContactWrapper>();

        if(getSelectedContactsObj() !=null)
            selectedContacts = getSelectedContactsObj();

        for(ContactWrapper wrapper : selectedContacts)
        {
        }
        contactMethodAddressStr = getContactMethodInfo(selectedContacts);

    }
    // Method to provide information about different Contacts Method 
    // Return Type - String 
    // Parameter - selectedContacts List<ContactWrapper>
    private String getContactMethodInfo(List<ContactWrapper> selectedContacts)
    {
        String addressStr = '';
        Integer index = 0;
        if('Email'.equalsIgnoreCase(contactMethod))
        {
            for(ContactWrapper c : selectedContacts)
            {   
              
               // String email = String.escapesingleQuotes(c.contactObj.Email);
                String email;
                if(c.contactObj.Email != null || c.contactObj.Email != '')
                {
                    email = c.contactObj.Email;
                }
               // email.replace('\\','');
               
                if(email == null || email == '')
                {
                    continue;
                }

                if(index == 0)
                {
                    addressStr = email;
                    index++;
                }
                else {
                    addressStr = addressStr + ';' + email;
                }
            }
        }
        else if('Phone'.equalsIgnoreCase(contactMethod))
        {       index = 0;
            for(ContactWrapper c : selectedContacts)
            {
                String phone =c.contactObj.Phone;
                if(phone == null || phone == '')
                {
                    continue;
                }

                if(index == 0)
                {
                    addressStr = phone;
                    index++;
                }
                else {
                    addressStr = addressStr + ';' + phone;
                }

            }}
        else if('Fax'.equalsIgnoreCase(contactMethod))
        {
            for(ContactWrapper c : selectedContacts)
            {   if(contactFaxField != null && contactFaxField != ''){
                    addressStr = addressStr + ';' + c.contactObj.get(contactFaxField);//FAX;
                    c.contactFieldValue = (String)c.contactObj.get(contactFaxField);
                    FaxServiceType = 'contactFaxField';
                }else{
                    addressStr = addressStr + ';' + c.contactObj.FAX;
                    c.contactFieldValue = (String)c.contactObj.FAX;
                    FaxServiceType = 'interFax';
                }
                
            }
        }
        return addressStr;
    }
    // Method to rerive list of selected contacts 
    // Return Type - List<ContactWrapper>
    // No parameters are passed in this method 
    private List<ContactWrapper> getSelectedContactsObj() {
        List<ContactWrapper> selectedContacts = new List<ContactWrapper>();
        if(contactList != null)
        {
            for(ContactWrapper c : contactList) {
                if(c != null && c.selected) {
                    if(c.contactobj.email != null && c.contactobj.email != ''){
                        c.contactobj.email =  c.contactobj.email.replace('\\','');
                    }
                    selectedContacts.add(c);
                }
            }
        }
        return selectedContacts;
    }
    // Method to retrive Ids of selected Contacts 
    // Return Type - String 
    // No parameters are specified in this method 
    public String getSelectedContactIdsString() {
        String cSelectedIds = '';
        if(contactList != null)
        {
            for(ContactWrapper c : contactList) {
                if(c != null && c.selected) {
                    
                    cSelectedIds = cSelectedIds + ',' + c.contactObj.Id;
                }
            }
        }
        return cSelectedIds;
    }
    //Sets the renderNewContactSection value
    public PageReference setRenderNewContactSection()
    {
        renderNewContactSection = true;
        renderContactList = false;

        //Create a new Contact Object
        newContact = new Contact();

        return null;
    }
    // Method to generate list of Contacts for a Account 
    // Return Type - PageRefernce 
    // No parameters are specified in this method 
    public PageReference setContactListForAccount()
    {
        usedFor = 'Contact';

        if('TO'.equalsIgnoreCase(addressType))
        {
            renderCheckBoxes = false;
        }
        else
        {
            renderCheckBoxes = true;
        }
        //Set the render variables
        renderNewContactSection = false;
        renderContactList = true;

        String searchTemp = searchText;
        if(searchTemp == null)
            searchTemp = '%';
        else
        {
            if(searchTemp.contains('*'))
                searchTemp = searchTemp.replace('*','%');
            else
                searchTemp = searchTemp + '%';
        }

        try {
            String SOQLQuery;
            if(contactFaxField != null && contactFaxField != ''){
                SOQLQuery = 'Select id, Name, Title,Account.Name, Phone,FAX,Email,'+contactFaxField+', default__c from Contact where Account.Id = \'' + accountId + '\' AND Name Like \''+searchTemp+'\' order by Name Limit 10000';
            }else{
                SOQLQuery = 'Select id, Name, Title,Account.Name, Phone,FAX,Email,default__c from Contact where Account.Id = \'' + accountId + '\' AND Name Like \''+searchTemp+'\' order by Name Limit 10000';
            }
                con = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQuery ));
            //totalContactsRec =con.getRecords().size();
            if(con != null)
            {
                if(con.getResultSize() > 0)
                {     totalContactsRec =con.getResultSize();
                      con.setPageSize(Integer.valueOf(recPerPage));
                      getContactList(); }else{
                    totalContactsRec  = 0;
                }
            }

        } catch(Exception e)
        {
            
            throw e;
        }

        return null;

    }
    // Method to save Contact List generated 
    // Return Type - PageReference 
    // Parameters - newContact Contact  
    public PageReference setContactListOnSave(Contact newContact)
    {
        usedFor = ' ';
        if('TO'.equalsIgnoreCase(addressType))
        {
            renderCheckBoxes = false;
        }
        else
        {
            renderCheckBoxes = true;
        }
        //Set the render variables
        renderNewContactSection = false;
        renderContactList = true;

        Id newContactId=null;
        if(newContact != null) {
            newContactId= newContact.Id;
        }
        
        try {
            String SOQLQuery;
            if(contactFaxField != null && contactFaxField != ''){
                SOQLQuery = 'Select id, Name, Title,Account.Name, Phone, FAX, Email,'+contactFaxField+', default__c from Contact where Id = :newContactId Limit 10000 ';
            }else{
                SOQLQuery = 'Select id, Name, Title,Account.Name, Phone, FAX, Email, default__c from Contact where Id = :newContactId Limit 10000 ';
            }
            con = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQuery ));
            if(con != null)
            {
                if(con.getResultSize() > 0 )
                {     totalContactsRec =con.getResultSize();
                      con.setPageSize(Integer.valueOf(recPerPage));
                      getContactList(); }else{
                    totalContactsRec = 0;
                }
            }
        } catch(Exception e)
        {
            
            throw e;
        }

        return null;
    }
     //Gets the contact list
    public List<ContactWrapper> getContactList()
    {
        this.contactList = new List<ContactWrapper>();

        if(con != null) {
            for(Contact contactObj : (List<Contact>)con.getRecords()) {
            //JIRA Issue AKTPROD-220 
            // By Kruti Tandel
            //commented the code due to error :Modified rows exist in the records collection! Error is in expression '{!next}' in page akritiv:contactlookupview
               
           /*    if(contactObj.Email != null && contactObj.Email != ''){
                   if(contactObj.Email.contains('\'') ){
                        contactObj.Email = String.escapesingleQuotes(contactObj.Email);
                        
                    }
                    integer i = ((List<Contact>)con.getRecords()).size();
                    
                  
                    
                          if(contactObj.Email.contains('\'')){
                            contactObj.Email = contactObj.Email.replace('\\','');
                            contactObj.Email = String.escapesingleQuotes(contactObj.Email);
                         }
              
                }*/
                this.contactList.add(new ContactWrapper(contactObj));
                
            }
        }
        return this.contactList;
    }

    //Sets the contact list
    public PageReference setSearchContactList()
    {
     try {
        usedFor = ' ';
        //Set the render variables
        renderNewContactSection = false;
        renderContactList = true;

        String searchTemp = searchText;
        if(searchTemp == null)
            searchTemp = '%';
        else
        {
            if(searchTemp.contains('*'))
                searchTemp = searchTemp.replace('*','%');
            else
                searchTemp = searchTemp + '%';
        }
        //SOQLStr = 'Select id, Name,Account.Name, Phone, FAX,'+contactFaxField+', Email from Contact where Account.Id = \'' + accountId + '\' AND Name Like \''+searchTemp+'\' order by Name';
        String SOQLQuery;
        if(contactFaxField != null && contactFaxField != ''){
            SOQLQuery = 'Select id, Name,Title,Account.Name, Phone, FAX, Email,'+contactFaxField+', default__c from Contact where Name Like \''+searchTemp+'\' order by Name Limit 10000';
        }else{
            SOQLQuery = 'Select id, Name,Title,Account.Name, Phone, FAX, Email, default__c from Contact where Name Like \''+searchTemp+'\' order by Name Limit 10000';
        }

        con = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQuery ));

        //@todo why is this required for search?
        if(con != null)
        {
            if(con.getResultSize() > 0)
            {
                totalContactsRec =con.getResultSize();
                con.setPageSize(Integer.valueOf(recPerPage));
                getContactList();
            }else{
                totalContactsRec = 0;
            }
        }
       }
        catch(Exception ex) {
            
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Label_Please_use_correct_format_in_filter_values));
        }
        return null;
    }

    public PageReference cancel()
    {
        //Set the render variables
        renderNewContactSection = false;
        renderContactList = false;
        //con.cancel();
        setContactListForAccount();
        return null;
    }

    
    public PageReference Save()
    {
        //Set the newContact accountId
        if(accountId !=null && accountId.trim() !='')
            newContact.accountId = accountId;

        //Insert the new contact
        
        System.debug('==newContact=='+newContact);
        
        if(newContact != null){
            //==========================================================================================  
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Contact.fields.getMap();
           for (String fieldToCheck : m.keyset()) {
              // System.debug('=====newContact12===='+newContact.get(fieldToCheck));
              // if(newContact.get(fieldToCheck) != null){
              // Check if the user has create access on the each field
                  System.debug('===fieldToCheck==='+fieldToCheck);
                  if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
                      if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                'Insufficient access'));
                        System.debug('====fieldToCheck in====='+fieldToCheck);
                       return null;
                      }
                  }
             // }
            }
            System.debug('==newContact===='+newContact);
            //========================================================================================
            insert newContact;
            
    
            //Set the contact list
            //return setContactListOnSave(newContact);
            return setContactListOnSave(newContact);
        }else{
            return null;
        }
    }

    //pagination start here

    public void changeRecPerPage(){
        if(con !=null && con.getResultSize() > 0) {
            con.setPageSize(Integer.valueOf(recPerPage));
            first();
        }

    }

    public String pageDetail {
        get {
            Integer totalPages = 1;
            if((totalContactsRec != 0 && totalContactsRec != null) && (recPerPage != null || recPerPage != '') ) {
                totalPages =  totalContactsRec/Integer.valueOf(recPerPage);
                
                if( math.mod(totalContactsRec,Integer.valueOf(recPerPage) )!= 0)
                {
                    totalPages =totalContactsRec/Integer.valueOf(recPerPage) + 1;
                }
            }
            return '<< '+Label.Label_Page+ ' '+ pageNumber + Label.Label_Of + ' ' + totalPages + ' >>';

        }
        set;
    }

    public void next() {

        if(hasNext ==true) {
            if(con !=null) {

                con.next();
                getContactList();
            }
        }
    }

    public Boolean hasNext {
        get {
            if(con !=null){
                system.debug('---Con value---'+con.getHasNext());
                return con.getHasNext();
            }else
                return false;
        }
        set;
    }

    public Boolean hasPrevious {
        get {
            if(con !=null)
                return con.getHasPrevious();
            else
                return false;
        }
        set;
    }

    public Integer pageNumber {
        get {
            if(con !=null)
                return con.getPageNumber();
            else
                return 0;
        }
        set;
    }
    public void first() {
        if(hasPrevious ==true) {
            if(con !=null)
            {
                con.first();
                //need to determine whrethet to call getContactList or
                getContactList();
            }
        }
    }

    public void last() {
        if(hasNext ==true) {
            if(con !=null)
            {
                con.last();
                getContactList();
            }
        }
    }
    public void previous() {
        if(hasPrevious ==true && con!= null) {
            con.previous();
            getContactList();
        }
    }

    /* End Contact Lookup Section */

/* Wrapper class to store Contact with selected flag */
    public class ContactWrapper {
        public Contact contactObj {get; set; }
         public String dispEmail{get;set;}
        public String contactFieldValue {get;set;}
        /* Selected flag set to true when a contact is selected/checked from the page */
        public Boolean selected {get; set; }

        public ContactWrapper()
        {
            this.contactObj = new Contact();
            this.selected = false;
            this.contactFieldValue  = null;
        }
        // Default construnctor for the Wrapper Class "ContactWrapper"
        public ContactWrapper(Contact c) {
            this.selected = false;
            this.contactObj = c;
            if(c.Email != null && c.Email != ''){
                 dispEmail = c.Email.replace('\\','');
            }
            if(ConfigUtilController.getContactFaxField() != null && ConfigUtilController.getContactFaxField() != ''){
                this.contactFieldValue =(String) c.get(ConfigUtilController.getContactFaxField());
                
            }else{
                this.contactFieldValue = c.FAX;
               
            }

        }
    }
}