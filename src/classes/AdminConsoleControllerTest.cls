/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This is test class AdminConsoleControllerTest.
 */
 
@istest(seeAllData = true)
public with sharing class AdminConsoleControllerTest{
    static testMethod void testController () {

        Test.startTest();
        AdminConsoleController c = new AdminConsoleController();
        String s = c.invoicetemplate;
        //String str = c.invoicetemplate;
        String str = c.TrxObjUrl; 
         String dis = c.DisObjUrl; 
         String lin = c.LineItemObjUrl; 
        
         System.assertEquals(str, str);
         System.assertEquals(s,s);
        
        Test.stopTest();

    }
}