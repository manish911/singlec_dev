/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for MassUpdateTaskDynamicController.
 */
 
@isTest
private class TestMassUpdateTaskDynamicController{
    static testMethod void TestUpdateTask(){
    integer tempVar = 5;
    Trigger_Configuration__c objtrg = Trigger_Configuration__c.getOrgDefaults();
    objtrg.Update_Account_ContactLog_Details__c = false;
    upsert objtrg;
    
    Account tempAccount = new Account();
    tempAccount = accountInsert();
        
    List<Task> tklist = new List<Task>();
    tklist=taskInsert(tempAccount,tempVar);
        
    // get comma separated ids of transaction to create a config object value
    String tkIds = '';

    for(Task tk : tkList)
    {
        tkIds += ','+tk.Id;
    }
        
    Temp_Object_Holder__c TempObj = new Temp_Object_Holder__c();
    TempObj.Key__c = Userinfo.getUserName()+Datetime.now();
    TempObj.value__c = tkIds;
    insert TempObj;
    system.debug('----TempObj----' + TempObj );
        
    // Passing selected list 
    ApexPages.currentPage().getParameters().put('key',TempObj.Key__c);
   
    Test.startTest();
    MassUpdateTaskDynamicController Updatetaskobj = new MassUpdateTaskDynamicController();
    Updatetaskobj.massUpdate();
    Test.stopTest();   
    }
    
    public static Account accountInsert()
    {
        //create an Account
        Account accObj = new Account(Name='testAccount');
        accObj.Account_Key__c =''+(Math.random()* 1000000).intValue() + (Math.random()* 1000000).intValue();
        accObj.ownerId = UserInfo.getUserId();
        accObj.Sticky_Note__c = 'Add Account Notes';
        accObj.Sticky_Note_Level__c = 'Normal';
        insert accObj;
        return accObj;
    } 
    
    public static List<Task> taskInsert(Account acc , Integer count)
    {
         List<Task> tkobjlist = new List<Task>();
         for(Integer i=0; i<count; i++){
             Task taskObj = new Task();
             taskObj.whatid= acc.id;
             taskobj.Description = 'abc';
             taskobj.ownerid=UserInfo.getUserId();
             taskobj.status = 'not started';
             taskobj.priority = 'Normal';
             taskobj.subject ='Related to Approval';
             //taskobj.CurrencyIsoCode = 'USD';
             taskObj.ActivityDate  = Date.today();
             tkobjlist.add(taskobj);
         }
         insert tkobjlist;
         return tkobjlist;
    }
    

   
}