public with sharing class MassTransferAccountController {
    public List<MoreFiltersWrapper> moreFilterWrapperList{get;set;}
    public MoreFiltersWrapper singleMoreFilter1{get;set;}
    public Boolean isInProgress { get; set; }
    public String u1Name{get;set;}
    public String u1Id{get;set;}
    public String u2Name{get;set;}
    public Integer progress {get; set; }
    public String u2Id{get;set;}
    public ID batchprocessid {get; set; }
    public AsyncApexJob aaj {get; set; }
    public List<Account> accList{get;set;}
    private String lastSearchQuery{get;set;}
    private Map<String,String> customFieldDataTypeMap = new Map<String,String>();
    public MassTransferAccountController(){
        
        accList = new List<Account>();
        moreFilterWrapperList = new List<MoreFiltersWrapper>();
        for(Integer i=0; i < 5 ;i++){
            singleMoreFilter1 = new MoreFiltersWrapper();
            if(i==4){
                singleMoreFilter1.displayAnd = '';
            }
            moreFilterWrapperList.add(singleMoreFilter1);
        }
        Schema.Sobjecttype objType = Account.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
        sortSelectOption.sort();
        String fieldType;
        String fieldName;
        for(String a : sortSelectOption) {
            Schema.Sobjectfield field = fieldMap.get(a);
            fieldName = field.getDescribe().getName();
            fieldType = field.getDescribe().getType().name();
            customFieldDataTypeMap.put(fieldName,fieldType);
        }
        progress = 0;
        isInProgress = false;
    }
    
     public PageReference searchSelectedaccount() {
        String finalQueryCondition;
        String moreQueryCondition='';
        if((finalQueryCondition == null || finalQueryCondition == '') && (u1Id == null || u1Id == '') && (u2Id == null || u2Id == '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Please_select_From_and_To_user_and_filter_Condition));
             return null;
        }else if((u1Id == null || u1Id == '')  && (u2Id == null || u2Id == '')){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Please_select_From_and_To_user));
             return null;
        }else if( u1Id == null || u1Id == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Please_select_From_user));
            return null;
        }else if (u2Id == null || u2Id == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Please_select_To_user));
            return null;
        }
        for(MoreFiltersWrapper filter : moreFilterWrapperList) {
             
            if(buildWhereCondition(filter) != '' ){
                String tempCondition =buildWhereCondition(filter);
                if(tempCondition != '' || tempCondition != null) {
                    if (moreQueryCondition == ''){
                        moreQueryCondition = buildWhereCondition(filter);
                    }
                    else{
                        moreQueryCondition = moreQueryCondition + ' AND '+ buildWhereCondition(filter);
                    }
                 
                }
            }
        }
        if(moreQueryCondition !='' && moreQueryCondition != null){
            finalQueryCondition = '( '+ moreQueryCondition + ')';
        }
        if(finalQueryCondition != '' && finalQueryCondition != null){
            lastSearchQuery = 'select Id,Name,OwnerId From Account where '+finalQueryCondition+' limit 1000';
            
            String u1Idesc=  string.escapeSingleQuotes(u1Id);
            String finalQueryCondition1 = string.escapeSingleQuotes(finalQueryCondition);
        //   String  finalQueryCondition1 =  '\'' +  String.escapeSingleQuotes(finalQueryCondition) + '\''; 

           system.debug('-----finalQueryCondition1-----'+finalQueryCondition1 );

            String query = 'select Id,owner.Name,Name,akritiv__Total_AR__c,akritiv__DPD__c from Account where ' + finalQueryCondition1 + ' AND ownerId = :u1Idesc limit 999';
            system.debug('---query----'+query);
            accList = Database.query(query);
            
        }else{
            String u1Idesc=  string.escapeSingleQuotes(u1Id);
            String query = 'select Id,owner.Name,Name,akritiv__Total_AR__c,akritiv__DPD__c from Account where ownerId = :u1Idesc limit 999';
            
            accList = Database.query(query);
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_No_Records_Found));
            
        }
        if(finalQueryCondition != null && u1Id != null && u2Id != null){
            
            BatchMassTransferAccounts bmta = new BatchMassTransferAccounts(finalQueryCondition,u1Id,u2Id);
            batchprocessid  = Database.executeBatch(bmta , 200);
        }else if(finalQueryCondition == null && u1Id != null && u2Id != null){
            BatchMassTransferAccounts bmta = new BatchMassTransferAccounts('',u1Id,u2Id);
            batchprocessid  = Database.executeBatch(bmta , 200);
        }
        
        if(accList.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_No_Records_Found));
            return null;
        }
        if(accList.size() > 0){
            progress = 0;
            isInProgress = true;
        }
        
        return null;
    }
    
    private String buildWhereCondition(MoreFiltersWrapper moreFilterWrapper){
        String finalQueryCondition = '';
        String contains;
        if(moreFilterWrapper.selectedCondition1 != null && moreFilterWrapper.selectedCondition1 != '' && moreFilterWrapper.selectedCondition1 != '--None--'){
            moreFilterWrapper.selectedaccountFilterCondition = moreFilterWrapper.selectedCondition1;
        }
        
        if(((moreFilterWrapper.selectedaccountCustomFields != null && moreFilterWrapper.selectedaccountCustomFields != '' && moreFilterWrapper.selectedaccountCustomFields != '--None--') && (moreFilterWrapper.selectedaccountFilterCondition != null && moreFilterWrapper.selectedaccountFilterCondition != '') && (moreFilterWrapper.strequals != null && moreFilterWrapper.strequals != ''))){
             if(('DATETIME').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields))){
                
                String dateCheck = '^(|(0[1-9])|(1[0-2]))\\/((0[1-9])|(1\\d)|(2\\d)|(3[0-1]))\\/((\\d{4}))$';
                String dateTimeCheck = '^((((([13578])|(1[0-2]))[\\-\\/\\s]?(([1-9])|([1-2][0-9])|(3[01])))|((([469])|(11))[\\-\\/\\s]?(([1-9])|([1-2][0-9])|(30)))|(2[\\-\\/\\s]?(([1-9])|([1-2][0-9]))))[\\-\\/\\s]?\\d{4})(\\s((([1-9])|(1[02]))\\:([0-5][0-9])((\\s)|(\\:([0-5][0-9])\\s))([AM|PM|am|pm]{2,2})))?$';
                Pattern MyPattern = Pattern.compile(dateCheck);
                Matcher MyMatcher = MyPattern.matcher(moreFilterWrapper.strequals);
                Pattern MyPatternDateTime = Pattern.compile(dateTimeCheck);
                Matcher MyMatcherDateTime = MyPattern.matcher(moreFilterWrapper.strequals);
                if ((MyMatcher.matches())) {
                    String finalDate1;
                    String finalDate2;
                    List<String> splited = moreFilterWrapper.strequals.split(' ');
                    
                    List<String> yyyy1 = splited.get(0).split('/');
                    String yyyy = yyyy1.get(2)+'-'+yyyy1.get(0)+'-'+yyyy1.get(1);
                    String splitedDate = yyyy +'T';
                    if(splited.size() != 2){
                        finalDate1 =  splitedDate+'00:00:00.000Z';
                        finalDate2 =  splitedDate+'23:59:59.000Z';
                    }else{
                        String removeAMPM = splited.get(1).replace('am','').replace('pm','');
                        finalDate1 =  splitedDate+' '+removeAMPM+':00.000Z';
                        finalDate2 =  splitedDate+' '+removeAMPM+':00.000Z';
                    }
                    //Do something meaningfull
                    if(('='.equals(moreFilterWrapper.selectedaccountFilterCondition))){
                        finalQueryCondition = moreFilterWrapper.selectedaccountCustomFields+'  >'+moreFilterWrapper.selectedaccountFilterCondition+' '+finalDate1+' AND '+moreFilterWrapper.selectedaccountCustomFields+'  <'+moreFilterWrapper.selectedaccountFilterCondition+ ' '+finalDate2;
                    }else{
                        if(('<').equals(moreFilterWrapper.selectedaccountFilterCondition) || ('<=').equals(moreFilterWrapper.selectedaccountFilterCondition)){
                            finalQueryCondition = moreFilterWrapper.selectedaccountCustomFields+' '+moreFilterWrapper.selectedaccountFilterCondition+'  ' +finalDate1;
                        }else if(('>').equals(moreFilterWrapper.selectedaccountFilterCondition) || ('>=').equals(moreFilterWrapper.selectedaccountFilterCondition)){
                            finalQueryCondition = moreFilterWrapper.selectedaccountCustomFields+' '+moreFilterWrapper.selectedaccountFilterCondition+'  ' +finalDate2;
                        }else if(('!=').equals(moreFilterWrapper.selectedaccountFilterCondition)){
                            finalQueryCondition = moreFilterWrapper.selectedaccountCustomFields+'  >= '+finalDate2+' AND '+moreFilterWrapper.selectedaccountCustomFields+'  <= '+finalDate1;
                        }else{
                            finalQueryCondition = moreFilterWrapper.selectedaccountCustomFields+' '+moreFilterWrapper.selectedaccountFilterCondition+'  ' +moreFilterWrapper.strequals;
                        }
                    }
                }
            }else if(('CURRENCY').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields)) || ('DOUBLE').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields))){
                String accountField;
                String numberCheck= '^\\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$';
                Pattern MyPattern = Pattern.compile(numberCheck);
                moreFilterWrapper.strequals = moreFilterWrapper.strequals.replace(',','').replace('USD ','');
                Matcher MyMatcher = MyPattern.matcher(moreFilterWrapper.strequals);
                if (MyMatcher.matches()){
                    finalQueryCondition =  moreFilterWrapper.selectedaccountCustomFields +' '+moreFilterWrapper.selectedaccountFilterCondition+' '+ moreFilterWrapper.strequals;
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Entered_value_is_Invalid));
                }
            }else if(('DATE').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields))){
                 List<String> splited = moreFilterWrapper.strequals.split('/');
                 String finalDate = ''; 
                 if(splited.get(2).length() == 4 && splited.get(0).length() == 2 && splited.get(1).length() == 2){
                    finalDate = splited.get(2)+'-'+splited.get(0)+'-'+splited.get(1);
                 }else{
                     if(splited.get(2).length() == 4){
                        finalDate += splited.get(2)+'-';
                     }
                     if(splited.get(0).length() == 2){
                        finalDate += splited.get(0)+'-';
                     }else{
                        finalDate += '0'+splited.get(0)+'-';
                     }
                     if(splited.get(1).length() == 2){
                        finalDate += splited.get(1);
                     }else{
                        finalDate += '0'+splited.get(1);
                     }
                 }
                 if(finalDate.length() < 10){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,Label.Label_Date_Format_is_Invalid_Message));
                    return null;
                 }
                 finalQueryCondition =  moreFilterWrapper.selectedaccountCustomFields +' '+moreFilterWrapper.selectedaccountFilterCondition+' '+ finalDate;
            }else if(('STRING').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields)) || ('PICKLIST').equals(customFieldDataTypeMap.get(moreFilterWrapper.selectedaccountCustomFields))){
                if(moreFilterWrapper.selectedaccountFilterCondition.contains('like')){
                    finalQueryCondition =  moreFilterWrapper.selectedaccountCustomFields +' '+moreFilterWrapper.selectedaccountFilterCondition +' \''+ moreFilterWrapper.strequals +'%\'';
                }else{
                     finalQueryCondition =  moreFilterWrapper.selectedaccountCustomFields +' '+moreFilterWrapper.selectedaccountFilterCondition+ ' '+'\''+ moreFilterWrapper.strequals+'\'';
                }
            }
        }
        return finalQueryCondition;
    }
    
    // method is used to refresh Job status 
    // return type : Pagereference
    public Pagereference refreshJobStatus() {
        if(batchprocessid != null)
            aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid ];
            
           //
        if(aaj != null && aaj.TotalJobItems != null && aaj.JobItemsProcessed != null)
        {

            if(aaj.TotalJobItems==0)
                progress=0;
            else
                progress = Integer.valueOf(aaj.JobItemsProcessed*100/aaj.TotalJobItems);
               
        }
        
        if(aaj != null && aaj.Status != null ){
        if(aaj.Status == 'Completed' || aaj.Status == 'Aborted' || aaj.Status == 'Failed')
            isInProgress = false;
        }
        return null;
    }
    
    
    public class MoreFiltersWrapper {
        public List<Selectoption> accountCustomFields{get;set;}
        public List<Selectoption> accountCustomFieldsCondition{get;set;}
        public List<Selectoption> accountCustomFieldsCondition1{get;set;}
        public List<Selectoption> accountCustomFieldsCondition2{get;set;}
        public String selectedaccountFilterCondition{get;set;}
        public String selectedCondition1 {get;set;}
        public String selectedaccountCustomFields{get;set;}
        public String strequals{get;set;}
        public String displayAnd{get;set;}
  
        public MoreFiltersWrapper(){
            displayAnd = 'AND';
            accountCustomFields = new List<Selectoption>();
            accountCustomFieldsCondition = new List<Selectoption>();
            accountCustomFieldsCondition1 = new List<Selectoption>();
            accountCustomFieldsCondition2 = new List<Selectoption>();
            //accountCustomFields.add(new selectOption('--None--','--None--'));
            accountCustomFieldsCondition.add(new selectOption('--None--','--None--'));
            accountCustomFieldsCondition.add(new Selectoption('=','equals'));
            accountCustomFieldsCondition.add(new Selectoption('!=','not equals to'));
            accountCustomFieldsCondition.add(new Selectoption('<','less than')); 
            accountCustomFieldsCondition.add(new Selectoption('>','greater than'));
            accountCustomFieldsCondition.add(new Selectoption('<=','less or equal'));
            accountCustomFieldsCondition.add(new Selectoption('>=','greater or equal'));
            accountCustomFieldsCondition.add(new Selectoption('like \'%','contains'));
            accountCustomFieldsCondition.add(new Selectoption('like','starts with'));
            
            Schema.Sobjecttype objType = Account.getSObjectType();
            Schema.Describesobjectresult sobjRes = objType.getDescribe();
            Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
            List<String> sortSelectOption = new List<String>(fieldMap.keyset());
            sortSelectOption.sort();
            for(String a : sortSelectOption) {
                Schema.Sobjectfield field = fieldMap.get(a);
                String fieldLabel = field.getDescribe().getLabel();
                String fieldName = field.getDescribe().getName();
                accountCustomFields.add(new selectOption(fieldName,fieldLabel));
            }
            accountCustomFields = UtilityController.sortOptionList(accountCustomFields);
            List < Selectoption > temp = new List < Selectoption >();
            temp.add( new Selectoption('--None--','--None--'));
            temp.addAll(accountCustomFields);
            accountCustomFields = temp;
        }
    }
}