/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
public with sharing class CommonAjaxController {

    public static final String TASKS = 'TASKS';
    public static final String TRANSACTIONS = 'TRANSACTIONS';
    public static final String ACCOUNTDETAIL = 'ACCOUNTDETAIL';
    public static final String DISPUTES = 'DISPUTES';
    public static final String NOTES = 'NOTES';
    public static final String PROMISES = 'PROMISES';
    public static final String OPPORTUNITIES = 'OPPORTUNITIES';
    public static final String NOTEBODY = 'NOTEBODY';

    public static final String ADD_NOTES_ON_TRANSACTIONS = 'ADD_NOTED_ON_TRANSACTIONS';

    public String jsonString {get; set; }

    // invoked on an Ajax request 
    public void doGet() {

        Map<string,string> params = ApexPages.currentPage().getParameters();

        String recordType = params.get('recordtype');
        String accId = params.get('accId');

        if ( TASKS.equals(recordType) ) {

            String taskkey = params.get('taskkey');
            //get tasks from temp object holder
            getMyTasks(taskkey);

        } else if ( ACCOUNTDETAIL.equals(recordType) ) {

            //get Account detail

            getAccountDetail(accId);
        } else if ( TRANSACTIONS.equals(recordType) ) {
            getTransactions(accId);
            //get transactions
        }else if ( DISPUTES.equals(recordType) ) {

            getDisputes( accId );
            //get disputes
        }else if ( NOTES.equals(recordType) ) {

            //get notes

            getNotes(accId);
        }else if ( PROMISES.equals(recordType) ) {

            //get promises
        }else if ( OPPORTUNITIES.equals(recordType) ) {

            //get opportunities
        } else if ( ADD_NOTES_ON_TRANSACTIONS.equals(recordType) ) {

            String txids  = String.escapeSingleQuotes(params.get('txIds'));
            String title  = params.get('title');
            String value  = params.get('body');
            //addNotesOnTransaction( title, value, txids);
        
        
        } else if ( NOTEBODY.equals(recordType) ) {
           
            String nId = String.escapeSingleQuotes(params.get('noteId'));
             
            getNoteBody( nId );
        }

    }

    // Does the SOQL query, using Ajax request data retrives Contacts 
    // Return Type - List<Contacts>
    // Parameteres - contactName String 
    public List<Contact> getRecords(String contactName) {
        List<Contact> records = new List<Contact>();
        if (contactName != null && contactName.trim().length() > 0) {
            // Protect from SOQL injection
            String contactNameToFilter = '%' + contactName  + '%';
            records = [select id, name, email from contact where Name like :contactNameToFilter];

        }
        return records;
    }

    // Returns the JSON result string
    public String getResult() {
        return jsonString;
    }
    // Tasks which are to be performed by user 
    // Return Type - Void 
    // Parameter - taskkey String 
    public void getMyTasks ( String taskkey) {

        //get task ids  from temp object holder
        if(taskkey != ''){
        List<Temp_Object_Holder__c> toh = [ select value__c from Temp_Object_Holder__c where key__c =:taskkey limit 1];
        String ids = toh.get(0).value__c;
        String [] sarry  = ids.split(',');
        
        List < Task > tasksOjs = [ Select t.WhoId,
                                   t.WhatId,
                                   t.SystemModstamp,
                                   t.Subject,
                                   t.Status,
                                   t.ReminderDateTime,
                                   t.RecurrenceType,
                                   t.RecurrenceTimeZoneSidKey,
                                   t.RecurrenceStartDateOnly,
                                   t.RecurrenceMonthOfYear,
                                   t.RecurrenceInterval,
                                   t.RecurrenceInstance,
                                   t.RecurrenceEndDateOnly,
                                   t.RecurrenceDayOfWeekMask,
                                   t.RecurrenceDayOfMonth,
                                   t.RecurrenceActivityId,
                                   t.Rating__c,
                                   t.Priority,
                                   t.OwnerId, t.Notes__c,
                                   t.LastModifiedDate,
                                   t.LastModifiedById,
                                   t.IsReminderSet,
                                   t.IsRecurrence,
                                   t.IsDeleted,
                                   t.IsClosed,
                                   t.IsArchived,
                                   t.Id,
                                   t.Description,
                                   t.CreatedDate,
                                   t.CreatedById,

                                   t.Contact_Level__c,
                                   t.CallType,
                                   t.CallObject,
                                   t.CallDurationInSeconds,
                                   t.CallDisposition,
                                   t.Activity_Category__c,
                                   t.ActivityDate,
                                   t.Account.Name,
                                   t.AccountId From Task t where Id in :sarry order by t.ActivityDate ];
        
        List<JSONObject.value> values = new List<JSONObject.value>();

        string jsonRecordsString = '';
        String blank ='';
        for (Task task : tasksOjs) {

            jsonRecordsString += '{';

            jsonRecordsString += '"Subject":' + '"'+task.Subject+'",';
            jsonRecordsString += '"Status":' + '"'+task.Status+'",';
            jsonRecordsString += '"Priority":' + '"'+task.Priority+'",';

            String temp = task.Description != null ? task.Description : blank;
            jsonRecordsString += '"Description":' + '"'+temp+'",';

            if ( task.AccountId == null ) {

                jsonRecordsString += '"Account":' + '"'+blank+'",';

            }else {

                jsonRecordsString += '"Account":' + '"'+task.Account.Name+'",';
            }

            String tempadate = task.ActivityDate != null ? task.ActivityDate.format() : blank;
            jsonRecordsString += '"DueDate":' + '"'+tempadate+'",';

            jsonRecordsString += '},';
            }
        jsonString = '({"total":"'+tasksOjs.size()+'", "tasks":[' + jsonRecordsString + ']})';
        jsonString = jsonString.replaceAll(',]',']');
    }
    }
    // Method fetches Account Detail for Account 
    // Return Type - Void 
    // Parameter - aId String 
    public void getAccountDetail ( String aId ) {
        
            List<Account> acc = [ Select a.Website,
    
                            a.Type,
                            a.Total_Promised__c,
                            a.Total_Disputed__c,
                            a.Total_AR__c,
                            a.TickerSymbol,
                            a.Terms_Key__c,
                            a.SystemModstamp,
                            a.Source__c,
                            a.Site,
                            a.Sic,
                            a.ShippingStreet,
                            a.ShippingState,
                            a.ShippingPostalCode,
                            a.ShippingCountry,
                            a.ShippingCity,
    
                            a.Rating_Sum__c,
                            a.Rating_Count__c,
                            a.Rating, a.Phone,
                            a.Percent_Disputed__c,
                            a.Percent_Disputed_Risk__c,
                            a.Percent_Credit_Utilized__c,
                            a.ParentId,
                            a.Ownership,
                            a.OwnerId,
                            a.Oldest_Due_Date__c,
                            a.Oldest_DPD__c,
                            a.Oldest_Bal__c,
    
                            a.NumberOfEmployees,
                            a.Name,
                            a.MasterRecordId,
                            a.Lead_Tx__c,
                            a.Last_Payment_Received__c,
                            a.Last_Payment_Amount__c,
                            a.Last_Credit_Review_Date__c,
                            a.Last_Contact_Type__c,
                            a.Last_Contact_Method__c,
                            a.Last_Contact_Date__c,
                            a.LastModifiedDate,
                            a.LastModifiedById,
                            a.LastActivityDate,
                            a.IsDeleted,
                            a.Initial_Credit_App__c,
                            a.Industry,
                            a.Id,
                            a.High_Past_Due_Tx__c,
                            a.High_Past_Due_Risk__c,
                            a.High_Past_Due_Date__c,
                            a.High_Past_Due_DPD__c,
                            a.High_Past_Due_Bal__c,
                            a.Fax, a.Description,
                            a.DPD_Std_Dev__c,
                            a.DPD_Mean__c,
    
                            a.Credit_Utilization__c,
                            a.Credit_Risk__c,
                            a.Credit_Remaining__c,
                            a.Credit_Limit__c,
                            a.CreatedDate,
                            a.CreatedById,
                            a.Contact_Risk__c,
                            a.Collections_Confidence__c,
                            a.Business_Unit__c,
                            a.Bucket_Links__c,
                            a.BillingStreet,
                            a.BillingState,
                            a.BillingPostalCode,
                            a.BillingCountry,
                            a.BillingCity,
                            a.Bill_To_Number__c,
                            a.Batch_Number__c,
                            a.Avg_DPD_Risk__c,
                            a.AnnualRevenue,
                            a.Amount_Over_Credit_Limit__c,
                            a.Aging_Risk__c,
                            a.Age5__c,
                            a.Age4__c,
                            a.Age3__c,
                            a.Age2__c,
                            a.Age1__c,
                            a.Age0__c,
    
                            a.Account_Type__c,
                            a.Account_Key__c,
                            a.AccountNumber
                            From Account a where Id=:aId];
    
            string jsonRecordsString = '';
    
            jsonRecordsString += '{';
            jsonRecordsString += '"AccountName":' + '"'+acc.get(0).Name+'",';
            jsonRecordsString += '"AccountNumber":' + '"'+acc.get(0).AccountNumber +'",';
            jsonRecordsString += '"Ownership":' + '"'+acc.get(0).Ownership+'",';
            
            jsonRecordsString += '"CreatedById":' + '"'+acc.get(0).CreatedById+'",';
            jsonRecordsString += '"Site":' + '"'+acc.get(0).Site+'",';
            jsonRecordsString += '"AnnualRevenue":' + '"'+acc.get(0).AnnualRevenue+'",';
            
            jsonRecordsString += '"Phone":' + '"'+acc.get(0).Phone+'",';
            jsonRecordsString += '"Fax":' + '"'+acc.get(0).Fax+'",';
            jsonRecordsString += '"Description":' + '"'+acc.get(0).Description+'",';
            jsonRecordsString += '"Industry":' + '"'+acc.get(0).Industry+'",';
            jsonRecordsString += '"Type":' + '"'+acc.get(0).Type+'",';
            jsonRecordsString += '"LastModifiedById":' + '"'+acc.get(0).LastModifiedById+'",';
            
            jsonRecordsString += '"NumberOfEmployees":' + '"'+acc.get(0).NumberOfEmployees+'",';
    
            jsonRecordsString += '},';
            jsonString = '{"account":[' + jsonRecordsString + ']}';
            jsonString = jsonString.replaceAll(',]',']');
            
          
    }
    // Fetches Transaction Detail of Account 
    // Return Type - Void 
    // Parameter - accId String  
    public void getTransactions ( String accId  ) {
            List< Transaction__c > txs  =[Select t.Undisputed_Balance__c,
                                          t.Transaction_Key__c,
                                          t.Transaction_Code__c,
                                          t.Tax__c, t.Tax_Exempt__c,
                                          t.SystemModstamp,
                                          t.Subtotal__c,
                                          t.Source_System__c,
                                          t.So_Number__c,
                                          t.Shipment_Method__c,
                                          t.Ship_To_State__c,
                                          t.Ship_To_Postal_Code__c,
                                          t.Ship_To_Number__c,
                                          t.Ship_To_Name__c,
                                          t.Ship_To_Country__c,
                                          t.Ship_To_City__c,
                                          t.Ship_To_Address__c,
                                          t.Sales_Rep_Name__c,
                                          t.Reserved_Reason_Code__c,
                                          t.Reserved_Date__c,
                                          t.Reserved_By__c,
                                          t.Reserved_Balance__c,
                                          t.Promised_Target_Balance__c,
                                          t.Promised_Amount__c,
                                          t.Promise_Date__c,
                                          t.Po_Number__c,
                                          t.Payment_Terms__c,
                                          t.Payment_Terms_Description__c,
                                          t.PDF__c, t.Opportunity_Id__c,
                                          t.Name, t.Last_Contacted_By__c,
                                          t.Last_Contact_Date__c,
                                          t.LastModifiedDate,
                                          t.LastModifiedById,
                                          t.LastActivityDate,
                                          t.IsDeleted,
                                          t.Indicator_Icons__c,
                                          t.Id, t.Freight__c,
                                          t.Due_Date__c,
                                          t.Disputed_Amount__c,
                                          t.Days_Past_Due__c,
                                          t.DPD_x_Amount__c,
                                          t.CreatedDate,
                                          t.CreatedById,
                                          t.Create_Date__c,
                                          t.Create_Broken_Promise_Note__c,
                                          t.Collection_Stage__c,
                                          t.Close_Date__c,
                                          t.Buyer_Number__c,
                                          t.Buyer_Name__c,
                                          t.Business_Unit__c,
                                          t.Bucket__c,
                                          t.Bill_To_State__c,
                                          t.Bill_To_Postal_Code__c,
                                          t.Bill_To_Number__c,
                                          t.Bill_To_Name__c,
                                          t.Bill_To_Country__c,
                                          t.Bill_To_City__c,
                                          t.Bill_To_Address__c,
                                          t.Batch_Number__c,
                                          t.Balance__c,
                                          t.Balance_Without_Sign__c,
                                          t.Amount__c,
                                          t.Amount_Without_Sign__c,
                                          t.Account__c From Transaction__c t where Account__c =:accId ];
                
            string jsonRecordsString = '';
            String blank ='';
            for (Transaction__c trans : txs) {
    
                jsonRecordsString += '{';
                jsonRecordsString += '"TransactionId":' + '"'+trans.Id+'",';
                jsonRecordsString += '"Name":' + '"'+trans.Name+'",';
    
                jsonRecordsString += '"Balance__c":' + '"'+trans.Balance__c+'",';
                string tmpSo=trans.So_Number__c!=null ? trans.So_Number__c : blank;
                jsonRecordsString += '"So_Number__c":' + '"'+tmpSo+'",';
    
                string tmpPo=trans.Po_Number__c!=null ? trans.Po_Number__c : blank;
                jsonRecordsString += '"Po_Number__c":' + '"'+tmpPo+'",';
    
                jsonRecordsString += '"Days_Past_Due__c":' + '"'+trans.Days_Past_Due__c+'",';
                jsonRecordsString += '"Amount__c":' + '"'+trans.Amount__c+'",';
                jsonRecordsString += '"Due_Date__c":' + '"'+trans.Due_Date__c.format()+'",';
    
                string lmd = trans.Last_Contact_Date__c!=null ? trans.Last_Contact_Date__c.format() : blank;
                jsonRecordsString += '"Last_Contact_Date__c":' + '"'+lmd+'"';
                jsonRecordsString += '},';
    
            }
            jsonString = '({"transactions":[' + jsonRecordsString + ']})';
            jsonString = jsonString.replaceAll(',]',']');
        
    }
    // Fetches details of Disputes created on Transactions of Accounts 
    // Return Type - Void 
    // Parameter - accId String  
    public void  getDisputes ( String accId  ) {

        List  < Dispute__c > lstdisputes = [Select d.status__c, d.Type__c, d.Transaction__c, d.SystemModstamp,
                                            d.Resolution_Code__c, d.Owner__c, d.Name, d.LastModifiedDate,
                                            d.LastModifiedById, d.LastActivityDate, d.IsDeleted, d.Id,
                                            d.Days_To_Resolve__c, d.Days_To_Identify__c, d.CreatedDate, d.CreatedById,
                                            d.Close_Date__c, d.Batch_Number__c, d.Balance__c,
                                            d.Balance_Without_Sign__c, d.Amount__c,
                                            d.Amount_Without_Sign__c,
                                            d.Account__c From Dispute__c d

                                            where d.Account__c =:accId];

        string jsonRecordsString = '';
        String blank ='';

        for (Dispute__c d : lstdisputes) {

            jsonRecordsString += '{';
            jsonRecordsString += '"Dispute":' + '"'+d.Name+'",';
            jsonRecordsString += '"Amount":' + '"'+d.Amount__c+'",';
            jsonRecordsString += '"Balance":' + '"'+d.Balance__c+'",';
            jsonRecordsString += '"Type":' + '"'+d.Type__c+'",';
            jsonRecordsString += '"Status":' + '"'+d.status__c+'",';
            jsonRecordsString += '"CloseDate":' + '"'+d.Close_Date__c+'",';

            jsonRecordsString += '},';
        }

        jsonString = '({"disputes":[' + jsonRecordsString + ']})';
        jsonString = jsonString.replaceAll(',]',']');

    }
    // Fetches details of Promise To Pay created on Transactions of Account
    // Return Type - List<Transaction__c>
    // Parameter - accId String 
    public List < Transaction__c > getPromises ( String accId  ) {

        return null;
    }
    // Fetches details of notes added on Transactions of Accounts
    // Return Type - Void 
    // Parameter - accId String 
    public void  getNotes ( String accId  ) {

        //account level notes
        List < Note > lstnotes = [Select n.Title, n.SystemModstamp, n.ParentId, n.OwnerId, n.LastModifiedDate, n.LastModifiedById, n.IsPrivate, n.IsDeleted, n.Id, n.CreatedDate, n.CreatedById, n.Body
                                  From Note n where ParentId =:accId];

        String html = '';
        for ( Note n : lstnotes ) {

            html = html + '<div id="op" style="padding-left:5px;padding-right:5px;">';
            html = html + '<table >';
            html = html + '<tr>';
            html = html + '<td class="divtitle">'+n.Title+'</td>';
            html = html + '</tr>';
            html = html + '<tr>';
            html = html + '<td class="divdesc"><img src="/resource/1275025367000/Ext/images/default/de.GIF" />';
            html = html + ' '+n.Body+'</td>';
            html = html + '</tr>';
            html = html + '<tr><td class="divrelatedto"><b>On </b>'+n.CreatedDate.format()+'</td></tr>';
            html = html + '<tr>';
            html = html + '<td class="divcreatedby" width="153">';
            html = html + '<img src="/resource/1275025367000/Ext/images/default/cr.GIF" />';
            html = html + 'Bhavik </td>';
            html = html + '</tr>';
            html = html + '<tr><td class="divt">&nbsp;</td></tr>';
            html = html + '</table>';
            html = html + '</div>';
            html = html + '</br>';
        }

        jsonString = html;
    }
    // Fetches details  of Opportunities created on Account 
    // Return Type - List<Opportunity>
    // Parameter - accId String 
    public List < Opportunity > getOpportunity ( String accId  ) {

        return null;
    }
    // For adding notes on selected Transactions 
    // Return Type - Void 
    // Parameters - title String, value String, txids String  
    public void addNotesOnTransaction ( String title, String value, String txids ) {

        String failed = 'Failed';
        String success = 'Success';
        string jsonRecordsString = '';
        if ( title == null || title.trim().length() == 0 ) {
            return;

        } else if (value == null || value.trim().length() == 0 ) {
            return;
        }

        String [] txidsArray = txids.split(',');
        List<Note> listNotes = new List<Note>();
        for ( String tx : txidsArray) {

            Note noteObj = new Note();
            noteObj.parentId = tx;
            noteObj.Title = title;
            noteObj.Body = value;
            listNotes.add(noteObj);

        }
        //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Note.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
            System.debug('=======fieldToCheck======'+fieldToCheck);
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              System.debug('=======fieldToCheck2======'+fieldToCheck);
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                System.debug('=======fieldToCheck in======'+fieldToCheck);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
        insert listNotes;

        String msg = 'Note Body Can Not Be Blank';
        jsonRecordsString += '{';
        jsonRecordsString += '"Status":' + '"'+success+'",';
        jsonRecordsString += '"Message":' + '"'+msg+'",';

        jsonString = '({"result":[' + jsonRecordsString + ']})';
        jsonString = jsonString.replaceAll(',]',']');

    }
    
    
    public void getNoteBody( String noteId ) {
    
        Note n = [ select Id ,Body from Note where Id =:noteId limit 1];
        
        
        jsonString = n.Body ;
        
        
    }
    
    // TestMethod to Test CommonAjaxController Class
    static testMethod void testController() {

        CommonAjaxController c = new CommonAjaxController();

        Account a = DataGenerator.accountInsert();
        Task t = DataGenerator.taskInsert(a);
        Temp_Object_Holder__c pdfConfig = new Temp_Object_Holder__c();
        pdfConfig.Key__c = Userinfo.getUserName()+'_'+Datetime.now();
        pdfConfig.value__c = t.Id+'';
        insert pdfConfig;
        List < Transaction__c > txs = DataGenerator.transactionInsert(a);
        DataGenerator.disputeInsert(a, txs);

        Test.startTest();
        c.getAccountDetail( a.Id);
        c.getTransactions( a.Id );
        c.getDisputes( a.Id );
        c.getMyTasks(pdfConfig.Key__c );

        Note n = new Note();

        n.ParentId = a.Id;
        n.Title = 'Akritiv Test';
        System.assertEquals(n.Title,'Akritiv Test');
        insert n;
        c.getNotes( a.Id+'' );
        c.addNotesOnTransaction('test','test',txs.get(0).Id+'');
        c.doGet();
        c.getRecords('abc');
        Test.stopTest();
    }
}