/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is used for adding Email Attachments,
 *        Removing Attachments and saving the Email that
 *        mail Attachments.
 */
public with sharing class AddEmailAttachmentsController {
    public List<CustomAttachment> attachmentsList {get; set; }
    public String attachmentIdToRemove {get; set; }
    public string attachParentId {get; set; }
    public String attachmentsIds {get; set; }
    public String attachmentFileNames {get; set; }
    private Temp_Object_Holder__c temp;
    public String tempTxkey{get; set; }
    public String TxId{get; set; }
    public String accountId{get; set; }
    public List<Attachment> ListAttachmentDisp{get;set;}
    public List<String> transactionIds;
    public set<String> accountIds;
    private Temp_Object_Holder__c txsConfig;
    public String attachmentIds{get; set; }
    public Map<Id,Attachment> attachmentmap = new Map<Id,Attachment>();
    public AddEmailAttachmentsController() {
        attachmentsList = new List<CustomAttachment>();
        accountIds= new set<String>();
     //   tempTxkey = ApexPages.CurrentPage().getParameters().get('TxIdskey');
       // TxId = ApexPages.CurrentPage().getParameters().get('TxId');
        String tempId = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('id'));
        tempId  = tempId.replaceAll('_','%').replaceAll('blanktest',' ').replaceAll('atTEST2','@').replaceAll('coltest',':');
        system.debug('---------'+tempId);
         List<String> tempstr = tempId.split('akritiv---');
         if(tempstr != null && tempstr.size()>1){
             accountId = tempstr.get(0);
             tempTxkey = tempstr.get(1);
             
         }else{
             TxId =  tempstr.get(0);
         }
         
         attachmentIds = ApexPages.CurrentPage().getParameters().get('attachmentIds');
         transactionIds = new List<String>();
         if(tempTxkey != null && tempTxkey != ''){ 
                
                txsConfig = [select value__c from Temp_Object_Holder__c where key__c =:tempTxkey limit 1];
                //if(txsConfig !=null && txsConfig.value__c != null &&  txsConfig.value__c != '')
                if(txsConfig !=null){
                   if(txsConfig.value__c != null && txsConfig.value__c != ''){
                        transactionIds = txsConfig.value__c.split(',');
                   }
                }
            }
            if(TxId != null && TxId != ''){
                transactionIds.add(TxId);
            }
             List <Transaction__c> txlst1 = [Select Id,Name,Account__c From Transaction__c a where a.id =: transactionIds];
            
                            
                            if(txlst1.size() > 0){
                                for ( Transaction__c tx1 : txlst1  ) {
                                   
                                accountIds.add(tx1.Account__c);
                                }
                        }
                        if( accountId  != null){
                            accountIds.add(accountId);
                        }
        attachmentmap = new Map<Id,Attachment>([select id, Name,ParentId,OwnerId,Parent.Name,Owner.Name,LastModifiedDate from Attachment where ParentId In :accountIds OR ParentId in :transactionIds]);
       if(attachmentmap != null && attachmentmap.values().size() >0){
            ListAttachmentDisp = (List<Attachment>) attachmentmap.values();
          }
    }
    // Method used to get Email Attachment
    // return - Attachment
    public Attachment attach {
        get {
            if(attach==null)
                attach = new Attachment();
            return attach;
        } set;
    }

    public class CustomAttachment {
        public String attachmentName {get; set; }
        public Id attachmentId {get; set; }
        // Constructor used to set CustomAttachment values
        // param - Id attachId, String attachName
        public CustomAttachment(Id attachId, String attachName) {
            this.attachmentName = attachName;
            this.attachmentId = attachId;
        }
    }
    // Method is use for adding Email Attachments
    // return type : Pagereference
    public Pagereference addAttachment() {
        if(temp == null) {
            temp = new Temp_Object_Holder__c();
            
            //========================================================================================== 
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Temp_Object_Holder__c.fields.getMap();
           for (String fieldToCheck : m.keyset()) {
                System.debug('=======fieldToCheck======'+fieldToCheck);
              // Check if the user has create access on the each field
              if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
                  System.debug('=======fieldToCheck2======'+fieldToCheck);
                  if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                    System.debug('=======fieldToCheck in======'+fieldToCheck);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                //   return null;
                  }
              }
            }
            //========================================================================================
            
            insert temp;
            attachParentId = temp.id;
        }
          
        if(attach == null || attach.Name == null || attach.body == null) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR,Label.Message_Select_File_To_Attach));
            return null;
        }
      
        Attachment a = new Attachment();
        a.Name = attach.Name;
        a.Body = attach.Body;
        a.ParentId = attachParentId;
        
        //========================================================================================== 
           Map<String,Schema.SObjectField> m = Schema.SObjectType.Attachment.fields.getMap();
           for (String fieldToCheck : m.keyset()) {
                System.debug('=======fieldToCheck======'+fieldToCheck);
              // Check if the user has create access on the each field
              if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
                  System.debug('=======fieldToCheck2======'+fieldToCheck);
                  if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                    System.debug('=======fieldToCheck in======'+fieldToCheck);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                            'Insufficient access'));
                 //  return null;
                  }
              }
            }
            //========================================================================================
        
        insert a;
        
        attachmentsList.add(new CustomAttachment(a.Id, a.Name));
        
        
        attach.body = null;
      // PageReference page = new PageReference('/apex/addemailattachments?accId='+accountId+'&TxIdskey='+tempTxkey);

      //  page.setRedirect(true);
        
      //  return page;
        return null;
    }
    public Pagereference addAttachment2() {
    List<Id> IDs;
        if (attachmentIds != null && attachmentIds != ''){
             IDs = attachmentIds.split(',');
             for(String id:IDs ){
                 Id tempid = (Id)attachmentmap.get(id).Id;
                  system.debug('----------'+tempid);
                 String attName =attachmentmap.get(id).Name;
                 system.debug('----------'+attName);
                 attachmentsList.add(new CustomAttachment(tempid ,attName ));
             }
         }
         system.debug('----------'+attachmentsList);
    return null;
    }
    // Method is use for Removing Email Attachments
    // return type : Pagereference 
    public Pagereference removeAttachment() {
        // id will like "pg:frm:pb:j_id95:j_id96:j_id99:0:attachmentFile"
        String[] attachIdTokens = attachmentIdToRemove.split(':');
        CustomAttachment ca = attachmentsList.get(Integer.valueOf(attachIdTokens[attachIdTokens.size()-2]));

        Attachment a = new Attachment(id=ca.attachmentId);
        //================================================================
    if(!Attachment.sObjectType.getDescribe().isDeletable()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                    'Insufficient access'));
            return null;
      }
    //=================================================================

        delete a;
        attachmentsList.remove(Integer.valueOf(attachIdTokens[attachIdTokens.size()-2]));

        return null;
    }
    // Method is use for Saving Email Attachments
    // return type : Pagereference 
    public Pagereference saveAttachments() {
        attachmentsIds = '';
        attachmentFileNames = '';
         List<Id> IDs;
          system.debug('-------attachmentIds ---'+attachmentIds);
        if (attachmentIds != null && attachmentIds != ''){
             IDs = attachmentIds.split(',');
             for(String id:IDs ){
                 Id tempid = (Id)attachmentmap.get(id).Id;
                  system.debug('----------'+tempid);
                 String attName =attachmentmap.get(id).Name;
                 system.debug('----------'+attName);
                 attachmentsList.add(new CustomAttachment(tempid ,attName ));
             }
         }
        for(CustomAttachment ca : attachmentsList) {
            attachmentsIds = attachmentsIds + ca.attachmentId+ ',';
            attachmentFileNames = attachmentFileNames + ca.attachmentName + ',';
        }

        return null;
    }

}