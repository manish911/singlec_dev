/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for Edit,update contact.
 */
public with sharing class EditContactController {

    //contact id
    private string contactId {get; set; }
    private Contact contact {get; set; }
    private boolean updateOperation {get; set; }

    public EditContactController()
    {
        try {
            contactId = ApexPages.currentPage().getParameters().get('id');
            if(contactId != null)
            {
                updateContact(contactId);
                updateOperation = true;
            }
            updateOperation = false;
        } catch(Exception e)
        {
            throw e;
        }
    }
    
    // method is used to update contact 
    // return type : void 
    // parameters : contactId - String 
    private void updateContact(String contactId)
    {
        Map<String, Schema.SObjectField> contactFieldsMap = Schema.SObjectType.Contact.fields.getMap();
        Contact ct = getContact(contactId, contactFieldsMap);
        if(ct != null)
        {
            contact = ct;
        }

    }
    // method is used to get contact 
    // return type : Contact
    // parameters : contactId - String , contactFieldsMap Map<String, Schema.SObjectField>
    private Contact getContact(String contactId, Map<String, Schema.SObjectField> contactFieldsMap )
    {
        Contact ct = null;
        String expr = '';
        try {
            for (String fieldName : contactFieldsMap.keySet())
            {
               // Schema.SObjectField sobj = contactFieldsMap.get(fieldName);
                //cannot use key in the expression as key doesnt contain namespace and is in lower case.
                
                Schema.SObjectField sobj; 
                
                if(fieldName != 'ShippingAddress' && fieldName != 'MailingAddress' && fieldName != 'OtherAddress'){
                    sobj = contactFieldsMap.get(fieldName);
                 
                   expr += ', c.' + sobj;
                }               
            }
            expr = expr.replaceFirst(',', ''); // remove leading comma
            String qry = 'Select ' + expr + ' from Contact c where c.id=:contactId';
            List<Contact> contactList =  Database.query(String.escapeSingleQuotes(qry) );
            if(contactList != null || contactList.size() > 0)
            {
                ct = contactList.get(0);
            }
        } catch(Exception e)
        {
            throw e;
        }
        return ct;
    }
    // method is used to update contact 
    // return type : pagereference
    public PageReference updateContact()
    {

        return null;
    }
}