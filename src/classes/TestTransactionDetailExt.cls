/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for TransactionDetailExt.
 */
@isTest
private class TestTransactionDetailExt
{
    static testMethod void testCase1()
    {
        Transaction__c tx = new Transaction__c();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (tx);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTabTx__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTabTx__c =1;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTabTx__c = 1;
            insert newUserPref;
        }

        Test.startTest();

        TransactionDetailExt txDetailObj = new TransactionDetailExt(apexObj);
        System.assertEquals(txDetailObj.selectedTabTx,'transactionsTab');

        Test.stopTest();

    }
    static testMethod void testCase2()
    {
        Transaction__c tx = new Transaction__c();
        ApexPages.StandardController apexObj = new ApexPages.StandardController (tx);

        List<User_Preference__c> tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTabTx__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if(tempUserPref.size() > 0 ) {
            tempUserPref[0].SelectedTabTx__c =2;
            update tempUserPref;
        }else{
            User_Preference__c newUserPref = new User_Preference__c();
            newUserPref.Name =  UserInfo.getUserName();
            newUserPref.User__c = UserInfo.getUserId();
            newUserPref.SelectedTabTx__c = 2;
            insert newUserPref;
        }

        Test.startTest();

        TransactionDetailExt txDetailObj = new TransactionDetailExt(apexObj);
        System.assertEquals(txDetailObj.selectedTabTx,'txNotesTab');

        Test.stopTest();
    }

}