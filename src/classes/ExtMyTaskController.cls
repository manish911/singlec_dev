/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class is used for fetching, finding , setting , creating
 *         the task.
 */
public with sharing class ExtMyTaskController {

    public List < Task > tasks = new List < Task >();
    public String sortField {get; set; }
    public String previousSortField {get; set; }
    List<SelectOption> taskfilters = new List<SelectOption>();
    public String lastFilter {get; set; }
    public Id SelectedUser{get; set; }
    public list<SelectOption> OwnUser {get; set; }  
    
    public ExtMyTaskController () {
           //to limit users in query-------------------------------------------------------------------
            List<User> owner1=new list<User>();
            SysConfig_Singlec__c sysConfig=SysConfig_Singlec__c.getOrgDefaults();
            if(String.isBlank(sysConfig.Account_Tab_Profiles__c))
            {
               owner1 = [select id,name from user where profile.name != 'Chatter Free User' ORDER BY Name ASC limit 999];
            }
            else
            {    string notSeparated = sysConfig.Account_Tab_Profiles__c;
                system.debug('===notseparated==='+notSeparated);
               
                list<String> separ= new list<String>();
                separ = notSeparated.split(','); 
                Set<String> setsepar = new Set<String>(separ);
                system.debug('===setSepar==='+separ);
               
                 owner1 =[select id,name from user where profile.name in:setsepar ORDER BY Name ASC limit 999];
                system.debug('***userlist***'+owner1);
            }
         //to limit users in query-----------------------------------------------------------------------    
         //old query:  List<User> owner1 = [select id,name from user where profile.name != 'Chatter Free User'];
         
         OwnUser = new List<selectoption>();
         OwnUser.add(new Selectoption('--Select--','--Select--'));
         for(User u : owner1){
          OwnUser.add(new Selectoption(u.Id,u.Name));
        // OwnUser.add(selectoptions(u.Id,u.Name));
         }

       // loadFilters ();
        //findTasks();
    }
    // Method load values of default filter Overdue for Task which are generated 
    // Return Type - Void
    // No parameters are passed in this method 
    private void loadFilters () {

        lastFilter = 'Overdue';

    }
    // Returns list of Task to be performed by user 
    // Return Type - List of Tasks 
    // No parameters are passed in this method 
    public List < Task > getTasks () {

        return tasks;
    }
    // Method set Task for User 
    // Return Type - Void 
    // Parameters - List of Task which are generated 
    public void setTasks( List < Task > tsk ){

        this.tasks = tsk;
    }
    // Method filters out tasks on the basis of filter criteria selected by user 
    // Return Type - Void
    // No parameters are passed in this method 
    public void  findTasks() {

        if ( lastFilter == 'Overdue') {

            Date d = Date.today();
            tasks = [ select AccountId, Account.Name, ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and ActivityDate < :d and  status != 'Completed' ];

        }else if ( lastFilter == 'Today') {

            Date d = Date.today();
            tasks = [ select AccountId,Account.Name, ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and ActivityDate =:d and  IsDeleted = false];

        }else if ( lastFilter == 'Today + Overdue') {

            Date d = Date.today();
            tasks = [ select AccountId, Account.Name,ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and ActivityDate <=:d and  IsDeleted = false];

        }else if ( lastFilter == 'Tomorrow') {

            Date d = Date.today() + 1;
            tasks = [ select AccountId,Account.Name, ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and ActivityDate =:d and  IsDeleted = false];

        }else if ( lastFilter == 'Next 7 Days') {

            Date d1 = Date.today();
            Date d7 = Date.today() + 7;
            tasks = [ select AccountId,Account.Name, ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and ActivityDate >:d1 and ActivityDate <=:d7 and  IsDeleted = false];

        }else if ( lastFilter == 'This Month') {

            Date d = Date.today();
            Integer month = d.month();
            Integer year = d.year();
        
        }else if ( lastFilter == 'All Open') {

            tasks = [ select AccountId,Account.Name, ActivityDate, CallType, CreatedDate, Description, Id, Status, Priority, Rating__c, Subject, OwnerId From Task t where OwnerId =: UserInfo.getUserId() and isClosed = false  and  IsDeleted = false];

        } else {

            tasks = new List < Task >();
        }

    }
    // Method provides functionality to user on start working on particular task assign 
    // Return Type - Pagereference
    // No parameters are passed in this method 
    public Pagereference startWorking() {

        Temp_Object_Holder__c temoh = new Temp_Object_Holder__c();
        temoh.value__c = getStringTasks();
        temoh.key__c = UtilityController.getRandomString(10);
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Temp_Object_Holder__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert temoh;

        PageReference p = new PageReference('/apex/ExtendedConsoleView?taskkey='+temoh.key__c+'&recordtype=TASKS');

        return p;
    }
    // Returns Id of tasks assigned to User
    // Return Type - String 
    // No parameters are passed in this method 
    private String getStringTasks () {

        String s ='';
        Integer i = 0;
        for ( Task t : tasks ) {

            if ( i == 0 ) {

                s = t.Id;
            } else {

                s = s + ','+t.Id;
            }

            i++;
        }

        return s;
    }

    public void doSort(){

        String order = 'asc';
        /*This checks to see if the same header was click two times in a row, if so
           it switches the order.*/
        if(previousSortField == sortField) {
            order = 'desc';
            previousSortField = null;
        }else{
            previousSortField = sortField;
        }

        sortList(tasks,sortField,order);
    }
    public void searchTasks() {

        findTasks();
    }

    public Pagereference createNew() {

        return new Pagereference('/00T/e?retURL=/apex/ExtMyTaskView?sfdc.tabName=01rA0000000LULy');
    }
    public static void sortList(List<sObject> items, String sortField, String order){
        /*I must give credit where it is due as the sorting algorithm I am using is the
           one supplied by Andrew Waite here: http://blog.sforce.com/sforce/2008/09/sorting-collect.html */
        
        try {
        Boolean isSortFieldReference = false;
        Map<Id,String> referenceName;

        /*Determine the type of the field that needs to be sorted, if it is a
           reference we will want sort by the name of the related object, not the
           ID itself*/
        if(items[0].getSObjectType().getDescribe().fields.getMap().get(sortField).getDescribe().getType().Name() == 'REFERENCE') {
            isSortFieldReference = true;
            referenceName = new Map<Id,String>();

            /*Determine the type of this object and populate the Id to Name map*/
            Set<Id> referenceIds = new Set<Id>();
            for(sObject s : items) {
                referenceIds.add((Id)s.get(sortField));
            }

            String objectID = (String)items[0].get(sortField);
            String prefix = objectID.substring(0,3);
            String objectType;
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            for(Schema.SObjectType s : gd.values()) {
                if(prefix == s.getDescribe().getKeyPrefix()) {
                    objectType = s.getDescribe().Name;
                }
            }

            //Query the related objects for the name and populate the Id -> Name map
            String queryString = 'select Id, Name from ' + objectType + ' where ID IN :referenceIDs';
            for(sObject s : Database.query(queryString )) {
                referenceName.put((Id)s.get('Id'),(String)s.get('Name'));
            }
        }

        /*Declare a list that will contain the sorted results. I think this is one of the
           coolest parts of this method as the system will not let you declare a list of
           sObjects (List<sObject> objects = new List<sObjects>();) but using a
           wrapper class you can bypass this system limitation to create this type of list */
        List<cObject> resultList = new List<cObject>();

        //Create a map that can be used for sorting
        Map<object, List<cObject>> objectMap = new Map<object, List<cObject>>();

        for(sObject ob : items) {
            if(isSortFieldReference == false) {
                if(objectMap.get(ob.get(sortField)) == null) {
                    objectMap.put(ob.get(sortField), new List<cObject>());
                }
                cObject o = new cObject(ob);
                objectMap.get(ob.get(sortField)).add(o);
            }else{
                if(objectMap.get(referenceName.get((Id)ob.get(sortField))) == null) {
                    objectMap.put(referenceName.get((Id)ob.get(sortField)), new List<cObject>());
                }
                cObject o = new cObject(ob);
                objectMap.get(referenceName.get((Id)ob.get(sortField))).add(o);
            }
        }

        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();

        for(object key : keys) {
            resultList.addAll(objectMap.get(key));
        }

        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc') {
            for(cObject ob : resultList) {
                items.add(ob.obj);
            }
        }else if(order.toLowerCase() == 'desc') {
            for(integer i = resultList.size()-1; i >= 0; i--) {
                items.add(resultList[i].obj);
            }
        }
    }
    catch( Exception e){
        }
    }

    public class cObject {
        sObject obj {get; set; }

        public cObject(sObject obj){
            this.obj = obj;
        }
    }
}