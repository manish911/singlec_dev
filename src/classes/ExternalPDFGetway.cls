public with sharing class ExternalPDFGetway{

    String packageHostAddress {get; set; }
    public static String SuccessInvoiceNumbers;
    public static Blob getInvoicePdf( String tempkey ) {
        if(tempkey != null ){
           //Map < String ,Transaction__c > trxmap = new Map < String , Transaction__c>();
         
            Temp_Object_Holder__c objtemp = [select ID,Key__c,value__c from Temp_Object_Holder__c where Key__c =: tempkey ];
           
            
            string trxList = objtemp.value__c;
         
            List<ID> txids = new List<ID>();
            for(String onetrx : trxList.split(',')) {
          
                if(onetrx !='') {
                    txids.add(onetrx);
                }
            }
            
            
            List<Transaction__c> objTrxlist = [select id,Name, Transaction_key__c from Transaction__c where Id in :txids   ];
          
        
            if(objTrxlist.size() == 1){
                return getInvoice(objTrxlist.get(0).Name);
            }else if(objTrxlist.size() > 1){
              
                String txnumbers = '';
                for(Transaction__c objtx : objTrxlist ){
                    txnumbers = txnumbers + ',' + objtx.name ;
                }
               
                return getMultipleInvoice(txnumbers);

            }else{

            }
        }
        return null;    
    }
    public static Blob getInvoice( String invoicenumber ){
    
        String url = ExternalPDFGetway.getcompleteURL(invoicenumber);
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');

        HttpResponse res = h.send(req);

        return EncodingUtil.base64Decode(res.getBody());
        
    }
    
    
    public static Blob getMultipleInvoice( String  invoicenumbers ){
    
        String url = ExternalPDFGetway.getcompleteURL(invoicenumbers);
        
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');

        HttpResponse res = h.send(req);

        return EncodingUtil.base64Decode(res.getBody());
        
    
    }
    
     public static List<Blob> getAllOpenInvoice( String transactionNums ) {
        SuccessInvoiceNumbers = '';
        List<Blob> b1 = new List<Blob>();

        String[] invNo = transactionNums.split(',');

        for(String s : invNo){
            //String st = getcompleteURL(s);
            try{
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(BuildPageReferenceToInvoiceGateway(s).getUrl());
                req.setMethod('GET');
                HttpResponse res = h.send(req);
                b1.add(EncodingUtil.base64Decode(res.getBody()));
                SuccessInvoiceNumbers = SuccessInvoiceNumbers  + ',' +  s;
            }catch(Exception ex){

                break;
            }            
        }
        return b1;
    }
    
    public static PageReference BuildPageReferenceToInvoiceGateway(String trxnum)
    {
        String host = '';
       
        String uri = '';
        String KeyParam = ConfigUtilController.getKeyParameter();
        String apiEnterpriseServerUrl = '';
        String invoiceGatewayUrl = '';
        
            
        if(ConfigUtilController.getHost() != null && ConfigUtilController.getHost() != '')
            host = ConfigUtilController.getHost();
            
        if(ConfigUtilController.getPort() != null && ConfigUtilController.getPort() != '')
            apiEnterpriseServerUrl  = ConfigUtilController.getPort();
            
        if(ConfigUtilController.getURI() != null && ConfigUtilController.getURI() != '')
            uri = ConfigUtilController.getURI();
            
        if(ConfigUtilController.getKeyParameter() != null && ConfigUtilController.getKeyParameter() != '')
            keyParam = ConfigUtilController.getKeyParameter();
               
        
        // apiEnterpriseServerUrl = 'https://c.cs0.visual.force.com/services/Soap/c/14.0/00DT0000000JjCa';
        
        invoiceGatewayUrl = 'https://'+host+'/'+uri;
        boolean useCustomerLocale = false;
        
        PageReference page = new PageReference( invoiceGatewayUrl );
        page.getParameters().put( keyParam , trxnum);
        page.getParameters().put( 'systemId', 'Akritiv' );
        page.getParameters().put( 'corpaction', 'ViewInvoice' );
        page.getParameters().put( 'cmd', 'CORPSFDCSSO' );
        page.getParameters().put( 'sessionId', UserInfo.getSessionId() );
        page.getParameters().put( 'sessionUrl', apiEnterpriseServerUrl  );
        page.getParameters().put( 'customerPreferredLocale', useCustomerLocale ? 'yes' : 'no' );
        page.getParameters().put( 'encoding', 'base64'  );
        

        
        return page;
    }
    
    
    public static String getcompleteURL(String trxnum){
        
        String host = ConfigUtilController.getHost();
        String port = '';
        String uri = ConfigUtilController.getURI();
        String KeyParam = ConfigUtilController.getKeyParameter();
        String completeurl='';
        //String apiEnterpriseServerUrl = EncodingUtil.urlEncode('https://c.cs0.visual.force.com/services/Soap/c/14.0/00DT0000000JjCa', 'UTF-8');
        String apiEnterpriseServerUrl = '';  // = EncodingUtil.urlEncode('https://c.na7.visual.force.com/services/Soap/c/14.0/00DA0000000gQk6', 'UTF-8');
        
        
        if(ConfigUtilController.getSystemConfigObj() != null){
            
            if(ConfigUtilController.getHost() != null && ConfigUtilController.getHost() != '')
                host = ConfigUtilController.getHost();
            if(ConfigUtilController.getPort() != null && ConfigUtilController.getPort() != '')
                apiEnterpriseServerUrl = ConfigUtilController.getPort();
            if(ConfigUtilController.getURI() != null && ConfigUtilController.getURI() != '')
                uri = ConfigUtilController.getURI();
            if(ConfigUtilController.getKeyParameter() != null && ConfigUtilController.getKeyParameter() != '')
                keyParam = ConfigUtilController.getKeyParameter();
            
            if(host != null && host != '' && uri != null && uri != '' && keyParam != null && keyParam != '' ){
               // completeurl = 'https://'+host+'/'+uri+'?'+keyParam+'='+trxnum+'&systemId=Akritiv&corpaction=ViewInvoice&cmd=CORPSFDCSSO&sessionId='+UserInfo.getSessionId()+'&sessionUrl='+apiEnterpriseServerUrl+'&customerPreferredLocale=no&encoding=base64';
                completeurl = 'http://'+host+'/'+uri+'?'+keyParam+'='+trxnum;
            }
        }
        
        return completeurl;
    }  
        
    
    
    static testMethod void testExternalPDFGetway() {  
          
          List<Transaction__c> txList = new List<Transaction__c>();
          Account tempAccount = DataGenerator.accountInsert();
          txList = DataGenerator.transactionInsert(tempAccount, 1);
          String temobj1 = DataGenerator.tempObjectInsert(tempAccount,txList);
        
             
        Test.startTest();
            ExternalPDFGetway controller = new ExternalPDFGetway();
            //Blob b = ExternalPDFGetway.getInvoicePdf(temobj1);
            System.assertEquals(txList.get(0).account__c,tempAccount.Id);
            ExternalPDFGetway.getMultipleInvoice(txList.get(0).Name);
            ExternalPDFGetway.getAllOpenInvoice(txList.get(0).Name);
        Test.stopTest();
       
    } 
    
    static testMethod void testExternalPDFGetway3() {  
          List<Transaction__c> txList = new List<Transaction__c>();
          Account tempAccount = DataGenerator.accountInsert();
          txList = DataGenerator.transactionInsert(tempAccount, 1);
          String temobj1 = DataGenerator.tempObjectInsert(tempAccount,txList);
        
          Test.startTest();        
             ExternalPDFGetway.getAllOpenInvoice(txList.get(0).Name);
             ExternalPDFGetway.getInvoicePdf(temobj1);
             System.assertEquals(txList.get(0).account__c,tempAccount.Id);      
          Test.stopTest();          
    } 
    
    static testMethod void testExternalPDFGetway1() {  
          List<Transaction__c> txList = new List<Transaction__c>();
          Account tempAccount = DataGenerator.accountInsert();
          txList = DataGenerator.transactionInsert(tempAccount, 2);
          String temobj1 = DataGenerator.tempObjectInsert(tempAccount,txList);
        
          Test.startTest();        
             ExternalPDFGetway.getAllOpenInvoice(txList.get(0).Name);
             ExternalPDFGetway.getInvoicePdf(temobj1);
             System.assertEquals(txList.get(0).account__c,tempAccount.Id);      
          Test.stopTest();          
    } 
    
    static testMethod void testExternalPDFGetway2() {  
          List<Transaction__c> txList = new List<Transaction__c>();
          Account tempAccount = DataGenerator.accountInsert();
          txList = DataGenerator.transactionInsert(tempAccount, 1);
          String temobj1 = DataGenerator.tempObjectInsert(tempAccount,txList);
                
        Test.startTest();
            ExternalPDFGetway.getMultipleInvoice(txList.get(0).Name);
            System.assertEquals(txList.get(0).account__c,tempAccount.Id);      
        Test.stopTest();         
    } 
    

}