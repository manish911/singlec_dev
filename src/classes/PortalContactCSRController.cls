/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This class is used for adding tasks to Portal User Account
 *        Also an email is sent to Portal User when tasks is added
 */
public with sharing class PortalContactCSRController {
    public List<User> currUser;
    public String defaultMail {get; set; }
    public String emailSubject {get; set; }
    public String emailBody {get; set; }
    private String[] toAddresses = new String[] {};
    public boolean isSuccess {get; set; }
    public List<Account> acc;
    public String currUserAccountId {get; set; }

    public PortalContactCSRController()
    {
        isSuccess = false;
        currUser =new List<User>([select name,Id, ContactId, Contact.AccountId from User where id=: Userinfo.getUserId()]);
        acc = new List<Account>([select name,Id, owner.Id, ownerId from Account where id=: currUser.get(0).Contact.AccountId]);
        List<User> newUser = new List<User>();
        newUser = [select email from User where Id =: acc.get(0).ownerId];
        if(newUser != null && newUser.size() > 0)
        {
            defaultMail = newUser[0].email;
            toAddresses.add(defaultMail);
        }
        currUserAccountId = currUser.get(0).Contact.AccountId;

    }
    // This method redirects Portal User to page for Sending Email  
    // Return Type - PageReference
    // No parameters are passes in this method
    public Pagereference redirectToEmailPage()
    {
        
        if(emailSubject == null || emailSubject.trim() == '')
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, Label.Message_Empty_Subject);
            ApexPages.addMessage(myMsg);
            isSuccess = false;
            return null;
        }
        if(toAddresses != null && toAddresses.size() > 0)
        {
        //--email mechanism with mandrill--start-----
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            MandrillEmailService objMandrill = new MandrillEmailService();
            //--email mechanism with mandrill--end-----
            mail.setPlainTextBody(' Customer Portal Inquiry For Account ' + acc.get(0).name + ' from ' + currUser.get(0).name + '\n' + emailBody);
        //--email mechanism with mandrill--start-----            
            mail.setSubject('Portal: ' + emailSubject);
            objMandrill.strSubject = 'Portal: ' + emailSubject;
            mail.setToaddresses(toAddresses);
            objMandrill.strToEmails = toAddresses;             
        //--email mechanism with mandrill--end-----
            mail.setSaveAsActivity(false);
        //--email mechanism with mandrill--start-----            
            Messaging.sendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            objMandrill.SendEmail();
                    //--email mechanism with mandrill--end-----
        }
        else
        {
        }

        Task newTask = new Task();
        newTask.Subject = 'Portal: ' + emailSubject;
        newTask.OwnerId = acc.get(0).ownerId;
        newTask.whatId = acc.get(0).Id;
        newTask.IsVisibleInSelfService=true;

        newTask.Description = 'Customer Portal Inquiry For Account ' + acc.get(0).name + ' from ' + currUser.get(0).name + '\n' + emailBody;
        //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Task.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return null;
              }
          }
        }
        //========================================================================================
        insert newTask;

        isSuccess = true;
        return null;

//       return new PageReference(page.AccountDetailPage.getUrl() + '?id=' + currUser.Contact.AccountId) ;
        return null;
    }
    // method is used to cancel 
    // return type : Pagereference
    public Pagereference Cancel()
    {
        return new PageReference(page.AccountDetailPage.getUrl() + '?id=' + currUser.get(0).Contact.AccountId);
    }
}