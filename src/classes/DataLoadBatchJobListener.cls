/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : Email services are automated processes that use Apex classes
 *         to process the contents, headers, and attachments of inbound
 *         email.
 */
global class DataLoadBatchJobListener implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        String myPlainText = email.plainTextBody;

        String orgId = Userinfo.getOrganizationId();
        String subject = email.Subject;
        String batch = null;
        String ss = null;
        String batchInfoId = null;
        String batchInfoSourcesystem = '';
        DateTime emailDate = null; 
        myPlainText = myPlainText.trim();
        

            if ( myPlainText != null && myPlainText.contains(orgId)) {

                
                Integer indexofend = myPlainText.indexOf('<END>');
                Integer lenghtOfString = myPlainText.length();
                myPlainText = myPlainText.subString(indexofend,lenghtOfString );
                indexofend = myPlainText.lastIndexOf('<END>');
                lenghtOfString = myPlainText.length();
                myPlainText = myPlainText.subString(0,indexofend+'<END>'.length());
                String [] mainar = myPlainText.split('<END>');

                if ( mainar == null || mainar.size() != 2 ){
                
                    return result ;
                }
                String [] arrays = mainar[1].split( orgId );
                
                if ( arrays != null && arrays.size() == 4 ) {

                    batch = arrays[0];
                    ss = arrays[1];
                    batchInfoId  = arrays[2];
                    emailDate = DateTime.valueOf(arrays[3].replace('\n',' '));
                }
            }

        
        if ( subject != null && (subject.equals('AR Close Process') || subject.contains('AR Close Process'))) {
            integer calc = 0;
            DateTime currentDate = DateTime.Now();
            string currentDate1 = string.valueOf(currentDate.formatGmt('yyyy-MM-dd')+' '+currentDate.formatGmt('HH:mm:ss'));
            DateTime currentDate2 = DateTime.valueOf(currentDate1);
            
            if(emailDate != null){
            
                calc = integer.valueOf(currentDate2.getTime() - emailDate.getTime());
            }
            
            Decimal hours = calc/(1000.0*60.0*60.0);
            hours = Math.abs(hours);
            //integer calculateDate = integer.valueOf(currentDate) - integer.valueOf(emailDate) ;
            
            if ( batchInfoId  != null && batch != null && ss != null ) {
                
               //* Data_Load_Batch__c batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c ,  d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
               List<Data_Load_Batch__c> batchInfo = null;
               if ( 'NOBATCHINFO' != batchInfoId ){
                  batchInfo  = new List<Data_Load_Batch__c>([ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId]);
                                      
               }
            
               if(hours > 2){
                   batchInfo.get(0).Batch_Job_Status__c = 'Aborted';
                   
                   //==========================================================================================  
                       list<String> lstFields = new list<String>{'Source_System__c', 'Batch_Number__c', 'Aging_Job_Status__c','Aging_Job_Start_Time__c','Aging_Job_End_Time__c','Percentage_Close__c','Batch_Job_Status__c','Batch_Job_Start_Time__c','Batch_Job_End_Time__c'};
                       Map<String,Schema.SObjectField> m = Schema.SObjectType.Data_Load_Batch__c.fields.getMap();
                       for (String fieldToCheck : lstFields) {
                          // Check if the user has create access on the each field
                          if(!m.get(fieldToCheck).getDescribe().isCalculated()){
                             if (!m.get(fieldToCheck).getDescribe().isUpdateable()) {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                                        'Insufficient access'));
                                return null;
                              }
                           }
                        }
                    //========================================================================================
                   
                   update batchInfo;
            
                   String toAddress = ConfigUtilController.getBatchJobNotificationEmailId();
                   
                    if (toAddress != null ) {
                    //--email mechanism with mandrill--start-----
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        MandrillEmailService objMandrill = new MandrillEmailService();
                        
                        mail.setToAddresses(new String[] {toAddress});
                        objMandrill.strToEmails = new String[] {toAddress};  

                        mail.setReplyTo(toAddress);
                        objMandrill.strReplyTo = toAddress;
                                                     
                        mail.setSenderDisplayName('Batch Processing :');
                        objMandrill.strFromName = 'Batch Processing :';
                                            //--email mechanism with mandrill--end-----       
                    //--email mechanism with mandrill--start-----                                        
                        mail.setSubject( 'Aborting Closing Process');
                        objMandrill.strSubject = 'Aborting Closing Process';
                        //--email mechanism with mandrill--end-----                                    
                        String mailBodyText ='<END>'+batch +''+Userinfo.getOrganizationId()+''+ss +''+Userinfo.getOrganizationId()+''+batchInfoId+'<END>';                   
                        
                       // String mailBodyText ='please run closing process manually';
                        mail.setPlainTextBody(mailBodyText);
                   
                   //--email mechanism with mandrill--start-----                                    
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        objMandrill.SendEmail();
                      //--email mechanism with mandrill--end-----                                      
                     }
                    
               }else { 
                if (batchInfo != null && (batchInfo.get(0).Batch_Job_Status__c == null || batchInfo.get(0).Batch_Job_Status__c != 'Aborted')) {
               
                Integer dataLoadBatchSize = 200;
                BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
                if(batchConfigObj != null) {
                    dataLoadBatchSize = (batchConfigObj.DataLoad_Batch_Size__c != null && batchConfigObj.DataLoad_Batch_Size__c > 0 && batchConfigObj.DataLoad_Batch_Size__c < 201 ? batchConfigObj.DataLoad_Batch_Size__c.intValue() : 200);
                }
                BatchAfterFullLoadController bdc = new BatchAfterFullLoadController(batch.trim(), ss.trim());
            
                if ( batchInfo != null ) {

                    bdc.batchInfoId = batchInfo.get(0).Id;
                }
                
                Database.executeBatch(bdc, dataLoadBatchSize);
            } else if ( batch != null && ss != null  ) {
            
                Integer dataLoadBatchSize = 200;
                BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
                if(batchConfigObj != null) {
                    dataLoadBatchSize = (batchConfigObj.DataLoad_Batch_Size__c != null && batchConfigObj.DataLoad_Batch_Size__c > 0 && batchConfigObj.DataLoad_Batch_Size__c < 201 ? batchConfigObj.DataLoad_Batch_Size__c.intValue() : 200);
                }
                BatchAfterFullLoadController bdc = new BatchAfterFullLoadController(batch.trim(), ss.trim());
                
                Database.executeBatch(bdc, dataLoadBatchSize);
            
            }
        }
        
        if (batchInfo != null &&  batchInfo.get(0).Batch_Job_Status__c == 'Aborted') {
             
                    //no need to run aging in  this case, just notify external email service of this finish
                    
                    String toAddress = ConfigUtilController.getBatchAutoCloseExternalEmailNotificationService();
                    
                    //notify system of this finish
                    if (toAddress != null ) {
                    //--email mechanism with mandrill--start-----
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        MandrillEmailService objMandrill = new MandrillEmailService();
                        
                        mail.setToAddresses(new String[] {toAddress});
                        objMandrill.strToEmails = new String[] {toAddress};              

                        mail.setReplyTo(toAddress);
                        objMandrill.strReplyTo = toAddress;
  
                        mail.setSenderDisplayName('Batch Processing :');
                        objMandrill.strFromName = 'Batch Processing :';
                       //--email mechanism with mandrill--end-----  
                    
                    //--email mechanism with mandrill--start-----        
                        mail.setSubject( 'Akritiv Close Process');
                        objMandrill.strSubject = 'Akritiv Close Process';
                     //--email mechanism with mandrill--end-----       
                        String mailBodyText ='<END>'+batch +''+Userinfo.getOrganizationId()+''+ss +''+Userinfo.getOrganizationId()+''+batchInfoId+'<END>';                   
                        
                
                        mail.setPlainTextBody(mailBodyText);
                   
                    //--email mechanism with mandrill--start-----       
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        objMandrill.SendEmail();
                         //--email mechanism with mandrill--end-----       
                     }
                }
            }
        
        }
        
        if ( subject != null && (subject.equals('Aging Process') || subject.contains('Aging Process') ) ) {
        
             List<Data_Load_Batch__c> batchInfo = null;
               if ( batchInfoId != null && 'NOBATCHINFO' != batchInfoId ){
                  batchInfo  = new List<Data_Load_Batch__c>([ Select d.Source_System__c, d.Id,Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId]);
                   batchInfoSourcesystem = batchInfo.get(0).Source_System__c;                    
               }
             
             system.debug('==============>>'+ss);
             system.debug('==============>>'+batchInfoId);
             if (ss != null && ss != '' && batchInfoId == 'NOBATCHINFO'  ) {
                  BatchAgingCalculations bac = new BatchAgingCalculations(ss );
                //  bac.batchInfoId = batchInfoId;
                  Database.executeBatch(bac);
             }else if(ss != null && ss != '' && batchInfoId != null && 'NOBATCHINFO' != batchInfoId  ) {
                 BatchAgingCalculations bac = new BatchAgingCalculations(ss);
                 bac.batchInfoId = batchInfoId;
                  Database.executeBatch(bac);
             }else{
                 BatchAgingCalculations bac = new BatchAgingCalculations();
                  Database.executeBatch(bac);
             }
            
        }
        
        if ( subject != null && (subject.equals('Akritiv ADPD Process') || subject.contains('Akritiv ADPD Process') )) {
        
             List<Data_Load_Batch__c> batchInfo = null;
           if ( batchInfoId != null && 'NOBATCHINFO' != batchInfoId ){
                   batchInfo  = new List<Data_Load_Batch__c>([ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.Percentage_Close__c , d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, d.Batch_Job_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId]);
                   batchInfoSourcesystem = batchInfo.get(0).Source_System__c;                    
               }
             
            system.debug('==============>>'+ss);
             system.debug('==============>>'+batchInfoId);
             if (ss != null && ss != '' && batchInfoId == 'NOBATCHINFO') {
                  BatchADPDCalculations bac = new BatchADPDCalculations(ss);
                 
                  Database.executeBatch(bac);
             }else if(ss != null && ss != '' && batchInfoId != null && 'NOBATCHINFO' != batchInfoId  ) {
                 BatchADPDCalculations bac = new BatchADPDCalculations(ss);
                  bac.batchInfoId = batchInfoId;
                  Database.executeBatch(bac);
             
             }else{
                 BatchADPDCalculations bac = new BatchADPDCalculations();
                  Database.executeBatch(bac);
             }
               
             /*BatchADPDCalculations bac = new BatchADPDCalculations();
             
             if (batchInfoId != null && 'NOBATCHINFO' != batchInfoId  ) {
                 
                  bac.batchInfoId = batchInfoId;
             }
             Database.executeBatch(bac);*/
            
        } 
            
            
        
        return result;
    }
      static testMethod void testDataLoadBatchJobListener() {
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Data_Load_Batch__c dataloadObj = new Data_Load_Batch__c();
        
        Test.startTest();
        dataloadObj.Source_System__c = 'SAP';
        dataloadObj.Batch_Number__c = '20102503';
        //dataloadObj.Load_Type__c = 'FULL';
        dataloadObj.Run_Batch_Aging__c = false;
       //==========================================================================================  
       Map<String,Schema.SObjectField> m = Schema.SObjectType.Data_Load_Batch__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================

        insert dataloadObj;
        email.plainTextBody = '<END>'+'ew'+Userinfo.getOrganizationId()+''+dataloadObj.Source_System__c+''+Userinfo.getOrganizationId()+''+dataloadObj.Id+''+Userinfo.getOrganizationId()+''+(System.Now()-1000)+'<END>';
        email.fromAddress ='bhavik@akritiv.com';
        email.Subject = 'Aging Process';
        
        System.assertEquals(email.Subject,'Aging Process');
        
        DataLoadBatchJobListener DataLoadListenerObj = new DataLoadBatchJobListener();
        
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        email.Subject = 'AR Close Process';
        DataLoadListenerObj = new DataLoadBatchJobListener();
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        DataLoadListenerObj = new DataLoadBatchJobListener();
       
        DataLoadListenerObj.handleInboundEmail(email, env);
        email.Subject = 'AR Close Process';
        
        dataloadObj.Batch_Job_Status__c = 'Aborted';
        update dataloadObj;
        
        DataLoadListenerObj = new DataLoadBatchJobListener();
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        Test.stopTest();
    }
    static testMethod void testDataLoadBatchJobListener1() {
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Data_Load_Batch__c dataloadObj = new Data_Load_Batch__c();
        
        Test.startTest();
        
         list<User> listu2 = [Select id from user where Profile.name='System Administrator' and isActive=true limit 1];
        User u2=listu2.get(0);
        System.RunAs(u2)
        { 
        dataloadObj.Source_System__c = 'SAP';
        dataloadObj.Batch_Number__c = '20102503';
        //dataloadObj.Load_Type__c = 'FULL';
        dataloadObj.Run_Batch_Aging__c = false;
        insert dataloadObj;
       
        email.plainTextBody = '<END>'+'ew'+Userinfo.getOrganizationId()+''+dataloadObj.Source_System__c+''+Userinfo.getOrganizationId()+''+dataloadObj.Id+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';
        email.fromAddress ='bhavik@akritiv.com';
        email.Subject = 'Aging Process';
        
        System.assertEquals(email.Subject,'Aging Process');
        
        DataLoadBatchJobListener DataLoadListenerObj = new DataLoadBatchJobListener();
        
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        email.Subject = 'AR Close Process';
        DataLoadListenerObj = new DataLoadBatchJobListener();
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        DataLoadListenerObj = new DataLoadBatchJobListener();
       
        DataLoadListenerObj.handleInboundEmail(email, env);
        email.Subject = 'AR Close Process';
        
        dataloadObj.Batch_Job_Status__c = 'null';
        update dataloadObj;
        
        DataLoadListenerObj = new DataLoadBatchJobListener();
        DataLoadListenerObj.handleInboundEmail(email, env);
        
        }
        Test.stopTest();
    }
     static testMethod void testDataLoadBatchJobListener2() {
        // Create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Data_Load_Batch__c dataloadObj = new Data_Load_Batch__c();
        
        Test.startTest();
        dataloadObj.Source_System__c = 'SAP';
        dataloadObj.Batch_Number__c = '20102503';
       // dataloadObj.Load_Type__c = 'FULL';
       dataloadObj.Run_Batch_Aging__c = false;
        insert dataloadObj;
      
        email.plainTextBody = '<END>' +'ew'+Userinfo.getOrganizationId()+''+dataloadObj.Source_System__c+''+Userinfo.getOrganizationId()+''+'NOBATCHINFO'+''+Userinfo.getOrganizationId()+''+System.Now()+'<END>';
        email.fromAddress ='bhavik@akritiv.com';
        email.Subject = 'Aging Process';
        
        System.assertEquals(email.Subject,'Aging Process');
        
 //       DataLoadBatchJobListener DataLoadListenerObj = new DataLoadBatchJobListener();
        
//        DataLoadListenerObj.handleInboundEmail(email, env);
        
        email.Subject = 'AR Close Process';
 //       DataLoadListenerObj = new DataLoadBatchJobListener();
 //       DataLoadListenerObj.handleInboundEmail(email, env);
        
 //       DataLoadListenerObj = new DataLoadBatchJobListener();
       
//       DataLoadListenerObj.handleInboundEmail(email, env);
//        email.Subject = 'AR Close Process';
        
        dataloadObj.Batch_Job_Status__c = 'null';
        update dataloadObj;
        
 //       DataLoadListenerObj = new DataLoadBatchJobListener();
//        DataLoadListenerObj.handleInboundEmail(email, env);
        
        Test.stopTest();
    }
 }