/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Batch job that set the dispute balance to zero
 */
global class DisputeBatchAfterFullLoadController implements Database.Batchable<SObject>, Database.Stateful {

    /* Query that will get the list of objects to maintain the batches*/
    global String query;
    global Double totalBalanceZeroedOut;
    global Integer totalDisputesProcessed;
    //@todo Will batch no be provided by user or we will always pick the highest batch no ?
    global String batchNumber;
    //global String sourceSystem;

    // globall Constructor which will take the query string as argument
    global DisputeBatchAfterFullLoadController(String batchNo)
    {
        //String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
        String namespace = '';
        totalBalanceZeroedOut = 0.0;
        totalDisputesProcessed = 0;
        batchNumber = batchNo;
        //sourceSystem = sourceSys;

        /**start Namespace for migrating to unmanaged package  **/
        String highestBatchNo = '';
        //String highestBatchNo = batchNumber;

        //query to get highest batch no
        List<Dispute__c> highestBatchNoDispute = [Select a.Batch_Number__c, a.Balance__c From Dispute__c a where a.Batch_Number__c !=null order by a.Batch_Number__c desc limit 1];

        if(highestBatchNoDispute != null && highestBatchNoDispute.size() > 0)
        {
            highestBatchNo = highestBatchNoDispute.get(0).Batch_Number__c;
        }
        if(highestBatchNo < batchNumber)
            highestBatchNo = batchNumber;

        //query all the dispute that does not have the highest Batch Number and change their balance to zero String SOQLQuery = '';
        if ( UtilityController.isMultiCurrency() ) {

            this.query = 'select CurrencyIsoCode ,'+namespace+'Balance__c, '+namespace+'Batch_Number__c from '+namespace+'Dispute__c where '+namespace+'Balance__c !=0';

        } else {

            this.query = 'select '+namespace+'Balance__c, '+namespace+'Batch_Number__c from '+namespace+'Dispute__c where '+namespace+'Balance__c !=0';

        }
        this.query = this.query + ' and ' + (batchNumber == null ? namespace+'Batch_Number__c = null' : namespace+'Batch_Number__c !=\''+ highestBatchNo +'\'');

    }

    // start method implementation of Bachable
    // return type : Database.Querylocator
    // parameters : BC - Database.BatchableContext
    global Database.Querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // execute method implementation this will set the transaction balance to zero
    // return type : void
    //parameters : BC - Database.BatchableContext , scope List<Sobject>
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        List<Dispute__c> dpList = (List<Dispute__c>)scope;
        Set<Id> dpIds = new set<Id>();
        for(Dispute__c dp : dpList) {
            dpIds.add(dp.Id);
        }

        List<Dispute__c> batchDisputes = new List<Dispute__c>();
        batchDisputes = [select Balance__c from Dispute__c where Balance__c !=0 and Id in: dpIds ];
        integer dpCount = 0;
        try {
            for(Dispute__c dp : batchDisputes) {
                totalBalanceZeroedOut += dp.Balance__c;
                //@todo Anjali : Balance is a formula field and is not editable
                dp.Status__c = 'Closed';
                dp.Resolution_Code__c = 'Auto';
                dpCount++;
            }

            update batchDisputes;

        } catch(Exception e) {
            throw e;
        }
        totalDisputesProcessed +=dpCount;
    }

    // finish method implementation of  database.batchable
    // return type : void 
    // parameters : BC Database.Batchablecontext
    global void finish(Database.BatchableContext BC) {

        AsyncApexJob a = null;
        if(BC != null)
        {
            a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                 from AsyncApexJob where Id =:BC.getJobId()];
        }
        Integer openDpLeft = [select count() from Dispute__c where (Batch_Number__c!= :batchNumber and Status__c  !='Closed' and Resolution_Code__c !='Auto')];

       String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
       String address = ConfigUtilController.getBatchJobNotificationEmailId();
       String[] toAddress  = addresses.split(',');
//--email mechanism with mandrill--start-----
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        MandrillEmailService objMandrill = new MandrillEmailService();
//--email mechanism with mandrill--end-----        
        //todo - configurable email or wire into existing admin email for the org.

        //@todo Anjali : custom setting method is not global and hence not accessible?
//--email mechanism with mandrill--start-----
        mail.setToAddresses(toAddress);
        objMandrill.strToEmails =toAddress;              
        mail.setReplyTo(address);
        objMandrill.strReplyTo = address;
     
        mail.setSenderDisplayName('Batch Processing');
         objMandrill.strFromName = 'Batch Processing';
//--email mechanism with mandrill--end-----           
//--email mechanism with mandrill--start-----        
        mail.setSubject('Batch Job Finished - Closing Disputes on organization: '+ Userinfo.getOrganizationName());
        objMandrill.strSubject = 'Batch Job Finished - Closing Disputes on organization: '+ Userinfo.getOrganizationName();
//--email mechanism with mandrill--end-----        
        String mailBodyText = ' Batch Job - Closing Disputes ';
        if(a != null)
        {
            mailBodyText += '\n Batches Processed: ' + a.TotalJobItems;
            mailBodyText += '\n Failures: ' + a.NumberOfErrors;
        }
        mailBodyText += '\n Closed Disputes : ' + totalDisputesProcessed;

        Decimal val = Decimal.valueOf(totalBalanceZeroedOut);
        mailBodyText += '\n Total Balance Zeroed Out: ' + val.setScale(2).toPlainString();
        mailBodyText += '\n Total Disputes Failed:' + openDpLeft;

        mail.setPlainTextBody(mailBodyText);

//--email mechanism with mandrill--start-----
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        objMandrill.SendEmail();
//--email mechanism with mandrill--end-----        

    }

}