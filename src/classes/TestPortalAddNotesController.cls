/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for PortalAddNotesController
 */
@isTest
private class TestPortalAddNotesController
{
    /*static testMethod void mycasetest()
       {
       Account acc = DataGenerator.accountInsert();
       User adminUser = DevUtilityController.getSystemAdministrator();
       //Contact con = new Contact();
       //con.AccountId= acc.Id;
       //con.LastName = 'Test Name';
       //insert con;
       PortalAddNotesController controller ;

       Test.startTest();
       ApexPages.StandardController apexObj = new ApexPages.StandardController (acc);

       //Initiallizing controller
       controller = new PortalAddNotesController(apexObj) ;
       pageReference pr = controller.SaveNew() ;
       controller.noteTitle = 'TestNote' ;
       controller.SaveNew() ;

       Test.stopTest();
       }*/

    static testMethod void testPortalAddNotesController()
    {

        Dispute__c dispObj;
        Contact conWithUser;
        Account accWithuser;

        User adminUser = DevUtilityController.getSystemAdministrator();
        if(adminUser != null)
        {
            System.runAs(adminUser)
            {
                //Create a new instance of Account Object with test values
                accWithuser = DataGenerator.accountInsert();

                //Create a new instance of the Contact with test values
                conWithUser = DataGenerator.contactInsert(accWithuser);

                List<Dispute__c> tempDispList = new List<Dispute__c>();
                tempDispList = DataGenerator.disputeInsert(accWithuser, null);

                dispObj = tempDispList[0];
            }
        }

        //List for getting the profile
        List<Profile> profile = [select id from profile where name like 'Akritiv Customer Portal User' LIMIT 1];

        //Create a new instance of the User with test values
        User portalUser;

        // Fetch the default available CEO role
        UserRole testRole =
        [select id from UserRole where name = 'CEO' LIMIT 1];

        System.assertNotEquals(null, testRole);
        if(profile.size() > 0)
        {
            portalUser = new User(alias = 'standt', email = 'test@9876test.com',
                                  emailencodingkey = 'UTF-8', lastname = 'TestLasName', languagelocalekey = 'en_US',
                                  localesidkey = 'en_US', profileid = profile.get(0).id, contactId = conWithUser.id,
                                  timezonesidkey = 'America/Los_Angeles', username = Math.random() + '@test.com');
            insert portalUser;

            ApexPages.currentPage().getParameters().put('Id',dispObj.Id);
            PortalAddNotesController controller;

            System.RunAs(portalUser)
            {
                Test.startTest();

                List<Note> tempNote = new List<Note>();

                //Getting all notes
                tempNote = [select Id from Note where parentId =: dispObj.Id];

                //Deleting all noted
                if(tempNote.size() > 0)
                {
                    for(Integer i = 0; i < tempNote.size(); i++)
                        delete tempNote[i];
                }

                //Before save there are note
                System.assertEquals(tempNote.size(), 0);

                ApexPages.StandardController apexObj = new ApexPages.StandardController (dispObj);

                //Initiallizing controller
                controller = new PortalAddNotesController(apexObj);
                pageReference pr = controller.SaveNew();
                controller.noteTitle = 'TestNote';
                controller.SaveNew();

                //Getting notes after save
                tempNote = [select Id, Title from Note where parentId =: dispObj.Id];

                //Note has been inserted after save
                System.assertEquals(tempNote.size(), 1);

                //'Portal : ' is added in title of note from controller
                System.assertEquals(tempNote[0].Title, 'Portal : TestNote');

                Test.stopTest();
            }
        }
    }

}