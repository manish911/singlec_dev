/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: For catching InvalidDataException
 *        if any invalid data is entered.
 */
public class InvalidDataException extends Exception {

    static testmethod void runTest() {
        final String  tmp = 'temp';
        InvalidDataException ex = new InvalidDataException();
        System.assertEquals(tmp,'temp');
        }
}