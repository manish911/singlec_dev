/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   job to close follow-up tasks if all the transactions listed in that job has been closed.
 */
global class CloseFollowupTasksJob implements schedulable {
	// Closes Followup task for Batch 
	// Return Type - Void 
	// Parameter - sc Schedulablecontext
    global void execute(SchedulableContext sc) {
        BatchCloseFollowupTasks closeTasksJob = new BatchCloseFollowupTasks();
        Database.executeBatch(closeTasksJob);
    }
}