/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for BatchPurgeDataJob
 */
@isTest
private class TestBatchPurgeDataJob {

    static testMethod void testBatchPurgeDataJob() {

        List<Transaction__c> txList = new List<Transaction__c>();

        //Insert Account
        Account tempAccount = DataGenerator.accountInsert();

        //List of Tx on Account
        txList = DataGenerator.transactionInsert(tempAccount);

        Test.startTest();
           
        BatchPurgeDataJob purgeJob = new BatchPurgeDataJob(180);
        Id jobId = Database.executeBatch(purgeJob);
        System.assertNotEquals(null, jobId);
        purgeJob.execute(null, (List<SObject>)txList);
        purgeJob.finish(null);

        Test.stopTest();
    }
}