/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 *Usage   :   Used to Remember which tab was user on in the last visit
 */

public with sharing class AccountDetailExt_copy {
    public String selectedTab {get; set; }
    public String tabNo {get; set; }
    public String objId {get; set; }
    List<User_Preference__c> tempUserPref {get; set; }
    public Boolean isPortalUser {get; set; }
    public  String taskId{get; set;}
    public Boolean isAkritivUser { get; set; }
    public Boolean isNotAkritivUser { get; set; }
    public Account account = null;
    public List<Account> accountObj { get; set; }
    public String stickyNote {get; set;}
    public String stickyNoteLevel {get; set;}
    public AccountDetailExt_copy(ApexPages.StandardController controller){
        
        taskId = ApexPages.currentPage().getParameters().get('taskId');
        account = ( Account )controller.getRecord();
       
           List<Profile> profile = [ SELECT Profile.Name FROM Profile where Id = :UserInfo.getProfileId() ];
       
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        String accountTabProfiles = sysConfObj.Account_Tab_Profiles__c;
        
        // Checks the profile of current logged in user 
        // Sets the display of tab on the basis of profile of user  
        if ( accountTabProfiles.contains(profile.get(0).Name) ) {

            isAkritivUser = true;
            isNotAkritivUser = false;
        } else {

            isAkritivUser = false;
            isNotAkritivUser = true;
        }
        if(account.Id != null || account.Id != ''){
           accountObj = new List<Account>();
            accountObj = [select sticky_Note__c ,Id, sticky_Note_Level__c from Account where id =:account.Id];
                 stickyNote = accountObj.get(0).sticky_Note__c;
                 stickyNoteLevel = accountObj.get(0).sticky_Note_Level__c;
            if(stickyNote  != null &&  stickyNote  != ''){
            
                
              // stickyNote  = stickyNote.unescapeHtml4();
                 stickyNote  = stickyNote.replaceAll('\n','<br/>');
               //---  stickyNote  = stickyNote.replaceAll('\r','');           
                 
                 system.debug('----stickyNote---'+stickyNote);
                 
                 stickyNote  = stickyNote.replaceAll('&lt;br/&gt;','<br/>');
                 
                
               //  stickyNote  = stickyNote.replaceAll('\n','');
              //   stickyNote  = stickyNote.replace('\\','\');
                 
       
               
               // stickyNote  = stickyNote .replace('"','"');
               
           //    stickyNote  = String.escapeSingleQuotes( stickyNote  );
           //     stickyNote  =  stickyNote.replace('"','"');
                 stickyNote  = stickyNote.replace('\\','&#92');
 
              //  stickyNote  = stickyNote.replace('&amp','&');
                stickyNote  = stickyNote .replace('"','&quot;');
               //--- stickyNote  = stickyNote.replace('<','&lt;');
               //--- stickyNote  = stickyNote.replace('>','&gt;');                
    
            }else if (stickyNote == Label.Label_Add_Account_Note && (stickyNoteLevel == null || stickyNoteLevel == '')){
            stickyNoteLevel = Label.Label_Normal;
        
        }else if ((stickyNote == '' || stickyNote == null) && stickyNoteLevel != '' ){
            stickyNote = Label.Label_Add_Account_Note;
        }
        else{
                stickyNote  = Label.Label_Add_Account_Note;
                stickyNoteLevel  = Label.Label_Normal;
                
            }
        }
        
        
        
        tempUserPref = new List<User_Preference__c>();
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];
        if ( tempUserPref.size() > 0){
        
            objId = tempUserPref[0].Id;
        
        }
        
        isPortalUser = UtilityController.isPortalUser(Userinfo.getUserId());
        String tabSel= ApexPages.currentPage().getParameters().get('tabsel');

        // Returns the value of selected tab for current user  
        if(tabSel == '1')
            selectedTab = 'accountTab';

        if(tabSel == '2')
            selectedTab = 'txTab';

        else if(tabSel == '3')
            selectedTab = 'disputesTab';

        else if(tabSel == '4')
            selectedTab = 'notesTab';

        else if(tabSel == '5')
            selectedTab = 'rmTab';

        else{
            getTabPreference();
        }
        
    }
    //Check current user is Akritiv user and redirect page on their account Id
    // If current user is not Akritiv User then the permission is not granted to that user 
    // He is not redirected to Account Detail Page 
    // return type - Pagereference
    // No parameters are passed in this method 
    public Pagereference chackAndRedirect () {
        
        String tabSel= ApexPages.currentPage().getParameters().get('tabsel');
        if(tabSel == '1' )
            selectedTab = 'accountTab';
        
        if ( !isAkritivUser ) {

            Pagereference page = new Pagereference('/'+account.Id+'?nooverride=1');
            page.setRedirect(true);
            return page;
        }

        return null;
    }
    // Get tab Index
    // Return type - Pagereference
    // This method calls method getTabPrefernce which remembers which was the last tab visited by user
    // Also the index of that tab is obtained 
    // No parameters are passed in this method
    public Pagereference getTabIndex() {
        getTabPreference();
        return null;
    }   
    // Get tab Preference
    // Return Type - Void
    // This method remebers which was the last visited by user along with its index number
    // On the basis of User Id or Profile this method redirects user to that tab
    // If user is first time logged in then default Account Tab is opened    
    // No parameters are passed in this method 
    public void getTabPreference(List<User_Preference__c> tempUserPref) {
        //tempUserPref = new List<User_Preference__c>();// TO-DO BHAVIK
        //tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];

        // Returns the value of previously selected tab by current user 
        // if user is coming first time then default Account Tab is selected 
        if(tempUserPref.size() > 0 ) {
            objId = tempUserPref[0].Id;
            if(tempUserPref[0].SelectedTab__c !=null ) {
                if(tempUserPref[0].SelectedTab__c == 1)
                    selectedTab = 'accountTab';
                else if (tempUserPref[0].SelectedTab__c == 2)
                    selectedTab = 'txTab';
                else if (tempUserPref[0].SelectedTab__c == 3)
                    selectedTab = 'disputesTab';
                else if (tempUserPref[0].SelectedTab__c == 4)
                    selectedTab = 'notesTab';
                else if (tempUserPref[0].SelectedTab__c == 5)
                    selectedTab = 'rmTab';

            }
        }
        else{
            selectedTab='accountTab';
        }
    }
    
    public void getTabPreference() {
        tempUserPref = new List<User_Preference__c>();// TO-DO BHAVIK
        tempUserPref = [select Name,SelectedTab__c from User_Preference__c where User__C=:UserInfo.getUserId() Limit 1];

        // Returns the value of previously selected tab by current user 
        // if user is coming first time then default Account Tab is selected 
        if(tempUserPref.size() > 0 ) {
            objId = tempUserPref[0].Id;
            if(tempUserPref[0].SelectedTab__c !=null ) {
                if(tempUserPref[0].SelectedTab__c == 1)
                    selectedTab = 'accountTab';
                else if (tempUserPref[0].SelectedTab__c == 2)
                    selectedTab = 'txTab';
                else if (tempUserPref[0].SelectedTab__c == 3)
                    selectedTab = 'disputesTab';
                else if (tempUserPref[0].SelectedTab__c == 4)
                    selectedTab = 'notesTab';
                else if (tempUserPref[0].SelectedTab__c == 5)
                    selectedTab = 'rmTab';

            }
        }
        else{
            selectedTab='accountTab';
        }
    }
}