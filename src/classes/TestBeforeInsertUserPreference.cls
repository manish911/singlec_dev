/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for the Trigger BeforeInsertUserPreference.
 */
@isTest
private class TestBeforeInsertUserPreference {
    static testMethod void testTrigger() {
        // delete UserPreference initially for the current user
        delete [select Id from User_Preference__c where User__c=: Userinfo.getUserId() limit 1];

        // insert the user preference for the current user.
        User_Preference__c up1 = new User_Preference__c();
        up1.User__c = Userinfo.getUserId();
        insert up1;

        System.assertNotEquals(null, up1.id);
        // insert the user preference for the same user.
        // it will throw an error
        Boolean isError = false;
        try {
            User_Preference__c up2 = new User_Preference__c();
            up2.User__c = Userinfo.getUserId();
            insert up2;
        }
        catch(Exception e) {
            isError = true;
        }
        System.assertEquals(true, isError);
    }
}