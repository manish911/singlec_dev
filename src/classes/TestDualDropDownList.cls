/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Unit tests for testing DualDropDownList apex class
 */
public class TestDualDropDownList {
    static testMethod void Test_DualDropDownList(){

        //create list of available items
        String [] defaultItemsList = new String[] {};

        String strValue='';
        // add list of fieldName + '#' +  their Labels ;
        for(Integer i=0; i< 10; i++)
        {
            string nextVal = 'key'+ String.ValueOf(i) + '#' + 'value' + String.ValueOf(i);
            strValue = strValue + '~#' + nextVal;
            defaultItemsList.add(strValue);
        }

        Test.startTest();

        //initilization
        DualDropDownList listObj = new DualDropDownList();

        //fill list
        System.assertEquals(listObj.getList(defaultItemsList).size(), 10);

        //set display
        String str1 = listObj.strDisplay;
        listObj.strDisplay = 'Block';
        System.assertNotEquals(listObj.strDisplay, null);

        //set width
        String str2 = listObj.lstWidth;
        listObj.lstWidth= '300';
        listObj.SelectedItems = listObj.SelectedItems;
        List<List<DualDropDownList.customOption>> list1 = listObj.SelectedItems;
        System.assertEquals(list1, null);

        listObj.strItems = new String[] {'1','2'};
        List<List<DualDropDownList.customOption>>  list2 = listObj.Items;
        System.assertEquals(list2.size(), 2);

        Test.stopTest();
    }
}