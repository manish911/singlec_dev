/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for UpdateDisputet.
 */

@isTest
private class TestUpdateDispute
{
    static testMethod void Test_UpdateDispute()
    {
        try
        {
            // Create and inserting an Account
            Account acc = DataGenerator.accountInsert();

            // Create and inserting an Dispute
            List<Dispute__c> dispRec = DataGenerator.disputeInsert(acc, null);

            String key = Userinfo.getUserName()+Datetime.now();
            Temp_Object_Holder__c tohObj = new Temp_Object_Holder__c();
            tohObj.key__c = key;
            tohObj.value__c = dispRec[0].Id;
            insert tohObj;

            //Passing paramenters to constructor
            ApexPages.currentPage().getParameters().put('key', key);
            ApexPages.currentPage().getParameters().put('parentid', acc.Id);

            // Test starts from here
            Test.startTest();

            UpdateDispute controller = new UpdateDispute();
            controller.updateDispute();

            // Asserts for controller
            System.assertEquals(Controller.keyParam,key);

            // resolve error on this line [parentId: variable doesnt exist]
            //System.assertEquals(Controller.parentId,acc.Id);

            System.assertEquals(Controller.selectedStatus,'None');

            System.assertEquals(Controller.selectedType,'None');

            System.assertEquals(Controller.isShowResolution,false);

            List<SelectOption> selectLst1 = new List<SelectOption>();
            selectLst1 = controller.resolutionCodes;

            System.assertEquals(selectLst1.size(),14);

            List<SelectOption> selectLst2 = new List<SelectOption>();
            selectLst2 = controller.status;

            System.assertEquals(selectLst2.size(),5);

            List<SelectOption> selectLst3 = new List<SelectOption>();
            selectLst3 = controller.disType;

            System.assertEquals(selectLst3.size(),5);

            controller.selectedStatus = 'Unassigned';
            controller.selectedType = 'Sales';
            PageReference Pg = controller.updateDispute();

            System.assertEquals(Pg,null);
             
            controller.selectedStatus = 'Closed';
            controller.showResolution();
            controller.selectedType = 'Sales';
            controller.noteTitle = 'abc';
            PageReference Pg1 = controller.updateDispute();

            System.assertEquals(Pg1,null);

           
            Test.stopTest();
        }
        catch(Exception e)
        {
            //fails if exception occured
            //System.assert(false,e.getMessage());
        }
    }
       
}