/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   batch job to use to populate Account, Transaction and Dispute level fields value on Task.
 */

global class BatchPopulateTaskWithARDetail implements Database.Batchable<SObject>,Schedulable, Database.Stateful {


     public List<ManageTaskFields__c> manageTaskList;
      
     
     Map<string,String> Mapfield = new Map<string,String>();
     Map<string,String> AccFieldList = new  Map<string,String>();
     Map<string,String> TxFieldList =  new  Map<string,String>();
     Map<string,String> DisputeFieldList =  new  Map<string,String>();
     
     String AccountField ='';
     String TransactionField= '';
     String DisputeField= '';
     private String commaSeperatedFields;
     public Data_Load_Batch__c batchInfo ;
     global Id batchInfoId ; 
      
     global database.querylocator start(Database.BatchableContext bc){
    
        manageTaskList = [select Name,Object__c,Object_Field__c,Task_fields__c from ManageTaskFields__c limit 500];
        String Query1 = 'Id,whatId';
    
       
           if(manageTaskList.size()>0){
                for(ManageTaskFields__c tfc : manageTaskList ){
            
                  Mapfield.put(tfc.Object_Field__c,tfc.Object__c);
                  
                  if(tfc.Object__c.equalsIgnoreCase('account')){
                
                      AccountField = AccountField + tfc.Object_Field__c + ',';
                      AccFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
                  }
                  else if(tfc.Object__c.equalsIgnoreCase('transaction__c')){ 
                      TransactionField = TransactionField + tfc.Object_Field__c + ',';
                      TxFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
                  }
                  else if (tfc.Object__c.equalsIgnoreCase('Dispute__c')){
                      DisputeField = DisputeField + tfc.Object_Field__c + ',';
                      DisputeFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
                  }
            
                Query1 =  Query1 + ',' + tfc.Task_fields__c;
            
           }
         }
        
      // Query1 =  Query1.removeEnd(',');
      //  Query1 = Query1.remove(Query1.lastIndexOf(','));
        String sQuery = '';
        sQuery = 'select  ' + Query1 + ' from Task where whatId != null' ;
        
        
         SysConfig_Singlec__c sysconfig  = SysConfig_Singlec__c.getOrgDefaults();

        if ( sysconfig.PopulateTaskFilter__c != null &&  sysconfig.PopulateTaskFilter__c.trim() != '') {

            sQuery += ' and ' + sysconfig.PopulateTaskFilter__c;
        }
        system.debug('----sQuery----'+sQuery);
        
          if ( batchInfoId  != null ) {
           //* batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, d.Aging_Job_End_Time__c , d.AR_RiskFactor__c, d.ARClose__c, d.ARClose_Start_Time__c, d.ARClose_End_Time__c From Data_Load_Batch__c d where Id=:batchInfoId];
            batchInfo = [ Select d.Source_System__c, d.Id, d.Batch_Number__c, 
                            d.Aging_Job_Status__c, d.Aging_Job_Start_Time__c, 
                            d.Aging_Job_End_Time__c, d.Percentage_Close__c , 
                            d.Batch_Job_Status__c, d.Batch_Job_Start_Time__c, 
                            d.Batch_Job_End_Time__c  
                            From Data_Load_Batch__c d 
                            where Id=:batchInfoId];
                            
            batchInfo.Aging_Job_Start_Time__c = System.now();
            
        }
        
        return Database.getQueryLocator(sQuery);
    
    }
  
    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
  
   //  manageTaskList = [select Name,Object__c,Object_Field__c,Task_fields__c from ManageTaskFields__c];
            List<Task> tsktoUpdate = new List<Task>();
            Set<Id> setId = new Set<Id>();            
      
             for(Sobject tskSobject : sObjs){
                Task tsk = (Task) tskSobject;
                setId.add(tsk.whatId);
             }
            
              String query = 'Select ' + DisputeField + ' Id,Account__c,Transaction__c from Dispute__c where Id in : setId limit 9999';
              List<Dispute__c> DisList = Database.query(query);
    
              Map<Id,Dispute__c> disMap = new Map<Id,Dispute__c>();
        
              if(DisList != NULL){
                  for(Dispute__c dis: DisList ){
                        setId.add(dis.Transaction__c);
                        setId.add(dis.account__c);
                        disMap.put(dis.ID,dis);
                  }
              }
            
              Map<Id,Transaction__c> txMap = new Map<Id,Transaction__c>();
              query = 'Select ' + TransactionField + ' Id,Account__c from Transaction__c where Id in : setId limit 9999';
              List<Transaction__c> txList = Database.query(query);
            
              if(txList != NULL){
                  for(Transaction__c tx : txList ){
                        setId.add(tx.Account__c);
                        txMap.put(tx.ID,tx);
                  }
              }
            
            query = 'Select ' + AccountField + ' Id from Account where Id in : setId limit 9999';
            List<Account> accList = Database.query(query);
         
            Map<Id,Account> accMap = new Map<Id,Account>();
            
            if(accList  != NULL){
                for(Account acc  : accList){
                    accMap.put(acc.ID,acc);
                }
            }
    
            for(Sobject tskSobject : sObjs){
                Task tsk = (Task) tskSobject;
                if(txMap.get(tsk.whatId) != NULL){
                    Transaction__c tx1 = txMap.get(tsk.whatId);
                    Account acc = accMap.get(tx1.Account__c);
                    Account accFill  = accMap.get(tx1.Account__c);
                    for(String str : TxFieldList.keyset()){
                        string tem = String.valueOf(TxFieldList.get(str));
                        tsk.put(str,tx1.get(tem));
                    }
                    for(String str : AccFieldList.keyset()){
                        string tem = String.valueOf(AccFieldList.get(str));
                        if(accFill != null)
                            tsk.put(str,accFill.get(tem));
                    }
    
                }
                if(accMap.get(tsk.whatId) != NULL){
                    Account acc3  = accMap.get(tsk.whatId); 
                    for(String str : AccFieldList.keyset()){
                        string tem = String.valueOf(AccFieldList.get(str));
                        tsk.put(str,acc3.get(tem));
                    }
                }
                if(disMap.get(tsk.whatId) != NULL){
                    Dispute__c dis = disMap.get(tsk.whatId);
                    Account acc1  = accMap.get(dis.Account__c);
                    Transaction__c txdis = txMap.get(dis.Transaction__C);
                    for(String str : DisputeFieldList.keyset()){
                        string tem = String.valueOf(DisputeFieldList.get(str));
                        tsk.put(str,dis.get(tem));
                    }
                    for(String str : AccFieldList.keyset()){
                        string tem = String.valueOf(AccFieldList.get(str));
                        if(acc1 != null)
                            tsk.put(str,acc1.get(tem));
                    }
                    for(String str : TxFieldList.keyset()){
                        string tem = String.valueOf(TxFieldList.get(str));
                        if(txdis != null)
                        tsk.put(str,txdis.get(tem));
                    }
                 }
                 
               tsktoUpdate.add(tsk);
               }
        
           update tsktoUpdate;
       
      }
  
  
     
  global void finish(Database.BatchableContext bc){
  
   if (batchInfo != null ) {
       
             String toAddress = ConfigUtilController.getBatchAutoCloseExternalEmailNotificationService();
                    
                //notify system of this finish
                if (toAddress != null ) {
    
                    //--email mechanism with mandrill--start-----
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    MandrillEmailService objMandrill = new MandrillEmailService();
                    
                    
                    mail.setToAddresses(new String[] {toAddress});
                    objMandrill.strToEmails = new String[] {toAddress};              


                    mail.setReplyTo(toAddress);
                    objMandrill.strReplyTo = toAddress;

                    mail.setSenderDisplayName('Batch Processing :');
                    objMandrill.strFromName = 'Batch Processing :';
                    //--email mechanism with mandrill--end-----
                    // Set subject

                    //--email mechanism with mandrill--start-----
                    mail.setSubject( 'Akritiv Close Process');
                    objMandrill.strSubject = 'Akritiv Close Process';                    
                    //--email mechanism with mandrill--end-----
                    // Set body
                    String mailBodyText ='<END>'+batchInfo.Batch_Number__c+''+Userinfo.getOrganizationId()+''+batchInfo.Source_System__c+''+Userinfo.getOrganizationId()+''+batchInfo.Id+'<END>';                   
                    
            
                    mail.setPlainTextBody(mailBodyText);
               
                   //--email mechanism with mandrill--start-----
                     SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                 boolean us = seo.Use_Mandrill_Email_Service__c;
                 if(us){
                        objMandrill.SendEmail();
                       }
                 else{
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                     }

                    //--email mechanism with mandrill--end-----
                    
                 }
        }
      System.debug('-------->> Finish <<---------- ');
  }
  
  global void execute(SchedulableContext sc){
  
      BatchPopulateTaskWithARDetail  bac = new BatchPopulateTaskWithARDetail();
      Database.executebatch(bac,200);
      
  }
  
        public static testMethod void  testStdDe1(){
        Test.startTest();
        Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Update_Account_ContactLog_Details__c = false;
        tc.Zero_AR_Account_Trigger__c = false;
        insert tc;
        
        SysConfig_Singlec__c sconf = new SysConfig_Singlec__c();
        sconf.Namespace__c = 'akritiv';
        sconf.Billing_Statement_PDF_page__c ='test';
        insert sconf;
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc; 
//        Account acc = [select id, name from account limit 1];
        System.assertEquals(acc.name,'testAcc');
        Transaction__c tx = new Transaction__c();
        tx.Name = 'Inv-11121';
        tx.Account__c = acc.Id;
        tx.Balance__c = 2123;
        tx.Amount__c = 2133;
        tx.Due_Date__c = Date.Today()-65;
        tx.Base_Balance__c = 5000;
        insert tx;
        List<Transaction__c> txs = new List<Transaction__c>();
        txs.add(tx); 
        
           
   
        
         Task taskObj = new Task();
        taskObj.WhatId = acc.Id;
        taskObj.ActivityDate  = Date.today();

        insert taskObj;
        List<Task> tsks = new List<Task>();
        tsks.add(taskObj ); 
     
       ManageTaskFields__c mtf = new ManageTaskFields__c();
       mtf.name = 'test';
       mtf.Object_Field__c = 'AccountNumber';
       mtf.Task_fields__c = 'Auto_Dunning_Error__c';
       mtf.Object__c = 'Account';
       
       //========================================================================================== 
       Map<String,Schema.SObjectField> m = Schema.SObjectType.ManageTaskFields__c.fields.getMap();
       for (String fieldToCheck : m.keyset()) {
          // Check if the user has create access on the each field
          if (m.get(fieldToCheck).getDescribe().isCustom() && !m.get(fieldToCheck).getDescribe().isCalculated() ) {
              if (!m.get(fieldToCheck).getDescribe().isCreateable() || !m.get(fieldToCheck).getDescribe().isUpdateable()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                                        'Insufficient access'));
               return;
              }
          }
        }
        //========================================================================================
       upsert mtf;
        
       Data_Load_Batch__c dlb = new Data_Load_Batch__c();
       dlb.Batch_Number__c = '12345';
       dlb.Source_System__c = 'test';
       insert dlb;
       
       BatchJobConfiguration_Singlec__c bjc = new BatchJobConfiguration_Singlec__c();
       bjc.Auto_Close_Email_Service__c = 'Test@akritiv.com';
       bjc.Custom_Email_Service__c = 'test@akritiv.com';
       bjc.name = 'test';
       upsert bjc;
        
        BatchPopulateTaskWithARDetail controller = new BatchPopulateTaskWithARDetail();
        controller.batchinfoId = dlb.Id;
      //  agingCalc.stdDevCalAll.Clear();
        controller.start(null);
        controller.execute(null,tsks);
        controller.finish(null);
        
  
        
        Test.stopTest();
    }
            public static testMethod void  testStdDe2(){
        Test.startTest();
        Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Update_Account_ContactLog_Details__c = false;
        tc.Zero_AR_Account_Trigger__c = false;
        insert tc;
        
         SysConfig_Singlec__c sconf = new SysConfig_Singlec__c();
        sconf.Namespace__c = 'akritiv';
        sconf.Billing_Statement_PDF_page__c ='test';
        insert sconf;
        
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc;
        System.assertEquals(acc.name,'testAcc');
        Transaction__c tx = new Transaction__c();
        tx.Name = 'Inv-11121';
        tx.Account__c = acc.Id;
        tx.Balance__c = 2123;
        tx.Amount__c = 2133;
        tx.Due_Date__c = Date.Today()-65;
        tx.Base_Balance__c = 5000;
        insert tx;
        List<Transaction__c> txs = new List<Transaction__c>();
        txs.add(tx); 
        
           
      
         Task taskObj = new Task();
        taskObj.WhatId = tx.Id;
        taskObj.ActivityDate  = Date.today();

        insert taskObj;
        List<Task> tsks = new List<Task>();
        tsks.add(taskObj ); 
     
       ManageTaskFields__c mtf = new ManageTaskFields__c();
       mtf.name = 'test';
       mtf.Object_Field__c = 'Name';
       mtf.Task_fields__c = 'Auto_Dunning_Error__c';
       mtf.Object__c = 'Transaction__c';
       upsert mtf;
        
       Data_Load_Batch__c dlb = new Data_Load_Batch__c();
       dlb.Batch_Number__c = '12345';
       dlb.Source_System__c = 'test';
       insert dlb;
       
       BatchJobConfiguration_Singlec__c bjc = new BatchJobConfiguration_Singlec__c();
       bjc.Auto_Close_Email_Service__c = 'Test@akritiv.com';
       bjc.Custom_Email_Service__c = 'test@akritiv.com';
       bjc.name = 'test';
       upsert bjc;
        
        BatchPopulateTaskWithARDetail controller = new BatchPopulateTaskWithARDetail();
        controller.batchinfoId = dlb.Id;
      //  agingCalc.stdDevCalAll.Clear();
        controller.start(null);
        controller.execute(null,tsks);
        controller.finish(null);
        
  
        
        Test.stopTest();
    }
            public static testMethod void  testStdDe3(){
        Test.startTest();
           Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Update_Account_ContactLog_Details__c = false;
        tc.Update_Transaction_Disputed_Amount__c = false;
        tc.Zero_AR_Account_Trigger__c = false;
        insert tc;
        SysConfig_Singlec__c sconf = new SysConfig_Singlec__c();
        sconf.Namespace__c = 'akritiv';
        sconf.Billing_Statement_PDF_page__c ='test';
        insert sconf;
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc;
        System.assertEquals(acc.name,'testAcc');
        Transaction__c tx = new Transaction__c();
        tx.Name = 'Inv-11121';
        tx.Account__c = acc.Id;
        tx.Balance__c = 2123;
        tx.Amount__c = 2133;
        tx.Due_Date__c = Date.Today()-65;
        tx.Base_Balance__c = 5000;
        insert tx;
        List<Transaction__c> txs = new List<Transaction__c>();
        txs.add(tx); 
        
        
        
        
     
        
        dispute__c dis = new  dispute__c();
        dis.account__c = acc.Id;
        dis.transaction__c = tx.Id;
        insert dis;
        
           
     
         Task taskObj = new Task();
        taskObj.WhatId = dis.Id;
        taskObj.ActivityDate  = Date.today();

        insert taskObj;
        List<Task> tsks = new List<Task>();
        tsks.add(taskObj ); 
     
       ManageTaskFields__c mtf = new ManageTaskFields__c();
       mtf.name = 'test';
       mtf.Object_Field__c = 'Name';
       mtf.Task_fields__c = 'Auto_Dunning_Error__c';
       mtf.Object__c = 'Dispute__c';
       upsert mtf;
        
       Data_Load_Batch__c dlb = new Data_Load_Batch__c();
       dlb.Batch_Number__c = '12345';
       dlb.Source_System__c = 'test';
       insert dlb;
       
       BatchJobConfiguration_Singlec__c bjc = new BatchJobConfiguration_Singlec__c();
       bjc.Auto_Close_Email_Service__c = 'Test@akritiv.com';
       bjc.Custom_Email_Service__c = 'test@akritiv.com';
       bjc.name = 'test';
       upsert bjc;
     
        
        BatchPopulateTaskWithARDetail controller = new BatchPopulateTaskWithARDetail();
        controller.batchinfoId = dlb.Id;
      //  agingCalc.stdDevCalAll.Clear();
        controller.start(null);
        controller.execute(null,tsks);
        controller.finish(null);
        
  
        
        Test.stopTest();
    }
  
  
}