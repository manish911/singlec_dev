public with sharing class UserSearchController {
    public String userName {get; set;}
    public List<User> ur{get; set;}
  
    public UserSearchController () {
       ur = [Select Id, Name,Email From User where Email != null order by Name];
    }
   
    public void searchUser() {
        //contactList = new List<Contact>();
        if(userName != null && userName.length() > 0 && userName != '') {
            userName = formatSearchText(userName).trim();
            String searchText = '%' + userName + '%';
            ur = [Select Id, Name From User Where Name like :searchText];
        } else {
            ur = [Select Id, Name,Email  From User where Email != null];
        }
    }
   
    //Method replacing extra asteric(*)
    private String formatSearchText(String s) {
        if(s != null && s.length() > 0) {
            while(s.indexOf('  ') > -1){
                s = s.replace('  ', ' ');
            }
           
        }
        
        return s;
    }
}