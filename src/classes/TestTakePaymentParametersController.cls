/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage:  Test class for TakePaymentParametersController
 */
@isTest
private class TestTakePaymentParametersController{

    static testmethod void testCase(){
       /*  Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Update_Account_ContactLog_Details__c = false;
        tc.Zero_AR_Account_Trigger__c = false;
        upsert tc; */
        

                       /* SysConfig_Singlec__c sic = new SysConfig_Singlec__c();
                        sic.Is_Multi_Currency__c = false;
                        sic.Billing_Statement_PDF_page__c = 'test';
                        sic.Billing_PDF_Rows_In_First_Page__c = 10;
                        sic.Billing_PDF_Rows_in_Subsequent_Pages__c = 1;
                        sic.Billing_Statement_Excel_Template__c = 'SYSTEM_BillingStatementExcel';
                        sic.Billing_Statement_PDF_page__c = 'test';
                        sic.Billing_Statement_Pdf_Template__c = 'SYSTEM_BillingStatementPDF';
                        sic.IconsetURL__c = 'test';
                        sic.Invoice_PDF_Url_Prefix__c = 'test';
                        sic.Default_Child_Account_List_View__c = 'Name,AccountNumber,Last_Contact_Date__c,Total_AR__c';
                         sic.Display_Child_Account_Invoice__c = true;
                         sic.account_field__c = 'Total_AR__c';
                        
                        upsert sic;*/
        
        Account accObj = new Account();
        accObj = DataGenerator.accountInsert();
      // accObj = [select id,name from account limit 1];
        List<Transaction__c> txList = new List<Transaction__c>();
        Payment_Processor__c process = new  Payment_Processor__c();
        process.name = 'UTA';
        process.Base_URL__c = 'test';
        insert process;
        Payment_Configuration__c pconfig = new Payment_Configuration__c();
        pconfig.Parameter_Name__c = 'UTA';
        pconfig.Parameter_Value__c = '123';
        pconfig.Processor__c = process.Id;
        insert pconfig ;
        System.AssertEquals(pconfig.Parameter_Value__c,'123');
     //   txList = DataGenerator.transactionInsert(accObj);
        TakePaymentParametersController tpc = new TakePaymentParametersController(); 
        Map<String,String> strMap = new Map<String,String>();
        
    
      
        strMap.put('test','test');
        
        Map<Id,Double> amtMap = new Map<Id,Double>();
        
      /*  for(Transaction__c tx: txList){
            amtMap.put(tx.Id,tx.balance__c);
        }*/
               
        tpc.createUserInfo();
   //     tpc.processInputeFields(strMap,txList,amtMap);    
    //    tpc.createRequestResponse(strMap,txList,amtMap);
       
        
    }
}