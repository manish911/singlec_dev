/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Schedulable job to run date forward job
 */
global class DateForwardScheduledJob implements Schedulable {
	// method is used to execute Schedulable Context 
	// return type : void
	// parameters : sc - SchedulableContext
	global void execute(SchedulableContext sc) {
		BatchJobConfiguration_Singlec__c batchConfigObj = ConfigUtilController.getBatchJobConfigObj();
		Integer daysToForward = ( batchConfigObj!= null && batchConfigObj.Days_To_Forward__c != null)
		                        ? batchConfigObj.Days_To_Forward__c.intValue() : 1;

		DateForwardBatchJob bca = new DateForwardBatchJob();
		bca.daysToForward = daysToForward;
		Database.executeBatch(bca);
	}
}