/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: For Portal User to download the generated Billing Statement
 */
public with sharing class PortalDownloadBillingStatementController {
    public SendBillingStatementController sendBillingStmtController=new SendBillingStatementController();
    public User currUser;
    public String pageContent {get; set; }

    //String defaultPdfPage ='TransactionPDFGenerator';
    public PortalDownloadBillingStatementController() {
        currUser = [select Id, ContactId, Contact.AccountId from User where id=: Userinfo.getUserId()];
        
        if (currUser.Contact.AccountId != null )
        ApexPages.currentPage().getParameters().put('accId', currUser.Contact.AccountId);

        String strTrans ='Transaction__c';
        String listviewName = AkritivConstants.CONST_SYSTEM_PORTAL_PDF;
        String selectedListView ='default';
        List<Custom_List_View__c> listObj = new  List<Custom_List_View__c>();
        listObj = [select Name,Object_Fields__c from Custom_List_View__c where Is_Global__c =true  and  ListView_For_Object__c =: strTrans and name =:listviewName Limit 1  ];
        if(listObj.size()>0)
            selectedListView = listObj[0].Id;

        sendBillingStmtController = new SendBillingStatementController();
        sendBillingStmtController.renderAsPDF = 'PDF';
        sendBillingStmtController.accountId = currUser.Contact.AccountId;
        sendBillingStmtController.emailTargetToContactId = currUser.ContactId;
        sendBillingStmtController.selectedList = selectedListView;

        Apexpages.currentPage().getHeaders().put('Content-Disposition', 'inline;filename=Statement.pdf');

        this.pageContent = sendBillingStmtController.getStatementTemplateStr();
    }
  
}