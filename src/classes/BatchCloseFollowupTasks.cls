/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   batch job to close follow-up tasks if all the transactions listed in that job has been closed.
 */
global class BatchCloseFollowupTasks implements Database.Batchable<SObject>, Database.Stateful {
    // List to hold the Task ids that need to be closed
    public List<Id> tasksToClose = new List<Id>();

    // start method that rerurn the query locator object
    // with all the active activityTransaction records
    global database.querylocator start(Database.BatchableContext bc){
        // please add Namespace for custom fields and object in query below
        String sQuery = 'select id, Task_id__c, Transaction_ids__c from Activity_Transaction__c where isActive__c=true';
        return Database.getQueryLocator( sQuery );
    }

    global void execute(Database.BatchableContext bc, List<sObject> sObjs){
        // iterate over all the Activity_Transaction records in batches
        for(Activity_Transaction__c ActvityTx : (List<Activity_Transaction__c>)sObjs) {

            // get the ids of activityTransaction in an array form from comma separated string form
            String[]  txIds;
            if(ActvityTx.Transaction_ids__c != null)
                txIds = ActvityTx.Transaction_ids__c.split(',');

            // get all associated transactions with the activity transaction
            List<Transaction__c> followedUpTxs = new List<Transaction__c>();
            if(ActvityTx.Transaction_ids__c != null && txids.size() > 0) {
                followedUpTxs = [select id, Balance__c from Transaction__c where id in:txIds ];

                //check all the transaction has been closed or not
                Boolean isAllClosed = true;
                for(Transaction__c tx : followedUpTxs) {
                    if(tx.Balance__c != 0) {
                        isAllClosed = false;
                        break;
                    }
                }

                // if all the related followed-up txs are closed then need to close the task and deactivate the activityTransaction record.
                if(isAllClosed == true) {
                    tasksToClose.add(ActvityTx.Task_Id__c);
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc){
        if(tasksToClose.size()>0) {
            //deactivate the activityTransaction record.
            List<Activity_Transaction__c> actTxs = [select id from Activity_Transaction__c where task_id__c in:tasksToClose ];
            for(Activity_Transaction__c at : actTxs) {
                at.isActive__c = false;
            }
            if(actTxs.size()>0)
                update actTxs;

            // close the follow-up tasks
            List<Task> tasksList = [select id from Task where id in:tasksToClose and Contact_Type__c =: AkritivConstants.CONTACT_TYPE_CONTACT ];
            for(Task t : tasksList) {
                t.status = 'Completed';
            }
            if(tasksList.size()>0)
                update tasksList;
        }
    }
}