public with sharing class RiskManagementViewController {

    public  String accountId{get; set;}
    public Account acc { get; set; }
    
   //Class constructor
    public RiskManagementViewController (ApexPages.StandardController controller)
    {   
         
         accountId = ApexPages.currentPage().getParameters().get('id');
         system.debug('--ID--'+accountId);
         this.acc= getAccountRM();
    
    }
    
    
    
   

    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.Risk_Management.getFields();
    }

    private Account getAccountRM() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, Name FROM Account where Id = :accountId LIMIT 1';
        return Database.query(query);
    }

}