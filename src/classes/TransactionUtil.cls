/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage : This class provide util methods for manupulate of Transaction
 */
public class TransactionUtil {
    public static final String STATUS_CURRENT = 'Current';
    public static final String STATUS_REMINDER = 'Reminder';
    public static final String STATUS_PASTDUE = 'Past-due';
    public static final String STATUS_SERIOUS = 'Serious';
    public static final String STATUS_COLLECTION = 'Collection';
    public static final String STATUS_DIPSUTED = 'Disputed';

    public static Integer getStatusNumber(String status) {
        if(STATUS_CURRENT.equalsIgnoreCase(status))
            return 1;
        else if(STATUS_REMINDER.equalsIgnoreCase(status))
            return 2;
        else if(STATUS_PASTDUE.equalsIgnoreCase(status))
            return 3;
        else if(STATUS_SERIOUS.equalsIgnoreCase(status))
            return 4;
        else if(STATUS_COLLECTION.equalsIgnoreCase(status))
            return 5;
        else if(STATUS_DIPSUTED.equalsIgnoreCase(status))
            return 6;

        return 0;
    }
    //method is used to get default List View
    // return type : List < String >
   /* public static List<String> getDefaultListView(){
        List<String> defaultList = new List<String>();

        //Note------------do not add Id and Name fields on this list------------//

        defaultList.add('So_Number__c');
        defaultList.add('Po_Number__c');
        defaultList.add('Due_Date__c');
        defaultList.add('Days_Past_Due__c');
        defaultList.add('Amount__c');
        defaultList.add('balance__c');

        return defaultList;

    }*/
    
    // Method for fetching and displaying fields in PDF which are configured
    // Return Type List<String>
    
    
    
    public static List<String> getDefaultListView(){
       String listOfFields = ConfigUtilController.getDefaultListViewFields();
       
       if(listOfFields == null){
       
           return new List<String >();
       }
       String[] listOfSelectedFields = listOfFields.split(',');
       
       List<String> defaultList = new List<String>();
       
       for(String s : listOfSelectedFields){
           defaultList.add(s);
       }
       
       return defaultList;
  
    }
    
      
    public static List<String> getDefaultChildAccountListView(){
       String listOfFields = ConfigUtilController.getDefaultChildAccountListView();
       
       if(listOfFields == null){
       
           return new List<String >();
       }
       String[] listOfSelectedFields = listOfFields.split(',');
       
       List<String> defaultList = new List<String>();
       
       for(String s : listOfSelectedFields){
           defaultList.add(s);
       }
       
       return defaultList;
  
    }
    // method is used to get Default Dispute ListView
    // return type : List < String >
    public static List<String> getDefaultDisputeListView(){
      /*   List<String> defaultDisputeList = new List<String>();
       
        //Note------------do not add Id and Name fields on this list------------//

        defaultDisputeList.add('Amount__c');
        defaultDisputeList.add('balance__c');
        defaultDisputeList.add('Type__c');
        defaultDisputeList.add('Status__c');
        defaultDisputeList.add('Close_Date__c');

        return defaultDisputeList;*/
        
        String listOfFields = ConfigUtilController.getDefaultDisputeListViewFields();
       
       List<String> defaultDisputeList = new List<String>();
       if(listOfFields == null){
       
           return new List<String >();
       }
       if(listOfFields != null && listOfFields != ''){
       String[] listOfSelectedFields = listOfFields.split(',');
       
      
       for(String s : listOfSelectedFields){
           defaultDisputeList.add(s);
       }
           
           }
       return defaultDisputeList ;

    }
    
    public static List<String> getDefaultDisputeLinesListView(){
       String listOfFields = ConfigUtilController.getDefaultDisputeLinesListViewFields();

       List<String> defaultDisputeList = new List<String>();
       if(listOfFields == null){
           return new List<String >();
       }
       if(listOfFields != null && listOfFields != ''){
           String[] listOfSelectedFields = listOfFields.split(',');
           for(String s : listOfSelectedFields){
               defaultDisputeList.add(s);
           }
       }
       return defaultDisputeList;
    }
}