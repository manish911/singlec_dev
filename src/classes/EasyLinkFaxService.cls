/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Easy Link Web Service
 */

public with sharing class EasyLinkFaxService{
        
    public static String SendFax(String DestinationFaxNumber,String FileData,String FileType){

        servicesWebtoolsEasylinkComSendfaxma.SendFaxManagerService objSendFaxManagerService = new  servicesWebtoolsEasylinkComSendfaxma.SendFaxManagerService();
        
        String EmailAddress = ConfigUtilController.getFaxUsername();
        String Password = ConfigUtilController.getFaxPassword(); 
            
        String AccountCode;
        Datetime cDT = System.now();         
        String UserCode = 'A00000' + cDT.format('yyyyMMddHHmmss');
        String TSI ;
        String AlternateDeliveryNotice=ConfigUtilController.getFaxEmailService();//'faxfeedbacklistener@7-5ffj39v4kdgp6t2cj8gv1qd9d.aypn3mak.a.apex.salesforce.com';
        String DeliveryNoticeType = 'ALL';
        String FaxResolution ;
        String To ;
        String From_x ;

        servicesWebtoolsEasylinkComSendfaxma.FaxDataFiles_element FaxDataFiles = new servicesWebtoolsEasylinkComSendfaxma.FaxDataFiles_element();
            servicesWebtoolsEasylinkComSendfaxma.Data_element objData_element = new servicesWebtoolsEasylinkComSendfaxma.Data_element();
            objData_element.FileContent =FileData;
            objData_element.FileType = FileType;
        FaxDataFiles.Data = new servicesWebtoolsEasylinkComSendfaxma.Data_element[]{objData_element};

        if (!system.Test.isRunningTest()){
            String Status = objSendFaxManagerService.SendFax(EmailAddress,Password,DestinationFaxNumber,AccountCode,UserCode,FaxDataFiles,TSI,AlternateDeliveryNotice,DeliveryNoticeType,FaxResolution,To,From_x);
            system.debug('*****Status*****EasyLinkFaxService.SendFax>>>'+Status);            
        }
        return UserCode;

    }
    
     // Generate Encrypted Data
    public static string  GenerateEncryptedData(String strData)
    {
        // Get Crypto Key
        //Crypto_Configuration__c sysConfObj = Crypto_Configuration__c.getInstance(Userinfo.getUserid());
        String EncodedBlobCryptoKey = 'rU7WA2ENzuZ9Lhcwj57HQw=='; //sysConfObj.CryptoKey__c;
        
        blob cryptoKey = EncodingUtil.base64Decode(EncodedBlobCryptoKey);
        
        // Encrypt and Encode Data
        Blob BlobData = Blob.valueOf(strData);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey, BlobData);
        String EncodedData = EncodingUtil.base64Encode(encryptedData); 
        return EncodedData;
    }
    
    @isTest
      public static void testSendFax(){                  
            //akritiv__Crypto_Configuration__c objCryptoConfiguration = new akritiv__Crypto_Configuration__c();
            //objCryptoConfiguration.akritiv__CryptoKey__c = 'rU7WA2ENzuZ9Lhcwj57HQw==';
            //insert objCryptoConfiguration;
            String str = GenerateEncryptedData('2cNvaOU+i3tfY8I90dagazQGCTfaGwh383+RlWarDy8=');
            Fax_Configuration__c faxconf = new Fax_Configuration__c();
            faxconf.Fax_Username__c = 'dipesh.patel@sailfintech.com';
            faxconf.Fax_Provider__c = 'EasyLink';
            faxconf.Fax_Email_Service__c = 'faxfeedbacklistenereasylink@c-1lvc1j9nxb0cz74v9fgnagcimb0dmtgf3sx33a86ydweow9lqj.a-ypn3mak.a.apex.salesforce.com';
            faxconf.Fax_Password__c = '2cNvaOU+i3tfY8I90dagazQGCTfaGwh383+RlWarDy8=';  
            Insert faxconf;            
            SendFax('448715283847','ABdcABdcABdcABdcABdcABdcABdcABdc','pdf');
            System.assertEquals(faxconf.Fax_Username__c,'dipesh.patel@sailfintech.com');
      }

}