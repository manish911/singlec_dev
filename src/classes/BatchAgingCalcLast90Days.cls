/*
* Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
* This software is the confidential and proprietary information
* (Confidential Information) of Akritiv Technologies, Inc.  You shall not
* disclose or use Confidential Information without the express written
* agreement of Akritiv Technologies, Inc.
*/
/*
* Usage : This class is used for calculating Batch aging values.
*/

global class BatchAgingCalcLast90Days implements Database.Batchable<SObject>, Database.Stateful,Schedulable {

public AccountWrapper aw;
String email;
public List<Data_Load_Batch__c> batchInfo ;
global Id batchInfoId ;
global String batchNumber;
global String sourceSystem;
global Account a = null;
public Map<String, InvoiceTypeConfiguration__c> mcs{get;set;}

global database.querylocator start(Database.BatchableContext bc){

String sQuery = '';

List<InvoiceTypeConfiguration__c> invoicelistconf = [select Name,Amount__c,Balance__c,Count_in_three_months_Billing__c,Country_Name__c,Credit_Debit__c,Group_Amount__c,
 Group_Balance__c,Invoice_Types__c,Remarks__c,
 Source_System__c from InvoiceTypeConfiguration__c where Count_in_three_months_Billing__c = true];
 mcs = new Map<String, InvoiceTypeConfiguration__c>();
 
 for(InvoiceTypeConfiguration__c inv :invoicelistconf ){
     mcs.put(inv.Name,inv );
 }
//mcs = InvoiceTypeConfiguration__c.getAll();

sQuery = 'select CurrencyIsoCode, Account__c,Local_Amount__c,Amount__c,Create_Date__c,Source_System__c,Transaction_Type__c, Account__r.CurrencyIsoCode,Balance__c , Base_Balance__c , Days_Past_Due__c from Transaction__c';


sQuery += ' where Account__c !=null and Balance__c != null and Due_Date__c != null ';

SysConfig_Singlec__c sysconfig1  = SysConfig_Singlec__c.getOrgDefaults();

        if ( sysconfig1.DSO_Query_Filter__c != null &&  sysconfig1.DSO_Query_Filter__c.trim() != '') {

            sQuery += ' and ' + sysconfig1.DSO_Query_Filter__c;
        }
        
         sQuery += '  order by Account__c , Days_Past_Due__c DESC';

//sQuery += ' and Create_Date__c = LAST_90_DAYS order by Account__c , Days_Past_Due__c DESC';


return Database.getQueryLocator(sQuery);
}

// Start calculation for Aging  
// Return Type - Void 
// Parameter - BC Database.Batchablecontext, sObjs List<sObject>
global void execute(Database.BatchableContext bc, List<sObject> sObjs){

 
   // Boolean isupdate = false;
    
    List<Account> accountsToUpdate = new List<Account>();
    List<Account> accountsToUpdate2 = new List<Account>();
    
    List<Id> accountIds= new List<Id>(); 
    
    for(Sobject txSobject : sObjs) {
    
        Transaction__c txtemp = (Transaction__c) txSobject;
        accountIds.add( txtemp.Account__c );
    }
    
    Map < Id , Account > accountsMap = new Map <Id, Account>([ select Id ,Base_Total_AR__c,Last_3_Months_Billing_Invoices__c, 
                                                                Total_AR__c,Age0__c,Last_3_Months_Credit_Invoices__c 
                                                                from Account where Id in: accountIds ]);
    
    
    
    for(Sobject txSobject : sObjs) {
        Transaction__c tx = (Transaction__c) txSobject;
        
        if(tx.Account__c != null && tx.Create_Date__c != null && tx.Amount__c != null) {
           

   
        
            if(aw == null) {
                aw = new AccountWrapper(tx.Account__c);
                a = new Account( id = tx.Account__c);
                // Check for isbucketonBase ( if true calcualte base total AR and Base Total Past due else Total past due )
         system.debug('===aw==='+aw);
                Account ac = accountsMap.get( a.Id );
                // ac.Last_3_Months_Billing_Invoices__c = ac.Last_3_Months_Billing_Invoices__c != null ? ac.Last_3_Months_Billing_Invoices__c : 0.00;
                 
                a.Last_3_Months_Billing_Invoices__c= 0;
                a.Last_3_Months_Credit_Invoices__c = 0;
                addDetails(tx);
                
            }else if(aw.accountId == tx.Account__c) {
                addDetails(tx);
                // accountsToUpdate2.add( a );
                
            }else{
                if(aw != null && aw.accountId != null) {
                    Account acc = new Account(Id = aw.accountId);             
                    accountsToUpdate.add(acc); 
                    accountsToUpdate2.add( a );
           
                    aw = new AccountWrapper(tx.Account__c);
                    a = new Account( id = tx.Account__c);
                         
                    Account ac = accountsMap.get( a.Id );
             
                    a.Last_3_Months_Billing_Invoices__c= 0;
                    a.Last_3_Months_Credit_Invoices__c=0;
     
                    addDetails(tx);
                      system.debug('===aw3==3='+aw);
            
                }
            }
        }
   
    }
    
     system.debug('===accountsToUpdate='+accountsToUpdate);
     system.debug('===accountsToUpdate2='+accountsToUpdate2);
    try {
    if(accountsToUpdate.size()>0){
    
        update accountsToUpdate;
    }
    
    if(accountsToUpdate2.size()>0){
    
        update accountsToUpdate2; 
    }
    
    } catch(Exception e){
        throw e;
        }
    }
    
    global void addDetails(Transaction__c tx){
    
    
  //  mcs = InvoiceTypeConfiguration__c.getAll();
    // Calculate Last 3 months billing invocies 
    //a.Last_3_Months_Billing_Invoices__c = a.Last_3_Months_Billing_Invoices__c != null ? a.Last_3_Months_Billing_Invoices__c : 0.00;
   String tempssTxtype = tx.Source_System__c+tx.Transaction_Type__c;
    
    system.debug('=====1==='+tempssTxtype);
     system.debug('=====mcs ==='+mcs.size());
       system.debug('=====mcs.containsKey(tempssTxtype)==='+mcs.containsKey(tempssTxtype));
  if(mcs.containsKey(tempssTxtype)){
   
   a.Last_3_Months_Billing_Invoices__c = a.Last_3_Months_Billing_Invoices__c + tx.Amount__c;
   
   // Only Last 3 Months Credit Invoices 
   /* if(tx.Amount__c < 0){
      a.Last_3_Months_Credit_Invoices__c = a.Last_3_Months_Credit_Invoices__c + tx.Amount__c; 
    }*/
    }
    system.debug('========'+a.Last_3_Months_Billing_Invoices__c);
   system.debug('========'+tx.Amount__c);
    
    
    }
    // Methods assins value to fields in tarnsaction the values which are calculated 
    // Return Type - Void 
    // Parameter - bc Database.Batchablecontext
    global void finish(Database.BatchableContext bc){
       if(aw != null && aw.accountId != null) {
            Account acc = new Account(Id = aw.accountId);
            update acc;
       }
         if(a != null){
              update a;
         }
        
    
    }
    
    global class AccountWrapper {
        public ID accountId {get; set; }  
        global AccountWrapper(Id accId) {
            this.accountId = accId;
    
        }
    }
    global void execute(SchedulableContext sc) {
        BatchAgingCalcLast90Days badpd = new BatchAgingCalcLast90Days();
        Database.executeBatch(badpd,200);
    
    }
}