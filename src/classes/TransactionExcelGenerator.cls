/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class creates Excel for  Transactions
 */

public with sharing class TransactionExcelGenerator {
    
    public String sidebar { get; set; }

    public String showHeader { get; set; }

    public String contentType { get; set; }
    public String resource  {get; set; }
    public String previousPage {get; set; }
   // private List<Temp_Object_Holder__c> pdfConf = [select Value__c from Temp_Object_Holder__c where Key__c =:UserInfo.getUserName()];
    public List<Temp_Object_Holder__c> txsConfig;
    public ApexPages.StandardSetController con {get; set; }
    public List<List<List<Object>>> lstTransactions {get; set; }
    public List<List<Object>>  accDetails {get; set; }
    String accId {get; set; }
    public String exportAll {get; set; }
    Boolean isData {get; set; }
    public List<String> fieldNames {get; set; }
    public String[] transactionIds {get; set; }
    public Integer myDMLLimit1{get;set;}
    public Boolean limitReached{get;set;}
    public String errorMessage{get;set;}
    public static Map<String, Schema.SObjectField> txMap = Schema.SObjectType.Transaction__c.fields.getMap();
    public String QuickFilter;
    public String selectedBucketFilter;
    public String ordertype;
    public String selectedAccount;


    public TransactionExcelGenerator() {
        try {
            
            contentType ='application/vnd.ms-excel#TransactionList.xls';
            limitReached = false;
            QuickFilter = '';
            ordertype = '';
            selectedBucketFilter = '';
            
            //get parameters
            String transactionIdsKey ;
            if(ApexPages.currentPage().getParameters().get('key') != null)
                transactionIdsKey = ApexPages.currentPage().getParameters().get('key');
                
            if(ApexPages.currentPage().getParameters().get('quickfilter') != null)
                QuickFilter = ApexPages.currentPage().getParameters().get('quickfilter');
            
            if(ApexPages.currentPage().getParameters().get('selectedBucket') != null && ApexPages.currentPage().getParameters().get('selectedBucket') != 'null')
                selectedBucketFilter = ApexPages.currentPage().getParameters().get('selectedBucket');
                
                     
            if(ApexPages.currentPage().getParameters().get('accId') != null)    
                this.accId = ApexPages.currentPage().getParameters().get('accId');
            
            if(ApexPages.currentPage().getParameters().get('exportall') != null)
                exportAll = ApexPages.currentPage().getParameters().get('exportall');
            
            String orderType ;
            if(ApexPages.currentPage().getParameters().get('ordertype') != null)
                orderType = ApexPages.currentPage().getParameters().get('ordertype');
                
           
            
            String sortFieldName;
            if(ApexPages.currentPage().getParameters().get('sortfield') != null)
                sortFieldName = ApexPages.currentPage().getParameters().get('sortfield');
            
            String sortFieldName1;
            if(ApexPages.currentPage().getParameters().get('sortfield1') != null)
                sortFieldName1 = ApexPages.currentPage().getParameters().get('sortfield1');
                
             if(ApexPages.currentPage().getParameters().get('selectedAccount') != null)
                selectedAccount = ApexPages.currentPage().getParameters().get('selectedAccount');

            if( sortFieldName1 == null) {
                sortFieldName1 ='';
            }

            String condition ='';
            lstTransactions = new List<List<List<Object>>>();

            Boolean isTxIds =false;
            
            if(exportAll=='false' && transactionIdsKey !=null && transactionIdsKey !='') {
                txsConfig = new List<Temp_Object_Holder__c>();
                txsConfig = [select value__c from Temp_Object_Holder__c where key__c =:transactionIdsKey limit 1];
                if(txsConfig.size() > 0 && txsConfig != null ) {
                    if(txsConfig.get(0).value__c != null && txsConfig.get(0).value__c != ''){
                        transactionIds = txsConfig.get(0).value__c.split(',');
                    
                        String Ids='';
                        for(String nextIdList : transactionIds) {
                            if(nextIdList.trim() !='' && nextIdList.trim() != null) {
                                Ids=Ids + '\'' + nextIdList + '\'' + ',';
                                isTxIds = true;
                            }
                        }
                        if(Ids != null && Ids != ''){
                            Ids = Ids.substring(0,Ids.length()-1);
                            condition = ' Id in (' + Ids + ') ';
                        }
                    }
                }
            }else{
            
              txsConfig = new List<Temp_Object_Holder__c>();
                txsConfig = [select value__c from Temp_Object_Holder__c where key__c =:transactionIdsKey limit 1];
                if(txsConfig.size() > 0 && txsConfig != null ) {
                    if(txsConfig.get(0).value__c != null && txsConfig.get(0).value__c != ''){
                        transactionIds = txsConfig.get(0).value__c.split(',');
                    
                        String Ids='';
                        for(String nextIdList : transactionIds) {
                            if(nextIdList.trim() !='' && nextIdList.trim() != null) {
                                Ids=Ids + '\'' + nextIdList + '\'' + ',';
                                isTxIds = true;
                            }
                        }
                        if(Ids != null && Ids != ''){
                            Ids = Ids.substring(0,Ids.length()-1);
                            condition = 'account__c in (' + Ids + ') ';
                        }                         
                    }
                }
            
            }

            //get current listview
            String selectedList = ApexPages.currentPage().getParameters().get('listview');
            
            /*************************Sorting ends ***********************************************/

            /*************************fetch Records starts ***********************************************/
            //Query the records
            //String SOQLQuery = 'Select '+ str + ' From Transaction__c where  Account__c =' + '\'' + controllerAccount.Id  + '\'' + ' order by ' + sortFieldName +   ' ' + orderType + sortFieldName1 + ' ' + ' Limit 9999';
            //begin filter contents

            String selectedFilterfield1;
            String selectedFilterOp1;
            String filterByValue1;
            String selectCondition1;

            String selectedFilterfield2;
            String selectedFilterOp2;
            String filterByValue2;
            String selectCondition2;

            String selectedFilterfield3;
            String selectedFilterOp3;
            String filterByValue3;
            String selectCondition3;

            String selectedFilterfield4;
            String selectedFilterOp4;
            String filterByValue4;
            String selectCondition4;

            String selectedFilterfield5;
            String selectedFilterOp5;
            String filterByValue5;

            String globalfilter = '';

            String namespace =   (UtilityController.getAkritivPackageNameSpace()=='') ? '' : (UtilityController.getAkritivPackageNameSpace()+'__');
            
              List<Custom_List_View__c> listViewObj = new  List<Custom_List_View__c>();
                       
            if(selectedList !='' || selectedList !=null || selectedList !='Default') {
                  
                  
                 // *  listViewObj  = [select Id,Object_Fields__c,User__c from Custom_List_View__c where Id =: selectedList and User__c =: UserInfo.getUserId()];
                  listViewObj  = [select Id,Object_Fields__c,User__c,Filter_Criteria__c from Custom_List_View__c where Id =: selectedList Limit 1];
                  
            }
            //String namespace =   '';
             Custom_List_View__c listView = null;
             if(listViewObj.size()>0) {
                listView = listViewObj[0];
             }
            String [] filters = null;
            if (listView != null && listView.Filter_Criteria__c != null ) {

                filters = listView.Filter_Criteria__c.split('NEXTFILTER');

                String filter1 = '';
                String filter2 = '';
                String filter3 = '';
                String filter4 = '';
                String filter5 = '';

                String filter1Condition = '';
                String filter2Condition = '';
                String filter3Condition = '';
                String filter4Condition = '';

                if ( filters.size() > 0 ) {

                    String [] firstFilterValues =  filters[0].split(':');
                    selectedFilterfield1 = firstFilterValues[0];
                    selectedFilterOp1 = firstFilterValues[1];

                    if(firstFilterValues.size() > 2 )
                    {
                        filterByValue1=firstFilterValues[2];
                        if (filterByValue1 != null && filterByValue1.trim().length() == 0 ) {

                            filterByValue1 = null;
                        }

                    }else{
                        filterByValue1=null;

                    }

                    if ( filterByValue1 != null ) {
                        Boolean b = quotesRequired(selectedFilterfield1);
                        if (b) {
                            filterByValue1 = '\''+filterByValue1+'\'';
                        }
                    }
                    if ( firstFilterValues.size() > 3 ) {
                        if(firstFilterValues[3].trim() != 'AND') {
                            selectCondition1 = firstFilterValues[3];
                        }
                        else{
                            selectCondition1 ='and';
                        }
                    }
                     //Added by Eshan For Does not Contains
                    if(selectedFilterOp1.trim() == 'NotContains'){
                        filter1 = '( NOT '+selectedFilterfield1   +' '+' Like' +' \'%'+  filterByValue1.replaceAll('\'','')+'%\' )';
                    }
                    else if(selectedFilterOp1.trim() == 'Contains'){
                        filter1 = selectedFilterfield1   +' '+' Like' +' \'%'+  filterByValue1.replaceAll('\'','')+'%\' ';
                    }
                    else{
                    filter1 = selectedFilterfield1   +' '+ selectedFilterOp1 +' '+  filterByValue1;
                    }
                    filter1Condition = selectCondition1;
                    system.debug('@@@Filter value@@'+filter1);
                 
                }

                if ( filters.size() > 1 ) {
                    String [] firstFilterValues =  filters[1].split(':');

                    selectedFilterfield2 = firstFilterValues[0];

                    selectedFilterOp2 = firstFilterValues[1];


                    if(firstFilterValues.size() > 2)
                    {
                        filterByValue2=firstFilterValues[2];
                        if (filterByValue2 != null && filterByValue2.trim().length() == 0 ) {

                            filterByValue2 = null;
                        }

                    }else{
                        filterByValue2=null;

                    }

                    if ( filterByValue2 != null ) {
                        Boolean b = quotesRequired(selectedFilterfield2);
                        if (b) {
                            filterByValue2 = '\''+filterByValue2+'\'';
                        }
                    }
                    if ( firstFilterValues.size() > 3 ) {
                        if(firstFilterValues[3].trim() != 'AND') {
                            selectCondition2 = firstFilterValues[3];
                        }
                        else{
                            selectCondition2 ='and';
                        }
                    }

                     //Added by Eshan For Does not Contains
                    if(selectedFilterOp2.trim() == 'NotContains'){
                        filter2 = '( NOT '+selectedFilterfield2   +' '+' Like' +' \'%'+  filterByValue2.replaceAll('\'','')+'%\' )';
                    }
                    else if(selectedFilterOp2.trim() == 'Contains'){
                        filter2 = selectedFilterfield2   +' '+' Like' +' \'%'+  filterByValue2.replaceAll('\'','')+'%\' ';
                    }
                    else{
                          filter2 = selectedFilterfield2 +' '+ selectedFilterOp2 +' '+  filterByValue2;
                    }
                    
                    filter2Condition = selectCondition2;
                }

                if ( filters.size() > 2 ) {

                    String [] firstFilterValues =  filters[2].split(':');

                    selectedFilterfield3 = firstFilterValues[0];

                    selectedFilterOp3 = firstFilterValues[1];

                    if(firstFilterValues.size() > 2)
                    {
                        filterByValue3=firstFilterValues[2];
                        if (filterByValue3 != null && filterByValue3.trim().length() == 0 ) {

                            filterByValue3 = null;
                        }

                    }else{
                        filterByValue3=null;

                    }

                    if ( filterByValue2 != null ) {
                        Boolean b = quotesRequired(selectedFilterfield3);
                        if (b) {
                            filterByValue3 = '\''+filterByValue3+'\'';
                        }
                    }

                    if ( firstFilterValues.size() > 3 ) {
                        if(firstFilterValues[3].trim() != 'AND') {
                            selectCondition3 = firstFilterValues[3];
                        }
                        else{
                            selectCondition3 ='and';
                        }
                    }

                     //Added by Eshan For Does not Contains
                    if(selectedFilterOp3.trim() == 'NotContains'){
                        filter3 = '( NOT '+selectedFilterfield3   +' '+' Like' +' \'%'+  filterByValue3.replaceAll('\'','')+'%\' )';
                    }
                    else if(selectedFilterOp3.trim() == 'Contains'){
                        filter3 = selectedFilterfield3   +' '+' Like' +' \'%'+  filterByValue3.replaceAll('\'','')+'%\' ';
                    }
                    else{
                          filter3 =  selectedFilterfield3 +' '+ selectedFilterOp3+' '+ filterByValue3;
                    }
                    system.debug('---In Filter 3---'+filter3);
                    filter3Condition = selectCondition3;
                }

                if ( filters.size() > 3 ) {

                    String [] firstFilterValues =  filters[3].split(':');
                    if(firstFilterValues.size() > 2){
                        selectedFilterfield4 = firstFilterValues[0];
    
                        selectedFilterOp4 = firstFilterValues[1];
    
                        if(firstFilterValues.size() > 2)
                        {
                            filterByValue4=firstFilterValues[2];
                            if (filterByValue4 != null && filterByValue4.trim().length() == 0 ) {
    
                                filterByValue4 = null;
                            }
    
                        }else{
                            filterByValue4=null;
    
                        }
    
                        if ( filterByValue4 != null ) {
                            Boolean b = quotesRequired(selectedFilterfield4);
                            if (b) {
                                filterByValue4 = '\''+filterByValue4+'\'';
                            }
                        }
    
                        if ( firstFilterValues.size() > 3 ) {
                            if(firstFilterValues[3].trim() != 'AND') {
                                selectCondition4 = firstFilterValues[3];
                            }
                            else{
                                selectCondition4 ='and';
                            }
                        }
    
                        //Added by Eshan For Does not Contains
                        if(selectedFilterOp4.trim() == 'NotContains'){
                            filter4 = '( NOT '+selectedFilterfield4   +' '+' Like' +' \'%'+  filterByValue4.replaceAll('\'','')+'%\' )';
                        }
                        else if(selectedFilterOp4.trim() == 'Contains'){
                            filter4 = selectedFilterfield4   +' '+' Like' +' \'%'+  filterByValue4.replaceAll('\'','')+'%\' ';
                        }
                        else{
                          filter4 = selectedFilterfield4 + ' '+ selectedFilterOp4 + ' '+filterByValue4;
                        }
                        
                        filter4Condition = selectCondition4;
                    }
                }

                if ( filters.size() > 4 ) {

                    String [] firstFilterValues =  filters[4].split(':');

                    selectedFilterfield5 = firstFilterValues[0];

                    selectedFilterOp5 = firstFilterValues[1];
                    if(firstFilterValues.size() > 2)
                    {
                        filterByValue5=firstFilterValues[2];
                        if (filterByValue5 != null && filterByValue5.trim().length() == 0 ) {

                            filterByValue5 = null;
                        }

                    }else{
                        filterByValue5=null;

                    }

                    if ( filterByValue4 != null ) {
                        Boolean b = quotesRequired(selectedFilterfield5);
                        if (b) {
                            filterByValue5 = '\''+filterByValue5+'\'';
                        }
                    }
                     //Added by Eshan For Does not Contains
                        if(selectedFilterOp5.trim() == 'NotContains'){
                            filter5 = '( NOT '+selectedFilterfield5   +' '+' Like' +' \'%'+  filterByValue5.replaceAll('\'','')+'%\' )';
                        }
                        else if(selectedFilterOp5.trim() == 'Contains'){
                            filter5 = selectedFilterfield5   +' '+' Like' +' \'%'+  filterByValue5.replaceAll('\'','')+'%\' ';
                        }
                        else{
                          filter5 = selectedFilterfield5 + ' '+ selectedFilterOp5 +' '+filterByValue5;
                        }
                }

                String orGroup = '';
                String endGroup = filter1;

                if ( filter1Condition == 'AND') {

                    if ( filter2.length() > 0 )
                        endGroup = endGroup + ' AND ' + filter2;
                }else {
                    if ( filter2.length() > 0 ){
                            
                            if(orGroup == null || orGroup == ''){
                                orGroup = filter1;
                            }

                        orGroup = orGroup + ' OR '+ filter2;
                    }
                }

                if ( filter2Condition == 'AND' ) {
                    if ( filter3.length() > 0 )
                        endGroup = endGroup + ' AND ' + filter3;
                }else {
                    if ( filter3.length() > 0 ) {
                        if(orGroup.length() > 0) {
                            orGroup = orGroup + ' OR ' + filter3;
                        }else{
                            orGroup = orGroup+ ' ' + filter3;
                        }
                    } 
                }

                if ( filter3Condition == 'AND' ) {
                    if ( filter4.length() > 0 )
                        endGroup = endGroup + ' AND ' + filter4;
                }else {
                    if ( filter4.length() > 0 ) {
                        if(orGroup.length() > 0) {
                            orGroup = orGroup + ' OR ' + filter4;
                        }else{
                            orGroup = orGroup+ ' ' + filter4;
                        }

                    }
                   
                }

                if ( filter4Condition == 'AND' ) {
                    if ( filter5.length() > 0 )
                        endGroup = endGroup + ' AND ' + filter5;
                }else {
                    if ( filter5.length() > 0 ) {
                        if(orGroup.length() > 0) {
                            orGroup = orGroup + ' OR ' + filter5;
                        }else{
                            orGroup = orGroup+ ' ' + filter5;
                        }

                    }
                   
                }

                if ( orGroup != null && orGroup.trim().length() > 0 ) {

                //    globalfilter = '(' +orGroup + ') AND (' + endGroup + ')';
                        globalfilter = '(' + orGroup + ')';

                } else {

                    globalfilter = '(' + endGroup+ ')' ;
                }

                if(globalfilter.length() > 0 )
                {
                    globalfilter = globalfilter + ' and ';
                    system.debug('--global filter-----'+globalfilter);
                }
            }
            
                // method is used to Promise to pay 
    // return type : boolean 
    // parameters : fieldName - String
    
    system.debug('---selectedBucketFilter ----'+selectedBucketFilter );
        system.debug('---selectedBucketFilter.trim()----'+selectedBucketFilter.length());
    
   
        if(selectedBucketFilter != null && selectedBucketFilter !=''){
        ConfigUtilController.AgingBucket bucket = ConfigUtilController.getBucketByAge(Integer.valueOf(selectedBucketFilter));
    
       globalfilter  = globalfilter  + 'Days_Past_Due__c <= ' + bucket.maxRange + ' and '+namespace+'Days_Past_Due__c >= ' + bucket.minRange + ' and ';
      
    } 
    else{
         system.debug('---selectedBucketFilter ----'+selectedBucketFilter );
    }
    
  /*  if(selectedBucket != null && selectedBucket != ''){
        globalfilter  = globalfilter + '' + selectedBucket + ' and ';
    }
    */
    if(QuickFilter  != null && QuickFilter != ''){
       
        //globalfilter  = globalfilter + '' + string.escapeSingleQuotes(QuickFilter);
        globalfilter  = globalfilter + '' + QuickFilter; //Add by Eshan
        system.debug('---Global filter---'+globalfilter);
    }
    
    system.debug('-----globalfilter -----'+globalfilter);
            List<String> fieldList = new List<String>();
            Set<String> setList = new Set<String>();

            fieldList.add('name');
            if(listViewObj.size()>0) {
                if(listViewObj[0].Object_Fields__c != null && listViewObj[0].Object_Fields__c  !='') {
                    string fieldTypes = listViewObj[0].Object_Fields__c;
                    for(String nextField : fieldTypes.split(',') ) {
                        if(!setList.contains(nextField) && nextField.trim() !='') {
                            fieldList.add(nextField );
                            setList.add(nextField );
                        }
                    }
                }
            }else{
                for(String nextField : TransactionUtil.getDefaultListView()) {
                    fieldList.add(nextField);
                }
            }

            fieldNames = new List<String>{};

            List<String> fieldType = new List<String>{};

            String str='';
            //system.debug('fieldList====>'+fieldList);
            for(String fieldName : fieldList)
            {

                TransactionDescribeCall.FieldDescibe fieldObj = TransactionDescribeCall.getFieldData(fieldName);

                //add extra field if field type Schema.DisplayType.REFERENCE exist
                if(fieldObj !=null && fieldObj.fieldType == Schema.DisplayType.REFERENCE ) {
                    String refFieldName = fieldName.subString(0,fieldName.length()-1)+ 'r.Name';
                    str =str +  refFieldName  + ',';
                }

                fieldType.add(fieldObj.fieldName);
                fieldNames.add(fieldObj.fieldLabel);
                if(fieldName != 'CurrencyIsoCode')
                    str = str + fieldName + ',';

            }

            //str = str.substring(0,str.length()-1);
           // if(ConfigUtilController.captureNoteOnTransaction()){
                str =str +  'Note_Date__c' + ',';
                        
                str =str +  'Notes__c' + ',';
                
                str =str +  'CreatedBy.Name' + ',';
                
          //  }
            str = str  + '(Select Id, Title,Body,CreatedDate,CreatedBy.Name from Notes order by createdDate DESC Limit 1)';
            List<Account> accObj = new List<Account>();
            accObj =new List<Account>([select name,AccountNumber from Account where Id=:this.accId]);
            
            system.debug('----sortFieldName----'+sortFieldName);
             system.debug('----sortFieldName1----'+sortFieldName1);
            
            String SOQLQuery = null;
            if ( UtilityController.isMultiCurrency() ) {
                 if(sortFieldName1 != null && sortFieldName1 != '')
                     SOQLQuery = 'Select CurrencyIsoCode,'+ str + ' From Transaction__c where ' + globalfilter +  ' ' + condition   + ' order by ' + string.escapeSingleQuotes(sortFieldName) +   ' ' + string.escapeSingleQuotes(orderType) + string.escapeSingleQuotes(sortFieldName1) + ' ' + ' Limit 10000';
                else
                  SOQLQuery = 'Select CurrencyIsoCode,'+ str + ' From Transaction__c where ' + globalfilter +  ' ' + condition   + ' order by ' + string.escapeSingleQuotes(sortFieldName)  + ' ' + string.escapeSingleQuotes(orderType) + ' Limit 10000';
            } else {

              //  SOQLQuery = 'Select '+ str + ' From Transaction__c where  Account__c =' + '\'' + accId + '\' ' + condition + ' and ' + globalfilter +   ' order by ' + sortFieldName +   ' ' + orderType + sortFieldName1 + ' ' + ' Limit 10000';
                 if(sortFieldName1 != null && sortFieldName1 != '')
                     SOQLQuery = 'Select '+ str + ' From Transaction__c where ' + globalfilter + '   ' + condition  + ' order by ' + string.escapeSingleQuotes(sortFieldName) +   ' ' + string.escapeSingleQuotes(orderType) + string.escapeSingleQuotes(sortFieldName1) + ' ' + ' Limit 10000';
                 else
                     SOQLQuery = 'Select '+ str + ' From Transaction__c where ' + globalfilter + '  ' + condition  + ' order by ' + string.escapeSingleQuotes(sortFieldName) + ' ' + string.escapeSingleQuotes(orderType) + ' Limit 10000';
                

            }
            //String SOQLQuery = 'Select '+ str + ' From Transaction__c where  Account__c =' + '\'' + accId + '\' ' + condition   + ' order by ' + sortFieldName +   ' ' + orderType + sortFieldName1 + ' ' + ' Limit 10000';
            system.debug('--SOQLQuery ---'+SOQLQuery);
            con = new ApexPages.StandardSetController(Database.getQueryLocator(SOQLQuery ));
            
            List<Transaction__c> txList1 = Database.query(SOQLQuery);
            
            system.debug('---con.size---'+con.getRecords());
            
            
           
            con.setPageSize(900);

            //add account details
            accDetails = new List<List<Object>>();
            List<Object> dateLst = new List<Object>();
            dateLst.add(Label.Label_Date);
            String strDate = Date.Today().format();
            dateLst.add(strDate);
            accDetails.add(dateLst);

            List<Object> accNameLst = new List<Object>();
            accNameLst.add(Label.Label_Account_Name);
            accNameLst.add(accObj.get(0).Name);
            accDetails.add(accNameLst);

            List<Object> accLst = new List<Object>();
            accLst.add(Label.Label_Account_Number);
            accLst.add(accObj.get(0).AccountNumber);
            accDetails.add(accLst);

            List<Object> blankLst = new List<Object>();
            blankLst.add(' ');
            blankLst.add(' ');
            accDetails.add( blankLst);

            isData = true;
            Integer i=0;

            if( con.getResultSize()>0 ) {

                while(isData) {
                    //add next list
                    if(i >1) {
                        con.next();
                    }
                    List<Transaction__c> nextTransactions = new List<Transaction__c>();
                   // nextTransactions = (List<Transaction__c>)con.getRecords();
                    nextTransactions = txList1 ;
                    List<List<Object>> obj = new List<List<Object>>();
                    //get up to 900 transactions
                    for(Transaction__c nextTr : nextTransactions ) {
                    if(nextTr.Notes__c != '' && nextTr.Notes__c != null){
                    nextTr.Notes__c = nextTr.Notes__c.replaceAll('\n','<br/>');
                       }
                        List<Object>  tempList = new List<Object>();

                        String noteTitle=''; String noteBody='';
                        String createdDate=''; String createdBy='';
                        //add row data
                        for(String field : fieldType )
                        {
                            TransactionDescribeCall.FieldDescibe fieldObj = TransactionDescribeCall.getFieldData(field);

                            //get  notes title and body
                            if(ConfigUtilController.useNativeNote()){
                            if(nextTr.Notes.size() >0) {
                                if(nextTr.Notes[0].Title != '' && nextTr.Notes[0].Title !=null) {
                                    noteTitle = nextTr.Notes[0].Title;
                                    noteBody = nextTr.Notes[0].Body;
                                    //system.debug('noteBody----' + noteBody);
                                    createdDate =nextTr.Notes[0].CreatedDate.format();
                                    createdBy = nextTr.Notes[0].CreatedBy.Name;
                                }
                            }
                           }
                           else{
                            system.debug('---nextTr.Notes__c----'+nextTr.Notes__c);
                                if(nextTr.Notes__c != '' && nextTr.Notes__c !=null) {
                                   // noteTitle = nextTr.Notes[0].Title;
                                    noteBody = nextTr.Notes__c;
                                    system.debug('noteBody----' + noteBody);
                                    createdDate =String.ValueOf(nextTr.Note_Date__c);
                                    createdBy = nextTr.CreatedBy.Name;
                                    
                                   
                                
                            }
                           }
                            if(fieldObj.fieldType ==Schema.DisplayType.DATE) {
                                if(nextTr.get(field) !=null ) {
                                    Date dtVar =(Date) nextTr.get(field);
                                    tempList.add(dtVar.format());                                    
                                }else{
                                    tempList.add(' ');
                                }
                            }
                            else if(fieldObj.fieldType ==Schema.DisplayType.CURRENCY) {
                                String formattedAmt = '';
                                Double amnt = (Double)nextTr.get(field);
                                if(amnt !=null)
                                {

                                    //@TODAO : Bhavik : Ref : On-Demand Multi-Currency
                                    String currencyIsoCode = ( String )(UtilityController.isMultiCurrency() ? nextTr.get('CurrencyIsoCode') : UtilityController.getSingleCurrencyOrgCurrencyCode());
                                    formattedAmt = ''+ UtilityController.formatDoubleValue(currencyIsoCode, amnt);

                                }

                                tempList.add(formattedAmt);
                            }
                            else if(fieldObj.fieldType == Schema.DisplayType.REFERENCE) {
                                if(nextTr.get(field) !=null ) {
                                    String refFieldName = field.subString(0,field.length()-1) + 'r';
                                    String str12 =  String.valueOf(nextTr.getSobject(refFieldName).get('name')).escapeHtml4();
                                    tempList.add((Object)str12);
                                }else{
                                    tempList.add(' ');
                                }
                            }
                            else if(nextTr.get(field)!=null)
                            {
                                tempList.add(nextTr.get(field));
                            }
                            else
                            {
                                tempList.add(' ');
                            }
                        }
                        //add note title value
                        if(ConfigUtilController.useNativeNote()){
                        tempList.add(noteTitle);
                        }
                        if(noteBody == null)
                            noteBody='';
                        tempList.add(noteBody.replaceall('<br/>','\r').escapeHtml4());
                        tempList.add(createdBy );
                        tempList.add(createdDate);
                          system.debug('tempList---' + tempList);  
                        //add header labels here
                        if(i==0) {
                            List<Object>  labelList = new List<Object>();
                            for(String nextLabel : fieldNames) {
                                labelList.add(nextLabel);
                            }
                            //add note label
                            if(ConfigUtilController.useNativeNote()){
                                labelList.add(Label.Label_Note_Title);
                            }
                            labelList.add(Label.Label_Note_Body);
                            labelList.add(Label.Note_created_By);
                            labelList.add(Label.Note_created_Date);
                            obj.add(labelList);
                        }

                        i=i+1;
                        obj.add(tempList);
                         system.debug('obj---' + Obj);
                        Integer myDMLLimit = Limits.getLimitScriptStatements();
                        Integer myDMLLimit1 = Limits.getScriptStatements();
                        if(myDMLLimit1 > myDMLLimit - 1000){
                          Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR,Label.Label_Exporting_view));
                          limitReached = true;
                          contentType = '';
                          
                          showHeader = 'true';
                          
                          sidebar = 'true';
                          
                          
                          return;
                        }
                        
                        //Issue #AKTPROD-90
                        //Done By Hemanshu Patel on 8th Jan,2013
                        Integer myDMLLimit2 = Limits.getHeapSize();
                        Integer myDMLLimit3 = Limits.getLimitHeapSize();
                        system.debug('myDMLLimit2-----'+myDMLLimit2 );
                        if(myDMLLimit2 > myDMLLimit3 ){
                          Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, Label.Label_Exporting_view));
                          limitReached = true;
                          contentType = '';
                          
                          showHeader = 'true';
                          
                          sidebar = 'true';
                          
                          
                          return;
                        }
                        
                    
                    }
                     system.debug('lstTransactions==>'+lstTransactions);
                    lstTransactions.add(obj);
                    isData =con.getHasNext();
                }
            }

        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }

    }
 public boolean quotesRequired(String fieldName)
    {

        Schema.SObjectField txfield = txMap.get(fieldName.trim());
        
        Schema.DescribeFieldResult txFieldDescribe = txField.getDescribe();

        Schema.DisplayType datatypeTx = txFieldDescribe.getType();

        if ( datatypeTx.name().equals('DATE') || datatypeTx.name().equals('Date') ) {

            return false;

        }else if ( datatypeTx.name().equals('STRING') || datatypeTx.name().equals('String') ) {

            return true;
        }else if ( datatypeTx.name().equals('PICKLIST') || datatypeTx.name().equals('Picklist') ) {

            return true;
        }else if ( datatypeTx.name().equals('Boolean') || datatypeTx.name().equals('BOOLEAN') ) {

            return false;
        }else if ( datatypeTx.name().equals('Integer') || datatypeTx.name().equals('INTEGER') ) {

            return false;
        }else if ( datatypeTx.name().equals('Double') || datatypeTx.name().equals('DOUBLE') ) {

            return false;
        }else if ( datatypeTx.name().equals('ID') || datatypeTx.name().equals('Id') ) {

            return true;
        }else if ( datatypeTx.name().equals('DateTime') || datatypeTx.name().equals('DATETIME') ) {

            return false;
        }else if ( datatypeTx.name().equals('URL') || datatypeTx.name().equals('url') ) {

            return true;
        }else if ( datatypeTx.name().equals('Email') || datatypeTx.name().equals('EMAIL') ) {

            return true;
        }else if ( datatypeTx.name().equals('Currency') || datatypeTx.name().equals('CURRENCY') ) {

            return false;
        }

        return true;
    }

   
}