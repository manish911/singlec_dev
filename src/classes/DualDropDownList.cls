/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Used to create multiple select dropdown list
 */
public class DualDropDownList
{
    //private variable declaration
    private List<List<customOption>> options;
    private List<List<customOption>> selectedOptions;
    private String listWidth = '300';
    private String SearcDisplay = 'block';

    public String strDisplay
    {
        get
        {
            return SearcDisplay;
        }
        set
        {
            if(value != null)
            {
                SearcDisplay = value;
            }

        }
    }
    public String lstWidth
    {
        get
        {
            return listWidth;
        }
        set
        {
            if(value != null)
            {
                if (Integer.Valueof(value) >= 200 && Integer.Valueof(value) <= 500)
                {
                    listWidth = value;
                }
            }

        }
    }

    //public properties
    //list of values to bind on the available list on UI
    public List<List<customOption>> Items
    {
        get
        {
            return options;
        }
        set
        {
            options = value;
        }
    }

    //list of values for available list received from the container page
    public string[] strItems
    {
        get;
        set
        {
            if(value != null)
            {
                options = getList(value);
            }
        }
    }

    //list of values for selected list received from the container page
    public string[] strSelectedItems
    {
        get;
        set
        {
            if(value != null)
            {
                selectedOptions  = getList(value);
            }
        }
    }
   
    
    //list of values to bind on the selected list on UI
    public List<List<customOption>> SelectedItems
    {
        get
        {
            return selectedOptions;
        }
        set
        {
            selectedOptions = value;
        }
    }

    //constructor
    public DualDropDownList()
    {

    }
    //method to extract data recieved in string array and convert it into desired format
    // return type : List<List<customOption>>
    // parameters : itemList - String[]
    public List<List<customOption>> getList(String[] itemsList)
    {

        List<list<customOption>> lstCustomOptions = new List<List<customOption>>();
        //loop on individual list element
        for(string singleList : itemsList)
        {
            list<customOption> lstCustomOptionsSingleList = new list<customOption>();
            // extract name value pairs separated by ~#~# in the string
            List<string> arrNameValue;
            
           // system.debug('--singleList --'+singleList);
            //if(singleList.contains('~#~#'))
            if(singleList != null && singleList !='')
            {
                arrNameValue = singleList.split('~#~#');
                //loop on all the name value pairs
                for(string nameValuePair : arrNameValue)
                {
                    if(nameValuePair != null && nameValuePair != '')
                    {
                        //extract individual options key and value
                        List<string>arrValues = nameValuePair.split('~#');
                        if(arrValues.size() >= 2)
                        {
                        
                       // system.debug('---arrValues[0]----'+arrValues[0]);
                       // system.debug('---arrValues[1]----'+arrValues[1]);
                            // create a new customOptions object and add it to the list
                            // Jira Issue #AKTPROD-86
                            // Updated on 1-Dec-2013
                            // By Twinkle Shah
                           if(arrValues[0] != 'Notes__c' && arrValues[0] != 'Note_Date__c' && arrValues[0] != 'akritiv__Notes__c' && arrValues[0] != 'akritiv__Note_Date__c' )
                                lstCustomOptionsSingleList.add(new customOption(arrValues[0],arrValues[1]));
                        }
                    }
                }

                //add to list of list 
                lstCustomOptions.add(lstCustomOptionsSingleList);
            }
        }
        return lstCustomOptions;
    }
   
    //class for custom options list
    public class customOption
    {
        //constructor to set the values
        public customOption(string name,string value)
        {
            key=name;
            val=value;
        }

        //public properties
        public string key {get; set; }
        public string val {get; set; }
    }
}