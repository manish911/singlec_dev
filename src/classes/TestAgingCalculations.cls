/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for AgingCalculations.
 */

@isTest

private class TestAgingCalculations

{

    static testMethod void testCase()

    {

        List<Transaction__c> txList = new List<Transaction__c>();

        //Account insert
        Account tempAccount = DataGenerator.accountInsert();

        //Tx insert on Account
        txList = DataGenerator.transactionInsert(tempAccount);

        Test.startTest();

        AgingCalculations controller = new AgingCalculations();

        controller.executeCalculations();
        controller.changeIsAgingProcess();

        System.assertEquals(controller.enableBtn, true);
        System.assertEquals(controller.AccUpdated, 0);

        Test.stopTest();

    }

}