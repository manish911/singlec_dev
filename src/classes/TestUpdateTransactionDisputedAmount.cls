/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Test class for the Trigger UpdateTransactionDisputedAmount.
 */
@isTest
private class TestUpdateTransactionDisputedAmount {
    static testMethod void testInsertUpdateAndDelete() {
        // create an test account
        Account acc = new Account();
        acc.Account_Key__c = '67263511';
        acc.Name = 'TestAccount';
        insert acc;

        // create a list of transaction related to the test account
        List<Transaction__c> txList = new List<Transaction__c>();
        for(Integer i=0; i<10; i++) {
            Transaction__c tx = new Transaction__c();
            tx.Name = '990000'+i;
            tx.Account__c = acc.Id;
            tx.Amount__c = 50000;
            tx.Balance__c = 1000;
            tx.Promised_Amount__c = 5000;

            if(i==0)
                tx.Disputed_Amount__c = null;
            else
                tx.Disputed_Amount__c = 1000;

            tx.Create_Date__c = Date.today()-7;
            txList.add(tx);
        }
        insert txList;

        Set<Id> txIds = new Set<Id>();
        for(Transaction__c tx : txList)
            txids.add(tx.Id);

        // create Disputes for all the transactions
        List<Dispute__c> disputesList = new List<Dispute__c>();
        for(Transaction__c tx : txList) {
            Dispute__c d1 = new Dispute__c();
            d1.Transaction__c = tx.Id;
            d1.Account__c = tx.Account__c;
            d1.Amount__c = tx.Balance__c;
            disputesList.add(d1);

            Dispute__c d2 = new Dispute__c();
            d2.Transaction__c = tx.Id;
            d2.Account__c = tx.Account__c;
            d2.Amount__c = tx.Balance__c;
            disputesList.add(d2);
        }

        // insert the newly created disputes
        insert disputesList;

        txList = [select Disputed_Amount__c from Transaction__c where id in:txIds ];
        for(Integer i=0; i<txList.size(); i++) {
            if(i==0)
                System.assertEquals(2000, txList.get(i).Disputed_Amount__c );
            else
                System.assertEquals(3000, txList.get(i).Disputed_Amount__c );
        }

        for(Dispute__c d : disputesList)
            d.Amount__c = d.Amount__c + 100;

        // update the disputes
        update disputesList;
        txList = [select Disputed_Amount__c from Transaction__c where id in:txIds ];
        for(Integer i=0; i<txList.size(); i++) {
            if(i==0)
                System.assertEquals(2200, txList.get(i).Disputed_Amount__c );
            else
                System.assertEquals(3200, txList.get(i).Disputed_Amount__c );
        }

        // delete all the disputes
        delete disputesList;
        txList = [select Disputed_Amount__c from Transaction__c where id in:txIds ];

        for(Integer i=0; i<txList.size(); i++) {
            if(i==0)
                System.assertEquals(0, txList.get(i).Disputed_Amount__c );
            else
                System.assertEquals(1000, txList.get(i).Disputed_Amount__c );
        }
    }
}