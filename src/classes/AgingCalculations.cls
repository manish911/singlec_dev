/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   This class calculate againg fileds on account level .
 */
/*
    This class is not in used - please see BatchAgingCalculations instead.
 */
public with sharing class AgingCalculations {
    public Integer AccUpdated {get; set; }
    public Boolean enableBtn {get; set; }
    
    // constructor to sets aging value
    public AgingCalculations(){

        enableBtn = true; AccUpdated =0;

    }
    
    // Method to Calculate Aging values
    // parap - no parameter
    // return - void
    public void executeCalculations(){
        enableBtn=false;
        //@TODO[harit] Why is this commented?
        enableBtn =true;
    }

    public void changeIsAgingProcess(){
            }
}