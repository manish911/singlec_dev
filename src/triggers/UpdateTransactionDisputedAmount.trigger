/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Trigger that will accumulate the disputed amount for a transaction.
 *             from its related Disputes
 */

trigger UpdateTransactionDisputedAmount on Dispute__c (after delete, after insert, after update,before update,before insert) {
    // ------------------------------------------------//
    // when dispute__c records are inserted.
    // ------------------------------------------------//
    
     Map<Id, String> disStatusBeforeMap = new Map<Id, String>();
    if(ConfigUtilController.getUpdateTransactionDisputedAmount()){

        if(Trigger.isInsert && Trigger.isAfter) {
            
            // Maintain a map of transaction id as key and new total dispute amount inserted as its value.
            // and update the map over the dispute amount for all disputes
            Map<Id,Double> txDispAmtMap = new Map<Id,Double>();
            for(Dispute__c disp:Trigger.new) {
                if(disp.Transaction__c != null && disp.Balance__c != null) {
                    Double txDispAmtVal = txDispAmtMap.get(disp.Transaction__c);
                    // if tx has no entry in the map
                    if(txDispAmtVal == null)
                        txDispAmtMap.put(disp.Transaction__c,disp.Balance__c);
                    // if tx already has an entry in the map
                    else
                        txDispAmtMap.put(disp.Transaction__c,disp.Balance__c + txDispAmtVal);
                }   
            }
            
            // get the actual transaction dispute amount and update it by the txDispAmtMap with the latest disputed amount inserted. 
            Map<Id,Transaction__c> txMap = new Map<Id,Transaction__c>([select Id, Disputed_Amount__c from Transaction__c where id in: txDispAmtMap.keySet()]);
            
            List<Transaction__c> txsToUpdate = new List<Transaction__c>();
            for(Id txId:txDispAmtMap.keySet()) {
                Transaction__c trans = txMap.get(txId);
                if(trans!= null) {
                    trans.Disputed_Amount__c = (trans.Disputed_Amount__c == null? 0:trans.Disputed_Amount__c)+txDispAmtMap.get(txId);
                }
                txsToUpdate.add(trans);
            }
            
            if(txsToUpdate.size()>0)
                update txsToUpdate;
                
                
        }
    
        // ------------------------------------------------//
        // when dispute__c records are updated.
        // ------------------------------------------------//   
        else if(Trigger.isUpdate && Trigger.isAfter) {
            Map<Id,Double> txDispAmtMap = new Map<Id,Double>();
            for(Dispute__c disp:Trigger.new) {
                if(disp.Transaction__c != null && disp.Balance__c != null) {
                    Double txDispAmtVal = txDispAmtMap.get(disp.Transaction__c);
                    if(txDispAmtVal == null)
                        txDispAmtMap.put(disp.Transaction__c,disp.Balance__c);
                    else
                        txDispAmtMap.put(disp.Transaction__c,txDispAmtVal + disp.Balance__c );
                }   
            }
            
            for(Dispute__c disp:Trigger.old) {
                if(disp.Transaction__c != null && disp.Balance__c != null) {
                    Double txDispAmtVal = txDispAmtMap.get(disp.Transaction__c);
                    if(txDispAmtVal != null)
                        txDispAmtMap.put(disp.Transaction__c,txDispAmtVal - disp.Balance__c);
                }   
            }
            
            Map<Id,Transaction__c> txMap = new Map<Id,Transaction__c>([select Id, Disputed_Amount__c, Balance__c from Transaction__c where id in: txDispAmtMap.keySet()]);
            
            List<Transaction__c> txsToUpdate = new List<Transaction__c>();
            for(Id txId:txDispAmtMap.keySet()) {
                Transaction__c trans = txMap.get(txId);
                if(trans!= null) {
                    Double changedAmount = (trans.Disputed_Amount__c == null? 0:trans.Disputed_Amount__c)+txDispAmtMap.get(txId);
                    
                    if(trans.Balance__c != 0 && trans.Disputed_Amount__c != changedAmount) {
                        trans.Disputed_Amount__c = changedAmount;
                        txsToUpdate.add(trans);
                    }
                }
            }
            if(txsToUpdate.size()>0)
                update txsToUpdate;
                
            
            for(Dispute__c dis:Trigger.old){
                disStatusBeforeMap.put(dis.Id,dis.status__c);
            
            }
        }
            
            
        // ------------------------------------------------//
        // when dispute__c records are DELETED.
        // ------------------------------------------------//       
        else if(Trigger.isDelete && Trigger.isAfter) {
            Map<Id,Double> txDispAmtMap = new Map<Id,Double>();
            for(Dispute__c disp:Trigger.old) {
                if(disp.Transaction__c != null && disp.Balance__c != null) {
                    Double txDispAmtVal = txDispAmtMap.get(disp.Transaction__c);
                    if(txDispAmtVal == null)
                        txDispAmtMap.put(disp.Transaction__c,disp.Balance__c);
                    else
                        txDispAmtMap.put(disp.Transaction__c,disp.Balance__c + txDispAmtVal);
                }   
            }
            
            Map<Id,Transaction__c> txMap = new Map<Id,Transaction__c>([select Id, Disputed_Amount__c from Transaction__c where id in: txDispAmtMap.keySet()]);
            
            List<Transaction__c> txsToUpdate = new List<Transaction__c>();
            for(Id txId:txDispAmtMap.keySet()) {
                Transaction__c trans = txMap.get(txId);
                if(trans!= null) {
                    trans.Disputed_Amount__c = (trans.Disputed_Amount__c == null? 0:trans.Disputed_Amount__c)-txDispAmtMap.get(txId);
                }
                txsToUpdate.add(trans);
            }
            
            if(txsToUpdate.size()>0)
                update txsToUpdate;
        }
         if(Trigger.isInsert || Trigger.isUpdate){
          for(Dispute__c dis:Trigger.new){
                 if(dis.status__c == 'Closed' && disStatusBeforeMap.get(dis.id) != 'Closed' && dis.Close_Date__c == null) {
                    dis.Close_Date__c = Date.today();
                }else if ( dis.status__c != 'Closed' && dis.Close_Date__c != null) {
                    dis.Close_Date__c = null;
                }
            } 
       }
    }      
    
     Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();

Boolean DailyNoteUpdate = triggerconf.Enable_Daily_Notes__c;
if(DailyNoteUpdate && Trigger.isUpdate ){
 for ( Dispute__c dipt: Trigger.new ){
      // To fetch today's  reportable notes
        //By kruti Tandel
      /*  if(Trigger.oldMap.get(dipt.Id).Note_Date__c != date.today() ){
            dipt.Daily_Notes__c = '';
        }*/
    //    if(dipt.Note_Date__c == date.today() ){
            String tempNote = '';
            string tempNote2 = '';
            String tempNoteSp = '';
            String tempNote2Sp = '';
            string tempNote3 = dipt.Daily_Notes__c;
            
            tempNote = dipt.Notes__c ;
           if(tempNote != null && tempNote != '')
                tempNoteSp = tempNote.replaceAll( '\\s+', '');
            tempNote2 = String.valueOf(Trigger.oldMap.get(dipt.Id).Notes__c);
            if(tempNote2 != null && tempNote2 != '')
               tempNote2Sp = tempNote2.replaceAll( '\\s+', '');
            
          // system.debug('===tempNote========'+Trigger.oldMap.get(dipt.Id).Notes__c.length());
            // system.debug('===tempNote2========'+dipt.Notes__c.length());
              if(trigger.isBefore && Trigger.isUpdate) {
              if(tempNotesp != tempNote2sp){
                   if(tempNote != null && tempNote != '' && tempNote2 != null && tempNote2 != '')
                    {tempNote = tempNote.subString(0, tempNote.length()-tempNote2.length());
                    }
                    dipt.Daily_Notes__c = tempNote;
            }else{
             dipt.Daily_Notes__c = tempNote3 ;
            }
            }
      
     
   }      
  }  
}