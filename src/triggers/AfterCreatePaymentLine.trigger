/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This Triggers is fired on Payment_Line__c
 *        after payment line is created, updated or deleted.
 */
trigger AfterCreatePaymentLine on Payment_Line__c (after insert, after update , after delete) {
    if(ConfigUtilController.getAfterCreatePaymentLine()){
        SysConfig_Singlec__c sysConfObj = ConfigUtilController.getSystemConfigObj();
        //to update transactions in bulk, keep al transactions in this list
        List < Transaction__c > transactionsToUpdate = new  List  < Transaction__c > ();
        //keep list of all the transaction ids
        List < Id > paymentLineTransactionIds = new List < Id >();
        //Id - Id of PaymentLine, Transaction - Corresponding transaction
        Map  < Id , Transaction__c > paymentLineTransactions = new Map < Id , Transaction__c >();

        if ( sysConfObj != null && sysConfObj.Sync_Payment_And_Invoice__c) {
        
        
               
                if ( Trigger.old != null ) {
                    
                    //populate list of trnasaction ids
                    for (Payment_Line__c paymentLine : Trigger.old) {
                
                        paymentLineTransactionIds.add( paymentline.Applied_To__c );
                    }
                    
                    //get all the transactions 
                    Map < Id, Transaction__c > trnasactionsMap = new Map< Id ,Transaction__c >([Select     
                                                                                                    a.Id,  
                                                                                                    a.Amount__c, 
                                                                                                    a.Balance__c
                                                                                                    From Transaction__c a 
                                                                                                    where a.Id in :paymentLineTransactionIds ]);
                                                                        
                    //populate map with Id = Id of payment line and Transactions = coresponding transaction
                    for (Payment_Line__c paymentLine : Trigger.old) {
                          paymentLineTransactions.put( paymentLine.Id ,trnasactionsMap.get(paymentLine.Applied_To__c) );
                    }                                                    
                    
                    
                    //iterate through all payment lines ( old value )
                    Map  < Id , Double> paymentLineAmount = new Map < Id , Double >();
                    set <Id> txId = new set<Id>();
                    
                    //iterate through all payment lines ( old value )
                    for ( Payment_Line__c paymentLine : Trigger.old ) {                         
                        
                            if ( paymentLine.Applied_Amount__c != null && paymentLine.Applied_To__c != null ) {                            
                                Transaction__c  invoice = trnasactionsMap.get(paymentLine.Applied_To__c);
                                boolean flag = txId.add(invoice.Id);
                                
                                if(!flag){
                                    Double amt = paymentLineAmount.get(invoice.Id);
                                    amt = amt + paymentLine.Applied_Amount__c;
                                    paymentLineAmount.put(invoice.Id , amt);
                                    
                                }else{
                                    paymentLineAmount.put(invoice.Id , paymentLine.Applied_Amount__c);
                                }
                            }
                        }
                        List <Transaction__c> trans = [Select a.Id,a.Amount__c,a.Balance__c From Transaction__c a where a.Id in :txId ];
                        for(Transaction__c tran : trans){
                            tran.Balance__c = tran.Balance__c + paymentLineAmount.get(tran.Id);                   
                            transactionsToUpdate.add(tran);
                        }
                    //bulk update
                    
                    update transactionsToUpdate;
                    
                    //clear list
                    transactionsToUpdate.clear();
                }
                if ( Trigger.new != null ) {
                            
                    //populate list of trnasaction ids
                    for (Payment_Line__c paymentLine : Trigger.new) {
                
                        paymentLineTransactionIds.add( paymentline.Applied_To__c );
                    }
                    
                    //get all the transactions 
                    Map < Id, Transaction__c > trnasactionsMap = new Map< Id ,Transaction__c >([Select     
                                                                                                a.Id,  
                                                                                                        a.Amount__c, 
                                                                                                        a.Balance__c
                                                                                                        From Transaction__c a 
                                                                                                        where a.Id in :paymentLineTransactionIds ]);
                                                            
                    //populate map with Id = Id of payment line and Transactions = coresponding transaction
                    if(Trigger.old != null){
                    for (Payment_Line__c paymentLine : Trigger.old) {
                    
                        paymentLineTransactions.put( paymentLine.Id ,trnasactionsMap.get(paymentLine.Applied_To__c) );

                    }
             }
             
             
            //Unique Transaction Map
            Map  < Id , Double> paymentLineAmount1 = new Map < Id , Double >();
            set <Id> txId1 = new set<Id>(); 
             //iterate through all payment lines ( new value )
            for ( Payment_Line__c paymentLine : Trigger.new ) {                         
                
                if ( paymentLine.Applied_Amount__c != null && paymentLine.Applied_To__c != null ) {                            
                    Transaction__c  invoice = trnasactionsMap.get(paymentLine.Applied_To__c);
                    boolean flag = txId1.add(invoice.Id);
                    
                    if(!flag){
                        Double amt = paymentLineAmount1.get(invoice.Id);
                        amt = amt + paymentLine.Applied_Amount__c;
                        paymentLineAmount1.put(invoice.Id , amt);
                    }else{
                        paymentLineAmount1.put(invoice.Id , paymentLine.Applied_Amount__c);
                    }
                }
            }
            List <Transaction__c> trans = [Select a.Id,a.Amount__c,a.Balance__c From Transaction__c a where a.Id in :txId1 ];
            for(Transaction__c tran : trans){
                tran.Balance__c = tran.Balance__c - paymentLineAmount1.get(tran.Id);                   
                transactionsToUpdate.add(tran);
            }
            //bulk update
            update transactionsToUpdate;
            //clear list
            transactionsToUpdate.clear();
        }
      }       
   }
}