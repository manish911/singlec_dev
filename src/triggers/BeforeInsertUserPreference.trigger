/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This Triggers is fired on User_Preference__c. 
 */
trigger BeforeInsertUserPreference on User_Preference__c (before insert) {
  if(ConfigUtilController.getBeforeInsertUserPreference()){
      Set<Id> newUsersPref = new Set<Id>();
      for(User_Preference__c userPref: Trigger.new) {
          newUsersPref.add(userPref.User__c);
      }
      
      /* Map<UserId, UserPref>*/
      Map<Id,User_Preference__c> userPrefMap = new Map<Id,User_Preference__c>();
      
      for(User_Preference__c up: [select Id, User__c from User_Preference__c where User__c in:newUsersPref]) {
          userPrefMap.put(up.User__c, up);    
      }
      
      for(User_Preference__c userPref: Trigger.new) {
          if(userPrefMap.get(userPref.User__c) != null)
              userPref.addError('User Preferences exists for selected user.');    
      }
  }
}