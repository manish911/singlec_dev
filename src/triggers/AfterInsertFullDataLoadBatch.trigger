/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Used to run FUll Batch Load for all transactions
 */
trigger AfterInsertFullDataLoadBatch on Data_Load_Batch__c (after insert, before update) {
    
    if(ConfigUtilController.getAfterInsertFullDataLoadBatch()){
        if(Trigger.isInsert) {
            String templateName = 'SYSTEM Data Load Fail Notification';
            EmailTemplate template = [select Name,HtmlValue from EmailTemplate where Name =: templateName limit 1];
            for(Data_Load_Batch__c dlb: Trigger.new) {
                if(dlb.Load_Type__c == 'FULL') {
                   
                   
                    AutomatedBatchCloseProcess bafl = new AutomatedBatchCloseProcess();
                    bafl.latestbatch = dlb.Batch_Number__c;
                    bafl.sourcesystem = dlb.Source_System__c;
                    bafl.batchInfoId = dlb.Id;
                    Database.executeBatch(bafl , 1);
                   
                  
                }else{
                    
                    if(dlb.Result__c != null && dlb.Result__c.equalsIgnoreCase('ERROR')){
                              String addresses = ConfigUtilController.getBatchJobNotificationEmailIds();
                              String [] toAddress = addresses.split(',');
                             
                             //--email mechanism with mandrill--start-----
                              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                             MandrillEmailService objMandrill = new MandrillEmailService();
                             mail.setToAddresses(toAddress);
                            objMandrill.strToEmails = toAddress;
                           
                            //mail.setReplyTo(toAddress);
                            mail.setSenderDisplayName('Data Load Failed :');
                            objMandrill.strFromName = 'Data Load Failed :';
                            
                            mail.setSubject( 'Data Load Failed'+' For '+ Userinfo.getOrganizationName());
                            objMandrill.strSubject = 'Data Load Failed'+' For '+ Userinfo.getOrganizationName();
                            
                            //String mailBodyText = 'Data load failed due to error in Data load.';
                            
                            String mailBody = template.HtmlValue;
                            mailBody = mailBody.replace('{!organizationname}',Userinfo.getOrganizationName());
                            mailBody = mailBody.replace('{!notificationdate}',Date.Today()+'');
                            
                            mailBody = mailBody.replace('{!loadtype}',dlb.Load_Type__c);
                            mailBody = mailBody.replace('{!result}',dlb.Result__c);
                            
                            mailBody = mailBody.replace('{!batchnumber}',dlb.Batch_Number__c);
                            mailBody = mailBody.replace('{!sourcesystem}',dlb.Source_System__c);
                            
                            mail.setHtmlBody(mailBody);
                            objMandrill.strHtmlBody = mailBody;
                            
                            SysConfig_Singlec__c seo=SysConfig_Singlec__c.getOrgDefaults();
                            boolean us = seo.Use_Mandrill_Email_Service__c;
                            if(us){
                               objMandrill.SendEmail(); 
                            }
                            else{
                               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                            }
                            //--email mechanism with mandrill--end-----
                            
                       }
                       
                    if ( dlb.Run_Batch_Aging__c ) {
                       // InvokeBatchAgingCalculations.startBatch();  
                       // InvokeBatchAgingCalculations.startADPDBatch();
                       InvokeBatchAgingCalculations.startBatchSourcesys(dlb.Source_System__c);  
                       InvokeBatchAgingCalculations.startADPDBatchSourcesys(dlb.Source_System__c);
                    }
                }
            }
        }
    
    }   
 
}