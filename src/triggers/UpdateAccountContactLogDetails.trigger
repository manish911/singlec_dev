/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage: This Trigger is used to populate Account/trnsaction/dispute field value at task level.
 */

trigger UpdateAccountContactLogDetails on Task (after insert,after update){

  Trigger_Configuration__c objtrg = Trigger_Configuration__c.getOrgDefaults();
 // boolean triggeractive =  objtrg.PopulateTaskWithARDetail__c;
        
 
    Set<Id> setId = new Set<Id>();
    Map<string,String> Mapfield = new Map<string,String>();
    Map<string,String> AccFieldList = new  Map<string,String>();
    Map<string,String> TxFieldList =  new  Map<string,String>();
    Map<string,String> DisputeFieldList =  new  Map<string,String>();
    String AccountField ='';
    String TransactionField= '';
    String DisputeField= '';
    private String commaSeperatedFields;
    public List<ManageTaskFields__c> manageTaskList;
    
 /* if(triggeractive){
    manageTaskList = [select Name,Object__c,Object_Field__c,Task_fields__c from ManageTaskFields__c limit 100];
    
    if(manageTaskList.size()>0){
        for(ManageTaskFields__c tfc : manageTaskList ){
          Mapfield.put(tfc.Object_Field__c,tfc.Object__c);
          if(tfc.Object__c.equalsIgnoreCase('account')){
              AccountField = AccountField + tfc.Object_Field__c + ',';
              AccFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
          }
          else if(tfc.Object__c.equalsIgnoreCase('transaction__c')){ 
              TransactionField = TransactionField + tfc.Object_Field__c + ',';
              TxFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
          }
          else if (tfc.Object__c.equalsIgnoreCase('Dispute__c')){
              DisputeField = DisputeField + tfc.Object_Field__c + ',';
              DisputeFieldList.put(tfc.Task_fields__c,tfc.Object_Field__c);
          }
        }
    }
    
    for(Task tsk : Trigger.New){
        setId.add(tsk.whatId);
    }
    
    String query = 'Select ' + DisputeField + 'Id,Account__c,Transaction__c from Dispute__c where Id in : setId';
    system.debug('--DisputeField ---'+query );
    List<Dispute__c> DisList = Database.query(query);
    Map<Id,Dispute__c> disMap = new Map<Id,Dispute__c>();
    
    if(DisList != NULL){
        for(Dispute__c dis: DisList ){
            setId.add(dis.Transaction__c);
            setId.add(dis.account__c);
            disMap.put(dis.ID,dis);
        }
    }
    
    Map<Id,Transaction__c> txMap = new Map<Id,Transaction__c>();
    query = 'Select ' + TransactionField + 'Id,Account__c from Transaction__c where Id in : setId';
    List<Transaction__c> txList = Database.query(query);
    
    if(txList != NULL){
        for(Transaction__c tx : txList ){
            setId.add(tx.Account__c);
            txMap.put(tx.ID,tx);
        }
    }
    
    query = 'Select ' + AccountField + 'Id from Account where Id in : setId';
    List<Account> accList = Database.query(query);
    Map<Id,Account> accMap = new Map<Id,Account>();
    
    if(accList  != NULL){
        for(Account acc  : accList){
            accMap.put(acc.ID,acc);
        }
    }
    
    for(Task tsk : Trigger.New){
        if(txMap.get(tsk.whatId) != NULL){
            Transaction__c tx1 = txMap.get(tsk.whatId);
            Account acc = accMap.get(tx1.Account__c);
            Account accFill  = accMap.get(tx1.Account__c);
            for(String str : TxFieldList.keyset()){
                string tem = String.valueOf(TxFieldList.get(str));
                tsk.put(str,tx1.get(tem));
            }
            for(String str : AccFieldList.keyset()){
                string tem = String.valueOf(AccFieldList.get(str));
                if(accFill != null)
                    tsk.put(str,accFill.get(tem));
            }
        }
        if(accMap.get(tsk.whatId) != NULL){
            Account acc3  = accMap.get(tsk.whatId); 
            for(String str : AccFieldList.keyset()){
                string tem = String.valueOf(AccFieldList.get(str));
                tsk.put(str,acc3.get(tem));
            }
        }
        if(disMap.get(tsk.whatId) != NULL){
            Dispute__c dis = disMap.get(tsk.whatId);
            Account acc1  = accMap.get(dis.Account__c);
            Transaction__c txdis = txMap.get(dis.Transaction__C);
            for(String str : DisputeFieldList.keyset()){
                string tem = String.valueOf(DisputeFieldList.get(str));
                tsk.put(str,dis.get(tem));
            }
            for(String str : AccFieldList.keyset()){
                string tem = String.valueOf(AccFieldList.get(str));
                if(acc1 != null)
                    tsk.put(str,acc1.get(tem));
            }
            for(String str : TxFieldList.keyset()){
                string tem = String.valueOf(TxFieldList.get(str));
                if(txdis != null)
                tsk.put(str,txdis.get(tem));
            }
         }
    }
    }  */
    
    if(ConfigUtilController.getUpdateAccountContactLogDetails()){
        /* get the map<AccountId, Task> for the new contactlog [task] inserted */
        Map<Id, Task> latestContactLogs = new Map<Id,Task>(); 
        
        Map<Id, Integer> ratingSumMap = new Map<Id, Integer>(); 
        Map<Id, Integer> ratingCountMap = new Map<Id, Integer>();
        
        // iterate over new Config_Log__c objects
        for(Task t: Trigger.new) {
            
            if(t.WhatId != null)  {
                String taskWhatId = t.WhatId;
                
                // update the account's contact log details only if the task is related to account.
                // so whatId can contain only Id of an account object and that will always starts with '001'
                if(t.AccountId != null && taskWhatId.startsWith('001') && t.Status == 'Completed' ) {
                    if(t.Activity_Category__c==AkritivConstants.CATEGORY_CONTACT) {
                        // get if the Contact_Log already exist in the map
                        Task accContactLogTask = latestContactLogs.get(t.AccountId);
                        
                        // if aleready exists in map then put the new config if if that is the latest one.
                        if(accContactLogTask != null && accContactLogTask.CreatedDate < t.CreatedDate)
                            latestContactLogs.put(t.AccountId, t);
                            
                        // put the config_log in the map            
                        else if(accContactLogTask == null )
                            latestContactLogs.put(t.AccountId, t);
                    }
                    
                    // FILL THE RATING MAPS
                    
                    Integer accountRatingCount = ratingCountMap.get(t.AccountId);
                    Integer accountRatingSum = ratingSumMap.get(t.AccountId);               
                    system.debug('accountRatingCount '+accountRatingCount);
                    if(accountRatingCount == null){
                    
                        system.debug('accountRatingCount '+accountRatingCount);
                        ratingCountMap.put(t.AccountId,0);
                        }
                        
                    if(accountRatingSum == null){
                        ratingSumMap.put(t.AccountId,0);
                        
                        }
                    ratingCountMap.put(t.AccountId,ratingCountMap.get(t.accountId)+1);
                    system.debug('---UtilityController.getRatingStarNumber(t.Rating__c)--'+UtilityController.getRatingStarNumber(t.Rating__c));
                    if(UtilityController.getRatingStarNumber(t.Rating__c) != null)
                    ratingSumMap.put(t.AccountId,ratingSumMap.get(t.accountId)+UtilityController.getRatingStarNumber(t.Rating__c));     
                    system.debug('---ratingCountMap--'+ratingCountMap);
                    system.debug('---ratingCountMap--'+ratingCountMap);
                    
                }
            }
        }
    
        // update the account fields for contact log info. by the value of ContactLog in the map.
        List<Account> accountsToUpdate = new List<Account>();
        for(Id accId : latestContactLogs.keySet()) {
            Task aTask = latestContactLogs.get(accId);
    
            Account acc = new Account(Id=accId);
            acc.Last_Contact_Date__c = aTask.CreatedDate.date();
            acc.Last_Contact_Method__c = aTask.type;
            acc.Last_Contact_Type__c = aTask.Contact_Type__c;        
            accountsToUpdate.add(acc);
        }
        
        List<Account> accountsToUpdate2 = new List<Account>();
        System.debug('--------------------> ratingCountMap : '+ ratingCountMap.keySet());
        Map<id, Account> accountMap = new Map<id,Account>();
        if(ratingCountMap != null){
            accountMap = new Map<id,Account>([select Rating_Count__c, Rating_Sum__c from Account where id in: ratingCountMap.keySet()]);
        }else{
            accountMap = new Map<id,Account>();
        }
        
        
        Integer ratingSum;
        Integer ratingCount;
        for(Id accId:ratingCountMap.keyset()){
            if(ratingSumMap.containsKey(accId)){
               ratingSum =  ratingSumMap.get(accId);
            }
            if(ratingCountMap.containsKey(accId)){
                ratingCount =  ratingCountMap.get(accId);
            }
            
            Account mapAccount =accountMap.get(accId);
            Account acc = new Account(Id=accId);
            acc.Rating_Sum__c  = (mapAccount!= null && mapAccount.Rating_Sum__c!= null ? mapAccount.Rating_Sum__c : 0 ) + ratingSum;
            acc.Rating_Count__c = (mapAccount!= null && mapAccount.Rating_Count__c!= null ? mapAccount.Rating_Count__c : 0 ) + ratingCount;
            
            accountsToUpdate2.add(acc);
        }
        
        // update the accounts if there is any.
        if(accountsToUpdate.size()>0) 
            update accountsToUpdate;
            
        if(accountsToUpdate2.size()>0) 
            update accountsToUpdate2;   
    }             
}