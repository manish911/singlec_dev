trigger AfterUpdateTestAccount on TestAccount__c (after Update) {
   
    for(TestAccount__C test : Trigger.new){
          TestAccount__c testAcc = Trigger.oldMap.get(test.Id);
          TestEMail__c testM = new TestEMail__c(user__c=testAcc.User__c, status__c='Send');
          Insert testM;
    }
}