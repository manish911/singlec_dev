/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :   Trigger provides the following functionality
 *             If  Transaction Balance is changing from non zero to zero and 
 *             Close Date is empty then set Close Date to today()
 *             for each of Disputes Related to this Transaction 
 *             set Dispute Balance to zero. 
 *             Marks Dispute Status as Closed with resolution code of Paid.
 */

trigger TransactionTrigger on Transaction__c (before update, after update) {

    //--------start--Validate Transaction Batch----------
    if(trigger.isBefore && Trigger.isUpdate)
    {
    system.debug('--inside condition----');    
    //--------------
    Trigger_Configuration__c triggerConf = Trigger_Configuration__c.getOrgDefaults();  
    system.debug('========triggerConf=========>>'+triggerConf);
                      
    boolean isChecked = false;
    if(triggerConf != null){
        system.debug('--inside if null condition----'); 
        isChecked = triggerConf.PopulateTaskWithARDetail__c; //checking Validate Transaction Batch in custom setting -Trigger Configuration
    }
    
    for(transaction__c tx : trigger.new)
        {
            if(tx.Promised_Amount__c == null || tx.Promised_Amount__c == 0)
                {
                   tx.Promise_Date__c = null;
                }
        }
     
    if (isChecked){
         system.debug('--inside ifchk----'); 
        for(transaction__c tx : trigger.new){
            system.debug('--chk cond----'+(tx.Batch_Number__c < Trigger.oldMap.get(tx.Id).Batch_Number__c));
            if ((tx.Batch_Number__c != null) && (tx.Batch_Number__c < Trigger.oldMap.get(tx.Id).Batch_Number__c)) {
                        
                        system.debug('--inside batchno----');
                        String OldTrxBatch = Trigger.oldMap.get(tx.Id).Batch_Number__c;
                        
                        tx.Batch_Number__c.addError('Update rejected because existing batch # ' +OldTrxBatch+ ' is newer than updating batch # '+tx.Batch_Number__c); 
                        system.debug('--aftr error debug----');  
                     
            }
        }
    }
    
    //-------------------
    }
    
    
    //--------end--Validate Transaction Batch----------
    List<Note> notesToInsert = new List<Note>();
    Set<Id> txIdsNonZeroToZeroBalance = new Set<Id>();
    // get the earlier balance for all the Transaction in update queue
    Map<Id, Double> txBalanceBeforeMap = new Map<Id, Double>();
    Map<Id, Double> balanceForReservedTxMap = new Map<Id, Double>();
    for(Transaction__c tx:Trigger.old)
        txBalanceBeforeMap.put(tx.Id, tx.Balance__c);
    for(Transaction__c tx:Trigger.old)
    {
         if(tx.Reserved_Balance__c != null && tx.Reserved_Balance__c > 0)
         {
             balanceForReservedTxMap.put(tx.Id, tx.Balance__c);
         }
    }
    
    // for each new Transaction values
    for(Transaction__c tx:Trigger.new) {
    
            /* Trigger will create a note if subject for the task 
            created is 'Broken Promise To Pay'.
            But will create the not only if the task 
            is related to a Transaction object.*/ 
      if(trigger.isBefore && Trigger.isUpdate) {              
        if(ConfigUtilController.getCreateNoteOnBrokenPromiseTrigger()){
            if(tx.Create_Broken_Promise_Note__c == true) {
                    Note n = new Note();
                    n.ParentId = tx.Id;
                    n.Title = 'Broken Promise';
                    n.Body = 'Tx Number: '+tx.Name+'\nTx Amount: '+tx.Amount__c+'\nTx Due Date: '+tx.Due_Date__c;
                    notesToInsert.add(n);
                    tx.Create_Broken_Promise_Note__c = false;
            }
        }
            // Trigger provides the following functionality
            // If  Transaction Balance is changing from non zero to zero and 
            // Close Date is empty then set Close Date to today()
            // for each of Disputes Related to this Transaction 
            // set Dispute Balance to zero. 
            // Marks Dispute Status as Closed with resolution code of Paid.
            
        if(ConfigUtilController.getZeroBalanceTransactionTrigger()){
            // check if balance changing from xon-zero to zero and close date in empty
            if(tx.Balance__c == 0 && txBalanceBeforeMap.get(tx.id) !=0 && tx.Close_Date__c == null) {
                // set the close date to today
                tx.Close_Date__c = Date.today();
                tx.Disputed_Amount__c = 0;
                txIdsNonZeroToZeroBalance.add(tx.id);
            } else if ( tx.Balance__c != 0 && tx.Close_Date__c != null) {
                
                tx.Close_Date__c = null;
            }
        }
            /* If  Transaction is mark for reserved 
            (Reserved_Balance__c is non zero) then check 
            a. if Balance is changing from non zero to zero 
            ie new tx balance is 0 Then update the tx balance 
            with new balance
            b. else, Dont do anything.
            This trigger basically takes care of not able to rewrite the tx balance with fresh data loads. It should update balance 
            only if new tx balance is 0. */
                
        if(ConfigUtilController.getHandleReserveTransaction()){
            if(balanceForReservedTxMap.containsKey(tx.id))
            {
               if(tx.Balance__c != 0){
                   //update new balance with old balance
                   Double oldTxBalance = balanceForReservedTxMap.get(tx.Id);
                   tx.Balance__c = oldTxBalance;
               }
            }
        }
      }
    }
    
    // Todos : Close Task, Delete Recurrence Task and Close Dispute when invoice Balance = 0 
    // Kruti and Hitesh -
    if(ConfigUtilController.getZeroBalanceTransactionTrigger()){
    
        List<Task> listTsk = new List<Task>();
        if(trigger.isBefore && Trigger.isUpdate) {
            List<Task> tskList = [Select Subject From Task where Subject='Broken Promise To Pay' and whatId in :txIdsNonZeroToZeroBalance];
            // remove all the Broken Promise Task for the Transactions
            for(Task lstTask:tskList ) {
               listTsk.add(lstTask);
            }
             delete listTsk;
        }
                
        String followupType =AkritivConstants.CONTACT_TYPE_CONTACT;
        
        if(trigger.isBefore && Trigger.isUpdate) {
            // close all the followup Task for paid/zero balance Transactions
            List<Task> tasksToUpdate  = new List<Task>();
            
            //delete all recursive tasks which are open
            List<Task> tasksToDelete  = new List<Task>();
            
            for(List<Task> taskList:[Select Subject,Contact_Type__c From Task where status != 'Completed' and RecurrenceType = null  and whatId in :txIdsNonZeroToZeroBalance]) {
                for(Task tk : taskList) {
                    tk.Status = 'Completed';
                    tasksToUpdate.add(tk);
                }
            }
            
            for(List<Task> taskList:[Select Subject,Contact_Type__c From Task where status != 'Completed' and RecurrenceType != null  and whatId in :txIdsNonZeroToZeroBalance]) {
                for(Task tk : taskList) {
                    
                    tasksToDelete.add(tk);
                }
            }
             
            if(tasksToUpdate.size()>0) {
                update tasksToUpdate;
            }
            
            if(tasksToDelete.size()>0) {
                delete tasksToDelete;
            }
        }
                
                
        if(trigger.isAfter && Trigger.isUpdate) {
        
        
            List< Transaction__c > closedTxIds = [select Id, Name, Balance__c, Close_Date__c from Transaction__c 
                                                where ID in : Trigger.new and Balance__c = 0 and Close_Date__c != null ] ;
           
            // dispute list to be updated
            List<Dispute__c> disputesToUpdate  = new List<Dispute__c>();
            
            // and update all the related disputes for the transaction. Set their status to "closed"
            for(List<Dispute__c> dispList : [select Status__c, Transaction__c,Transaction__r.Close_Date__c from Dispute__c where Transaction__c in : closedTxIds]) {
                for(Dispute__c disp : dispList) {
                    disp.Status__c = 'Closed';
                    disp.Resolution_Code__c = 'Paid';
                    disp.Close_Date__c = disp.Transaction__r.Close_Date__c;
                    disputesToUpdate.add(disp);
                }
            }
            
            if(disputesToUpdate.size()>0) {
                update disputesToUpdate;
            }
        }
        if(notesToInsert.size()>0)
            insert notesToInsert;
    }

Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();

Map<Id,Transaction__c> mapTransaction = new Map<Id,Transaction__c>();
Boolean DailyNoteUpdate = triggerconf.Enable_Daily_Notes__c;
if(DailyNoteUpdate && Trigger.isUpdate ){
 for ( Transaction__c tx : Trigger.new ){
      // To fetch today's  reportable notes
        //By kruti Tandel
      /*  if(Trigger.oldMap.get(tx.Id).Note_Date__c != date.today() ){
            tx.Daily_Notes__c = '';
        }*/
    //    if(tx.Note_Date__c == date.today() ){
            String tempNote = '';
            string tempNote2 = '';
            String tempNoteSp = '';
            String tempNote2Sp = '';
            string tempNote3 = tx.Daily_Notes__c;
            
            tempNote = tx.Notes__c ;
           if(tempNote != null && tempNote != '')
                tempNoteSp = tempNote.replaceAll( '\\s+', '');
            tempNote2 = String.valueOf(Trigger.oldMap.get(tx.Id).Notes__c);
            if(tempNote2 != null && tempNote2 != '')
               tempNote2Sp = tempNote2.replaceAll( '\\s+', '');
            
          // system.debug('===tempNote========'+Trigger.oldMap.get(tx.Id).Notes__c.length());
            // system.debug('===tempNote2========'+tx.Notes__c.length());
              if(trigger.isBefore && Trigger.isUpdate) {
              if(tempNotesp != tempNote2sp){
                   if(tempNote != null && tempNote != '' && tempNote2 != null && tempNote2 != '')
                    {tempNote = tempNote.subString(0, tempNote.length()-tempNote2.length());
                    }
                    tx.Daily_Notes__c = tempNote;
            }else{
             tx.Daily_Notes__c = tempNote3 ;
            }
            }
      
     
   }      
  }  
}