/*
 * Copyright (c) 2009-2010 Akritiv Technologies, Inc.  All Rights Reserved.
 * This software is the confidential and proprietary information
 * (Confidential Information) of Akritiv Technologies, Inc.  You shall not
 * disclose or use Confidential Information without the express written
 * agreement of Akritiv Technologies, Inc.
 */
/*
 * Usage   :  This Trigger is fired on Account.
 *            to assign a unique AccountKey to all Accounts and close the related task when AR is zeros.
 */
trigger ManageAccountKeyTrigger on Account (before insert, before update,after insert, after update) {
    Set<Id> accIdsNonZeroToZeroAR = new Set<Id>();
    Map<Id, Double> accARBeforeMap = new Map<Id, Double>();
    String str = '';

   if(Trigger.isUpdate && ConfigUtilController.getZeroZeroARAccountTrigger() ){ 
       str =  ConfigUtilController.getAccountFieldForClosing().trim();
       if(str != null && !''.equals(str)){  
         
           for(Account acc :Trigger.old){
           
                accARBeforeMap.put(acc.Id, Double.valueOf(acc.get(str)));
           }
       
       }
   }
        
   try {

    if(ConfigUtilController.getManageAccountKey()){
        for(Account acc : Trigger.new){
            if(acc.Account_Key__c == '0' || acc.Account_Key__c == '' || acc.Account_Key__c == null){
                       acc.Account_Key__c = 'AK'+System.currentTimeMillis()+math.random()*999999;
            }   
            if(ConfigUtilController.getZeroZeroARAccountTrigger() && Trigger.isUpdate && Trigger.isAfter  ){
           
           
                // check if Total AR changing from xon-zero to zero and close date in empty
                if(str != null && str != ''){
                    if(Double.valueOf(acc.get(str)) == 0 && accARBeforeMap.get(acc.id) !=0 ) {
                        // set the close date to today
                     
                        accIdsNonZeroToZeroAR.add(acc.id);
                    }
                } 
            }    
        }
   }    
 
  }catch( Exception e ){ System.debug(e);}
  
       
    if(Trigger.isUpdate && ConfigUtilController.getZeroZeroARAccountTrigger() ) {
        // close all the followup Task for paid/zero balance Transactions
        List<Task> tasksToUpdate  = new List<Task>();
        
        for(List<Task> taskList:[Select Subject,Contact_Type__c From Task 
                                        where status != 'Completed' and whatId in :accIdsNonZeroToZeroAR]) {
            for(Task tk : taskList) {
                tk.Status = 'Completed';
                tasksToUpdate.add(tk);
            }
        }
         
        if(tasksToUpdate.size()>0) {
            update tasksToUpdate;
        }
    } 
/*********************DailyNoteUpdateOnAccount************************/
Boolean DailyNoteUpdate = false;
Trigger_Configuration__c triggerconf = Trigger_Configuration__c.getOrgDefaults();
If(triggerconf != null){
DailyNoteUpdate = triggerconf.Enable_Daily_Notes__c;
}
if(DailyNoteUpdate && Trigger.isUpdate && Trigger.isBefore){
          for ( Account acc: Trigger.new ){
          // To fetch today's  reportable notes
            //By kruti Tandel
         /*   if(Trigger.oldMap.get(acc.Id).Note_Date__c != date.today() ){
                acc.Daily_Notes__c = '';
            }*/
       
              //   acc.Daily_Notes__c = '';
                String tempNote = '';
                string tempNote2 = '';
                String tempNoteSp = '';
                String tempNote2Sp = '';
                string tempNote3 = acc.Daily_Notes__c;
                
                tempNote =acc.Notes__c;
                if(tempNote != null && tempNote != '')
                tempNoteSp = tempNote.replaceAll( '\\s+', '');
                tempNote2 = String.valueOf(Trigger.oldMap.get(acc.Id).Notes__c);
                 if(tempNote2 != null && tempNote2 != '')
                tempNote2Sp = tempNote2.replaceAll( '\\s+', '');
                if(trigger.isBefore && Trigger.isUpdate) {
                    if(tempNotesp != tempNote2sp){
                     if(tempNote != null && tempNote != '' && tempNote2 != null && tempNote2 != '')
                        tempNote = tempNote.subString(0, tempNote.length()-tempNote2.length());
                         acc.Daily_Notes__c = tempNote;
                    }else{
                         acc.Daily_Notes__c = tempNote3 ;
                    }
                }
                
     
   
  }     
}
}